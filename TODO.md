# The (not so) great server TODO


## Open

- The DoopServerSideAnalysisConfiguration is the InputBundle now. We keep the class only for the post-processing logic (perhaps rename it).
- Backport heapdl support from master.
- Verify search in all cases
- The analysis id is provided as a query parameter and not as a URI component (with one exception). This will also allow us to support multiple analyses. We keep only the /analyses/{id} endpoint for analysis deletion purposes.
- The "logical" tree view will not hide any unreachable Classes/Symbols but provide an indication.
- Revisit all WebApp URL endpoints and verify they actually function correctly.
- Discuss and revisit search implementation / capabilities / experience.
- Introduce repackaging functionality.
- Taint sources.
- Revisit UI (as a whole).
- Revisit all clue components that utilize the server (client, gradle plugin, crawler) to catch up with new server API.
- Performance of input bundle initialization.
- Improve, expand and make more interactive: visualizations / stats / reports / ready-made queries.
- Doop output dir should be placed under the user dir.
- The 'MainClass' doop analysis option is given to doop even though it is null.
- isAbstract, isInterface filters in reachable/unreachable methods of a Class.

## Resolved?
- Best way to determine app / dep / platform packages and classes.
- Utilize Class-Artifact.csv from the analysis vs manually process the jar files.

## Resolved
- Run post-processors correctly.
- Explode app/dep/platform jars
- Refactor search in web.persistent.model.doop.Value 
- Show real directory contents in physical view with the exception to render .java/.class files in a logical view.
- When there is no analysis selected, all Methods are unreachable (by query logic). The UI should not render reachability info in this case.
- Backport gfour fixes from master.
