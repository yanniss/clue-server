package org.clyze.server.web

import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.persistent.model.SessionToken
import org.clyze.server.web.persistent.model.User
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.utils.CheckSum

/**
 * Created by saiko on 24/8/2015.
 */
@Singleton
class DefaultAuthManager implements AuthManager {

	private String passHash(String pass, String salt) {
		return CheckSum.checksum(pass + salt, "SHA-256") //hash password and salt using SHA-256
	}

	@Override
	void createUser(String username, String password) throws RuntimeException {
		if (userExists(username)) {
			throw new RuntimeException("User ${username} already exists")
		} else {
			//Use a random UUID as the salt
			String salt = UUID.randomUUID().toString()
			User user = new User(
					id: username,
					name: username,
					password: passHash(password, salt),
					salt: salt
			)
			Config.instance.storage.storeNewUser(user)
		}
	}

	@Override
	void removeUser(String username) throws RuntimeException {
		Config.instance.logger.debug "DefaultAthManager - removing user $username"

		if (!userExists(username)) {
			throw new RuntimeException("Invalid username: $username")
		}

		Config.instance.storage.removeUser(username)
	}

	@Override
	void authenticateUser(String username, String password) throws RuntimeException {
		User user = Config.instance.storage.loadUser(username)
		String hashedPassword = passHash(password, user.salt)
		if (hashedPassword != user.password) {
			throw new RuntimeException("Authentication failed")
		}
	}

	@Override
	boolean userExists(String username) {
		return Config.instance.storage.userExists(username)
	}

	@Override
	boolean isAdmin(String username) {
		return username == "admin"
	}

	@Override
	String createSessionToken(String username) throws RuntimeException {
		String id = UUID.randomUUID().toString()

		//The token expires after 7 days
		Date now = new Date()
		GregorianCalendar cal = new GregorianCalendar()
		cal.setTime(now)
		cal.add(Calendar.DATE, 7)

		SessionToken token = new SessionToken(
				id: id,
				userId: username,
				expires: cal.getTimeInMillis()
		)

		Config.instance.storage.storeNewToken(token)

		return id
	}

	@Override
	String getUserOfToken(String token) throws RuntimeException {
		SessionToken tokenObject = Config.instance.storage.loadToken(token)		
		if (tokenObject.expires > new Date().getTime()) {
			return tokenObject.userId
		} else {
			Config.instance.storage.removeToken(tokenObject)
			throw new RuntimeException("Authentication failed")
		}
	}

	@Override
	void removeSessionToken(String token) throws RuntimeException {
		SessionToken tokenObject = Config.instance.storage.loadToken(token)		
		Config.instance.storage.removeToken(tokenObject)
	}

	@Override
	void updateUserPassword(String username, String password) throws RuntimeException {		
		User user = Config.instance.storage.loadUser(username, User)
		user.password = passHash(password, user.salt)
		Config.instance.storage.updateUser(user)
	}
}
