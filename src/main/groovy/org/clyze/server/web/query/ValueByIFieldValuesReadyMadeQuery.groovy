package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.IFIELD_VALUES

class ValueByIFieldValuesReadyMadeQuery extends ValuesReadyMadeQuery {

	ValueByIFieldValuesReadyMadeQuery() {
		super(IFIELD_VALUES, "Top Values (#Single Field Values)")
	}

}