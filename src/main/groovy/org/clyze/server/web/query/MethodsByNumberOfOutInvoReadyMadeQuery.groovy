package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

class MethodsByNumberOfOutInvoReadyMadeQuery extends MethodsReadyMadeQuery {

	MethodsByNumberOfOutInvoReadyMadeQuery() {
		super(OUTGOING_INVO, "Top Methods (#Out Invo)")
	}

}