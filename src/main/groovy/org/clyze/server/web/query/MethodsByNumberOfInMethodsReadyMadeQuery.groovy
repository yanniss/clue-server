package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

class MethodsByNumberOfInMethodsReadyMadeQuery extends MethodsReadyMadeQuery {

	MethodsByNumberOfInMethodsReadyMadeQuery() {
		super(INCOMING_METHODS, "Top Methods (#In Methods)")
	}

}