package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

class MethodsByNumberOfOutMethodsReadyMadeQuery extends MethodsReadyMadeQuery {

	MethodsByNumberOfOutMethodsReadyMadeQuery() {
		super(OUTGOING_METHODS, "Top Methods (#Out Methods)")
	}
	
}