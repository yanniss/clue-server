package org.clyze.server.web.query

import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.doop.InvocationZeroTargets
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation

import static org.clyze.server.web.restlet.ResponseSchema.conform

class InvocationZeroTargetsReadyMadeQuery extends ReadyMadeQuery {

	InvocationZeroTargetsReadyMadeQuery() {
		super("InvoZeroTargets", "Invo with 0 targets")
	}

	Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException {
		def res = []

		searchLogic.searchInvocationZeroTargets { String json ->
			def element = new InvocationZeroTargets().fromJSON(json)
			def m = searchLogic.findInvocation(element.invocationId)
			if (m) res << conform(MethodInvocation, m, searchLogic)
		}

		new JsonRepresentation([results: res])
	}
}