package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

class ValuesByNumberOfReceiverReadyMadeQuery extends ValuesReadyMadeQuery {

	ValuesByNumberOfReceiverReadyMadeQuery() {
		super(VALUE_AS_RECEIVER, "Top Values (#As Receiver)")
	}
	
}