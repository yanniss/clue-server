package org.clyze.server.web.query

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

class MethodsByNumberOfInInvoReadyMadeQuery extends MethodsReadyMadeQuery {

	MethodsByNumberOfInInvoReadyMadeQuery() {
		super(INCOMING_INVO, "Top Methods (#In Invo)")
	}

}