package org.clyze.server.web.query

import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation

import static org.clyze.server.web.restlet.ResponseSchema.conform

/**
 * A ready-made query that calculates top-N classes based on the number of fields.
 */
class ClassesByNumberOfFieldsReadyMadeQuery extends ReadyMadeQuery {

	ClassesByNumberOfFieldsReadyMadeQuery() {
		super("classesByNumberOfFields", "Top Classes (#Fields)", "...", false)
	}

	Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException {
		def classes = []

		searchLogic.groupFieldsByDeclaringClass(50).each { String declaringClassDoopId, Long count ->
			def c = searchLogic.findClass(declaringClassDoopId)
			if (c) classes << (conform(Klass, c, searchLogic) << [count: count])
		}

		new JsonRepresentation([results: classes])
	}
}