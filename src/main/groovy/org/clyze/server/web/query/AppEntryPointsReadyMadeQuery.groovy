package org.clyze.server.web.query

import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.doop.EntryPoint
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation

import static org.clyze.server.web.restlet.ResponseSchema.conform

class AppEntryPointsReadyMadeQuery extends ReadyMadeQuery {

	AppEntryPointsReadyMadeQuery() {
		super("AppEntryPoints", "App Entry Points")
	}

	Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException {
		def res = []

		searchLogic.searchEntryPoint { String json ->
			def element = new EntryPoint().fromJSON(json)
			def m = searchLogic.findMethod(element.methodId)
			if (m) res << conform(Method, m, searchLogic)
		}
		
		new JsonRepresentation([results: res])
	}
}