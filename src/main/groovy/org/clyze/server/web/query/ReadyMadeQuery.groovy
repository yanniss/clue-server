package org.clyze.server.web.query

import groovy.transform.TupleConstructor
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 * A ready-made query that calculates a significant piece of user-oriented information.
 * No inputs are currently supported.
 */
@TupleConstructor
abstract class ReadyMadeQuery {

	protected Log log = LogFactory.getLog(getClass())

	String id
	String name
	String description = "..."
	boolean requiresAnalysis = true

	/**
	 * Fetch results for the requested query
	 *   def results = [
	 *       Each array element should be an object of the possible types and in the same format returned by
	 *       ResponseSchema.conform() with any additional fields (e.g. count) being dynamically injected
	 *   ]
	 *   new JsonRepresentation([ "results": results ])
	 */
	abstract Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException
}