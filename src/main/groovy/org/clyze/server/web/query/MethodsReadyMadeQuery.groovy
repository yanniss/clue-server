package org.clyze.server.web.query

import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.doop.Metrics
import org.clyze.server.web.persistent.model.doop.Metrics.Kind
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

abstract class MethodsReadyMadeQuery extends ReadyMadeQuery {

	Kind kind

	MethodsReadyMadeQuery(Kind kind, String name) {
		super(kind.toString(), name)
		this.kind = kind
	}

	Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException {
		def res = searchLogic.findMetrics(kind).collect { Metrics metric ->
			def m = searchLogic.findMethod(metric.doopId)
			m ? (conform(Method, m, searchLogic) << [count: metric.counter]) : null
		}.findAll()

		new JsonRepresentation([results: res])
	}
}