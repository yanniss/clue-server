package org.clyze.server.web.query

import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 * A ready-made query that calculates top-N packages based on the number of classes.
 */
class PackagesByNumberOfClassesReadyMadeQuery extends ReadyMadeQuery {
	
	PackagesByNumberOfClassesReadyMadeQuery() {
		super("packagesByNumberOfClasses", "Top Packages by number of Classes", "...", false)
	}

	Representation fetch(Searcher searchLogic, DoopInputBundle bundle, ServerSideAnalysis analysis) throws ResourceException {		
	
		Map<String, Long> buckets = searchLogic.groupPackagesByNumberOfClasses(50)		
		def packages = buckets.collect { String packageName, Long count -> 
			return [
				name : packageName,
				count: count
			]
		}		
		return new JsonRepresentation([results: packages]) 
	}
}