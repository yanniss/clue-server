package org.clyze.server.web

/**
 * The server-side analysis state.
 * A simple way to express the state transition constraints is provided.
 * @author: Kostas Saidis (saiko@di.uoa.gr)
 * Date: 3/1/2015
 */
public enum AnalysisState {
	ERROR,
	READY,
	RUNNING,
	CANCELLED,
	TIMED_OUT,
	COMPLETED,
	POST_PROCESSING,
	FINISHED

	//A map with the allowed analysis state transitions (from -> [to])
	static final Map<AnalysisState, Set<AnalysisState>> ALLOWED_TRANSITIONS = [
			(ERROR)          : EnumSet.of(READY, POST_PROCESSING, ERROR),
			(READY)          : EnumSet.of(RUNNING),
			(RUNNING)        : EnumSet.of(ERROR, CANCELLED, TIMED_OUT, COMPLETED),
			(CANCELLED)      : EnumSet.of(READY),
			(TIMED_OUT)      : EnumSet.of(READY),
			(COMPLETED)      : EnumSet.of(POST_PROCESSING, READY),
			(POST_PROCESSING): EnumSet.of(ERROR, READY, FINISHED),
			(FINISHED)       : EnumSet.of(POST_PROCESSING, READY)
	]

	static final Set<AnalysisState> ALLOW_QUERY_DATASTORE = EnumSet.of(FINISHED)
	static final Set<AnalysisState> RUNNING_STATES = EnumSet.of(RUNNING, POST_PROCESSING)

	boolean transitionAllowed(AnalysisState newState) {
		return ALLOWED_TRANSITIONS.get(this).contains(newState)
	}

	boolean datastoreQueryAllowed() {
		return ALLOW_QUERY_DATASTORE.contains(this)
	}

	boolean isRunning() {
		return RUNNING_STATES.contains(this)
	}
}
