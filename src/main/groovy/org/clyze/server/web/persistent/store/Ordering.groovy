package org.clyze.server.web.persistent.store

enum Ordering {
	ASC,
	DESC
}
