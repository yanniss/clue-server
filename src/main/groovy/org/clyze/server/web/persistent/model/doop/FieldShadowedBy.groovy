package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class FieldShadowedBy extends SymbolRelation {

	String fieldId
	String shadowFieldId

	FieldShadowedBy(String anId, String fieldId, String shadowFieldId) {
		super(anId)
		this.fieldId = fieldId
		this.shadowFieldId = shadowFieldId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.fieldId       = this.fieldId
		map.shadowFieldId = this.shadowFieldId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.fieldId       = map.fieldId
		this.shadowFieldId = map.shadowFieldId
	}
}
