package org.clyze.server.web.persistent.store

interface DataStoreConnector {

	void connect() throws RuntimeException

	void disconnect() throws RuntimeException

	void createStore(String storeId) throws RuntimeException

	void createStoreIfNotExists(String storeId) throws RuntimeException

	void dropStoreIfExists(String storeId) throws RuntimeException

	void dropStore(String storeId) throws RuntimeException

	DataStore connectTo(String storeId) throws RuntimeException
}