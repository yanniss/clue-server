package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class InvocationValues extends SymbolRelation {

	String fromMethodId
	String invocationId
	String toMethodId

	InvocationValues(String anId, String invokingMethodId, String invocationId, String toMethodId) {
		super(anId)
		this.fromMethodId = invokingMethodId
		this.invocationId = invocationId
		this.toMethodId = toMethodId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.fromMethodId = this.fromMethodId
		map.invocationId = this.invocationId
		map.toMethodId   = this.toMethodId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.fromMethodId = map.fromMethodId
		this.invocationId = map.invocationId
		this.toMethodId   = map.toMethodId
	}
}
