package org.clyze.server.web.persistent.store

import static org.clyze.server.web.persistent.store.BooleanQuery.*

class BooleanQueryBuilder {

	static BooleanQuery build(Closure c) {
        BooleanQueryBuilder qb = new BooleanQueryBuilder()
        Operator op = runClosure(c, qb) as Operator        
        if (!op) throw new RuntimeException("Empty query")
        return new BooleanQuery(operator:op)
    }

    static BooleanQuery buildAND(Map<String, Object> filters) {
    	def operators = []
    	filters.each { String filter, Object value ->
            if (value != null) { //ingore null values, but not empty ones
            	operators.add operatorFrom(filter, value)                
            }
        }

        if (!operators) {
        	throw new RuntimeException("No operators for AND")
        }

        return new BooleanQuery(operator: new CompoundOperator(OP.AND, operators as Operator[]))
    }

    static BooleanQuery build(String filter, Object value) {
    	new BooleanQuery(operator: operatorFrom(filter, value))
    }

    private static Operator operatorFrom(String filter, Object value) {   
    	validate(filter, value) 	
    	value instanceof Collection ? new InOperator(filter, value as Set) : new EqualsOperator(filter, value)
    }
       
    /*   
    def methodMissing(String name, args) {
        def argsInfo = args.collect { 
            "${it.getClass().simpleName} -> $it"
        }
        println ("method $name missing with args: ${argsInfo.join(', ')}")
    }
    
    def propertyMissing(String name) {
        println ("property $name missing")
    } 
    */   
     
    Operator EQ(String operand, Object rhs) {
    	validate(operand, rhs)
        return new EqualsOperator(operand, rhs)
    }

    Operator LT(String operand, Object rhs) {
        validate(operand, rhs)
        return new LessThanOperator(operand, rhs)
    }

    Operator LTE(String operand, Object rhs) {
        validate(operand, rhs)
        return new LessThanOrEqualsOperator(operand, rhs)
    }

    Operator GT(String operand, Object rhs) {
        validate(operand, rhs)
        return new GreaterThanOperator(operand, rhs)
    }

    Operator GTE(String operand, Object rhs) {
        validate(operand, rhs)
        return new GreaterThanOrEqualsOperator(operand, rhs)
    }
            
    Operator IN(String operand, Set rhs) {
    	validate(operand, rhs)
        return new InOperator(operand, rhs)
    }    
        

    Operator AND(Operator... operators) {
        if (!operators) {
            throw new RuntimeException("No operators for AND")
        }       

        new CompoundOperator(OP.AND, operators)        
    }
    
    Operator OR(Operator... operators) {
        if (!operators) {
            throw new RuntimeException("No operators for OR")
        }       

        new CompoundOperator(OP.OR, operators)
    }    
    
    Operator NOT(Operator... operators) {
        if (!operators) {
            throw new RuntimeException("No operators for NOT")
        }       

        new CompoundOperator(OP.NOT, operators)
    }    
    
    private static Operator runClosure(Closure c, def delegate) {
        def cc = c.clone()
        
        cc.delegate = delegate
        cc.resolveStrategy = Closure.DELEGATE_ONLY
        
        cc() as Operator
    }

    private static final void validate(String lhs, def rhs) {
    	if (!lhs) {
    		throw new RuntimeException("Empty LHS not allowd")
    	}
    	if (rhs == null) {
    		throw new RuntimeException("Null RHS not allowed")
    	}
    }

}