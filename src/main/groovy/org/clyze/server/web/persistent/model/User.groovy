package org.clyze.server.web.persistent.model

import org.clyze.persistent.model.ItemImpl

class User extends ItemImpl {
	
	String name
	String password
	String salt

	void saveTo(Map<String, Object> map) {		
		map.name     = this.name
		map.password = this.password
		map.salt     = this.salt
	}

	void loadFrom(Map<String, Object> map){		
		this.name     = map.name	
		this.password = map.password
		this.salt     = map.salt
	}
}
