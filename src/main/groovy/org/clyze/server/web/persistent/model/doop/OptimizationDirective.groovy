package org.clyze.server.web.persistent.model.doop

import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.Method

interface OptimizationDirective {    

    static final Set<String> SYMBOL_TYPES = [
        Klass.simpleName,
        Method.simpleName,
        Field.simpleName
    ] as Set    

    static enum OriginType {
        USER,
        ANALYSIS,
        SEED
    }

    void setOriginType(OriginType origin)
    OriginType getOriginType()
    void setConfigSet(String configSet)
    String getConfigSet()
    Set<String> getOrigin()
    void setOrigin(Set<String> origin)
        
}