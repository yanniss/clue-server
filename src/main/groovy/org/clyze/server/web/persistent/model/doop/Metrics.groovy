package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class Metrics extends SymbolRelation {

	enum Kind {
		INVOCATION_TARGETS, INCOMING_INVO, INCOMING_METHODS, OUTGOING_INVO, OUTGOING_METHODS, VALUE_AS_RECEIVER, IFIELD_VALUES
	}

	String kind
	String doopId
	long counter

	Metrics(String anId, Kind kind, String doopId, long counter) {
		super(anId)
		this.kind = kind.name()
		this.doopId = doopId
		this.counter = counter
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.kind    = this.kind
		map.doopId  = this.doopId
		map.counter = this.counter		
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.kind    = map.kind
		this.doopId  = map.doopId
		this.counter = map.counter		
	}
}
