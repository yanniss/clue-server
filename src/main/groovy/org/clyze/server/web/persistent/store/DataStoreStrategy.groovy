package org.clyze.server.web.persistent.store


import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import com.google.common.cache.RemovalListener
import com.google.common.cache.RemovalNotification
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.Item
import org.clyze.server.web.AnalysisState
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.model.SessionToken
import org.clyze.server.web.persistent.model.User
import org.clyze.server.web.persistent.model.doop.EffectiveMethodDirective
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit

/**
 * A datastore strategy (that acts a lot like a facade) for dealing with the persistent storage of server-side data.
 * A cache is also employed for holding ServerSideAnalysis objects.
 * This class replaces the old AnalysisManager. 
 */
abstract class DataStoreStrategy {

	protected final Log logger = LogFactory.getLog(getClass())

	protected Cache<String, Item> cache

	DataStoreConnector connector	

	void setupCache(int timeoutInMinutes, int maxCacheSize) {
		cache = CacheBuilder.newBuilder().
				maximumSize(maxCacheSize).
				expireAfterWrite(timeoutInMinutes + 1, TimeUnit.MINUTES). //just a tick larger than analysis timeout
				removalListener(new RemovalListener<String, Item>() {
					@Override
					void onRemoval(RemovalNotification<String, Item> n) {
						logger.warn("Item ${n.key} (${n.value.getClass()}) removed from cache: ${n.cause}")
					}
				}).
				build()
	}

	/* ---- Stores ---- */		
	protected DataStore accountStore() {
		return connector.connectTo(nameOfStoreForUserAccounts())	
	}

	protected DataStore tokenStore() {
		return connector.connectTo(nameOfStoreForUserTokens())	
	}

	protected DataStore projectStore() {
		return connector.connectTo(nameOfStoreForUserProjects())
	}

	abstract DataStore bundleDoopStore(DoopInputBundle bundle)

	abstract DataStore initializeBundleDoopStore(DoopInputBundle bundle)

	abstract DataStore analysisDoopStore(ServerSideAnalysis a)
	
	protected abstract String nameOfStoreForUserAccounts()

	protected abstract String nameOfStoreForUserTokens()

	protected abstract String nameOfStoreForUserProjects()


	/* ---- Lifecycle ---- */	
	abstract void startup()

	abstract void shutdown()

	/* ---- InputBundle ---- */		
	abstract void storeNewInputBundle(DoopInputBundle bundle, Identifiers.BundleInfo info)

	abstract void removeInputBundle(DoopInputBundle bundle) 

	abstract DoopInputBundle loadInputBundle(String bundleId) 

	/* ---- ServerSideAnalysis ---- */
	abstract void storeNewAnalysis(ServerSideAnalysis a, Identifiers.AnalysisInfo info)

	//TODO: Remove this method?
	abstract void removeAnalysis(ServerSideAnalysis a)

	abstract void updateAnalysis(ServerSideAnalysis a)

	abstract ServerSideAnalysis loadAnalysis(String anId)

	/* ---- OptimizationDirective ---- */
	abstract void storeNewOptimizationDirective(DoopInputBundle bundle, KeepRemoveMethodDirective directive) 

	abstract KeepRemoveMethodDirective loadOptimizationDirectiveOfBundle(DoopInputBundle bundle, String directiveId)

	abstract void updateOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive)

	abstract void removeOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive)

	/* ---- ConfigSet ---- */

	BundleConfiguration createAndStoreNewConfigSet(String name, DoopInputBundle inputBundle, Date created, Date modified) {
		BundleConfiguration configSet = new BundleConfiguration(inputBundle.getId(), name, created, modified)
		storeNewConfigSet(inputBundle, configSet)
		return configSet
	}

	BundleConfiguration createAndStoreNewConfigSet(String name, DoopInputBundle inputBundle) {
		Date now = new Date()
		return createAndStoreNewConfigSet(name, inputBundle, now, now)
	}

	abstract void storeNewConfigSet(DoopInputBundle bundle, BundleConfiguration configSet)

	abstract BundleConfiguration loadConfigSet(DoopInputBundle bundle, String configSetId)

	BundleConfiguration loadConfigSetByName(DoopInputBundle bundle, String configSetName) {
		try {
			BooleanQuery q = BooleanQueryBuilder.buildAND([
				'_type': BundleConfiguration.simpleName,
				'bundleId': bundle.id,
				'nameOfSet': configSetName
			])
			DataStore store = bundleDoopStore(bundle)
			def limits = new SearchLimits(start:0, count: 2)
			String configSetId = null
			store.search(q, [], limits) { String id, String type, String json ->
				configSetId = id
			}

			if (limits.hits == 0) {
				throw new RuntimeException("Config not found with name: $configSetName")
			}

			if (limits.hits == 1) {
				return loadConfigSet(bundle, configSetId)
			}

			if (limits.hits > 1) {
				throw new RuntimeException("There are ${limits.hits} configs with name $configSetName")
			}
		}
		catch(all) {
			logger.warn(all.getMessage(), all)
			return null
		}
	}

	abstract void updateConfigSet(DoopInputBundle bundle, BundleConfiguration configSet)

	abstract void removeConfigSet(DoopInputBundle bundle, BundleConfiguration configSet)

	abstract boolean existsConfigSet(DoopInputBundle bundle, String configSetId)

	/* ---- PersistentBundleConfigurationRule ---- */

	boolean existsRuleInBundleConfig(DoopInputBundle bundle, BundleConfiguration configSet, String checksum) {
		DataStore store = bundleDoopStore(bundle)
		BooleanQuery q = BooleanQueryBuilder.buildAND([
			'_type': PersistentBundleConfigurationRule.simpleName,
			'bundleId': bundle.id,
			'configSet': configSet.id,
			'checksum': checksum
		])
		return store.count(q) > 0
	}

	void removeRulesOfBundleConfig(DoopInputBundle bundle, BundleConfiguration configSet, Set<String> ruleIds) {
		DataStore store = Config.instance.storage.bundleDoopStore(bundle)

		//Using two separate queries for clarity
		BooleanQuery q = BooleanQueryBuilder.buildAND([
			'_type': PersistentBundleConfigurationRule.simpleName,
			'configSet': configSet.id,
			'id': ruleIds
		])
		store.deleteItems(q)

		q = BooleanQueryBuilder.buildAND([
			'_type': KeepRemoveMethodDirective.simpleName,
			'configSet': configSet.id,
			'ruleId': ruleIds
		])
		store.deleteItems(q)

		store.refresh()
	}

	void removeDirectivesOfRule(DoopInputBundle bundle, BundleConfiguration configSet, String ruleId) {
		DataStore store = Config.instance.storage.bundleDoopStore(bundle)
		BooleanQuery q = BooleanQueryBuilder.buildAND([
			'_type': KeepRemoveMethodDirective.simpleName,
			'configSet': configSet.id,
			'ruleId': ruleId
		])
		store.deleteItems(q)
		store.refresh()
	}

	void forEachBundleConfigurationRule(DoopInputBundle bundle, BundleConfiguration bundleConfiguration, Closure processor) {
		DataStore store = Config.instance.storage.bundleDoopStore(bundle)
		def filters = [
			'_type': PersistentBundleConfigurationRule.simpleName,
			'configSet': bundleConfiguration.id,
			'bundleId': bundle.id
		]
		BooleanQuery q = BooleanQueryBuilder.buildAND(filters)

		OrderBy orderBy = new OrderBy(field:'userIndex', ordering: Ordering.ASC)
		store.scan(q, [orderBy], processor)
	}

	void removeEffectiveDirectivesOfBundleConfig(DoopInputBundle bundle, BundleConfiguration bundleConfig) {

		DataStore store = Config.instance.storage.bundleDoopStore(bundle)
		def filters = [
			'_type': EffectiveMethodDirective.simpleName,
			'configSet': bundleConfig.id,
		]
		BooleanQuery q = BooleanQueryBuilder.buildAND(filters)
		store.deleteItems(q)

		store.refresh()

	}

	/* ---- User ---- */	
	
	void storeNewUser(User user) {
		accountStore().saveNew(user)
	}

	void updateUser(User user) {
		accountStore().updateExisting(user)
	}

	void removeUser(User user) {
		//TODO:
		// delete user from store
		// delete user directory
		// delete session/token/whatever from store
		throw new UnsupportedOperationException("Not implemented yet")
	}

	boolean userExists(String userId) {
		return accountStore().exists(userId, User)
	}

	User loadUser(String userId) {
		return accountStore().load(userId, User)
	}	

	/* ---- SessionToken ---- */
	
	void storeNewToken(SessionToken token) {
		tokenStore().saveNew(token)
	}

	void removeToken(SessionToken token)	{
		tokenStore().delete(token, null)
	}

	SessionToken loadToken(String tokenId) {
		return tokenStore().load(tokenId, SessionToken)
	}

	/* ---- Project ---- */
	void storeNewProject(Project project) {
		logger.debug "Storing project ${project.getId()} of user ${project.owner}"
		projectStore().saveNew(project)
		cache.put(project.getId(), project)
	}

	void updateProject(Project project) {
		projectStore().updateExisting(project)
	}

	void removeProject(Project project) {
		throw new UnsupportedOperationException("Not implemented yet")
	}

	Project loadProject(String projectId) {
		logger.debug "Getting project ${projectId}"
		return (Project) cache.get(projectId, new Callable<Project>() {
			@Override
			Project call() {
				return projectStore().load(projectId, Project)
			}
		})		
	}

	boolean existsProject(String projectId) {
		projectStore().exists(projectId, Project)
	}

	Project findProject(String owner, String name) {
		BooleanQuery q = BooleanQueryBuilder.build {
			AND(
				EQ('_type', Project.simpleName),
				EQ('owner', owner),
				EQ('name', name)
			)
		}


		def limits = new SearchLimits(start: 0, count: 1)
		Project project = null
		projectStore().search(q, null, limits) { id, type, String json ->
			project = new Project().fromJSON(json)
		}

		if (limits.hits > 1) {
			throw new RuntimeException("Multiple projects with owner $owner and name $name")
		}

		return project
	}

	
	/* ---- Various ---- */

	abstract void cleanDeploy(Closure<Void> setupInitialState)

	protected void validateAnalysisStateForDeletion(String id) {		
		ServerSideAnalysis analysis			
		//Try to load the analysis
		try {
			analysis = loadAnalysis(id)
		}
		catch(all) {
			//if something fails, just ignore it, the analysis is in an inconsistent state and
			//we can delete it
		}
		if (analysis) {
			//Given that the analysis can be loaded, verify it is not actually running
			AnalysisState state = analysis.getState()
			if (state.isRunning()) {
				throw new IllegalStateException("Analysis $id of bundle $bundle.id is still running")
			}			
		}			
	}	

	Searcher givenUser(String userId) {
		given(userId, null, null, null)
	}

	Searcher givenProject(Project project) {
		given(null, project, null, null)
	}

	Searcher givenBundle(DoopInputBundle bundle) {
		given(null, null, bundle, null)
	}

	Searcher givenAnalysis(ServerSideAnalysis a) {
		given(null, null, null, a)
	}

	Searcher given(String userId, DoopInputBundle bundle, ServerSideAnalysis a) {
		given(userId, null, bundle, a)
	}

	abstract Searcher given(String userId, Project project, DoopInputBundle bundle, ServerSideAnalysis a)
}