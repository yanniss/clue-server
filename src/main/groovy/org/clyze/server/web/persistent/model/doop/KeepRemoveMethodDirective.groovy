package org.clyze.server.web.persistent.model.doop

import com.android.tools.r8.shaking.BundleConfigurationRule
import groovy.transform.CompileStatic

import org.clyze.persistent.model.doop.Artifact
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.SymbolWithDoopId
import org.clyze.server.web.Config
import org.clyze.server.web.bundle.ClueJsonFile
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.store.Searcher

class KeepRemoveMethodDirective extends SymbolWithDoopId implements OptimizationDirective {

	static enum Type {
		KEEP(BundleConfigurationRule.Type.KEEP),
		KEEP_CLASS_MEMBERS(BundleConfigurationRule.Type.KEEP_CLASS_MEMBERS),
		KEEP_CLASSES_WITH_MEMBERS(BundleConfigurationRule.Type.KEEP_CLASSES_WITH_MEMBERS),
		KEEP_NAMES(BundleConfigurationRule.Type.KEEP_NAMES),
		KEEP_CLASS_MEMBER_NAMES(BundleConfigurationRule.Type.KEEP_CLASS_MEMBER_NAMES),
		KEEP_CLASSES_WITH_MEMBER_NAMES(BundleConfigurationRule.Type.KEEP_CLASSES_WITH_MEMBER_NAMES),
		REMOVE(BundleConfigurationRule.Type.REMOVE),
		FORCE_REMOVE(BundleConfigurationRule.Type.FORCE_REMOVE)

		final BundleConfigurationRule.Type ruleType

		Type(BundleConfigurationRule.Type ruleType) {
			this.ruleType = ruleType
		}

		boolean isKeep() {
			return ruleType.isKeep()
		}

		static Type fromBundleConfigurationRuleType(BundleConfigurationRule.Type ruleType) {
			return values().find { it.ruleType == ruleType }
		}
	}

	static boolean isValidTypeOfDirective(String typeOfDirective) {
        try {
            Type.valueOf(typeOfDirective)
        }
        catch(any) {
            false
        }
    }

	Set<String> origin
	OriginType originType
	String configSet
	Type   typeOfDirective
	String ruleId //the id of the rule that "generated" this directive

	//from SymbolWithDoopId
	//sourceFileName, position, doopId, rootElemId

	//from the Artifact
	String artifactName	

	//from the Class	
	String packageName
	String className
	String declaringSymbolDoopId

	//from the Method
	String methodName
	String returnType
	String[] params
	String[] paramTypes
	boolean isStatic
	boolean isInterface
	boolean isAbstract
	boolean isNative

	KeepRemoveMethodDirective() {

	}

	KeepRemoveMethodDirective(String rootElemId, String configSet, String ruleId, MethodDirectiveContext ctx, Set<String> origin, OriginType originType, Type directiveType) {
		this.id              = UUID.randomUUID().toString()				

		this.rootElemId      = rootElemId
		this.sourceFileName  = ctx.method.sourceFileName
		this.position        = ctx.method.position		
		this.doopId          = ctx.method.doopId		
		
		this.methodName      = ctx.method.name
		this.returnType      = ctx.method.returnType
		this.params          = ctx.method.params
		this.paramTypes      = ctx.method.paramTypes
		this.isStatic        = ctx.method.isStatic()
		this.isInterface     = ctx.method.isInterface()
		this.isAbstract      = ctx.method.isAbstract()
		this.isNative        = ctx.method.isNative()

		this.packageName     = ctx.klass.packageName
		this.className       = ctx.klass.name
		this.declaringSymbolDoopId = ctx.klass.declaringSymbolDoopId

		this.artifactName    = ctx.artifact?.name

		this.origin          = origin
		this.originType      = originType
		this.configSet       = configSet
		this.typeOfDirective = directiveType
		this.ruleId          = ruleId
	}

	//this should be removed
	KeepRemoveMethodDirective(String rootElemId, String configSet, KeepRemoveMethodDirective directive, OriginType originType) {
		this.id              = UUID.randomUUID().toString()				

		this.rootElemId      = rootElemId
		this.sourceFileName  = directive.sourceFileName
		this.position        = directive.position		
		this.doopId          = directive.doopId		
		
		this.methodName      = directive.methodName
		this.returnType      = directive.returnType
		this.params          = directive.params
		this.paramTypes      = directive.paramTypes
		this.isStatic        = directive.isStatic
		this.isInterface     = directive.isInterface
		this.isAbstract      = directive.isAbstract
		this.isNative        = directive.isNative		

		this.packageName     = directive.packageName
		this.className       = directive.className
		this.declaringSymbolDoopId = directive.declaringSymbolDoopId

		this.artifactName    = directive.artifactName	

		this.origin          = directive.origin
		this.originType      = originType
		this.configSet       = configSet
		this.typeOfDirective = directive.typeOfDirective
		this.ruleId          = directive.ruleId
	}	

	@CompileStatic
	@Override
	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map) //rootElemId, sourceFileName, position, doopId

		map.put("id", id)

		map.put("methodName", methodName)
		map.put("returnType", returnType)
		map.put("params", params)
		map.put("paramTypes", paramTypes)
		map.put("isStatic", isStatic)
		map.put("isInterface", isInterface)
		map.put("isAbstract", isAbstract)
		map.put("isNative", isNative)

		map.put("packageName", packageName)
		map.put("className", className)
		map.put("declaringSymbolDoopId", declaringSymbolDoopId)

		map.put("artifactName", artifactName)

		map.put("origin", origin)
		map.put("originType", originType.name())
		map.put("configSet", configSet)
		map.put("typeOfDirective", typeOfDirective.name())
		map.put("ruleId", ruleId)
	}

	@CompileStatic
	@Override
	protected void loadFrom(Map<String, Object> map){
		super.loadFrom(map)//rootElemId, sourceFileName, position, doopId

		this.id              = map.id as String

		this.methodName      = map.methodName as String
		this.returnType      = map.returnType as String
		this.params          = map.params as String[]
		this.paramTypes      = map.paramTypes as String[]
		this.isStatic        = map.isStatic as Boolean
		this.isInterface     = map.isInterface as Boolean
		this.isAbstract      = map.isAbstract as Boolean
		this.isNative        = map.isNative as Boolean

		this.packageName     = map.packageName as String
		this.className       = map.className as String
		this.declaringSymbolDoopId = map.declaringSymbolDoopId as String

		this.artifactName    = map.artifactName as String

		this.origin          = ClueJsonFile.readOriginFromJson(map.origin)
		this.originType      = OriginType.valueOf(map.originType as String)
		this.configSet       = map.configSet as String
		this.typeOfDirective = Type.valueOf(map.typeOfDirective as String)
		this.ruleId          = map.ruleId as String
	}

	@Override
	boolean equals(Object object) {
		if (this == object) return true
		if (!(object instanceof KeepRemoveMethodDirective)) return false

		KeepRemoveMethodDirective other = object as KeepRemoveMethodDirective
		
		return Objects.equals(id, other.id)
    }

    @Override
    int hashCode() {
        return Objects.hash(id)
    }

    void export(Writer w) {
    	if (typeOfDirective == Type.REMOVE) {
    		w.write("remove\t" + doopId + "\n")
    	}
    	else {
    		w.write("keep\t" + doopId + "\n")
    	}
    }

	static MethodDirectiveContext contextForDoopId(DoopInputBundle inputBundle, String doopId) {
		Searcher searcher = Config.instance.storage.givenBundle(inputBundle)

		Method method = searcher.findMethod(doopId)
		if (!method) {
			throw new RuntimeException("Method not found $doopId")
		}

		Klass klass = searcher.findClass(method.declaringClassDoopId)
		if (!klass) {
			throw new RuntimeException("Class not found ${method.declaringClassDoopId}")
		}

		Artifact artifact = inputBundle.findArtifactByName(klass.artifactName)
		if (!artifact) {
			throw new RuntimeException("Arifact not found ${klass.artifactName}")
		}

		return new MethodDirectiveContext(artifact, klass, method, null)
	}

	static KeepRemoveMethodDirective forDoopId(DoopInputBundle inputBundle, String doopId, String configSet, String origin, OriginType originType, Type type) {
		MethodDirectiveContext ctx = contextForDoopId(inputBundle, doopId)
		return new KeepRemoveMethodDirective(inputBundle.id, configSet, null, ctx, [origin] as Set, originType, type)
	}

}