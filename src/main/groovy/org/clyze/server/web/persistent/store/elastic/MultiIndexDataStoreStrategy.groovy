package org.clyze.server.web.persistent.store.elastic


import org.apache.commons.io.FileUtils
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.persistent.store.Ordering
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.store.Searchable
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.model.doop.VarValues
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.restlet.api.PaginationParams

import java.util.concurrent.Callable

/**
 * A multi-index strategy, where:
 * <ul>
 * <li>The user info (accounts and tokens) and the bundle and analysis objects are stored in the MAIN_STORE index.
 * <li>For each bundle, a different index ({user}_{bundle}) is used to store the doop-generated bundle data.
 * <li>For each analysis, a different index ({user}_{analysis}) is used to store the doop-generated analysis data.	
 * </ul>
 */
class MultiIndexDataStoreStrategy extends DataStoreStrategy {

    private static final String MAIN_STORE = "clyze_main"

    /** The master datastore (for storing users, tokens, bundles and analyses, etc) */
    private DataStore mainStore

    @Override
    void startup() {
        connector.connect()
        //init the master index
        connector.createStoreIfNotExists(MAIN_STORE)
        mainStore = connector.connectTo(MAIN_STORE)
    }

    @Override
    void shutdown() {
        connector?.disconnect()
    }

    @Override
    DataStore bundleDoopStore(DoopInputBundle bundle) {
        if (!bundle) throw new IllegalArgumentException("Bundle is null")
        //The index has already been created
        String nameOfStore = nameOfStore(bundle.userId, bundle.id)
        connector.connectTo(nameOfStore)
    }

    @Override
    DataStore initializeBundleDoopStore(DoopInputBundle bundle) {
        if (!bundle) throw new IllegalArgumentException("Bundle is null")
        String nameOfStore = nameOfStore(bundle.userId, bundle.id)
        connector.createStore(nameOfStore)
        connector.connectTo(nameOfStore)
    }		

    @Override
    DataStore analysisDoopStore(ServerSideAnalysis a) {		
        if (!a) throw new IllegalArgumentException("Analysis is null")
        //The index has already been created
        String nameOfStore = nameOfStore(a.userId, a.id)
        connector.connectTo(nameOfStore)
    }

    @Override
    protected String nameOfStoreForUserAccounts() {
        return MAIN_STORE
    }

    @Override 
    protected String nameOfStoreForUserTokens() {
        return MAIN_STORE
    }

    @Override 
    protected String nameOfStoreForUserProjects() {
        return MAIN_STORE
    }

    protected String nameOfStore(String userId, String bundleOrAnalysisId) {
        return "clyze_${userId}_${bundleOrAnalysisId}"
    }	

    @Override
    void storeNewInputBundle(DoopInputBundle bundle, Identifiers.BundleInfo info) {
        logger.debug "Storing new input bundle ${bundle.id} of user ${bundle.userId}"

        synchronized (info.project) {
            info.project.bundlesCnt += 1
            updateProject(info.project)
            bundle.displayName = "bundle" + info.project.bundlesCnt

            //Store the bundle object in the MAIN_STORE
            mainStore.saveNew(bundle)
            cache.put(bundle.id, bundle)
        }
    }

    @Override
    void removeInputBundle(DoopInputBundle bundle) {
        String bundleId = bundle.id
        String userId = bundle.userId

        logger.debug "Removing input bundle ${bundleId} of user ${userId}"		
		
        def analyses = []
        //cycle through analyses to verify they are in proper state
        givenBundle(bundle).searchAnalysesOfBundle { String id ->
            //Add the id returned
            analyses.add id

            validateAnalysisStateForDeletion(id)						
        }
		
        //Delete the bundle & analyses objects
        Set<String> idsForRemoval = ([bundleId] + analyses).findAll() as Set
        BooleanQuery q = BooleanQueryBuilder.build {
            AND(
                IN('_type', [DoopInputBundle.simpleName, ServerSideAnalysis.simpleName] as Set),
                IN('_id', idsForRemoval)
            )

        }
        mainStore.deleteItems(q)
		
        //Drop the bundle and analyses doop-data indices
        idsForRemoval.collect { nameOfStore(userId, it) }.each {
            connector.dropStore it 
        }

        //Delete the bundle dir
        logger.debug "Removing bundle dir of ${bundleId}"
        FileUtils.deleteQuietly(bundle.baseDir)

        //remove the bundle from the cache
        cache.invalidate(bundleId)
    }

    @Override
    DoopInputBundle loadInputBundle(String bundleId) {	
        logger.debug "Loading input bundle ${bundleId}"
        return (DoopInputBundle) cache.get(bundleId, new Callable<DoopInputBundle>() {
                @Override
                DoopInputBundle call() {
                    DoopInputBundle bundle = mainStore.load(bundleId, DoopInputBundle)
				
                    //Verify that the bundle index exists and works before returning the bundle to the caller
                    String bundleStoreName = nameOfStore(bundle.userId, bundleId)
                    connector.connectTo(bundleStoreName)
				
                    bundle
                }
            })
    }


    @Override
    void storeNewAnalysis(ServerSideAnalysis a, Identifiers.AnalysisInfo info)  {
        logger.debug "Storing new analysis ${a.id} of user ${a.userId}"

        synchronized (info.bundle) {

            info.bundle.analysesCnt += 1
            mainStore.updateExisting(info.bundle)
            a.displayName = "analysis" + info.bundle.analysesCnt

            //Create a new index to hold the analysis' doop-generated data
            String analysisStoreName = nameOfStore(a.userId, a.id)
            connector.createStore(analysisStoreName)

            //Store the analysis object in the MAIN_STORE
            mainStore.saveNew(a)
            cache.put(a.id, a)
        }
    }

    @Override
    void removeAnalysis(ServerSideAnalysis a) {

        throw new UnsupportedOperationException("Operation not supported")
    }

    @Override
    ServerSideAnalysis loadAnalysis(String anId) {		
        logger.debug "Loading analysis ${anId}"
        return (ServerSideAnalysis) cache.get(anId, new Callable<ServerSideAnalysis>() {
                @Override
                ServerSideAnalysis call() {
                    ServerSideAnalysis analysis = mainStore.load(anId, ServerSideAnalysis)
				
                    //Verify that the analysis index exists and works before returning the analysis to the caller
                    String analysisStoreName = nameOfStore(analysis.userId, anId)
                    connector.connectTo(analysisStoreName)
				
                    analysis
                }
            })
    }

    @Override
    void updateAnalysis(ServerSideAnalysis a) {		
        mainStore.updateExisting(a)
    }

    @Override
    void storeNewOptimizationDirective(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        //Store them in the bundle index
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        store.saveNew(directive)
        store.refresh()
    }

    @Override
    KeepRemoveMethodDirective loadOptimizationDirectiveOfBundle(DoopInputBundle bundle, String directiveId) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        connector.connectTo(bundleStoreName).load(directiveId, KeepRemoveMethodDirective)
    }

    @Override
    void updateOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        store.updateExisting(directive)
        store.refresh()

    }

    @Override
    void removeOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        store.delete(directive) {
            store.refresh()
        }
    }


    @Override
    void storeNewConfigSet(DoopInputBundle bundle, BundleConfiguration configSet) {
        //Store them in the bundle index
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        store.saveNew(configSet)
        store.refresh()
        cache.put(configSet.getId(), configSet)
    }

    @Override
    BundleConfiguration loadConfigSet(DoopInputBundle bundle, String configSetId) {
        logger.debug "Loading configSet ${configSetId}"
        return (BundleConfiguration) cache.get(configSetId, new Callable<BundleConfiguration>() {
            @Override
            BundleConfiguration call() {
                String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
                connector.connectTo(bundleStoreName).load(configSetId, BundleConfiguration)
            }
        })
    }

    @Override
    void updateConfigSet(DoopInputBundle bundle, BundleConfiguration configSet) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        configSet.touch()
        store.updateExisting(configSet)
        store.refresh()
    }

    @Override
    void removeConfigSet(DoopInputBundle bundle, BundleConfiguration configSet) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        DataStore store = connector.connectTo(bundleStoreName)
        store.delete(configSet, null)

        BooleanQuery q = BooleanQueryBuilder.build {
            AND (
                EQ('_type', KeepRemoveMethodDirective.simpleName),
                EQ('configSet', configSet.id)
            )
        }

        store.deleteItems(q)

        store.refresh()

        cache.invalidate(bundle.id)
    }


    @Override
    boolean existsConfigSet(DoopInputBundle bundle, String configSetId) {
        String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
        connector.connectTo(bundleStoreName).exists(configSetId, BundleConfiguration)
    }


    @Override
    void cleanDeploy(Closure<Void> setupInitialState) {

        //Get the ids of all indices and drop them
        [ServerSideAnalysis, DoopInputBundle].each {
            BooleanQuery q = BooleanQueryBuilder.build('_type', it.simpleName)
            mainStore.scan(q, null) { String id, String type, String json ->
                try {		
                    def item = mainStore.load(id, it)
                    String indexName = nameOfStore(item.userId, id)
                    logger.debug("Dropping index $indexName")
                    connector.dropStoreIfExists(indexName)
                }
                catch (all) {
                    logger.warn("Failed to drop index of $type: $id")
                }
            }
        }

        //Drop the MAIN_STORE
        try {
            logger.debug("Cleaning up $MAIN_STORE")
            mainStore.clear()
        }
        catch(all) {
            logger.warn("Failed to drop index: $MAIN_STORE")
        }

        setupInitialState?.call()
    }	

    /*
    @Override
    Searcher givenUser(String userId) {
    Searchable searchable = new ElasticSearchable(connector, [MAIN_STORE] as String[])
    newSearcher(searchable, userId, null, null)
    }

    @Override
    Searcher givenBundle(DoopInputBundle bundle) {
    String bundleStoreName = nameOfStore(bundle.userId, bundle.id)
    Searchable searchable = new ElasticSearchable(connector, [bundleStoreName] as String[])
    newSearcher(searchable, null, bundle.id, null)
    }

    @Override
    Searcher givenAnalysis(ServerSideAnalysis a) {
    String analysisStoreName = nameOfStore(a.userId, a.id)
    Searchable searchable = new ElasticSearchable(connector, [analysisStoreName] as String[])
    newSearcher(searchable, null, null, a.id)
    }


    @Override
    Searcher given(final String userId, final DoopInputBundle bundle, final ServerSideAnalysis a) {
    String bundleStoreName   = bundle ? nameOfStore(bundle.userId, bundle.id) : null
    String analysisStoreName = a      ? nameOfStore(a.userId, a.id)           : null

    List<String> indices = userId ? [MAIN_STORE] : []

    if (bundleStoreName)   indices << bundleStoreName
    if (analysisStoreName) indices << analysisStoreName

    Searchable searchable = new ElasticSearchable(connector, indices as String[])
    newSearcher(searchable, userId, bundle?.id, a?.id)
    }
     */

    //protected Searcher newSearcher(Searchable searchable, String userId, String bundleId, String analysisId) {}

    @Override
    Searcher given(final String userId, final Project project, final DoopInputBundle bundle, final ServerSideAnalysis a) {

        Searchable searchable

        List<String> indices = [MAIN_STORE]

        final String bundleId = bundle ? bundle.id : null
        final String bundleStoreName   = bundle ? nameOfStore(bundle.userId, bundle.id) : null
        final String analysisStoreName = a      ? nameOfStore(a.userId, a.id)           : null

        if (bundleStoreName)   indices << bundleStoreName
        if (analysisStoreName) indices << analysisStoreName

        if (project) {
            searchable = new ElasticSearchable(connector, ["_all"] as String[])
        }
        else {
            searchable = new ElasticSearchable(connector, indices as String[])
        }

        return new Searcher(searchable) {

            @Override
            protected BooleanQuery createQuery(Map<String, Object> filters) {				
                BooleanQueryBuilder.buildAND(filters)
            }

            @Override
            SearchLimits processProjectsOfUser(String text, int start, int count, List<OrderBy> orderBy, Closure processor) {
                return processProjects(mainStore, userId, text, start, count, orderBy, processor)
            }

            @Override
            SearchLimits processInputBundlesOfProject(int start, int count, List<OrderBy> orderBy, Closure processor) {
                return processInputBundlesOfProject(searchable, project?.id as String, start, count, orderBy, processor)
            }

            @Override
            void getMethodOptimizationDirectivesOfConfigSet(String configSet, Set<String> excluding = [], KeepRemoveMethodDirective.Type typeOfDirective = null, Closure processor) {

                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")				

                connector.connectTo(bundleStoreName).scan(queryForMethodOptimizationDirectives(configSet, excluding, typeOfDirective?.name()), null, processor)
            }

            @Override
            void getMethodOptimizationDirectivesOfAnalysis(Set<String> excluding = [], Closure processor) {

                if (!analysisStoreName) throw new RuntimeException("No analysis store in search context")				

                connector.connectTo(analysisStoreName).scan(queryForMethodOptimizationDirectives(null, excluding, null), null, processor)
            }

            private static BooleanQuery queryForMethodOptimizationDirectives(String configSet, Set<String> excluding = [], String typeOfDirective) {
                BooleanQueryBuilder builder = new BooleanQueryBuilder()
                List<BooleanQuery.Operator> andOps = [
                    builder.EQ('_type', KeepRemoveMethodDirective.simpleName)
                ]
                if (typeOfDirective) {
                    andOps.add(builder.EQ('typeOfDirective', typeOfDirective))
                }
                if (configSet) {
                    andOps.add(builder.EQ('configSet', configSet))
                }
                if (excluding) {
                    andOps.add(builder.NOT(builder.IN('doopId', excluding)))
                }

                return new BooleanQuery(
                    operator: new BooleanQuery.CompoundOperator(BooleanQuery.OP.AND, andOps as BooleanQuery.Operator[])
                )
            }

            @Override
            long searchAnalysesOfBundle(Closure processor) {	

                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                def filters = [
                    _type   : ServerSideAnalysis.simpleName,
                    bundleId: bundleId
                ]
                return searchForIds(mainStore, BooleanQueryBuilder.buildAND(filters), processor)
            }

            @Override
            long searchClassesOfArtifact(String artifactName, Closure processor) {						

                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")

                def query = createQuery (
                    _type       : Klass.simpleName,
                    artifactName: artifactName,
                ) 
                return search(connector.connectTo(bundleStoreName), query, processor)
            }

            @Override
            SearchLimits searchMethodOptimizationDirectives(Searcher.DirectivesSearchParams searchParams,
                                                            int start,
                                                            int count,
                                                            List<OrderBy> orderByList,
                                                            Closure<Void> processor) {

                def filters = searchParams.asSearchFilters()

                logger.debug("searchMethodOptimizationDirectives with filters: $filters")

                def limits = new SearchLimits(start: start, count: count)
                searchable.search(searchParams.textualSearch, BooleanQueryBuilder.buildAND(filters), orderByList, limits) { id, type, String json ->
                    //logger.debug("Processing $json")
                    processor?.call(json)
                }

                return limits
            }	
            
            @Override
            Map<String, Long> facetOfOptimizationDirectives(Searcher.DirectivesSearchParams searchParams, String facetField, int maxResults) {
                
                def filters = searchParams.asSearchFilters()

                logger.debug("facetOfOptimizationDirectives with filters: $filters")
                
                return searchable.groupBy(searchParams.textualSearch, BooleanQueryBuilder.buildAND(filters), facetField, maxResults)
            }

            @Override
            void getConfigSetsOfBundle(List<OrderBy> orderByList, Closure<Void> processor) {

                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")

                def query = BooleanQueryBuilder.build('_type', BundleConfiguration.simpleName)

                searchable.scan(query, orderByList, processor)

            }

            @Override
            Map<String, Long> groupOptimizationDirectivesByRule(String configSetName) {
                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")

                def query = createQuery(_type: KeepRemoveMethodDirective.simpleName, configSet: configSetName)

                searchable.groupBy(query, 'ruleId', 1000)
            }

            SearchLimits searchConfigSetRules(Searcher.RulesSearchParams searchParams, int start, int count, List<OrderBy> orderByList, Closure<Void> processor) {

                def filters = searchParams.asSearchFilters()

                def limits = new SearchLimits(start: start, count: count)
                searchable.search(searchParams.textualSearch, BooleanQueryBuilder.buildAND(filters), orderByList, limits) { id, type, String json ->
                    //logger.debug("Processing $json")
                    processor?.call(json)
                }

                return limits
            }

            @Override
            long countConfigSetRules(String configSet) {

                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")

                BooleanQuery q = createQuery ([
                    '_type': PersistentBundleConfigurationRule.simpleName,
                    'configSet': configSet
                ])
                return searchable.count(q)
            }

            @Override
            Map<String, Long> facetOfConfigSetRules(Searcher.RulesSearchParams searchParams, String facetField, int maxResults) {

                def filters = searchParams.asSearchFilters()

                return searchable.groupBy(searchParams.textualSearch, BooleanQueryBuilder.buildAND(filters), facetField, maxResults)
            }

            @Override
            long searchAtPosition(Set<String> allowedTypes, String sourceFileName, String line, String column, Closure processor)  {

                if (!bundleStoreName) throw new RuntimeException("No bundle store in search context")

                BooleanQuery q = BooleanQueryBuilder.build {
                    AND(						
                        EQ('sourceFileName', sourceFileName),
                        EQ('position.startLine', line),
                        LTE('position.startColumn', column),
                        GTE('position.endColumn', column),
                        IN('_type', allowedTypes)
                    )
                }

                def limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
                connector.connectTo(bundleStoreName).search(q, null, limits, processor)
                return limits.hits
            }				

            @Override
            void searchClosestVarValues(String var, long line, Closure closure) {

                if (!analysisStoreName) return

                DataStore analysisStore = connector.connectTo(analysisStoreName)

                // Initial outermost query: find closest previous line.
                BooleanQuery q = BooleanQueryBuilder.build {
                    AND(						
                        EQ('_type', VarValues.simpleName),
                        EQ('varId', var),
                        LTE('line', line)
                    )
                }
                def orderByLine = new OrderBy(field: 'line', ordering: Ordering.DESC)
                def limits = new SearchLimits(start: 0, count: 1)

                // Inner query: process results
                def innerQuery = { String id0, String type0, String json ->					
                    def vpt = new VarValues().fromJSON(json)				
                    //logger.debug "Found VarValues ${vpt.toMap()}"	
                    BooleanQuery innerQ = BooleanQueryBuilder.build {
                        AND(							
                            EQ('_type', VarValues.simpleName),
                            EQ('varId', var),
                            EQ('line', vpt.line)
                        )
                    }
                    limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
                    //logger.debug("Searching for VarValues in line: ${vpt.line}")
                    analysisStore.search(innerQ, null, limits, closure)
                }

                //logger.debug "Search closest VarValues - var:$var, line<=$line, baseFilter:$baseFilter"
                analysisStore.search(q, [orderByLine], limits, innerQuery)
				
                // Optional second outermost query: if no results were found
                // in the query above, rerun query with opposite line filter
                // and ordering -- now finds closest next line.
                if (limits.hits == 0) {
                    //logger.debug "Second search for VarValues - var:$var, line>$line, baseFilter:$baseFilter"
                    q = BooleanQueryBuilder.build {
                        AND(							
                            EQ('_type', VarValues.simpleName),
                            EQ('varId', var),
                            GT('line', line)
                        )
                    }				
                    def orderByLine2 = new OrderBy(field: 'line', ordering: Ordering.ASC)
                    def limits2 = new SearchLimits(start: 0, count: 1)
                    analysisStore.search(q, [orderByLine2], limits2, innerQuery)
                }
            }

        }
    }

}
