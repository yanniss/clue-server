package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class VarReturn extends SymbolRelation {

	String varId
	String methodId

	VarReturn(String anId, String varId, String methodId) {
		super(anId)
		this.varId = varId
		this.methodId = methodId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.varId    = this.varId
		map.methodId = this.methodId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.varId    = map.varId
		this.methodId = map.methodId
	}
}
