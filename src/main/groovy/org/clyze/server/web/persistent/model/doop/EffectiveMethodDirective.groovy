package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors

@InheritConstructors
class EffectiveMethodDirective extends KeepRemoveMethodDirective {

    EffectiveMethodDirective(String rootElemId, String configSet, KeepRemoveMethodDirective directive) {
        this.id              = UUID.randomUUID().toString()

        this.rootElemId      = rootElemId
        this.sourceFileName  = directive.sourceFileName
        this.position        = directive.position
        this.doopId          = directive.doopId

        this.methodName      = directive.methodName
        this.returnType      = directive.returnType
        this.params          = directive.params
        this.paramTypes      = directive.paramTypes
        this.isStatic        = directive.isStatic
        this.isInterface     = directive.isInterface
        this.isAbstract      = directive.isAbstract
        this.isNative        = directive.isNative

        this.packageName     = directive.packageName
        this.className       = directive.className
        this.declaringSymbolDoopId = directive.declaringSymbolDoopId

        this.artifactName    = directive.artifactName

        this.origin          = directive.origin
        this.originType      = directive.originType
        this.configSet       = configSet
        //the type of effective directives is either KEEP or REMOVE
        this.typeOfDirective = directive.typeOfDirective.isKeep() ? Type.KEEP : Type.REMOVE
        this.ruleId          = directive.ruleId
    }
}