package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class ReachableSymbol extends SymbolRelation {

	String doopId

	ReachableSymbol(String anId, String doopId) {
		super(anId)
		this.doopId = doopId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.doopId = this.doopId		
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.doopId = map.doopId		
	}
}
