package org.clyze.server.web.persistent.store

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.doop.*
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.model.doop.*
import org.clyze.server.web.persistent.model.doop.Metrics.Kind
import org.clyze.server.web.restlet.api.PaginationParams

import java.lang.Class

import static org.clyze.server.web.persistent.model.doop.ValueKind.*

import static org.clyze.server.web.postprocess.doop.Constants.*

abstract class Searcher {

    private static Log logger = LogFactory.getLog(getClass())

    private static final Set<String> SYMBOLS = (JC_PLUGIN_SYMBOLS + JC_PLUGIN_BASIC_ELEMENTS).collect { it.simpleName } as Set

    private static final Map boxConversion = [
        "boolean": "java.lang.Boolean",
        "byte"   : "java.lang.Byte",
        "char"   : "java.lang.Character",
        "float"  : "java.lang.Float",
        "int"    : "java.lang.Integer",
        "long"   : "java.lang.Long",
        "short"  : "java.lang.Short",
        "double" : "java.lang.Double"
    ]

    private static final String MOCK_LISTENER_PRE = "<mock android listener-like object "
    private static final String MOCK_NATIVE_PRE = "<native "
    private static final String REIFIED_CONS_PRE = "<<reified constructor "
    private static final String REIFIED_METH_PRE = "<<reified method "
    private static final String ANDROID_COMP_PRE = "<android component object "
    private static final String ANDROID_LIB_PRE = "<android library object "
    private static final String LAYOUT_CONTROL_PRE = "<layout control object "
    private static final String PRIMITIVE_VAL_PRE = "num-constant-"
    private static final String STRING_CONST_PRE = "<string constant "

    private static final int MAX_METHODS = 5000
    private static final int MAX_FIELDS = 5000
    private static final int MAX_PACKAGES = 5000

    static final enum TYPE_HIERARCHY {
        SUBTYPES,
        SUPERTYPES
    }

    protected final Searchable searchable

    protected Searcher(Searchable searchable) {
        this.searchable = searchable
    }

    protected abstract BooleanQuery createQuery(Map<String, Object> filters)

    /**
     * Processes the projects of the given user using the processor.
     * The text arg -if present- will be used to filter the results with a free-text search on the allText field.
     */
    abstract SearchLimits processProjectsOfUser(String text, int start, int count, List<OrderBy> orderBy, Closure processor)

    /**
     * Processes the input bundles of the given user using the processor.	
     */
    abstract SearchLimits processInputBundlesOfProject(int start, int count, List<OrderBy> orderBy, Closure processor)			

    abstract void getMethodOptimizationDirectivesOfConfigSet(String configSet,
                                                             Set<String> excluding = [],
                                                             KeepRemoveMethodDirective.Type typeOfDirective = null,
                                                             Closure processor)

    abstract void getMethodOptimizationDirectivesOfAnalysis(Set<String> excluding = [], Closure processor)

    abstract long searchAnalysesOfBundle(Closure processor)

    /**
     * Accepts a Closure { String json -> }*/
    abstract long searchClassesOfArtifact(String artifactName, Closure processor)


    /**
     * Accepts a Closure { String json -> }*/	
    SearchLimits searchMethodOptimizationDirectives(Set<String> configSets,
                                                    String sourceFileName, 
                                                    int start,
                                                    int count,
                                                    List<OrderBy> orderByList,
                                                    Closure<Void> processor) {
        return searchMethodOptimizationDirectives(
            new DirectivesSearchParams(configSet: configSets, sourceFileName: [sourceFileName] as Set),            
            start,
            count,
            orderByList,
            processor
        )
    }

    /**
     * Accepts a Closure { String json -> }*/	
    abstract SearchLimits searchMethodOptimizationDirectives(DirectivesSearchParams params,
                                                             int start,
                                                             int count,
                                                             List<OrderBy> orderByList,
                                                             Closure<Void> processor)

    void searchMethodOptimizationDirectives(String configSet, KeepRemoveMethodDirective.Type type, Closure<Void> processor) {
        searchMethodOptimizationDirectives(
            new DirectivesSearchParams(configSet: [configSet] as Set, typeOfDirective: [type.name()] as Set),
            0,
            10000,
            null,
            processor
        )
    }
                                                         
    abstract Map<String, Long> facetOfOptimizationDirectives(DirectivesSearchParams params, String facetField, int maxResults)

    /**
     * Accepts a Closure { String json -> }*/
    abstract void getConfigSetsOfBundle(List<OrderBy> orderByList, Closure<Void> processor)

    abstract Map<String, Long> groupOptimizationDirectivesByRule(String configSetName)

    SearchLimits searchConfigSetRules(String configSet, int start, int count, List<OrderBy> orderByList, Closure<Void> processor) {
        searchConfigSetRules(
            new RulesSearchParams(configSet: [configSet] as Set),
            start,
            count,
            orderByList,
            processor
        )
    }

    abstract SearchLimits searchConfigSetRules(RulesSearchParams params, int start, int count, List<OrderBy> orderByList, Closure<Void> processor)

    abstract long countConfigSetRules(String configSet)

    abstract Map<String, Long> facetOfConfigSetRules(RulesSearchParams params, String facetField, int maxResults)

    /**
     * Accepts a Closure { String id, String type, String json -> }*/
    long searchSymbolsAtPosition(String sourceFileName, String line, String column, Closure processor) {
        return searchAtPosition(SYMBOLS, sourceFileName, line, column, processor)
    }

    abstract long searchAtPosition(Set<String> allowedTypes, String sourceFileName, String line, String column, Closure processor)

    /**
     * Accepts a Closure { String id, String type, String json -> }*/
    abstract void searchClosestVarValues(String var, long line, Closure closure)	

    /** Used by the DatastoreQueryHelper, perhaps it should be removed **/
    void search(String freeText, Map<String, Object> filters, List<OrderBy> orderByList, SearchLimits limits, Closure processor) {
        //TO BE REMOVED
        throw new UnsupportedOperationException("Not supported")
    }

    /** Used by the SearchSymbolByPrefix **/
    void autoCompleteSymbolName(String prefix, Set<String> types, Closure processor) {
        //TO BE REMOVED
        throw new UnsupportedOperationException("Not supported")
    }

    Klass findClass(String doopId) {
        String json = fetchRawJson(doopId, Klass.simpleName)
        return json ? new Klass().fromJSON(json) : null
    }

    Method findMethod(String doopId) {
        String json = fetchRawJson(doopId, Method.simpleName)
        return json ? new Method().fromJSON(json) : null
    }

    MethodInvocation findInvocation(String doopId) {
        String json = fetchRawJson(doopId, MethodInvocation.simpleName)
        return json ? new MethodInvocation().fromJSON(json) : null
    }

    Field findField(String doopId) {
        String json = fetchRawJson(doopId, Field.simpleName)
        return json ? new Field().fromJSON(json) : null
    }

    Variable findVariable(String doopId) {
        String json = fetchRawJson(doopId, Variable.simpleName)
        return json ? new Variable().fromJSON(json) : null
    }


    Set<Variable> findVariables(List<String> doopIds) {
        def res = [] as Set

        def query = createQuery(
            _type: Variable.simpleName,
            doopId: doopIds
        )

        search(searchable, query) { String json ->
            res << new Variable().fromJSON(json)
        }

        return res
    }

    boolean isSymbolReachable(String doopId) {
        def query = createQuery(
            _type : ReachableSymbol.simpleName,
            doopId: doopId
        )

        return (searchForOne(searchable, query, null) == 1)
    }

    Set<Value> findValues(String doopId) {
        def values = [] as Set
        def hits = searchHeapAllocations(doopId) { String json ->
            Value val = new Value().fromJSON(json)
            val.kind = NORMAL_ALLOCATION
            values << val
        }
        // If no results were found, it might be a value that is not a heap allocation
        if (!hits) {
            def val = infer(doopId)
            if (val) values << val
        }
        return values
    }


    long searchArrayValues(String arrayValueId, Closure processor) {
        def query = createQuery(
            _type: ArrayValues.simpleName,
            arrayValueId: arrayValueId
        )

        search(searchable, query, processor)
    }


    Set<Value> findAllocationsOfType(String type) {

        def res = [] as Set

        def query = createQuery(
            _type: HeapAllocation.simpleName,
            allocatedTypeDoopId: type
        )

        search(searchable, query) { String json ->
            Value val = new Value().fromJSON(json)
            val.kind = NORMAL_ALLOCATION
            res << val
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchHeapAllocations(String doopId, Closure processor) {
        def query = createQuery(
            _type: HeapAllocation.simpleName,
            doopId: doopId
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchInvocationCHAValues(String invocationId, Closure processor) {
        def query1 = createQuery(
            _type: InvocationParts.simpleName,
            invocationId: invocationId
        )

        final def localSearchable = searchable
        search(localSearchable, query1) { String json1 ->
            def t1 = new InvocationParts().fromJSON(json1) as InvocationParts
            if (t1.isSingleDispatch) {
                def query3 = createQuery(
                    _type: MethodLookup.simpleName,
                    nameWithDescriptor: t1.nameWithDescriptor,
                    typeId: t1.baseTypeId
                )
                search(localSearchable, query3, processor)
            } else {
                def query2 = createQuery(
                    _type: ClassSubtype.simpleName,
                    classId: t1.baseTypeId
                )
                search(localSearchable, query2) { String json2 ->
                    def t2 = new ClassSubtype().fromJSON(json2) as ClassSubtype
                    def query3 = createQuery(
                        _type: MethodLookup.simpleName,
                        nameWithDescriptor: t1.nameWithDescriptor,
                        typeId: t2.subClassId
                    )
                    search(localSearchable, query3, processor)
                }
            }
        }
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchInvocationCHAValuesTo(String toMethodId, Closure processor) {
        def query1 = createQuery(
            _type: MethodLookup.simpleName,
            methodId: toMethodId
        )

        final def localSearchable = searchable
        search(localSearchable, query1) { String json1 ->
            def t1 = new MethodLookup().fromJSON(json1) as MethodLookup
            if (t1.isSingleDispatch) {
                def query3 = createQuery(
                    _type: InvocationParts.simpleName,
                    nameWithDescriptor: t1.nameWithDescriptor,
                    baseTypeId: t1.typeId
                )
                search(localSearchable, query3, processor)
            } else {
                def query2 = createQuery(
                    _type: ClassSubtype.simpleName,
                    subClassId: t1.typeId
                )
                search(localSearchable, query2) { String json2 ->
                    def t2 = new ClassSubtype().fromJSON(json2) as ClassSubtype
                    def query3 = createQuery(
                        _type: InvocationParts.simpleName,
                        nameWithDescriptor: t1.nameWithDescriptor,
                        baseTypeId: t2.classId
                    )
                    search(localSearchable, query3, processor)
                }
            }
        }
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchInvocationValues(String invocationId, Closure processor) {
        def query = createQuery(
            _type: InvocationValues.simpleName,
            invocationId: invocationId
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchInvocationValuesTo(String toMethodId, Closure processor) {
        def query = createQuery(
            _type: InvocationValues.simpleName,
            toMethodId: toMethodId
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchUsages(String varDoopId, UsageKind usageKind, Closure processor) {
        def query = createQuery(
            _type: Usage.simpleName,
            doopId: varDoopId,
            usageKind: usageKind.name()
        )

        search(searchable, query, processor)
    }

    /**
     * Search for usages and return the results as a Set
     */
    Set<Usage> findUsages(String varDoopId, UsageKind usageKind) {
        def res = [] as Set
        searchUsages(varDoopId, usageKind) { String json ->
            res << new Usage().fromJSON(json)
        }
        return res
    }

    /**
     * Search for closest VarValues and return the results as a Set
     */
    Set<VarValues> findClosestVarValues(String var, long line) {
        Set<VarValues> res = [] as Set
        searchClosestVarValues(var, line) { String id, String type, String json ->
            logger.debug "Processing VarValues - id:$id, type:$type"
            res << new VarValues().fromJSON(json)
        }
        return res
    }

    /**
     * Search for closest Values and return the results as a Set
     */
    Set<Value> findClosestValues(String var, long line) {
        Set<Value> values = [] as Set
        searchClosestVarValues(var, line) { String id, String type, String json ->
            logger.debug "Processing VarValues - id:$id, type:$type"
            def t = new VarValues().fromJSON(json)
            logger.debug "Processing VarValues - ${t.toMap()}"
            values.addAll(findValues(t.valueId))
        }
        return values
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchVarValues(String valueId, Closure processor) {
        def query = createQuery(
            _type: VarValues.simpleName,
            valueId: valueId
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchClassReachesField(String classId, Closure processor) {
        def query = createQuery(
            _type: TypeCanAccessField.simpleName,
            classId: classId
        )

        search(searchable, query, processor)
    }


    Set<Field> findAllFieldsOfThis(String type) {
        def res = [] as Set
        searchClassReachesField(type) { String json1 ->
            def t = new TypeCanAccessField().fromJSON(json1)
            Field f = findField(t.fieldId)
            if (f) res << f
        }
        return res
    }

    Set<Field> findFieldsOfType(String type) {
        def res = [] as Set

        def query = createQuery(
            _type: Field.simpleName,
            type: type
        )

        search(searchable, query) { String json ->
            res << new Field().fromJSON(json)
        }

        return res
    }


    Set<Variable> findVariablesOfType(String type) {
        def res = [] as Set

        def query = createQuery(
            _type: Variable.simpleName,
            type: type
        )

        search(searchable, query) { String json ->
            res << new Variable().fromJSON(json)
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchClassesOfPackage(String packageName, Closure processor) {
        def query = createQuery(
            _type: Klass.simpleName,
            packageName: packageName
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchPackageCallsPackage(Closure processor) {
        def query = createQuery(_type: PackageCallsPackage.simpleName)
        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchSymbolsOfSourceFiles(List<String> sourceFileNames, Class symbolType, Closure processor) {
        def query = createQuery(
            _type: symbolType.simpleName,
            sourceFileName: sourceFileNames
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchDeclaringMethods(String declaringClassDoopId, Closure processor) {
        def query = createQuery(
            _type: Method.simpleName,
            declaringClassDoopId: declaringClassDoopId,
            isAbstract: false,
            isInterface: false
        )

        search(searchable, query, processor)
    }


    Set<Method> findDeclaringMethods(String declaringClassDoopId) {
        def res = [] as Set

        searchDeclaringMethods(declaringClassDoopId) { String json ->
            res << new Method().fromJSON(json)
        }

        return res
    }

    Set<Klass> findSubTypes(String klass) {
        def res = [] as Set

        searchTypeHierarchy(klass, TYPE_HIERARCHY.SUBTYPES) { String json ->
            def t = new ClassSubtype().fromJSON(json) as ClassSubtype
            if (t.classId == t.subClassId) return
            Klass k = findClass(t.subClassId)
            if (k) res << k
        }

        return res
    }


    Set<Klass> findSuperTypes(String klass) {
        def res = [] as Set

        searchTypeHierarchy(klass, TYPE_HIERARCHY.SUPERTYPES) { String json ->
            def t = new ClassSubtype().fromJSON(json) as ClassSubtype
            if (t.classId == t.subClassId) return
            Klass k = findClass(t.classId)
            if (k) res << k
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchTypeHierarchy(String klass, TYPE_HIERARCHY hier, Closure processor) {
        def filters = [
            _type: ClassSubtype.simpleName
        ]
        if (hier == TYPE_HIERARCHY.SUBTYPES) {
            filters << [classId: klass]
        } else {
            filters << [subClassId: klass]
        }

        search(searchable, createQuery(filters), processor)
    }


    Set<Method> findMethodSubTypes(String method) {
        def res = [] as Set

        searchMethodTypeHierarchy(method, TYPE_HIERARCHY.SUBTYPES) { String json ->
            def t = new MethodSubtype().fromJSON(json)
            Method m = findMethod(t.subMethodId)
            if (m) res << m
        }

        return res
    }


    Set<Method> findMethodSuperTypes(String method) {
        def res = [] as Set

        searchMethodTypeHierarchy(method, TYPE_HIERARCHY.SUPERTYPES) { String json ->
            def t = new MethodSubtype().fromJSON(json)
            Method m = findMethod(t.methodId)
            if (m) res << m
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchMethodTypeHierarchy(String method, TYPE_HIERARCHY hier, Closure processor) {
        def filters = [
            _type: MethodSubtype.simpleName
        ]
        if (hier == TYPE_HIERARCHY.SUBTYPES) {
            filters << [methodId: method]
        } else {
            filters << [subMethodId: method]
        }

        search(searchable, createQuery(filters), processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchVarReturn(String methodId, Closure processor) {
        def query = createQuery(
            _type: VarReturn.simpleName,
            methodId: methodId
        )

        search(searchable, query, processor)
    }


    Set<Field> findFieldsShadowedBy(String field) {
        def res = [] as Set

        searchFieldShadowedBy(field) { String json ->
            def t = new FieldShadowedBy().fromJSON(json)
            Field f = findField(t.shadowFieldId)
            if (f) res << f
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchFieldShadowedBy(String field, Closure processor) {
        def query = createQuery(
            _type: FieldShadowedBy.simpleName,
            fieldId: field
        )

        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchStaticFieldValues(String field, Closure processor) {
        def query = createQuery(
            _type: StaticFieldValues.simpleName,
            fieldId: field
        )

        search(searchable, query, processor)
    }


    Set<Value> findStaticFieldValues(String field) {
        def res = [] as Set

        searchStaticFieldValues(field) { String json ->
            def t = new StaticFieldValues().fromJSON(json)
            res.addAll(findValues(t.valueId))
        }

        return res
    }

    /**
     * Accepts a Closure { String json -> }*/
    long searchInstanceFieldValues(String field, String baseValue, Closure processor) {
        def query = createQuery(
            _type: InstanceFieldValues.simpleName,
            fieldId: field,
            baseValueId: baseValue
        )

        search(searchable, query, processor)
    }


    Set<Value> findInstanceFieldValues(String field, String baseValue) {
        def res = [] as Set

        searchInstanceFieldValues(field, baseValue) { String json ->
            def t = new InstanceFieldValues().fromJSON(json)
            res.addAll(findValues(t.valueId))
        }

        return res
    }

    long searchInvocationZeroTargets(Closure processor) {
        def query = createQuery(_type: InvocationZeroTargets.simpleName)
        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String id, String type, String json -> }*/
    long searchMetrics(Kind kind, long count = 20, Closure processor) {
        def query = createQuery(
            _type: Metrics.simpleName,
            kind: kind.toString()
        )

        def limits = new SearchLimits(start: 0, count: count)
        def orderBy = new OrderBy(field: "counter", ordering: Ordering.DESC)

        searchable.search(query, [orderBy], limits) { id, type, String json ->
            processor?.call(json)
        }

        return limits.hits
    }

    List<Metrics> findMetrics(Kind kind, long count = 20) {
        def res = []

        searchMetrics(kind, count) { String json ->
            res << new Metrics().fromJSON(json)
        }

        res
    }

    long searchEntryPoint(Closure processor) {
        def query = createQuery(_type: EntryPoint.simpleName)
        search(searchable, query, processor)
    }

    /**
     * Accepts a Closure { String id, String type, String json -> }
     */
    long searchByDoopId(String doopId, List<String> elementTypes = [], Closure processor) {

        def filters = [
            doopId: doopId
        ]

        if (elementTypes) {
            filters['_type'] = elementTypes
        }

        def query = createQuery(filters)

        def limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
        searchable.search(query, null, limits, processor)
        return limits.hits
    }

    /**
     * Accepts a Closure { String id, String type, String json -> }
     */
    long searchByDoopIds(Collection<String> doopIds, List<String> elementTypes = [], Closure processor) {
        def filters = [
            doopId: doopIds
        ]

        if (elementTypes) {
            filters['_type'] = elementTypes
        }

        def query = createQuery(filters)

        def limits = new SearchLimits(start: 0, count: doopIds.size())
        searchable.search(query, null, limits, processor)
        return limits.hits	
    }

    String fetchRawJson(String doopId, String elementType) {
        def query = createQuery(
            _type: elementType,
            doopId: doopId
        )

        String result = null
        searchForOne(searchable, query) { String json -> result = json }
        result
    }


    Map<String, Long> groupMethodsByDeclaringClass(int max = MAX_METHODS) {
        def query = createQuery(_type: Method.simpleName)
        return searchable.groupBy(query, 'declaringClassDoopId', max)
    }


    Map<String, Long> groupFieldsByDeclaringClass(int max = MAX_FIELDS) {
        def query = createQuery(_type: Field.simpleName)
        return searchable.groupBy(query, 'declaringClassDoopId', max)
    }

    Map<String, Long> groupPackagesByNumberOfClasses(int max = MAX_PACKAGES) {
        def query = createQuery(_type: Klass.simpleName)
        return searchable.groupBy(query, 'packageName', max)
    }

    // Filter out elements with same position but different doopId (due to init blocks)
    static def filterPositionDuplicates(def elems) {
        elems.unique { a, b -> (a.kind == b.kind && (a.position != null && b.position != null && (a.sourceFileName == b.sourceFileName && a.position == b.position && a.doopId != b.doopId))) ? 0 : 1 }
    }

    /*
     * --------------------------
     * Value helpers (private)
     * --------------------------
     */

    private final Value infer(String id) {
        switch (id) {
        case "<<string-constant>>":
            return merged(id, "merged string constant")
        case "<<string-buffer>>":
            return merged(id, "merged string buffer")
        case "<<string-builder>>":
            return merged(id, "merged string builder")
        case "<<main-thread>>":
            return merged(id, "main thread")
        case "<<main method array content>>":
            return merged(id, "main method array content")
        case "<<main method array>>":
            return merged(id, "main method array")
        case "<<main-thread-group>>":
            return merged(id, "main thread group")
        case "<<system-thread-group>>":
            return merged(id, "system thread group")
        case "<<XL-pt-set>>":
            return merged(id, "too many objects")
        case "<reflective dummy ClassLoader>":
            return merged(id, "reflective dummy ClassLoader")
        case "<reflective dummy URL resource>":
            return merged(id, "reflective dummy URL resource")
        case "<<null pseudo heap>>":
            return null
        case "<<reified class null_type>>":
            return null
        default:
            def parts = id.split('::: ')
            // Generate generic value unless 'id'
            // has one of the following shapes:
            //
            // Length | Value type | Format
            // 2      | mock       | (type, "(Mock)")
            // 3      | taint      | (instruction, type, breadcrumb)
            // 4      | lambda     | (instruction, type, "(Mock)", description)

            if (parts.length == 2 && parts[1] == "(Mock)") {
                def type = parts[0]
                return mockValue(id, type, "external value: ${parts[0]}")
            } else if (parts.length == 3) {
                // the third part is currently a breadcrumb, typically with value "ASSIGN"
                def (String instruction, type, breadcrumb) = parts
                String desc = breadcrumb == "ASSIGN" ? "tainted ${type} value originating in ${instruction}" : ""

                // If last part is a number, then the instruction is an invocation. Otherwise, it is a variable
                def isInvocation = instruction.split("/").last().isNumber()
                def elem = isInvocation ? findInvocation(instruction) : findVariable(instruction)
                return new Value(
                    position: elem.position,
                    sourceFileName: elem.sourceFileName,
                    allocatedTypeDoopId: type,
                    allocatingMethodDoopId: isInvocation ? elem.invokingMethodDoopId : elem.declaringMethodDoopId,
                    description: desc,
                    doopId: id,
                    kind: TAINT)
            } else if (parts.length == 4 && parts[2] == "(Mock)") {
                // Lambda fields: invokedynamic-id, lambda-type, "(Mock)", description
                def (instruction, type, _, desc) = parts

                MethodInvocation m = findInvocation(instruction)
                // This keeps the UI working even if
                // another part of Doop creates 4-field
                // mock values or lambda support is buggy.
                if (m == null) {
                    return mockValue(id, type, desc)
                } else {
                    return new Value(
                        position: m.position,
                        sourceFileName: m.sourceFileName,
                        allocatedTypeDoopId: type,
                        allocatingMethodDoopId: m.invokingMethodDoopId,
                        doopId: id,
                        description: desc,
                        kind: MOCK)
                }
            } else if (id.startsWith("<class ") && id.endsWith(">")) {
                def klass = id[7..-2]
                return findKlass(id, klass)
            } else if (id.startsWith("<<reified class ") && id.endsWith(">>")) {
                def klass = id[16..-3]
                if (klass.startsWith('$Proxy$for$')) {
                    return findProxyClass(id)
                } else {
                    return findKlass(id, klass)
                }
            } else if (id.startsWith(REIFIED_METH_PRE) && id.endsWith(">>")) {
                def method = id[REIFIED_METH_PRE.size()..-3]
                return findMetaMethod(id, method)
            } else if (id.startsWith(REIFIED_CONS_PRE) && id.endsWith(">>")) {
                def method = id[REIFIED_CONS_PRE.size()..-3]
                return findMetaMethod(id, method)
            } else if (id.startsWith("<<reified field ") && id.endsWith(">>")) {
                def field = id[16..-3]
                return findMetaField(id, field)
            } else if (id.startsWith("<reflective ") && id.endsWith(">")) {
                int idx = id.lastIndexOf("/new ")
                if (idx != -1) {
                    def klass = id[(idx + "/new ".size())..-2]
                    return mockValue(id, klass, "reflective ${klass} object")
                } else {
                    return new Value(
                        doopId: id,
                        description: "unrecognized reflective value $id",
                        position: null,
                        kind: UNKNOWN)
                }
            } else if (id.startsWith(STRING_CONST_PRE)) {
                def desc = id[1..-2]
                return new Value(doopId: id, description: desc, position: null, kind: NORMAL_ALLOCATION)
            } else if (id.startsWith("<special object for missing ")) {
                return merged(id, id)
            } else if (id.startsWith(ANDROID_COMP_PRE) && id.endsWith(">")) {
                def klass = id[ANDROID_COMP_PRE.size()..-2]
                return mockValue(id, klass, "Android component object ${klass}")
            } else if (id.startsWith(MOCK_LISTENER_PRE) && id.endsWith(">")) {
                def desc = id[1..-2]
                def klass = id[MOCK_LISTENER_PRE.size()..-2]
                return mockValue(id, klass, desc)
            } else if (id.startsWith(MOCK_NATIVE_PRE) && id.endsWith(">")) {
                def desc = id[1..-2]
                def idParts = id.split(' ')
                def klass = idParts[1]
                return mockValue(id, klass, desc)
            } else if (id.startsWith("<computed method handle") && id.endsWith(">")) {
                def desc = id[1..-2]
                return mockValue(id, "java.lang.invoke.MethodHandle", desc)
            } else if (id.startsWith(ANDROID_LIB_PRE) && id.endsWith(">")) {
                def klass = id[ANDROID_LIB_PRE.size()..-2]
                return mockValue(id, klass, "Android library object ${klass}")
            } else if (id.startsWith(LAYOUT_CONTROL_PRE)) {
                def klass = id[(id.lastIndexOf(' ') + 1)..-2]
                return mockValue(id, klass, "UI component (${klass})")
            } else if (id.startsWith(PRIMITIVE_VAL_PRE)) {
                String numConst = id.substring(PRIMITIVE_VAL_PRE.size())
                return new Value(doopId: id,
                    description: "constant ${numConst}",
                    position: null,
                    kind: PRIMITIVE_VALUE)
            } else {
                Value gValue = guessUnknownValue(id)
                if (gValue != null) {
                    return gValue
                } else {
                    return new Value(
                        doopId: id,
                        description: "$id",
                        position: null,
                        kind: UNKNOWN)
                }
            }
        }
    }

    private final Value findKlass(def metaId, def klass) {
        def i = klass.indexOf("[") // remove array part
        klass = (i != -1 ? klass[0..(i - 1)] : klass)
        if (i != -1) {
            // If the type undergoes boxing (e.g. id = "float[]"), fix
            // it to point to its boxed type.
            klass = boxConversion.get(klass) ?: klass
        }
        Klass k = findClass(klass)
        return handleMetaObject(metaId, k, "class")
    }


    private final Value findProxyClass(def metaId) {
        def proxiedInterface = metaId[27..-3]
        Klass intf = findClass(proxiedInterface)
        Value v = new Value(doopId: metaId,
            description: "dynamic proxy class for $proxiedInterface",
            position: null,
            kind: intf ? METAOBJECT : UNKNOWN)
        if (intf) {
            v.position = intf.position
            v.sourceFileName = intf.sourceFileName
        }
        return v
    }


    private final Value findMetaMethod(def metaId, def method) {
        Method m = findMethod(method)
        return handleMetaObject(metaId, m, "method")
    }

    private final findMetaField(def metaId, def field) {
        Field f = findField(field)
        return handleMetaObject(metaId, f, "field")
    }

    private final Value mockValue(def id, def type, def description) {
        // Initially construct a metaobject of the type and then
        // tweak it to become a mock value having that type.
        def value = findKlass(id, type)
        value.allocatedTypeDoopId = type
        value.description = description
        value.position = null
        value.kind = MOCK
        return value
    }

    // Sometimes an allocation points to an instruction in
    // unreachable code (such as merged exception objects, see issue
    // CLUE-174) or in native code. Thus, if the id contains "/new
    // T/" and T is reachable, we show a better description.
    private final Value guessUnknownValue(String id) {
        int idx1 = id.lastIndexOf("/new ")
        if (idx1 == -1) {
            return null
        }

        int idx2 = id.lastIndexOf("/")
        // To extract T, we asume the id is "method/new T/i" or "method/new T".
        String typeId = null;
        if ((idx2 != -1) && (idx1 + 5 < idx2)) {
            typeId = id.substring(idx1 + 5, idx2)
        } else if (idx2 == idx1) {
            typeId = id.substring(idx1 + 5)
        }
        if (typeId != null) {
            String methodId = id.substring(0, idx1)
            Method m = findMethod(methodId)
            if (m != null) {
                String desc = (m?.isNative = true) ? " in native method ${methodId}" : ""
                return new Value(position: m.position,
                    sourceFileName: m.sourceFileName,
                    doopId: id,
                    description: "${typeId} object${desc}",
                    allocatedTypeDoopId: typeId,
                    kind: MOCK)
            } else {
                def value = findKlass(id, typeId)
                if (value.kind != UNKNOWN) {
                    value.description = "${typeId} object"
                    value.position = null
                    value.kind = MERGED_ALLOCATION
                    return value
                }
            }
        }
        return null
    }

    private static final Value handleMetaObject(def metaId, def obj, def kind) {
        if (obj) return new Value(
            position: obj.position,
            sourceFileName: obj.sourceFileName,
            doopId: metaId,
            description: "$kind metaobject ($metaId}",
            kind: METAOBJECT)
        else return new Value(
            doopId: metaId,
            description: "unrecognized $kind metaobject $metaId",
            position: null,
            kind: UNKNOWN)
    }


    private static final Value merged(def id, def description) {
        new Value(
            doopId: id,
            description: description,
            position: null,
            kind: MERGED_ALLOCATION
        )
    }

    /**
     * Search helpers
     */
    static final long search(Searchable searchable, BooleanQuery query, Closure processor) {
        return search(searchable, query, new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT), processor)
    }


    static final long searchForOne(Searchable searchable, BooleanQuery query, Closure processor) {
        return search(searchable, query, new SearchLimits(start: 0, count: 1), processor)
    }

    static final long search(Searchable searchable, BooleanQuery query, SearchLimits limits, Closure processor) {
        def wrapper = { String id, String type, String json -> processor?.call(json) }
        searchable.search(query, null, limits, wrapper)
        return limits.hits
    }

    static final long searchForIds(Searchable searchable, BooleanQuery query, Closure processor) {
        return searchForIds(searchable, query, new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT), processor)
    }

    static final long searchForIds(Searchable searchable, BooleanQuery query, SearchLimits limits, Closure processor) {
        def wrapper = { String id, String type, String json -> processor?.call(id) }
        searchable.search(query, null, limits, wrapper)
        return limits.hits
    }

    /**
     * Reusable search methods
     */
    static SearchLimits processProjects(Searchable searchable, String userId, String text, int start, int count, List<OrderBy> orderBy, Closure processor) {
        if (!userId) throw new RuntimeException("No userId in search context")

        BooleanQuery q = BooleanQueryBuilder.build {
            AND(
                EQ('_type', Project.simpleName),
                OR(
                    EQ('owner', userId),
                    EQ('members', userId)
                )
				
            )
        }

        def limits = new SearchLimits(start: start, count: count)
        def txt = text?.trim()
        txt ? searchable.search(TextualSearch.newFullTextSearch(txt), q, orderBy, limits, processor)
            : searchable.search(q, orderBy, limits, processor)

        return limits
    }


    static long countInputBundlesOfProject(Searchable searchable, String projectId) {
		
        if (!projectId) throw new RuntimeException("No projectId in search context")								

        BooleanQuery q = BooleanQueryBuilder.build {
            AND(
                EQ('_type', DoopInputBundle.simpleName),
                EQ('projectId', projectId)
            )
        }

        return searchable.count(q)
    }

    static SearchLimits processInputBundlesOfProject(Searchable searchable, String projectId, int start, int count, List<OrderBy> orderBy, Closure processor) {
        if (!projectId) throw new RuntimeException("No project in search context")

        BooleanQuery q = BooleanQueryBuilder.build {
            AND(
                EQ('_type', DoopInputBundle.simpleName),
                EQ('projectId', projectId)						
            )
        }

        def limits = new SearchLimits(start: start, count: count)				
        searchable.search(q, orderBy, limits, processor)

        return limits
    }
    
    static class DirectivesSearchParams {

        TextualSearch textualSearch
        Set<String> configSet //clue files
        Set<String> typeOfDirective //keep, remove
        Set<String> ruleId // the id of the bundle config rule
        Set<String> originType //user, analysis
        Set<String> origin //user name or analysis name
        Set<String> packageName //names of packages
        Set<String> className //names of classes 
        Set<String> methodName //names of methods
        Set<String> sourceFileName //source or jimple file names
        boolean effectiveDirectives = false
        
        Map<String, Object> asSearchFilters() {
            return [
                _type          : effectiveDirectives ? EffectiveMethodDirective.simpleName : KeepRemoveMethodDirective.simpleName,
                configSet      : configSet,
                typeOfDirective: typeOfDirective,
                ruleId         : ruleId,
                origin         : origin,
                originType     : originType,
                packageName    : packageName,
                className      : className,
                methodName     : methodName,
                sourceFileName : sourceFileName
            ].findAll { it.value }		                
        }

        DirectivesSearchParams createCopyWithNullifiedField(String field) {
            def copy = new DirectivesSearchParams(
                textualSearch  : textualSearch,
                configSet      : configSet,
                typeOfDirective: typeOfDirective,
                ruleId         : ruleId,
                origin         : origin,
                originType     : originType,
                packageName    : packageName,
                className      : className,
                methodName     : methodName,
                sourceFileName : sourceFileName,
                effectiveDirectives: effectiveDirectives
            )
            copy[(field)] = null
            return copy
        }
    }

    static class RulesSearchParams {
        TextualSearch textualSearch //search in origin and comment
        Set<String> configSet //clue files
        Set<String> ruleType //keep, remove
        Set<String> origin
        Set<String> originType
        Set<String> packageName
        Set<String> className

        Map<String, Object> asSearchFilters() {
            return [
                _type      : PersistentBundleConfigurationRule.simpleName,
                configSet  : configSet,
                ruleType   : ruleType,
                origin     : origin,
                originType : originType,
                packageName: packageName,
                className  : className
            ].findAll { it.value }
        }

        RulesSearchParams createCopyWithNullifiedField(String field) {
            def copy = new RulesSearchParams(
                textualSearch : textualSearch,
                configSet     : configSet,
                ruleType      : ruleType,
                origin        : origin,
                originType    : originType,
                packageName   : packageName,
                className     : className
            )
            copy[(field)] = null
            return copy
        }
    }
}
