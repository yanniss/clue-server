package org.clyze.server.web.persistent.store

class TextualSearch {

    private final TextualSearchType type
    private final String textExpr
    private final String[] onFields

    TextualSearch(TextualSearchType type, String textExpr, String... onFields) {
        this.type     = type
        this.textExpr = textExpr
        this.onFields = onFields
    }

    TextualSearchType getType() { return type }

    String getTextExpr() { return textExpr }

    String[] getFields() { return onFields }

    static TextualSearch newFullTextSearch(String textExpr, String... onFields) {
        if (!textExpr) throw new RuntimeException("No text for free text search")
        //if (!onFields) throw new RuntimeException("No fields for free text search")
        //the text will be searched via all fields
        return new TextualSearch(TextualSearchType.FULL_TEXT, textExpr, onFields)
    }

    static TextualSearch newWildcardSearch(String textExpr, String... onFields) {
        if (!textExpr) throw new RuntimeException("No text for wildcard search")
        if (!onFields) throw new RuntimeException("No fields for wildcard search")
        return new TextualSearch(TextualSearchType.WILDCARD, textExpr, onFields)
    }

    static TextualSearch newRegexSearch(String textExpr, String... onFields) {
        if (!textExpr) throw new RuntimeException("No text for regex search")
        if (!onFields) throw new RuntimeException("No fields for regex search")
        return new TextualSearch(TextualSearchType.REGEX, textExpr, onFields)
    }
}
