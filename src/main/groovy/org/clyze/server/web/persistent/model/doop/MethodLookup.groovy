package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class MethodLookup extends SymbolRelation {

	String nameWithDescriptor
	String typeId
	String methodId
	boolean isSingleDispatch

	MethodLookup(String anId, String nameWithDescriptor, String typeId, String methodId, boolean isSingleDispatch) {
		super(anId)
		this.nameWithDescriptor = nameWithDescriptor
		this.typeId = typeId
		this.methodId = methodId
		this.isSingleDispatch = isSingleDispatch
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.nameWithDescriptor = this.nameWithDescriptor
		map.typeId = this.typeId
		map.methodId = this.methodId
		map.isSingleDispatch = this.isSingleDispatch
	}

	protected void loadFrom(Map<String, Object> map) {
		super.loadFrom(map)
		this.nameWithDescriptor = map.nameWithDescriptor
		this.typeId = map.typeId
		this.methodId = map.methodId
		this.isSingleDispatch = map.isSingleDispatch
	}
}
