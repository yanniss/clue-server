package org.clyze.server.web.persistent.store

/**
 * A searchable data source.
 */
interface Searchable {

    void search(TextualSearch textualSearch,
                BooleanQuery filters,
                List<OrderBy> orderByList,
                SearchLimits limits,
                Closure resultProcessor)

    void search(BooleanQuery filters,
                List<OrderBy> orderByList,
                SearchLimits limits,
                Closure resultProcessor)

    long count(BooleanQuery filters)

    long count(TextualSearch textualSearch, BooleanQuery filters)

    void scan(BooleanQuery filters, List<OrderBy> orderByList, Closure resultProcessor)

    void scan(TextualSearch textualSearch, BooleanQuery filters, List<OrderBy> orderByList, Closure resultProcessor)

    Map<String, Long> groupBy(BooleanQuery filters, String field, int maxResults)

    Map<String, Long> groupBy(TextualSearch textualSearch, BooleanQuery filters, String field, int maxResults)

}