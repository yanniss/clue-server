package org.clyze.server.web.persistent

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.Element
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Bulk
import org.clyze.server.web.persistent.store.DataStore

import java.util.concurrent.ExecutorService
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class BulkProcessor {

	private static Log log = LogFactory.getLog(getClass())
	private final ExecutorService executor = Config.instance.postProcessExecutor
	private final int bulkSize
	private final DataStore store
	private Bulk currentBulk
	private int bulkElementsCount = 0
	private final List<Future> futures = []	

	BulkProcessor(int bulkSize, DataStore store) {
		this.bulkSize = bulkSize
		this.store = store
		this.currentBulk = this.store.createBulk()
	}

	DataStore getDataStore() {
		return store
	}

	synchronized void scheduleSave(Element el) {
		if (bulkElementsCount == bulkSize) {
			submitCurrentBulkAndResetCounters()
		}

		currentBulk.saveNew(el)
		bulkElementsCount++		
	}

	synchronized void scheduleSave(String type, String id, String json) {
		if (bulkElementsCount == bulkSize) {
			submitCurrentBulkAndResetCounters()
		}

		currentBulk.saveNew(type, id, json)
		bulkElementsCount++		
	}


	synchronized void scheduleDelete(String type, String id) {
		if (bulkElementsCount == bulkSize) {
			submitCurrentBulkAndResetCounters()
		}

		currentBulk.delete(type, id)
		bulkElementsCount++		
	}


	synchronized void finishUp() {
		//def now = System.currentTimeMillis()
		submitCurrentBulk()

		futures.eachWithIndex { Future future, int index ->
			future.get(Config.instance.timeoutInMinutes, TimeUnit.MINUTES)
		}
		//log.debug("${Thread.currentThread().getName()} - Finished up bulk in ${System.currentTimeMillis() - now} ms")

		//Always refresh the datastore after a bulk
		//now = System.currentTimeMillis()
		store.refresh()
		//log.debug("${Thread.currentThread().getName()} - Refreshed datastore in ${System.currentTimeMillis() - now} ms")
	}

	protected void submitCurrentBulkAndResetCounters() {
		//log.debug("${Thread.currentThread().getName()} - Submitting bulk ${futures.size()}")
		submitCurrentBulk()
		currentBulk = store.createBulk()
		bulkElementsCount = 0
	}

	protected void submitCurrentBulk() {
		Bulk bulk = currentBulk		
		//int index = futures.size()
		Future future = executor.submit(new Runnable() {
			@Override
			void run() {
				//def now = System.currentTimeMillis()
				bulk.execute()
				//log.debug("${Thread.currentThread().getName()} - Executed bulk $index in ${System.currentTimeMillis() - now} ms")
			}
		})
		futures.add future
	}
}
