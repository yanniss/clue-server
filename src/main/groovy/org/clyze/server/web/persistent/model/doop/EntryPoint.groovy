package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class EntryPoint extends SymbolRelation {

	String methodId

	EntryPoint(String anId, String methodId) {
		super(anId)
		this.methodId = methodId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.methodId = this.methodId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.methodId = map.methodId
	}
}
