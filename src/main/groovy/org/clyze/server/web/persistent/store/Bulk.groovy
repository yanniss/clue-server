package org.clyze.server.web.persistent.store

import org.clyze.persistent.model.Item

interface Bulk {

	void delete(Item item)

	void delete(String itemType, String itemId)

	void saveNew(Item item)

	void saveNew(String itemType, String itemId, String json)

	void execute()
}
