package org.clyze.server.web.persistent.store.elastic

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.Item
import org.clyze.server.web.persistent.SuggestionAware
import org.clyze.server.web.persistent.store.Bulk
import org.elasticsearch.action.bulk.BulkItemResponse
import org.elasticsearch.action.bulk.BulkRequestBuilder
import org.elasticsearch.action.bulk.BulkResponse
import org.elasticsearch.client.Client

//@CompileStatic
class ElasticBulk implements Bulk {

	private final Log logger = LogFactory.getLog(getClass()) 

	private final Client client
	private final String indexName
	private final BulkRequestBuilder bulk

	ElasticBulk(Client client, String indexName, BulkRequestBuilder bulk) {
		this.client = client
		this.indexName = indexName
		this.bulk = bulk
	}

	@Override
	void delete(Item item) {
		delete(item.class.getSimpleName(), item.getId())
	}

	@Override
	void delete(String itemType, String itemId) {
		bulk.add(client.prepareDelete(indexName, itemType, itemId))
	}

	@Override
	void saveNew(Item item) {
		//use (SuggestionAware) {
			saveNew(item.class.simpleName, item.getId(), item.toJSON())
		//}
	}

	@Override
	void saveNew(String itemType, String itemId, String json) {
		//itemId can be null
		bulk.add(client.prepareIndex(indexName, itemType, itemId).setSource(json))
	}

	@Override
	void execute() {
		if (bulk.numberOfActions() > 0) {
			//BulkResponse bulkResponse = bulk.setRefresh(true).execute().actionGet()
			BulkResponse bulkResponse = bulk.execute().actionGet()
			if (bulkResponse.hasFailures()) {
				bulkResponse.iterator().each { BulkItemResponse bir ->
					if (bir.isFailed()) {
						def failure = bir.getFailure()
						logger.error("Bulk failure on " + 
									 "${failure.getType()} ${failure.getId()} with status: ${failure.getStatus()}, " +
									 "isAborted: ${failure.isAborted()}, message: ${failure.getMessage()}",
									 failure.getCause())
					}
				}
				throw new RuntimeException("Bulk execution failed")
			}
		}
	}
}
