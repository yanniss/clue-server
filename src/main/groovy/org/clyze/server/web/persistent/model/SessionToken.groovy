package org.clyze.server.web.persistent.model

import org.clyze.persistent.model.ItemImpl

class SessionToken extends ItemImpl {
	
	String userId
	long expires

	void saveTo(Map<String, Object> map) {		
		map.userId  = this.userId
		map.expires = this.expires
	}

	void loadFrom(Map<String, Object> map){		
		this.userId  = map.userId
		this.expires = map.expires
	}
}
