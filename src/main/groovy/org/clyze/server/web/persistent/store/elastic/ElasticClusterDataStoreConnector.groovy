package org.clyze.server.web.persistent.store.elastic

import org.clyze.server.web.persistent.store.DataStoreConnector
import org.clyze.server.web.persistent.store.DataStore

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

import org.elasticsearch.client.Client
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.transport.client.PreBuiltTransportClient
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsAction
import org.elasticsearch.action.admin.indices.exists.types.TypesExistsRequestBuilder

import com.google.common.cache.*

import groovy.transform.CompileStatic

@CompileStatic
@Singleton
class ElasticClusterDataStoreConnector implements DataStoreConnector {

	private static Log logger = LogFactory.getLog(this)

	static final int    DATASTORE_CACHE_SIZE           = 100

	static final String DEFAULT_INDEX_NAME             = "doop"
    static final int    DEFAULT_NUMBER_OF_SHARDS       = 1
    static final int    DEFAULT_NUMBER_OF_REPLICAS     = 0    
    static final String DEFAULT_REFRESH_INTERVAL       = '1s'
	static final String DEFAULT_CLUSTER_NAME           = "elasticsearch"
	static final String DEFAULT_HOST                   = "127.0.0.1"
	static final String DEFAULT_PORT                   = "9300"

	protected LoadingCache<String, ElasticDataStore> cache

	protected Client client
	protected String clusterName
	protected String host
	protected int port
	protected int numberOfShards
    protected int numberOfReplicas
    //Hack: publicly accessible temporarily
    String refreshInterval

	void configure(Properties props) {
		//indexName = props.getProperty("indexName", DEFAULT_INDEX_NAME)
		clusterName = props.getProperty("clusterName", DEFAULT_CLUSTER_NAME)
		host = props.getProperty("host", DEFAULT_HOST)
		port = props.getProperty("port", DEFAULT_PORT) as Integer
		numberOfShards = props.getProperty("numberOfShards", DEFAULT_NUMBER_OF_SHARDS as String) as Integer
		numberOfReplicas = props.getProperty("numberOfReplicas", DEFAULT_NUMBER_OF_REPLICAS as String) as Integer
		refreshInterval = props.getProperty("refreshInterval", DEFAULT_REFRESH_INTERVAL)

		cache = CacheBuilder.newBuilder().
				maximumSize(DATASTORE_CACHE_SIZE).
				removalListener(new RemovalListener<String, ElasticDataStore>() {
					@Override
					void onRemoval(RemovalNotification<String, ElasticDataStore> n) {
						logger.warn("ElasticDataStore removed from cache: ${n.key}: ${n.cause}")
					}
				}).
				build(new CacheLoader<String, ElasticDataStore>() {
					@Override
					ElasticDataStore load(String key) throws RuntimeException {
						logger.debug("Creating ElasticDataStore for $key")
						return new ElasticDataStore(ElasticClusterDataStoreConnector.this, key)
					}
				})
	}

	@Override
	void connect() throws RuntimeException {
		logger.debug "Connecting to cluster $clusterName @ $host:$port"
		Settings settings = Settings.builder().put("cluster.name", clusterName).build()
		client = new PreBuiltTransportClient(settings)
		((TransportClient) client).addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), port))
		//wait for the cluster to reach its yellow status (-> shards OK, replicas NOT)
		client.admin().cluster().prepareHealth().setWaitForYellowStatus().execute().actionGet()		
	}

	@Override
	void disconnect() throws RuntimeException {
		logger.debug "Disconnecting from cluster $clusterName @ $host:$port"
		client?.close()
	}

	protected boolean indexExists(String indexName) {
        return client.admin().indices().prepareExists(indexName).execute().actionGet().isExists()
    }

    protected void createIndex(String indexName) {
        client.admin().indices().prepareCreate(indexName).setSettings([
                refresh_interval      : refreshInterval,
                number_of_shards      : numberOfShards,
                number_of_replicas    : numberOfReplicas,
                'index.mapper.dynamic': false,
                analysis: [
                        analyzer: [
                                'default': [
                                        tokenizer: 'standard',
                                        type     : 'custom',
                                        filter   : ['snowball', 'prefixFilter']
                                ],
                                'case_insensitive_keyword': [
                                        tokenizer: 'keyword',
                                        type     : 'custom',
                                        filter   : ['lowercase']
                                ]
                        ],
                        filter  : [
                                prefixFilter: [
                                        type    : 'edgeNGram',
                                        max_gram: 20,
                                        min_gram: 3
                                ]
                        ]
                ]
        ]).execute().actionGet()

        client.admin().cluster().prepareHealth(indexName).setWaitForGreenStatus().execute().actionGet()
    }

    protected void setMappings(String indexName) {
    	//TODO: Use different mappings for the accounts and the user_* indexes
        Mappings.LIST.each {
            addMapping(indexName, it.cl.simpleName, [
                    properties: it.properties,
                    _all:[
                            enabled:it.allFieldEnabled
                    ]
            ])
        }
    }

    protected void addMapping(String indexName, String typeId, Map fields) {
        TypesExistsRequestBuilder builder = TypesExistsAction.INSTANCE.
                newRequestBuilder(client).
                setIndices(indexName).
                setTypes(typeId)
        if (!builder.execute().actionGet().isExists()) {
            logger.debug "Adding mapping to $indexName: $typeId -> $fields"
            client.admin().indices().preparePutMapping(indexName).setType(typeId).setSource(fields).execute().actionGet()
        }
    }

    protected void initIndex(String indexName) {
        createIndex(indexName)
        setMappings(indexName)
    }

	@Override
	void createStoreIfNotExists(String storeId) throws RuntimeException {
		if (!indexExists(storeId)) {
			createStore(storeId)
		}
	}

	@Override
	void createStore(String storeId) throws RuntimeException {		
		logger.debug "Creating elastic index $storeId"
		initIndex(storeId)			
	}

	@Override
	void dropStoreIfExists(String storeId) throws RuntimeException {
		if (indexExists(storeId)) {			
			dropStore(storeId)
	    }
	}

	@Override
	void dropStore(String storeId) throws RuntimeException {
		logger.debug "Dropping elastic index $storeId"
        client.admin().indices().prepareDelete(storeId).execute().actionGet()
	}

	@Override
	DataStore connectTo(String storeId) throws RuntimeException {
		logger.debug "Conntecting to elastic index $storeId"
		return cache.getUnchecked(storeId)
	}
}