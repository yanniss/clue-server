package org.clyze.server.web.persistent.model

import groovy.transform.InheritConstructors
import org.clyze.persistent.model.Element

@InheritConstructors
abstract class SymbolRelation extends Element {

	//These elements belong to an analysis (the rootElemId holds the analysis id)
	SymbolRelation(String anId) {
		this.rootElemId = anId				
	}
}
