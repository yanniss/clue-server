package org.clyze.server.web.persistent

import com.google.gson.stream.JsonWriter
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.server.web.persistent.store.Exporter

class DefaultExporter implements Exporter {

	protected Log logger = LogFactory.getLog(getClass())

	protected final File outputDirectory
	protected JsonFile jsonFile
	protected long counter = 0
	protected long startedAt = 0

	DefaultExporter(File outputDirectory) {
		this.outputDirectory = outputDirectory
	}

	void start() {
		startedAt = System.currentTimeMillis()
		jsonFile = newJsonFile(outputDirectory, "data")
		jsonFile.start()
	}

	@Override
	void export(String id, String type, String json) {
		jsonFile.write(id, type, json)
		counter++
	}

	void end() {
		long duration = ((System.currentTimeMillis() - startedAt) / 1000).longValue()
		logger.debug("Exported $counter elements in $duration secs")
		jsonFile.end()
	}


	static JsonFile newJsonFile(File outDir, String name) {
		Writer fileWriter = new File(outDir, name + ".json").newWriter("UTF-8")
		return new JsonFile(new JsonWriter(fileWriter))
	}


	static class JsonFile {

		private final JsonWriter writer

		JsonFile(JsonWriter jsonWriter) {
			writer = jsonWriter
		}

		void start() {
			writer.beginArray()
		}

		void write(String id, String type, String json) {
			writer.beginObject()
			writer.name("id")
			writer.value(id)
			writer.name("type")
			writer.value(type)
			writer.name("src")
			writer.jsonValue(json)
			writer.endObject()
		}

		void end() {
			writer.endArray()
			writer.close()
		}
	}
}
