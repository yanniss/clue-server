package org.clyze.server.web.persistent.store.elastic

import org.clyze.persistent.model.doop.*
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.model.SessionToken
import org.clyze.server.web.persistent.model.User
import org.clyze.server.web.persistent.model.doop.*
import org.clyze.server.web.persistent.store.Dates

interface Mappings {

	Map LONG = [type: "long"]
	Map BOOLEAN = [type: "boolean"]
	Map STRING = [type: "string"]
	Map STRING_NOT_ANALYZED = [
			type : "string",
			index: "not_analyzed"
	]
	Map OBJECT = [type: "object"]
	Map DATE = [
			type  : "date",
			format: Dates.DATE_FORMAT
	]

	Map COMMON = [
			id: [
					type : "string",
					index: "not_analyzed"
			]
	]

	Map ELEMENT = [
			rootElemId: STRING_NOT_ANALYZED
	] << COMMON

	Map POSITION = [
			position: [
					type      : "object",
					properties: [
							startLine  : LONG,
							startColumn: LONG,
							endLine    : LONG,
							endColumn  : LONG
					]
			]
	]

	Map SYMBOL = [
			sourceFileName: STRING_NOT_ANALYZED
	] << ELEMENT + POSITION

	Map ANNOTATEABLE = [
	        annotationTypes: STRING_NOT_ANALYZED
	]

	Map DOOP_INPUT_BUNDLE = [
			userId           : STRING_NOT_ANALYZED,
			projectId        : STRING_NOT_ANALYZED,
			displayName      : STRING_NOT_ANALYZED,
			baseDir          : STRING_NOT_ANALYZED,
			isPublic         : BOOLEAN,
			created          : DATE,
			hasSrcMetadata   : BOOLEAN,
			sourceClasses    : STRING_NOT_ANALYZED,
			options          : OBJECT,
			appArtifacts     : OBJECT,
			depArtifacts     : OBJECT,
			platformArtifacts: OBJECT,
			configSets       : OBJECT,
			analysesCnt      : LONG,
			optimizationsCnt : LONG
	] << COMMON

	Map BUNDLE_CONFIGURATION = [
			bundleId    : STRING_NOT_ANALYZED,
			nameOfSet   : STRING_NOT_ANALYZED,
			created     : DATE,
			modified    : DATE,
			enabledSteps: STRING_NOT_ANALYZED,
			ruleIndex   : LONG,
			dirty       : BOOLEAN,
			currentAnalysisId: STRING_NOT_ANALYZED
	] << COMMON

	Map BUNDLE_CONFIGURATION_RULE = [
			checksum       : STRING_NOT_ANALYZED,
			configSet      : STRING_NOT_ANALYZED,
			bundleId       : STRING_NOT_ANALYZED,
			ruleBody       : STRING_NOT_ANALYZED,
			ruleType       : STRING_NOT_ANALYZED,
			comment        : STRING,
			originType     : STRING_NOT_ANALYZED,
			origin         : STRING_NOT_ANALYZED,
			packageName    : STRING_NOT_ANALYZED,
			className      : STRING_NOT_ANALYZED,
			matchingMethods: LONG,
			userIndex      : LONG

	] << ELEMENT

	Map SERVER_SIDE_ANALYSIS = [
			displayName     : STRING_NOT_ANALYZED,
			created         : DATE,
			bundleId        : STRING_NOT_ANALYZED,
			userId          : STRING_NOT_ANALYZED,
			lowercase_id    : STRING_NOT_ANALYZED,
			anName          : STRING,
			anOptions       : OBJECT,
			anInputs        : STRING_NOT_ANALYZED,
			anJars          : STRING_NOT_ANALYZED,
			state           : STRING_NOT_ANALYZED,
			errorMessage    : STRING,
			errorTrace      : STRING,
			metrics         : OBJECT,
			family          : STRING_NOT_ANALYZED,
			configuration   : OBJECT,
			durationOfPhases: OBJECT,
			allText         : STRING
	] << COMMON

	Map SUGGESTER = [
			type    : "completion",
			contexts: [
					[name: "type", type: "category", path: "_type"],
					[name: "rootElemId", type: "category", path: "rootElemId"],
					[name: "typeAndRootElem", type: "category"] //ES5 workaround for contexts being ORed and not ANDed
			]
	]

	Map METHOD_PROPERTIES = [
			name                : STRING,
			returnType          : STRING_NOT_ANALYZED,
			params              : STRING_NOT_ANALYZED, //Could this be of any use for freeflow text queries?
			paramTypes          : STRING_NOT_ANALYZED,
			isStatic            : BOOLEAN,
			isInterface         : BOOLEAN,
			isAbstract          : BOOLEAN,
			isNative            : BOOLEAN,
			isSynchronized      : BOOLEAN,
			isFinal             : BOOLEAN,
			isSynthetic         : BOOLEAN,
			isPublic            : BOOLEAN,
			isProtected         : BOOLEAN,
			isPrivate           : BOOLEAN,
			declaringClassDoopId: STRING_NOT_ANALYZED,
			totalInvocations    : LONG,
			totalAllocations    : LONG,
			doopId              : STRING_NOT_ANALYZED,
			suggestName         : SUGGESTER
	] << SYMBOL + ANNOTATEABLE

	Map KEEP_REMOVE_METHOD_DIRECTIVE = [

			doopId               : STRING_NOT_ANALYZED,

			methodName           : STRING_NOT_ANALYZED,
			returnType           : STRING_NOT_ANALYZED,
			params               : STRING_NOT_ANALYZED,
			paramTypes           : STRING_NOT_ANALYZED,
			isStatic             : BOOLEAN,
			isInterface          : BOOLEAN,
			isAbstract           : BOOLEAN,
			isNative             : BOOLEAN,
			declaringClassDoopId : STRING_NOT_ANALYZED,

			packageName          : STRING_NOT_ANALYZED,
			className            : STRING_NOT_ANALYZED,
			declaringSymbolDoopId: STRING_NOT_ANALYZED,

			artifactName         : STRING_NOT_ANALYZED,

			originType           : STRING_NOT_ANALYZED,
			origin               : STRING_NOT_ANALYZED,
			configSet            : STRING_NOT_ANALYZED,
			typeOfDirective      : STRING_NOT_ANALYZED,
			ruleId               : STRING_NOT_ANALYZED
	] << SYMBOL

	List<Mapping> LIST = [
			new Mapping(
					cl: User,
					properties: [
							name: STRING,
							pass: STRING_NOT_ANALYZED,
							salt: STRING_NOT_ANALYZED
					] << COMMON
			),
			new Mapping(
					cl: SessionToken,
					properties: [
							userId : STRING_NOT_ANALYZED,
							expires: LONG
					] << COMMON
			),
			new Mapping(
					cl: Project,
					properties: [
							name            : STRING,
							created         : DATE,
							owner           : STRING_NOT_ANALYZED,
							members         : STRING_NOT_ANALYZED,
							bundlesCnt      : LONG
					] << COMMON
			),
			new Mapping(
					cl: DoopInputBundle,
					properties: DOOP_INPUT_BUNDLE
			),
			new Mapping(
					cl: ServerSideAnalysis,
					properties: SERVER_SIDE_ANALYSIS
			),
			new Mapping(
					cl: BundleConfiguration,
					properties: BUNDLE_CONFIGURATION
			),
			new Mapping(
					cl: PersistentBundleConfigurationRule,
					properties: BUNDLE_CONFIGURATION_RULE
			),
			new Mapping(
					cl: KeepRemoveMethodDirective,
					properties: KEEP_REMOVE_METHOD_DIRECTIVE
			),
			new Mapping(
					cl: EffectiveMethodDirective,
					properties: KEEP_REMOVE_METHOD_DIRECTIVE
			),
			//Symbol mappings			
			new Mapping(
					cl: Klass,
					properties: [
							name        : STRING,
							packageName : STRING_NOT_ANALYZED,
							artifactName: STRING_NOT_ANALYZED,
							isInterface : BOOLEAN,
							isEnum      : BOOLEAN,
							isStatic    : BOOLEAN,
							isInner     : BOOLEAN,
							isAnonymous : BOOLEAN,
							isAbstract  : BOOLEAN,
							isFinal     : BOOLEAN,
							isPublic    : BOOLEAN,
							isProtected : BOOLEAN,
							isPrivate   : BOOLEAN,
							sizeInBytes : LONG,
							superTypes  : STRING_NOT_ANALYZED,
							suggestName : SUGGESTER
					] << SYMBOL + ANNOTATEABLE
			),
			new Mapping(
					cl: Field,
					properties: [
							name                : STRING,
							doopId              : STRING_NOT_ANALYZED,
							type                : STRING_NOT_ANALYZED,
							declaringClassDoopId: STRING_NOT_ANALYZED,
							isStatic            : BOOLEAN,
							suggestName         : SUGGESTER
					] << SYMBOL + ANNOTATEABLE
			),
			new Mapping(
					cl: HeapAllocation,
					properties: [
							doopId                : STRING_NOT_ANALYZED,
							allocatedTypeDoopId   : STRING_NOT_ANALYZED,
							allocatingMethodDoopId: STRING_NOT_ANALYZED,
							inIIB                 : BOOLEAN,
							isArray               : BOOLEAN
					] << SYMBOL
			),
			new Mapping(
					cl: Method,
					properties: METHOD_PROPERTIES
			),
			new Mapping(
					cl: MethodInvocation,
					properties: [
							name                : STRING,
							doopId              : STRING_NOT_ANALYZED,
							invokingMethodDoopId: STRING_NOT_ANALYZED,
							paramTypes          : STRING_NOT_ANALYZED
					] << SYMBOL
			),
			new Mapping(
					cl: Usage,
					properties: [
							doopId   : STRING_NOT_ANALYZED,
							usageKind: STRING_NOT_ANALYZED
					] << SYMBOL
			),
			new Mapping(
					cl: Variable,
					properties: [
							name                 : STRING,
							doopId               : STRING_NOT_ANALYZED,
							type                 : STRING_NOT_ANALYZED,
							declaringMethodDoopId: STRING_NOT_ANALYZED,
							isLocal              : BOOLEAN,
							isParameter          : BOOLEAN,
							suggestName          : SUGGESTER
					] << SYMBOL
			),
			//Symbol relation mappings
			new Mapping(
					cl: ClassSubtype,
					properties: [
							classId   : STRING_NOT_ANALYZED,
							subClassId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: MethodLookup,
					properties: [
							nameWithDescriptor: STRING_NOT_ANALYZED,
							typeId            : STRING_NOT_ANALYZED,
							methodId          : STRING_NOT_ANALYZED,
							isSingleDispatch  : BOOLEAN
					] << ELEMENT
			),
			new Mapping(
					cl: InvocationParts,
					properties: [
							invocationId      : STRING_NOT_ANALYZED,
							nameWithDescriptor: STRING_NOT_ANALYZED,
							baseTypeId        : STRING_NOT_ANALYZED,
							isSingleDispatch  : BOOLEAN
					] << ELEMENT
			),
			new Mapping(
					cl: TypeCanAccessField,
					properties: [
							classId: STRING_NOT_ANALYZED,
							fieldId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: FieldShadowedBy,
					properties: [
							fieldId      : STRING_NOT_ANALYZED,
							shadowFieldId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: InstanceFieldValues,
					properties: [
							baseValueId: STRING_NOT_ANALYZED,
							fieldId    : STRING_NOT_ANALYZED,
							valueId    : STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: InvocationValues,
					properties: [
							fromMethodId: STRING_NOT_ANALYZED,
							invocationId: STRING_NOT_ANALYZED,
							toMethodId  : STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: Metrics,
					properties: [
							kind   : STRING_NOT_ANALYZED,
							doopId : STRING_NOT_ANALYZED,
							counter: LONG
					] << ELEMENT
			),
			new Mapping(
					cl: EntryPoint,
					properties: [
							methodId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: InvocationZeroTargets,
					properties: [
							invocationId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: PackageCallsPackage,
					properties: [
							fromPackage: STRING_NOT_ANALYZED,
							toPackage  : STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: ReachableSymbol,
					properties: [
							methodId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: MethodSubtype,
					properties: [
							methodId   : STRING_NOT_ANALYZED,
							subMethodId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: StaticFieldValues,
					properties: [
							fieldId: STRING_NOT_ANALYZED,
							valueId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: VarValues,
					properties: [
							varId  : STRING_NOT_ANALYZED,
							line   : LONG,
							valueId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: VarReturn,
					properties: [
							varId   : STRING_NOT_ANALYZED,
							methodId: STRING_NOT_ANALYZED
					] << ELEMENT
			),
			new Mapping(
					cl: ArrayValues,
					properties: [
							arrayValueId: STRING_NOT_ANALYZED,
							valueId     : STRING_NOT_ANALYZED
					] << ELEMENT
			),
	]
}
