package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class InvocationParts extends SymbolRelation {

	String invocationId
	String nameWithDescriptor
	String baseTypeId
	boolean isSingleDispatch

	InvocationParts(String anId, String invocationId, String nameWithDescriptor, String baseTypeId, boolean isSingleDispatch) {
		super(anId)
		this.invocationId = invocationId
		this.nameWithDescriptor = nameWithDescriptor
		this.baseTypeId = baseTypeId
		this.isSingleDispatch = isSingleDispatch
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.invocationId = this.invocationId
		map.nameWithDescriptor = this.nameWithDescriptor
		map.baseTypeId = this.baseTypeId
		map.isSingleDispatch = this.isSingleDispatch
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.invocationId = map.invocationId
		this.nameWithDescriptor = map.nameWithDescriptor
		this.baseTypeId = map.baseTypeId
		this.isSingleDispatch = map.isSingleDispatch
	}
}
