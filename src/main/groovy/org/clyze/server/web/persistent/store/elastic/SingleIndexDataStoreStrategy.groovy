package org.clyze.server.web.persistent.store.elastic


import org.apache.commons.io.FileUtils
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.persistent.store.Ordering
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.model.doop.VarValues
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.restlet.api.PaginationParams

import java.util.concurrent.Callable

class SingleIndexDataStoreStrategy extends DataStoreStrategy {

    private static final String SINGLE_STORE = "doop"

    /** The single datastore (for storing all server-side data) */
    private DataStore store

    @Override
    void startup() {
        connector.connect()
        //init the single index
        connector.createStoreIfNotExists(SINGLE_STORE)
        store = connector.connectTo(SINGLE_STORE)
    }

    @Override
    void shutdown() {
        connector?.disconnect()
    }	

    @Override
    DataStore bundleDoopStore(DoopInputBundle bundle) {
        store
    }

    @Override
    DataStore initializeBundleDoopStore(DoopInputBundle bundle) {
        store
    }		

    @Override
    DataStore analysisDoopStore(ServerSideAnalysis a) {
        store
    }

    @Override
    protected String nameOfStoreForUserAccounts() {
        return SINGLE_STORE
    }

    @Override 
    protected String nameOfStoreForUserTokens() {
        return SINGLE_STORE
    }

    @Override 
    protected String nameOfStoreForUserProjects() {
        return SINGLE_STORE
    }		

    @Override
    void storeNewInputBundle(DoopInputBundle bundle, Identifiers.BundleInfo info) {
        logger.debug "Storing input bundle ${bundle.getId()} of user ${bundle.userId}"

        synchronized (info.project) {
            info.project.bundlesCnt += 1
            updateProject(info.project)
            bundle.displayName = "bundle" + info.project.bundlesCnt

            store.saveNew(bundle)
            cache.put(bundle.getId(), bundle)
        }
    }

    @Override
    void removeInputBundle(DoopInputBundle bundle) {
        String bundleId = bundle.id
        logger.debug "Removing input bundle ${bundleId} of user ${bundle.userId}"		
		
        def analyses = []
        //cycle through analyses to verify they are in proper state
        givenBundle(bundle).searchAnalysesOfBundle { String id ->
            //Add the id returned
            analyses.add id

            validateAnalysisStateForDeletion(id)			
        }

        //Delete the bundle tuple
        logger.debug "Removing the bundle ${bundleId} tuple"
        store.delete(bundle) 
        cache.invalidate(bundleId)			
		
        //Delete all bundle-related tuples
        logger.debug "Removing all bundle-related tuples of ${bundleId}"		
        BooleanQuery q
        if (analyses) {
            q = BooleanQueryBuilder.build {
                OR(
                    IN('rootElemId', ([bundleId] + analyses) as Set),
                    EQ('bundleId', bundleId)
                )
            }
        }
        else {
            q = BooleanQueryBuilder.build {
                EQ('rootElemId', bundleId)
            }
        }		
        store.deleteItems(q)

        //Delete the bundle dir
        logger.debug "Removing bundle dir of ${bundleId}"
        FileUtils.deleteQuietly(bundle.baseDir)
    }

    @Override
    DoopInputBundle loadInputBundle(String bundleId) {	
        logger.debug "Getting input bundle ${bundleId}"
        return (DoopInputBundle) cache.get(bundleId, new Callable<DoopInputBundle>() {
                @Override
                DoopInputBundle call() {
                    return store.load(bundleId, DoopInputBundle)
                }
            })
    }

    @Override
    void storeNewAnalysis(ServerSideAnalysis a, Identifiers.AnalysisInfo info)  {

        synchronized (info.bundle) {

            info.bundle.analysesCnt += 1
            store.updateExisting(info.bundle)
            a.displayName = "analysis" + info.bundle.analysesCnt

            store.saveNew(a)
            cache.put(a.getId(), a)
        }
    }

    @Override
    void removeAnalysis(ServerSideAnalysis a) {

        throw new UnsupportedOperationException("Operation not supported")

        /*
        logger.debug "Removing analysis ${a.getId()} of user ${a.userId}"				

        String userId = a.userId

        File outDir = a.get().outDir		
        store.delete(a) {			
        //remove analysis from cache
        cache.invalidate(a.getId())
        //delete analysis directory
        FileUtils.deleteQuietly(outDir)
        //delete all analysis-related datastore items
        store.deleteItems([
        anId  : id,
        userId: userId
        ])
        }
         */
    }

    @Override
    ServerSideAnalysis loadAnalysis(String anId) {		
        logger.debug "Getting analysis ${anId}"
        return cache.get(anId, new Callable<ServerSideAnalysis>() {	
                @Override
                ServerSideAnalysis call() {		
                    return store.load(anId, ServerSideAnalysis)
                }
            })
    }

    @Override 
    void updateAnalysis(ServerSideAnalysis a) {
        store.updateExisting(a)
    }

    @Override
    void storeNewOptimizationDirective(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        store.saveNew directive
        store.refresh()
    }

    @Override
    KeepRemoveMethodDirective loadOptimizationDirectiveOfBundle(DoopInputBundle bundle, String directiveId) {
        store.load(directiveId, KeepRemoveMethodDirective)
    }

    @Override
    void updateOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        store.updateExisting(directive)
        store.refresh()
    }

    @Override
    void removeOptimizationDirectiveOfBundle(DoopInputBundle bundle, KeepRemoveMethodDirective directive) {
        store.delete(directive)
        store.refresh()
    }


    @Override
    void storeNewConfigSet(DoopInputBundle bundle, BundleConfiguration configSet)  {
        store.saveNew configSet
        store.refresh()
    }

    @Override
    BundleConfiguration loadConfigSet(DoopInputBundle bundle, String configSetId) {
        store.load(configSetId, BundleConfiguration)
    }

    @Override
    void updateConfigSet(DoopInputBundle bundle, BundleConfiguration configSet) {
        configSet.touch()
        store.updateExisting(configSet)
        store.refresh()
    }

    @Override
    void removeConfigSet(DoopInputBundle bundle, BundleConfiguration configSet) {
        store.delete(configSet) {
            store.refresh()
        }
    }

    @Override
    boolean existsConfigSet(DoopInputBundle bundle, String configSetId) {
        store.exists(configSetId, BundleConfiguration)
    }


    @Override
    void cleanDeploy(Closure<Void> setupInitialState) {

        AuthManager manager = Config.instance.authManager

        store.clear()

        setupInitialState?.call()
    }	

    Searcher given(final String userId, final Project project, final DoopInputBundle bundle, final ServerSideAnalysis a) {

        String bundleId   = bundle?.id
        String analysisId = a?.id
		
        final List<String> rootElemId = [bundleId, analysisId].findAll()
        final Map<String, Object> baseFilter = [rootElemId: rootElemId]		

        return new Searcher(store) {

            @Override
            protected BooleanQuery createQuery(Map<String, Object> filters) {

                if (baseFilter.size() == 0) throw new RuntimeException("Empty base filter in search context")				

                BooleanQueryBuilder.buildAND(filters ? filters << baseFilter : baseFilter)
            }

            @Override
            SearchLimits processProjectsOfUser(String text, int start, int count, List<OrderBy> orderBy, Closure processor) {
                return processProjects(store, userId, text, start, count, orderBy, processor)
            }

            @Override
            SearchLimits processInputBundlesOfProject(int start, int count, List<OrderBy> orderBy, Closure processor) {
                return processInputBundlesOfProject(project?.id, start, count, orderBy, processor)
            }

            @Override
            void getMethodOptimizationDirectivesOfConfigSet(String configSet,
                                                            Set<String> excluding = [],
                                                            KeepRemoveMethodDirective.Type typeOfDirective = null,
                                                            Closure processor) {

                if (!bundleId) throw new RuntimeException("No bundleId in search context")				

                searchable.scan(queryForMethodOptimizationDirectives(configSet, bundleId, excluding, typeOfDirective?.name()), null, processor)
            }

            @Override
            void getMethodOptimizationDirectivesOfAnalysis(Set<String> excluding = [], Closure processor) {

                if (!analysisId) throw new RuntimeException("No analysisId in search context")				

                searchable.scan(queryForMethodOptimizationDirectives(null, analysisId, excluding, null), null, processor)
            }
			
            private static BooleanQuery queryForMethodOptimizationDirectives(String configSet, String rootElem, Set<String> excluding = [], String typeOfDirective) {
                BooleanQueryBuilder builder = new BooleanQueryBuilder()
                List<BooleanQuery.Operator> andOps = [
                    builder.EQ('_type', KeepRemoveMethodDirective.simpleName),
                    builder.EQ('rootElemId', rootElem),
                ]
                if (typeOfDirective) {
                    andOps.add(builder.EQ('typeOfDirective', typeOfDirective))
                }
                if (configSet) {
                    andOps.add(builder.EQ('configSet', configSet))
                }
                if (excluding) {
                    andOps.add(builder.NOT(builder.IN('doopId', excluding)))
                }

                return new BooleanQuery(
                    operator: new BooleanQuery.CompoundOperator(BooleanQuery.OP.AND, andOps as BooleanQuery.Operator[])
                )
            }			

            @Override
            long searchAnalysesOfBundle(Closure processor) {	

                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                def filters = [
                    _type   : ServerSideAnalysis.simpleName,
                    bundleId: bundleId
                ]
                return searchForIds(searchable, BooleanQueryBuilder.buildAND(filters), processor)
            }

            @Override
            long searchClassesOfArtifact(String artifactName, Closure processor) {						

                def query = createQuery (
                    _type       : Klass.simpleName,
                    artifactName: artifactName,
                ) 
                return search(searchable, query, processor)
            }

            @Override
            SearchLimits searchMethodOptimizationDirectives(Searcher.DirectivesSearchParams searchParams,
                                                            int start,
                                                            int count,
                                                            List<OrderBy> orderByList,
                                                            Closure<Void> processor) {

                def filters = searchParams.asSearchFilters()

                def query = createQuery(filters)

                def limits = new SearchLimits(start: start, count: count)
                searchable.search(searchParams.textualSearch, query, orderByList, limits) { id, type, String json ->
                    processor?.call(json)
                }

                return limits
            }	
            
            @Override
            Map<String, Long> facetOfOptimizationDirectives(Searcher.DirectivesSearchParams searchParams, String facetField, int maxResults) {
                
                def filters = searchParams.asSearchFilters()
                
                return searchable.groupBy(searchParams.textualSearch, createQuery(filters), facetField, maxResults)
            }

            @Override
            void getConfigSetsOfBundle(List<OrderBy> orderByList, Closure<Void> processor) {

                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                def filters = [
                    _type    : BundleConfiguration.simpleName,
                    bundleId : bundleId
                ]

                def query = createQuery(filters)

                searchable.scan(query, orderByList, processor)
            }

            @Override
            Map<String, Long> groupOptimizationDirectivesByRule(String configSetName) {
                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                def filters = [
                    _type    : KeepRemoveMethodDirective.simpleName,
                    bundleId : bundleId,
                    configSet: configSetName
                ]

                def query = createQuery(filters)

                searchable.groupBy(query, 'ruleId', 1000)
            }

            @Override
            SearchLimits searchConfigSetRules(Searcher.RulesSearchParams searchParams, int start, int count, List<OrderBy> orderByList, Closure<Void> processor) {

                def filters = searchParams.asSearchFilters()

                def query = createQuery(filters)

                def limits = new SearchLimits(start: start, count: count)
                searchable.search(searchParams.textualSearch, query, orderByList, limits) { id, type, String json ->
                    processor?.call(json)
                }

                return limits
            }

            @Override
            long countConfigSetRules(String configSet) {

                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                BooleanQuery q = createQuery ([
                    '_type': PersistentBundleConfigurationRule.simpleName,
                    'bundleId': bundleId,
                    'configSet': configSet
                ])
                return searchable.count(q)
            }

            @Override
            Map<String, Long> facetOfConfigSetRules(Searcher.RulesSearchParams searchParams, String facetField, int maxResults) {

                def filters = searchParams.asSearchFilters()

                return searchable.groupBy(searchParams.textualSearch, BooleanQueryBuilder.buildAND(filters), facetField, maxResults)
            }

            @Override
            long searchAtPosition(Set<String> allowedTypes, String sourceFileName, String line, String column, Closure processor)  {

                if (!bundleId) throw new RuntimeException("No bundleId in search context")

                BooleanQuery q = BooleanQueryBuilder.build {
                    AND(
                        EQ('rootElemId', bundleId),
                        EQ('sourceFileName', sourceFileName),
                        EQ('position.startLine', line),
                        LTE('position.startColumn', column),
                        GTE('position.endColumn', column),
                        IN('_type', allowedTypes)
                    )
                }

                def limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
                searchable.search(q, limits, processor)
                return limits.hits
            }				

            @Override
            void searchClosestVarValues(String var, long line, Closure closure) {

                if (baseFilter.size() == 0) throw new RuntimeException("Empty base filter in search context")							

                // Initial outermost query: find closest previous line.
                BooleanQuery q = BooleanQueryBuilder.build {
                    AND(
                        IN('rootElemId', baseFilter.rootElemId as Set),
                        EQ('_type', VarValues.simpleName),
                        EQ('varId', var),
                        LTE('line', line)
                    )
                }
                def orderByLine = new OrderBy(field: 'line', ordering: Ordering.DESC)
                def limits = new SearchLimits(start: 0, count: 1)

                // Inner query: process results
                def innerQuery = { String id0, String type0, String json ->					
                    def vpt = new VarValues().fromJSON(json)				
                    //logger.debug "Found VarValues ${vpt.toMap()}"	
                    BooleanQuery innerQ = BooleanQueryBuilder.build {
                        AND(
                            IN('rootElemId', baseFilter.rootElemId as Set),
                            EQ('_type', VarValues.simpleName),
                            EQ('varId', var),
                            EQ('line', vpt.line)
                        )
                    }
                    limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
                    //logger.debug("Searching for VarValues in line: ${vpt.line}")
                    searchable.search(innerQ, null, limits, closure)
                }

                //logger.debug "Search closest VarValues - var:$var, line<=$line, baseFilter:$baseFilter"
                searchable.search(q, [orderByLine], limits, innerQuery)
				
                // Optional second outermost query: if no results were found
                // in the query above, rerun query with opposite line filter
                // and ordering -- now finds closest next line.
                if (limits.hits == 0) {
                    //logger.debug "Second search for VarValues - var:$var, line>$line, baseFilter:$baseFilter"
                    q = BooleanQueryBuilder.build {
                        AND(
                            IN('rootElemId', baseFilter.rootElemId as Set),
                            EQ('_type', VarValues.simpleName),
                            EQ('varId', var),
                            GT('line', line)
                        )
                    }				
                    def orderByLine2 = new OrderBy(field: 'line', ordering: Ordering.ASC)
                    def limits2 = new SearchLimits(start: 0, count: 1)
                    searchable.search(q, [orderByLine2], limits2, innerQuery)
                }
            }

        }
    }
}
