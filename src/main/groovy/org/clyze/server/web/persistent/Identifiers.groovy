package org.clyze.server.web.persistent

import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.Project

@Singleton
class Identifiers {

	static class BundleInfo {
		String id
		String userId
		Project project
	}

	static class AnalysisInfo {
		String id
		String userId
		Project project
		DoopInputBundle bundle
	}

	static class Info {
		String id
		String userId
		Project project
	}

	BundleInfo newBundleInfo(String userId, Project project) {
		return new BundleInfo(
			id         : UUID.randomUUID().toString(),
			userId     : userId,
			project    : project
		)
	}

	AnalysisInfo newAnalysisInfo(String userId, Project project, DoopInputBundle bundle) {
		return new AnalysisInfo(
			id         : UUID.randomUUID().toString(),
			project    : project,
			bundle     : bundle,
			userId     : userId
		)
	}
}