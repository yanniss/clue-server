package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class InstanceFieldValues extends SymbolRelation {

	String baseValueId
	String fieldId
	String valueId

	InstanceFieldValues(String anId, String baseValueId, String fieldId, String valueId) {
		super(anId)
		this.baseValueId = baseValueId
		this.fieldId = fieldId
		this.valueId = valueId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.baseValueId = this.baseValueId
		map.fieldId     = this.fieldId
		map.valueId     = this.valueId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.baseValueId = map.baseValueId
		this.fieldId     = map.fieldId
		this.valueId     = map.valueId
	}
}
