package org.clyze.server.web.persistent.store.elastic

import groovy.transform.PackageScope
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.Item
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.SuggestionAware
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.TextualSearch

import static org.clyze.server.web.persistent.store.BooleanQuery.*
import org.clyze.server.web.persistent.store.Bulk
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.persistent.store.SearchLimits
import org.elasticsearch.action.delete.DeleteResponse
import org.elasticsearch.action.get.GetResponse
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.search.SearchHit

class ElasticDataStore implements DataStore {

    private static Log logger = LogFactory.getLog(this)

    private static final int DEFAULT_NUMBER_OF_TERM_BUCKETS = 5000

    private final ElasticClusterDataStoreConnector connector    
    private final String indexName    
    private final @Delegate ElasticSearchable searchable

    @PackageScope ElasticDataStore(ElasticClusterDataStoreConnector connector, String indexName) {
        this.connector = connector
        this.indexName = indexName
        this.searchable  = new ElasticSearchable(connector, indexName)
    }    

    //Temp hack to change index setting(s)
    void updateSetting(String setting, def value) {
        connector.client.admin().indices().prepareUpdateSettings(indexName).setSettings(
                Settings.builder().put(setting, value)
        ).execute().actionGet()
    }

    @Override
    void clear() throws RuntimeException {
        connector.dropStoreIfExists(indexName)
        connector.initIndex(indexName)
    }

    @Override
    boolean exists(String id, Class cl) throws RuntimeException {
        logger.debug "Checking if $id exists in $indexName"
        return connector.client.prepareGet(indexName, cl.getSimpleName(), id).execute().actionGet().isExists()
    }

    @Override
    <T extends Item> T load(String id, Class<T> cl) throws RuntimeException {
        logger.debug "Loading $id (${cl.simpleName}) from $indexName"
        GetResponse resp = connector.client.prepareGet(indexName, cl.getSimpleName(), id).execute().actionGet()
        if (!resp.isExists())
            throw new RuntimeException("Not found: $id (${cl.getSimpleName()}) in $indexName")

        return getItem(id, cl, resp.sourceAsString)
    }


    @Override
    void updateExisting(Item item) throws RuntimeException {        
        //use(SuggestionAware)  {
            String id = item.getId()
            if (!id) throw new RuntimeException("The ${item.class.simpleName} does not have an id: ${item.toMap()}")

            String json = item.toJSON()
            logger.debug "Updating ${item.getId()} (${item.class.getSimpleName()}) in $indexName -> $json"
            connector.client.prepareIndex(indexName, item.class.getSimpleName()).setId(item.getId()).setSource(json).execute().actionGet()
        //}
    }

    @Override
    void saveNew(Item item) throws RuntimeException {        
        //use(SuggestionAware)  {
            String json = item.toJSON()            

            String id = item.getId()
            if (id) {
                logger.debug "Saving new (${item.class.getSimpleName()}) with id ${item.getId()} in $indexName -> $json"
                connector.client.prepareIndex(indexName, item.class.getSimpleName()).setId(item.getId()).setSource(json).execute().actionGet()
            }
            else {
                logger.debug "Saving new (${item.class.getSimpleName()}) in $indexName -> $json"
                id = connector.client.prepareIndex(indexName, item.class.getSimpleName()).setSource(json).execute().actionGet().getId()
                item.setId(id)
            }                        
        //}
    }

    @Override
    void delete(Item item, Closure callback=null) {
        logger.debug "Deleting ${item.getId()} (${item.class.getSimpleName()}) from $indexName"
        DeleteResponse resp = connector.client.prepareDelete(indexName, item.class.getSimpleName(), item.getId()).execute().actionGet()
        def result = resp.getResult()
        if (result == org.elasticsearch.action.DocWriteResponse.Result.DELETED) {
            callback?.call(item)
        }
        else {
            throw new RuntimeException("Deletion of ${item.getId()} from $indexName failed - $result")
        }
    }    

    @Override
    void deleteItems(BooleanQuery filters) {
        deleteItems(null, filters)
    }

    @Override
    void deleteItems(TextualSearch textualSearch, BooleanQuery filters) {
        if (!filters || filters.isEmpty()) {
            return
        }

        SearchRequestBuilder searchRequestBuilder = connector.client.prepareSearch(indexName)

        ElasticSearchable.createQuery(searchRequestBuilder, textualSearch, filters, [new OrderBy(field: "_doc")])

        int scrollSize = Config.instance.deletionBulkSize //This is per shard
        BulkProcessor bulkProcessor = new BulkProcessor(Config.instance.bulkSize, this)
        SearchResponse searchScroll = searchRequestBuilder.
                setScroll("1m").
                setSize(scrollSize).
                execute().actionGet()

        SearchHit[] hits = searchScroll.getHits().getHits()
        long counter = 0
        while (hits.length > 0) {
            hits.each { SearchHit hit ->
                bulkProcessor.scheduleDelete(hit.type, hit.id)
                counter++
                //log.debug "${counter}. item ${hit.id} (${hit.type}) - to be deleted."
            }

            searchScroll = connector.client.prepareSearchScroll(searchScroll.getScrollId()).setScroll("1m").execute().actionGet()
            hits = searchScroll.getHits().getHits()
        }

        logger.warn "Deleting $counter items from $indexName"
        if (counter > 0) {
            bulkProcessor.finishUp()
        }
    }

    @Override
    Bulk createBulk() {
        return new ElasticBulk(connector.client, indexName, connector.client.prepareBulk())
    }    

    @Override
    void refresh() {
        connector.client.admin().indices().prepareRefresh(indexName).execute().actionGet()
    }

    static <T extends Item> T getItem(String id, Class<T> cl, String json) {
        T t = cl.getConstructor().newInstance()
        t.setId(id)
        return t.fromJSON(json)        
    }

    
}
