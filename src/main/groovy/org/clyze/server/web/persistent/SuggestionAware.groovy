package org.clyze.server.web.persistent


import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.doop.Variable

class SuggestionAware {

	private static Set<String> classNameSplit(String fullyQualifiedName, String packageName, String className) {

		Set<String> parts = [] as Set<String>
		
		String fqName = fullyQualifiedName.replaceAll('\\$', '.')
		String clName = className.replaceAll('\\$', '.')
		String nameWithoutPackage = fqName - packageName - '.'

		parts.add nameWithoutPackage
		nameWithoutPackage.split('\\.').each {
			if (it.length() > 1) {					
				camelCaseSplit(it, parts)								
			}
		}		

		return parts
	}

	private static void camelCaseSplit(String s, Set<String> parts) {
		parts.add s //add the original value in the list
		int len = s.length()
		int prevUpperIndex = 0
		int i = 0
		while (i < len) {
			if (Character.isUpperCase(s.charAt(i)) && i != prevUpperIndex) {
				parts.add s.substring(prevUpperIndex, i)				
				prevUpperIndex = i
			}
			i++
		}
		parts.add s.substring(prevUpperIndex, i) //use of set prevents duplicates
	}

	private static Set<String> camelCaseSplit(String s) {
		Set<String> parts = [] as Set<String>
		camelCaseSplit(s, parts)
		return parts
	}

	private static final String suggestionAwareJson(Map map, String type) {	
        if(map.name) {
        	Set<String> inputs = type == Klass.simpleName ? 
        						 classNameSplit(map.doopId, map.packageName, map.name) : 
        						 camelCaseSplit(map.name)        						 
	        map['suggestName'] = [
	            input    : inputs,
	            contexts : [
	            	typeAndRootElem: [type + "_" + map.rootElemId] //ES5 workaround for contexts being ORed and not ANDed
	            ]
	        ]
	    }
        return groovy.json.JsonOutput.toJson(map)
	}

	static String toJSON(Klass k) {
		return suggestionAwareJson(k.toMap(), Klass.simpleName)
	}

	static String toJSON(Field f) {
		return suggestionAwareJson(f.toMap(), Field.simpleName)
	}

	static String toJSON(Method m) {
		return suggestionAwareJson(m.toMap(), Method.simpleName)
	}

	static String toJSON(Variable v) {
		return suggestionAwareJson(v.toMap(), Variable.simpleName)
	}
}
