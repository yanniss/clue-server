package org.clyze.server.web.persistent.store.elastic

import groovy.transform.PackageScope

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.Searchable
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.TextualSearch
import org.clyze.server.web.persistent.store.TextualSearchType
import org.elasticsearch.index.query.ConstantScoreQueryBuilder
import org.elasticsearch.index.query.MatchAllQueryBuilder

import static org.clyze.server.web.persistent.store.BooleanQuery.*
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.persistent.store.SearchLimits
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.suggest.*
import org.elasticsearch.index.query.BoolQueryBuilder
import org.elasticsearch.index.query.QueryBuilder
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.aggregations.AggregationBuilder
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.terms.Terms
import org.elasticsearch.search.sort.SortOrder

class ElasticSearchable implements Searchable {

	private static Log logger = LogFactory.getLog(this)

	private final ElasticClusterDataStoreConnector connector    
    private final String[] indexNames    		

    @PackageScope ElasticSearchable(ElasticClusterDataStoreConnector connector, String... indexNames) {
    	this.connector  = connector
    	this.indexNames = indexNames
    }

    @Override
    void search(TextualSearch textualSearch,
                BooleanQuery filters,
                List<OrderBy> orderByList,
                SearchLimits limits,
                Closure resultProcessor) {

        if (!textualSearch && (!filters || filters.isEmpty())) {
            return
        }

        SearchRequestBuilder searchRequestBuilder = connector.client.prepareSearch(indexNames)

        createQuery(searchRequestBuilder, textualSearch, filters, orderByList)

        int from = limits?.start ?: 0
        int size = limits?.count ?: 10

        searchRequestBuilder.setFrom(from).setSize(size)

        logger.debug "Searching  in ${indexNames} with:${searchRequestBuilder})"
        SearchResponse resp = searchRequestBuilder.execute().actionGet()

        limits.hits = resp.hits.totalHits
        //logger.debug "Search results: ${limits.hits}"
        resp.hits.hits.each { SearchHit hit ->
            //log.debug "Processing ${hit.id} (${hit.type})"
            resultProcessor?.call(hit.id, hit.type, hit.sourceAsString)
        }

    }

    @Override
    void search(BooleanQuery filters, List<OrderBy> orderByList, SearchLimits limits, Closure resultProcessor) {
        search(null, filters, orderByList, limits, resultProcessor)
    }

    @Override
    long count(BooleanQuery filters) {
        return count(null, filters)
    }

    @Override
    long count(TextualSearch textualSearch, BooleanQuery filters) {
        if (!filters || filters.isEmpty()) {
            return 0
        }

        SearchRequestBuilder searchRequestBuilder = connector.client.prepareSearch(indexNames)

        createQuery(searchRequestBuilder, textualSearch, filters)

        searchRequestBuilder.setSize(0)

        logger.debug "Counting results in ${indexNames} of:${searchRequestBuilder})"
        return searchRequestBuilder.execute().actionGet().hits.totalHits
    }


    @Override
    void scan(BooleanQuery filters, List<OrderBy> orderByList, Closure resultProcessor) {
         scan(null, filters, orderByList, resultProcessor)
    }

    @Override
    void scan(TextualSearch textualSearch, BooleanQuery filters, List<OrderBy> orderByList, Closure resultProcessor) {
        if (!filters || filters.isEmpty()) {
            return
        }

        int scrollSize = 1000
        SearchRequestBuilder searchScrollBuilder = connector.client.prepareSearch(indexNames).setScroll("1m").setSize(scrollSize)

        createQuery(searchScrollBuilder, textualSearch, filters, orderByList)
        logger.debug "Scanning in ${indexNames} with:${searchScrollBuilder})"
        SearchResponse searchScroll = searchScrollBuilder.execute().actionGet()
        SearchHit[] hits = searchScroll.getHits().getHits()
        long counter = 0
        while (hits.length > 0) {
            hits.each { SearchHit hit ->
                resultProcessor?.call(hit.id, hit.type, hit.sourceAsString)
                counter++
            }

            searchScroll = connector.client.prepareSearchScroll(searchScroll.getScrollId()).setScroll("1m").execute().actionGet()
            hits = searchScroll.getHits().getHits()
        }

        logger.debug("Scanned $counter items")
    }

    @Override
    Map<String, Long> groupBy(BooleanQuery filters, String field, int maxResults) {
        return groupBy(null, filters, field, maxResults)
    }

    @Override
    Map<String, Long> groupBy(TextualSearch textualSearch, BooleanQuery filters, String field, int maxResults) {
        if (!filters || filters.isEmpty()) {
            return
        }

        SearchRequestBuilder searchRequestBuilder = connector.client.prepareSearch(indexNames)
        createQuery(searchRequestBuilder, textualSearch, filters)
        AggregationBuilder termsAggregation = AggregationBuilders.terms('groupBy').field(field).size(maxResults)

        SearchResponse resp = searchRequestBuilder.addAggregation(termsAggregation).execute().actionGet()
        Terms terms = resp.aggregations.get('groupBy')

        return terms.getBuckets().collectEntries { Terms.Bucket bucket ->
            [(bucket.key): bucket.docCount]
        }
    }

    @PackageScope static void createQuery(SearchRequestBuilder searchRequestBuilder, TextualSearch textualSearch, BooleanQuery filters, List<OrderBy> orderByList = []) {

        //log.debug("Create query\ntext: $text\nfilters:$filters\norderBy:$orderByList")

        /*
        BoolQueryBuilder query = QueryBuilders.boolQuery()
        //We always expect filters, only textualSearch is optional
        if (textualSearch) {
            List<QueryBuilder> textQueries
            if (textualSearch.type == TextualSearchType.WILDCARD) {
                textQueries = toWildcardQuery(textualSearch)
            }
            else if (textualSearch.type == TextualSearchType.REGEX) {
                textQueries = toRegexQuery(textualSearch)
            }
            else if (textualSearch.type == TextualSearchType.FULL_TEXT) {
                textQueries = toFullTextQuery(textualSearch)
            }
            else {
                throw new RuntimeException("Unknown textual search type")
            }
            if (textQueries.size() == 1) {
                query.must(textQueries[0])
            }
            else {
                textQueries.each {
                    query.should(it)
                }
            }
        }
        
        //logger.debug("Parsing boolean query: $filters")
        BoolQueryBuilder filtersQuery = new ElasticBoolQuery(q:filters).transform()
        //logger.debug("Transformed into: $filtersQuery")
        query.filter(filtersQuery)
        searchRequestBuilder.setQuery(query)
        */

        searchRequestBuilder.setQuery(
            QueryBuilders.constantScoreQuery(
                new ElasticBoolQuery(textualSearch: textualSearch, q:filters).transform()
            )
        )

        if (orderByList) {
            orderByList.each { OrderBy orderBy ->
                searchRequestBuilder.addSort(orderBy.field, SortOrder.fromString(orderBy.ordering.toString()))
            }
        }
    }

    private static List<QueryBuilder> toWildcardQuery(TextualSearch textualSearch) {
        textualSearch.fields.collect { String field ->
            QueryBuilders.wildcardQuery(field, textualSearch.textExpr)
        }
    }

    private static List<QueryBuilder> toRegexQuery(TextualSearch textualSearch) {
        textualSearch.fields.collect { String field ->
            QueryBuilders.regexpQuery(field, textualSearch.textExpr)
        }
    }

    private static List<QueryBuilder> toFullTextQuery(TextualSearch textualSearch) {
        return [QueryBuilders.multiMatchQuery(textualSearch.textExpr, textualSearch.fields)]
    }
 
    private static class ElasticBoolQuery {

        Deque<OP> stack = new ArrayDeque<>();

        TextualSearch textualSearch
        BooleanQuery q

        BoolQueryBuilder transform() {
            BoolQueryBuilder elasticQuery = QueryBuilders.boolQuery()
            addOperator(q.operator, elasticQuery)
            if (textualSearch) {
                List<QueryBuilder> textQueries
                if (textualSearch.type == TextualSearchType.WILDCARD) {
                    textQueries = toWildcardQuery(textualSearch)
                }
                else if (textualSearch.type == TextualSearchType.REGEX) {
                    textQueries = toRegexQuery(textualSearch)
                }
                else if (textualSearch.type == TextualSearchType.FULL_TEXT) {
                    textQueries = toFullTextQuery(textualSearch)
                }
                else {
                    throw new RuntimeException("Unknown textual search type")
                }
                /*
                if (textQueries.size() == 1) {
                    elasticQuery.must(textQueries[0])
                }
                else {
                    textQueries.each {
                        elasticQuery.should(it)
                    }
                }
                */
                textQueries.each {
                    elasticQuery.should(it)
                }
            }
            return elasticQuery
        }        

        private void addOperator(Operator op, BoolQueryBuilder q) {
            switch(op.kind) {
                case OP.EQ:                                    
                     def childQuery = transformEQ(op as EqualsOperator)
                     add(q, childQuery)                    
                     break
                case [OP.LT]:
                     def childQuery = transformLT(op as LessThanOperator)
                     add(q, childQuery)
                     break
                case [OP.LTE]:
                     def childQuery = transformLTE(op as LessThanOrEqualsOperator)
                     add(q, childQuery)
                     break 
                case [OP.GT]:
                     def childQuery = transformGT(op as GreaterThanOperator)
                     add(q, childQuery)
                     break 
                case [OP.GTE]:
                     def childQuery = transformGTE(op as GreaterThanOrEqualsOperator)
                     add(q, childQuery)
                     break 
                case OP.IN:
                     def childQuery = transformIN(op as InOperator)
                     add(q, childQuery)                    
                     break
                case [OP.AND, OP.OR, OP.NOT]:
                     def childQuery = transformCompound(op as CompoundOperator)
                     add(q, childQuery)                    
                     break
                default:
                     throw new RuntimeException("Operator not supported: $op")
            }        
        }        

        private QueryBuilder transformEQ(EqualsOperator op) {
            QueryBuilders.termQuery(op.operand, op.rhs)
        }

        private QueryBuilder transformLT(LessThanOperator op) {                
            QueryBuilders.rangeQuery(op.operand).lt(op.rhs)
        }

        private QueryBuilder transformLTE(LessThanOrEqualsOperator op) {            
            QueryBuilders.rangeQuery(op.operand).lte(op.rhs)
        }        

        private QueryBuilder transformGT(GreaterThanOperator op) {            
            QueryBuilders.rangeQuery(op.operand).gt(op.rhs)
        }

        private QueryBuilder transformGTE(GreaterThanOrEqualsOperator op) {            
            QueryBuilders.rangeQuery(op.operand).gte(op.rhs)
        }

        private QueryBuilder transformIN(InOperator op) {
            QueryBuilders.termsQuery(op.operand, op.rhs.asList())
        }

        private QueryBuilder transformCompound(CompoundOperator op) {
            BoolQueryBuilder childQuery = QueryBuilders.boolQuery()
            stack.push(op.kind)
            op.operators.each {
                addOperator(it, childQuery)
            }
            stack.pop()
            childQuery
        }

        private void add(BoolQueryBuilder parentQuery, QueryBuilder childQuery) {
            OP context = stack.peek()
            if (!context) {
                context = OP.AND
            }        
            switch(context) {
                case OP.AND: 
                    parentQuery.must(childQuery)
                    break
                case OP.OR:
                    parentQuery.should(childQuery)
                    break
                case OP.NOT:
                    parentQuery.mustNot(childQuery)
                    break
                default:
                    throw new RuntimeException("Invalid context: $context, cannot add query: $childQuery")
            }
        }
    }

}