package org.clyze.server.web.persistent

class SeparateFilesExporter extends DefaultExporter {

	private static final long DEFAULT_ITEMS_PER_FILE = 500

	private final long itemsPerFile

	SeparateFilesExporter(File outputDirectory, long itemsPerFile = DEFAULT_ITEMS_PER_FILE) {
		super(outputDirectory)
		this.itemsPerFile = itemsPerFile
	}

	@Override
	void export(String id, String type, String json) {
		if (counter > 0 && counter % itemsPerFile == 0) {
			if (jsonFile) {
				jsonFile.end()
			}
			jsonFile = newJsonFile(outputDirectory, "data-$counter")
			jsonFile.start()
		}

		super.export(id, type, json)
	}

}
