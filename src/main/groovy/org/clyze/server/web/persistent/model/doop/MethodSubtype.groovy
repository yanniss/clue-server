package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class MethodSubtype extends SymbolRelation {

	String methodId
	String subMethodId

	MethodSubtype(String anId, String methodId, String subMethodId) {
		super(anId)
		this.methodId = methodId
		this.subMethodId = subMethodId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.methodId    = this.methodId
		map.subMethodId = this.subMethodId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.methodId    = map.methodId
		this.subMethodId = map.subMethodId
	}
}
