package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class PackageCallsPackage extends SymbolRelation {

	String fromPackage
	String toPackage

	PackageCallsPackage(String anId, String fromPackage, String toPackage) {
		super(anId)
		this.fromPackage = fromPackage
		this.toPackage = toPackage
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.fromPackage = this.fromPackage
		map.toPackage   = this.toPackage
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.fromPackage = map.fromPackage
		this.toPackage   = map.toPackage
	}
}
