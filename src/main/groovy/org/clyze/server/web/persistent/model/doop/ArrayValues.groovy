package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class ArrayValues extends SymbolRelation {

	String arrayValueId
	String valueId

	ArrayValues(String anId, String arrayValueId, String valueId) {
		super(anId)
		this.arrayValueId = arrayValueId
		this.valueId = valueId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.arrayValueId = arrayValueId
		map.valueId      = valueId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.arrayValueId = map.arrayValueId
		this.valueId      = map.valueId
	}
}
