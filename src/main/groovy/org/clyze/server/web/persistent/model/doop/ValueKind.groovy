package org.clyze.server.web.persistent.model.doop

enum ValueKind {
	PRIMITIVE_VALUE,
	NORMAL_ALLOCATION,
	MERGED_ALLOCATION,
	MOCK,
	TAINT,
	UNKNOWN,
	METAOBJECT
}
