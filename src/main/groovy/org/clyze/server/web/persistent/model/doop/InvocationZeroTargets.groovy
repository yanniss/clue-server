package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class InvocationZeroTargets extends SymbolRelation {

	String invocationId

	InvocationZeroTargets(String anId, String invocationId) {
		super(anId)
		this.invocationId = invocationId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.invocationId = this.invocationId		
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)		
		this.invocationId = map.invocationId
	}
}
