package org.clyze.server.web.persistent.store

interface Exporter {

	void start()

	void export(String id, String type, String json)

	void end()
}
