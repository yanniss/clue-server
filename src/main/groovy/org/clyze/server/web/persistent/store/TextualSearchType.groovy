package org.clyze.server.web.persistent.store

enum TextualSearchType {
    FULL_TEXT("FT"),
    WILDCARD("W"),
    REGEX("R")

    private final shortName

    private TextualSearchType(String shortName) {
        this.shortName = shortName
    }

    String getShortName() {
        return shortName
    }
}
