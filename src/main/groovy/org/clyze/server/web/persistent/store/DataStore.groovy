package org.clyze.server.web.persistent.store

import org.clyze.persistent.model.Item

interface DataStore extends Searchable {

	void clear() throws RuntimeException

	boolean exists(String id, Class cl) throws RuntimeException

	def <T extends Item> T load(String id, Class<T> cl) throws RuntimeException

	void updateExisting(Item item) throws RuntimeException

	void saveNew(Item item) throws RuntimeException

	void delete(Item item, Closure callback)

	void deleteItems(BooleanQuery filters)

	void deleteItems(TextualSearch textualSearch, BooleanQuery filters)

	Bulk createBulk()

	/*
	 * This is the only method that is elastic-specific but has to bleed-through.
	 */
	void refresh() 

}
