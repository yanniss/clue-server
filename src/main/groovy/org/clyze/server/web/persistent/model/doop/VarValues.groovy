package org.clyze.server.web.persistent.model.doop

//import groovy.transform.EqualsAndHashCode
import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

//@EqualsAndHashCode(callSuper = true)
@InheritConstructors
class VarValues extends SymbolRelation {

	String varId
	Integer line
	String valueId

	VarValues(String anId, String varId, Integer line, String valueId) {
		super(anId)
		this.varId = varId
		this.line = line
		this.valueId = valueId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.varId   = this.varId
		map.line    = this.line
		map.valueId = this.valueId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.varId   = map.varId
		this.line    = map.line as Integer
		this.valueId = map.valueId
	}
}
