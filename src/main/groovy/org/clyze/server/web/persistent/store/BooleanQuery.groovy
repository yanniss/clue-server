package org.clyze.server.web.persistent.store

class BooleanQuery {	

	Operator operator

	boolean isEmpty() {
		operator == null
	}

	@Override
	String toString() {
		operator ? operator.toString() : "<EMPTY>"
	}	

	static enum OP {
		EQ,
		LT,
		LTE,
		GT,
		GTE,
		IN,
		NOT,
		AND,
		OR
	}		

	static class Operator {

		OP kind

		Operator(OP kind) {
			this.kind = kind
		}
	}

	static class BinaryOperator<T> extends Operator {

		String operand
		T rhs

		BinaryOperator(OP kind, String operand, T rhs) {
			super(kind)
			this.operand = operand
			this.rhs = rhs      
		}

		@Override
		String toString() {
			"${kind.name()}($operand, $rhs)"
		}
	}	

	static class EqualsOperator extends BinaryOperator<Object> {		
		EqualsOperator(String operand, Object rhs) {
			super(OP.EQ, operand, rhs)			
		}
	}

	static class LessThanOperator extends BinaryOperator<Object> {
		LessThanOperator(String operand, Object rhs) {
			super(OP.LT, operand, rhs)
		}
	}

	static class LessThanOrEqualsOperator extends BinaryOperator<Object> {
		LessThanOrEqualsOperator(String operand, Object rhs) {
			super(OP.LTE, operand, rhs)
		}
	}

	static class GreaterThanOperator extends BinaryOperator<Object> {
		GreaterThanOperator(String operand, Object rhs) {
			super(OP.GT, operand, rhs)
		}
	}

	static class GreaterThanOrEqualsOperator extends BinaryOperator<Object> {
		GreaterThanOrEqualsOperator(String operand, Object rhs) {
			super(OP.GTE, operand, rhs)
		}
	}
	
	static class InOperator extends BinaryOperator<Set> {		
		InOperator(String operand, Set rhs) {
			super(OP.IN, operand, rhs)			
		}
	}

	static class CompoundOperator extends Operator {

		List<Operator> operators

		CompoundOperator(OP kind, Operator... operators) {
			super(kind)
			this.operators = operators as List
		}

		@Override
		String toString() {
			"${kind.name()}($operators)"
		}
	}

}