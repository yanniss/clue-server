package org.clyze.server.web.persistent.model.doop

import groovy.transform.EqualsAndHashCode
import groovy.transform.InheritConstructors
import org.clyze.persistent.model.SymbolWithDoopId
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.HeapAllocation
import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.server.web.persistent.store.DataStore
import static org.clyze.server.web.persistent.model.doop.ValueKind.*
import static org.clyze.server.web.restlet.resource.AbstractServerResource.search

@EqualsAndHashCode(callSuper = true, includes = ["kind"])
@InheritConstructors
class Value extends SymbolWithDoopId {

	String allocatedTypeDoopId
	String allocatingMethodDoopId	
	String description
	boolean isArray = false
	ValueKind kind

	Value() {}	

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.allocatedTypeDoopId    = this.allocatedTypeDoopId
		map.allocatingMethodDoopId = this.allocatingMethodDoopId		
		map.description            = this.description
		map.isArray                = this.isArray
		map.kind                   = this.kind.name()
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.allocatedTypeDoopId    = map.allocatedTypeDoopId
		this.allocatingMethodDoopId = map.allocatingMethodDoopId		
		this.description            = map.description
		this.isArray                = map.isArray
		this.kind                   = map.kind ? ValueKind.valueOf(map.kind as String) : null
	}

}
