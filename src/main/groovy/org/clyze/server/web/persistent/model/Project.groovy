package org.clyze.server.web.persistent.model

import org.clyze.persistent.model.ItemImpl
import org.clyze.server.web.persistent.store.Dates

import groovy.transform.CompileStatic

@CompileStatic
class Project extends ItemImpl {
	
	String name			
	Date created	
	
	String owner
	List<String> members = []

	long bundlesCnt      = 0

	Project() {
		
	}

	Project(String owner, String name) {		
		this.id      = UUID.randomUUID().toString()
		this.owner   = owner
		this.name    = name
		this.created = new Date()		
	}

	@Override
	void saveTo(Map<String, Object> map) {
		map.id               = id
		map.name             = name
		map.created          = Dates.format(created)		
		map.owner            = owner
		map.members          = members	
		map.bundlesCnt       = bundlesCnt
	}

	@Override
	void loadFrom(Map<String, Object> map){
		this.id               = map.get("id") as String
		this.name             = map.get("name") as String
		this.created          = Dates.parse(map.get("created") as String)		
		this.owner            = map.get("owner") as String
		this.members          = map.get("members") as List<String>
		this.bundlesCnt       = map.get("bundlesCnt") as long
	}			
}
