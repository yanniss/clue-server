package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class ClassSubtype extends SymbolRelation {

	String classId
	String subClassId

	ClassSubtype(String anId, String classId, String subClassId) {
		super(anId)
		this.classId = classId
		this.subClassId = subClassId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.classId    = this.classId
		map.subClassId = this.subClassId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.classId    = map.classId
		this.subClassId = map.subClassId
	}
}
