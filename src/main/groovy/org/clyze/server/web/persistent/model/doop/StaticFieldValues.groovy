package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class StaticFieldValues extends SymbolRelation {

	String fieldId
	String valueId

	StaticFieldValues(String anId, String fieldId, String valueId) {
		super(anId)
		this.fieldId = fieldId
		this.valueId = valueId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.fieldId = this.fieldId
		map.valueId = this.valueId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.fieldId = map.fieldId
		this.valueId = map.valueId
	}
}
