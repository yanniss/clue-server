package org.clyze.server.web.persistent.model.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.SymbolRelation

@InheritConstructors
class TypeCanAccessField extends SymbolRelation {

	String classId
	String fieldId

	TypeCanAccessField(String anId, String classId, String fieldId) {
		super(anId)
		this.classId = classId
		this.fieldId = fieldId
	}

	protected void saveTo(Map<String, Object> map) {
		super.saveTo(map)
		map.classId = this.classId
		map.fieldId = this.fieldId
	}

	protected void loadFrom(Map<String, Object> map){		
		super.loadFrom(map)
		this.classId = map.classId
		this.fieldId = map.fieldId
	}
}
