package org.clyze.server.web

import io.reactivex.Flowable
import org.clyze.server.web.persistent.model.doop.EffectiveMethodDirective
import org.clyze.utils.FileOps

import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.RedexRepackager
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

import groovy.transform.CompileStatic

@CompileStatic
class Optimizer {

	static class Output {
		File optimized
		File directives
	}

	static Optimizer.Output optimize(File apk, String dirForNewAPK, Collection<EffectiveMethodDirective> directives) {

		String apkIn             = FileOps.findFileOrThrow(apk, "APK not found: $apk").canonicalPath
		File newDir              = FileOps.ensureDirExistsOrThrow(dirForNewAPK, "Could not create output dir")
		String apkOut            = "${newDir}/optimized.apk"
		File redexDirectives   = new File("${newDir}/methods_to_remove.csv")
		File effectiveDirectives = new File("${newDir}/effective.clue")

		Set<String> jvmDescriptors = [] as Set<String> //To be removed

		effectiveDirectives.withWriter { w ->

			directives.each { EffectiveMethodDirective directive ->
				if (directive.typeOfDirective == KeepRemoveMethodDirective.Type.REMOVE ||
					directive.typeOfDirective == KeepRemoveMethodDirective.Type.FORCE_REMOVE) {
					//for removal
					jvmDescriptors.add jvmMethodDescriptorFromDoopId(directive.doopId)
				}
				directive.export(w)
			}
		}

		redexDirectives.withWriter { w ->
			jvmDescriptors.each {
				w.write(it)
				w.write("\n")
			}
		}

		File newAPK = new RedexRepackager(apkIn: apkIn, apkOut: apkOut, removeDirectives: redexDirectives.canonicalPath).call()

		return new Output(optimized: newAPK, directives: effectiveDirectives)
	}
	

	// Hand-rolled transformation from Doop method ids to JVM
    // descriptors. If possible, Method.facts can be used instead to
    // generate the descriptor.
    public static String jvmMethodDescriptorFromDoopId(String doopId) {
        if (!doopId || !doopId.startsWith("<") || !doopId.endsWith(">")) {
            throw new RuntimeException("Malformed Doop id [1]: $doopId.")
        }

        int colon = doopId.indexOf(": ")
        int lParen = doopId.indexOf('(');
        int rParen = doopId.indexOf(')', lParen);
        if ((colon == -1) || (lParen == -1) || (rParen == -1)) {
            throw new RuntimeException("Malformed Doop id [2]: $doopId.")
        }

        String className = doopId[1..colon-1]
        int nextSpace = doopId.indexOf(' ', colon + 2)
        if (nextSpace == -1) {
            throw new RuntimeException("Malformed Doop id [3]: $doopId.")
        }
        String retType = doopId[colon+2..nextSpace-1]
        String simpleName = doopId[nextSpace+1..lParen-1]
        List<String> params
        if (rParen == lParen + 1) {
            params = []
        } else {
            params = doopId[lParen + 1..rParen - 1].split(',').toList() 
        }
        String sig = "(" + (params.collect { jvmDescriptorFromType(it) }.join('')) + ")" + jvmDescriptorFromType(retType)
        return "L" + slashify(className) + ";:" + simpleName + sig;
    }

    private static final Map PRIMITIVES = [ "double" : "D",
                                            "float" : "F",
                                            "int" : "I",
                                            "byte" : "B",
                                            "short" : "S",
                                            "char" : "C",
                                            "boolean" : "Z",
                                            "long" : "J",
                                            "void" : "V" ]

    static String jvmDescriptorFromType(String t) {
        String ret = ""
        List<String> components = t.tokenize('[' as char)
        int arrayNesting = components.size() - 1
        arrayNesting.times { ret += "[" }

        ret += PRIMITIVES[components[0]] ?: "L" + slashify(components[0]) + ";"
        return ret
    }

    static String slashify(String s) {
        s.replace('.'.charAt(0), '/'.charAt(0))
    }
    
}