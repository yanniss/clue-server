package org.clyze.server.web.analysis

class ServerSideAnalysisMetrics {
	// Size metrics
	long appClasses
	long totalClasses
	long appMethods
	long totalMethods
	long totalFields
	long totalVariables
	long totalHeapAllocations
	long totalMethodInvocations

	// Quality metrics
	long appVirtuals
	long appVirtualsResolved
	double appVirtualsPercent
	long appMethodsReachable
	double appMethodsPercent
}
