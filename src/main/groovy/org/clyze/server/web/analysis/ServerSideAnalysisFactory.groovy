package org.clyze.server.web.analysis

import org.clyze.analysis.Analysis
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.bundle.InputBundle
import org.restlet.Request

/**
 * A factory for creating server-side analyses.
 */
interface ServerSideAnalysisFactory<A extends Analysis, B extends InputBundle> {

	B getBundle()

	ServerSideAnalysis<A> newAnalysis(Identifiers.AnalysisInfo info, Request request)
}
