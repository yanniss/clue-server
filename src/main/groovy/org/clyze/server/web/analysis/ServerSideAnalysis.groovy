package org.clyze.server.web.analysis

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import org.apache.commons.io.FileUtils
import org.clyze.analysis.Analysis
import org.clyze.persistent.model.Item
import org.clyze.server.web.AnalysisState
import org.clyze.server.web.Config
import org.clyze.server.web.Stateful
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.DefaultExporter
import org.clyze.server.web.persistent.store.Dates
import org.clyze.server.web.persistent.store.Export
import org.codehaus.groovy.runtime.StackTraceUtils

import java.util.concurrent.CancellationException
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * The server-side analysis which wraps an analysis object, offering additional functionality
 * regarding its state, its post-processor, its timeout, etc.
 * The custom inputs required per analysis are held in the input bundle, while the behavior is held in a
 * ServerSideAnalysisConfiguration object.
 */
@Log4j
class ServerSideAnalysis<A extends Analysis> implements Item, Stateful<AnalysisState> {

	String displayName
	String bundleId
	String userId
	Date created
	A analysis

	ServerSideAnalysisMetrics metrics = new ServerSideAnalysisMetrics()

	AnalysisState state
	String errorMessage
	String errorTrace
	/*
	 A Map where the key is the phase (RUNING, etc, as expressed in AnalysisState)
	 and the value the duration of the phase in seconds.

	  Only RUNNING and POST_PROCESSING phases are currently monitored.

	  In case of a non-graceful completion of the analysis, the monitored values should be ignored.
	*/
	Map<String, Long> durationsOfPhases

	protected volatile Future future

	Closure onEveryStateChange = { AnalysisState oldState, AnalysisState newState ->
		log.debug "ServerSideAnalysis $id switched from ${oldState} to ${newState}"
		// Persist the state change
		Config.instance.storage.updateAnalysis(this)
	}

	Map mostRecentMonitorValues = [:]

	Closure monitorClosure = { Map values ->
		mostRecentMonitorValues = [:] << values
		mostRecentMonitorValues.remove("PID")
		mostRecentMonitorValues.remove("USER")
		mostRecentMonitorValues.remove("COMMAND")
	}

	//This will run after the analysis has reached its FINISHED state
	Closure onFinish = null

	/**
	 * Empty constructor, used when loading the instance from the datastore.
	 * In this case, the initialization logic is carried out by the fromJSON() method.
	 */
	ServerSideAnalysis() {}

	/**
	 * Constructor used by the server-side analysis factories.
	 * In this case, we have a brand new analysis, not yet stored in the datastore.
	 */
	ServerSideAnalysis(String bundleId, String userId, A analysis) {
		this.bundleId    = bundleId
		this.userId      = userId
		this.analysis    = analysis
		this.created     = new Date()
		this.state       = AnalysisState.READY
	}

	@Override
	String toString() { toJSON() }

	/*
	 * #######################
	 * Start of Item interface
	 */

	@Override
	String getId() { analysis.id }

	@Override
	void setId(String id) { 
		//do nothing 
	}

	@Override
	String toJSON() { JsonOutput.toJson(toMap()) }

	@Override
	ServerSideAnalysis fromJSON(String json) {
		def map = new JsonSlurper().parseText(json)

		String bundleId = map.bundleId as String
		DoopInputBundle bundle = Config.instance.storage.loadInputBundle(bundleId)
		this.displayName = map.displayName
		this.analysis = bundle.initializeAnalysis(map)
		this.created = Dates.parse(map.created)
		this.errorMessage = map.errorMessage
		this.errorTrace = map.errorTrace
		this.bundleId = bundleId
		this.userId = map.userId
		this.metrics = map.metrics
		this.durationsOfPhases = map.durationsOfPhases

		validateDatastoreState(map.state)

		return this
	}

	@Override
	Map<String, Object> toMap() {
		return [
				id               : analysis.id,
				displayName      : displayName,
				bundleId         : bundleId,
				userId			 : userId,
				lowercase_id     : analysis.id.toLowerCase(),
				created          : Dates.format(created),
				anName           : analysis.name,
				anOptions        : analysis.options.collectEntries { [(it.key): it.value.value as String] },
				family           : analysis.family.name,
				state            : state.name(),
				errorMessage     : errorMessage,
				errorTrace       : errorTrace,
				metrics          : metrics,
				durationsOfPhases: durationsOfPhases,
				allText          : null //computeAllTextField()
		]
	}

	protected String computeAllTextField() {
		/* TODO: Move this to InputBundle?
		def dot = '.'.charAt(0)
		def dash = '-'.charAt(0)
		def under = '_'.charAt(0)
		def space = ' '.charAt(0)
		return [this.orgName, this.projectName, this.projectVersion].findAll { it }.collect {
			[it, it.replace(dot, space).replace(dash, space).replace(under, space)].join(" ")
		}.join(" ")
		*/
	}

	/*
	 * End of Item interface
	 * #####################
	 */

	/*
	 * ###########################
	 * Start of Stateful interface
	 */

	@Override
	AnalysisState getState() { state }

	@Override
	void changeStateTo(AnalysisState newState, Closure closure = null) throws IllegalStateException {
		if (state.transitionAllowed(newState)) {
			monitorDurationOf(newState)
			AnalysisState oldState = state
			state = newState
			onEveryStateChange.call(oldState, newState)
			closure?.call()
		} else {
			throw new IllegalStateException("Cannot switch state from: $state to $newState")
		}
	}

	protected void monitorDurationOf(AnalysisState newState) {
		if (!durationsOfPhases) {
			durationsOfPhases = [:]
		}
		if (newState.isRunning()) {
			//transition into a running state, start the clock
			durationsOfPhases[newState.toString()] = System.currentTimeMillis()
		} else if (state.isRunning()) {
			//transition from a running state, stop the clock
			Long startedAt = durationsOfPhases[state.toString()]
			if (startedAt) {
				def duration = (System.currentTimeMillis() - startedAt) / 1000
				log.debug "Duration of phase $state: $duration secs"
				durationsOfPhases[state.toString()] = duration
			}
		}
	}

	void validateDatastoreState(String stateValueFromDatastore) {
		AnalysisState s
		try {
			s = AnalysisState.valueOf(stateValueFromDatastore)
		} catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.warn(e.getMessage(), e)
			state = AnalysisState.ERROR
			errorMessage = "Invalid stored state ($stateValueFromDatastore)"
			errorTrace = null
		}

		if (s) {
			if (s.isRunning()) {
				log.warn "Analysis was stored in invalid state ($stateValueFromDatastore)"
				state = AnalysisState.ERROR
				errorMessage = "Invalid stored state ($stateValueFromDatastore)"
				errorTrace = null
			} else {
				state = s
			}
		}
	}
	/*
	 * End of Stateful interface
	 * #########################
	 */

	/*
	 * #########################
	 * Start of Analysis actions
	 */

	synchronized void start() {
		if (future) {
			throw new RuntimeException("Internal error - attempted to start analysis ${analysis.id} while it is already running.")
		}

		changeStateTo(AnalysisState.RUNNING) {
			//submit a runnable to execute the analysis
			future = Config.instance.executor.submit(new Runnable() {
				@Override
				void run() {
					analysis.monitorClosure = monitorClosure
					log.info "Running analysis ${analysis.id}"
					def now = System.currentTimeMillis()
					analysis.run()
					def duration = ((System.currentTimeMillis() - now) / 1000).longValue()
					log.info "Completed analysis ${analysis.id} in $duration secs"
					changeStateTo(AnalysisState.COMPLETED)
					postProcess()
				}
			})

			//start a thread to monitor the analysis execution
			Config.instance.monitorThreads.startNewThread(new Runnable() {

				@Override 
				void run() {
					try {
						future.get(Config.instance.timeoutInMinutes, TimeUnit.MINUTES)
					} catch (InterruptedException | CancellationException ce) {
						ce = StackTraceUtils.deepSanitize ce
						log.warn "Analysis ${analysis.id} has been cancelled/interrupted: ${ce.getMessage()}", ce
						changeStateTo(AnalysisState.CANCELLED)
					} catch (TimeoutException te) {
						future.cancel(true)
						te = StackTraceUtils.deepSanitize te
						log.warn "Analysis ${analysis.id} timed out: ${te.getMessage()}", te
						changeStateTo(AnalysisState.TIMED_OUT)
					} catch (Throwable t) {
						t = StackTraceUtils.deepSanitize t
						log.warn "Analysis ${analysis.id} threw an error: ${t.getMessage()}", t
						errorMessage = t.getMessage()
						changeStateTo(AnalysisState.ERROR)
					} finally {
						//nullify the future so that stop cannot cancel it (and start can run again)
						future = null
					}
				}
			}) 
		}
	}

	synchronized void stop() {
		if (future) {
			future.cancel(true)
		} else {
			throw new IllegalStateException("Cannot switch state from: $state to ${AnalysisState.CANCELLED}")
		}
	}

	void postProcess() {
		changeStateTo(AnalysisState.POST_PROCESSING) {
			try {

				DoopInputBundle bundle = Config.instance.storage.loadInputBundle(bundleId)
				bundle.postProcess(this)				
				File exportDir = new File(Config.instance.exportDir, analysis.id)
				exportDir.mkdirs()
				if (Config.instance.isExportEnabled) {
					DefaultExporter exporter = new DefaultExporter(exportDir)
					new Export(Config.instance.storage.analysisDoopStore(this), null, exporter).export()
				}
				changeStateTo(AnalysisState.FINISHED, onFinish)
			}
			catch (all) {
				log.warn "Analysis ${analysis.id} threw an error: ${all.getMessage()}", all
				errorMessage = all.getMessage()
				//errorTrace = Helper.stackTraceToString(all)
				changeStateTo(AnalysisState.ERROR)
			}
		}
	}


	synchronized void reset() {
		changeStateTo(AnalysisState.READY) {
			FileUtils.cleanDirectory(analysis.outDir)
			log.info "Reseted analysis ${analysis.id}"
		}
	}

	synchronized void restart() {
		reset()
		start()
	}
	/*
	 * End of Analysis actions
	 * #######################
	 */

	//Other methods
	Analysis get() { analysis }

	String getErrorMessage() { errorMessage }

	String getErrorTrace() { errorTrace }

	/*
	//TODO: Move the following method in a helper
	static long search(DataStore store, Map filters, Closure processor) {
		SearchLimits limits = new SearchLimits(start: 0, count: PaginationParams.MAX_COUNT)
		store.search(null, filters, null, limits, processor)
		return limits.hits
	}
	*/
}
