package org.clyze.server.web.analysis

import groovy.transform.TupleConstructor
import org.clyze.analysis.AnalysisOption

@TupleConstructor
class DecoratedAnalysisOption {

	// * If `formDescription` is not null, this will be diplayed in the form
	// * If it is null, `option.description` is used instead
	// * If it is NO_DESCRIPTION, no description will be used
	static final String NO_DESCRIPTION = "NIL"

	@Delegate AnalysisOption option

	String label

	String formDescription

	boolean isMandatory

	String toString() { option as String }

	DecoratedAnalysisOption clone() { new DecoratedAnalysisOption(option.clone(), label, formDescription, isMandatory) }
}
