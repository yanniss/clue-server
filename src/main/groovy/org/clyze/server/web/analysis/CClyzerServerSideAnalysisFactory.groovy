package org.clyze.server.web.analysis

import org.apache.commons.fileupload.FileItem
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.analysis.AnalysisFamilies
import org.clyze.analysis.AnalysisFamily
import org.clyze.analysis.AnalysisOption
import org.clyze.cclyzer.CClyzerAnalysis
import org.clyze.cclyzer.CClyzerAnalysisFactory
import org.clyze.cclyzer.CClyzerAnalysisFamily
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.auth.AuthUtils
import org.clyze.server.web.bundle.CClyzerInputBundle
import org.clyze.utils.FileOps
import org.restlet.Request
import org.restlet.ext.fileupload.RestletFileUpload

/**
 * The server-side cclyzer analysis factory.
 */
class CClyzerServerSideAnalysisFactory extends CClyzerAnalysisFactory implements ServerSideAnalysisFactory<CClyzerAnalysis, CClyzerInputBundle> {

	protected Log logger = LogFactory.getLog(getClass())

	private final CClyzerInputBundle bundle

	CClyzerServerSideAnalysisFactory(CClyzerInputBundle bundle) {
		this.bundle = bundle
	}

	CClyzerInputBundle getBundle() {
		return bundle
	}

	@Override
	CClyzerAnalysis newAnalysis(AnalysisFamily family, Map<String, AnalysisOption> options) {
		throw new RuntimeException("NOT IMPLEMENTED")
	}

	@Override
	ServerSideAnalysis<CClyzerAnalysis> newAnalysis(Identifiers.AnalysisInfo info, Request request) {
		logger.debug "Creating new cclyzer analysis to upload dir $bunlde.baseDir"

		assert AnalysisFamilies.isRegistered('cclyzer')
		AnalysisFamily family = AnalysisFamilies.get('cclyzer')

		RestletFileUpload upload = new RestletFileUpload(Config.instance.fileItemFactory)
		List<FileItem> items = upload.parseRequest(request)

		Map<String, AnalysisOption> options = [:]
		List<String> inputFiles = []
		List<String> libraryFiles = []

		family.supportedOptions().each { AnalysisOption option ->
			options.put(option.id, AnalysisOption.newInstance(option))
		}

		for (Iterator<FileItem> iter = items.iterator(); iter.hasNext();) {
			FileItem item = iter.next()
			String fieldName = item.getFieldName()

			if (item.isFormField()) {

				String value = item.getString()?.trim()

				logger.debug "Processing form field: $fieldName = $value"

				if (options.containsKey(fieldName)) {
					AnalysisOption option = options.get(fieldName)
					if (value) { //ignore empty or false values
						if (option.argName) {
							// if the option has an arg, the value of this arg defines the value of the respective
							// analysis option
							option.value = value
							logger.debug "Set option $fieldName to ${option.value}"
						} else {
							// the option has no arg and thus it is a boolean flag, enabling
							// the respective analysis option (boolean options are checkboxes in the form)
							option.value = true
							logger.debug "Toggled option $fieldName to ${option.value}"
						}
					}
					continue
				}

				logger.debug "Ignoring form field $fieldName"
			} else {
				if (fieldName == "inputFiles") {
					String inputFile = writeFile(item, bunlde.baseDir, item.getName())
					inputFiles.add inputFile
					continue
				}

				if (fieldName == "libraryFiles") {
					String libFile = writeFile(item, bunlde.baseDir, item.getName())
					libraryFiles.add libFile
					continue
				}

				logger.debug "Ignoring file field $fieldName"
			}
		}


		options.get(CClyzerAnalysisFamily.OUTPUT_DIRECTORY_OPTION).value = bunlde.baseDir.canonicalPath

		String id = UUID.randomUUID().toString()
		CClyzerAnalysis analysis = newAnalysis(family, id, 'cclyzer', options, inputFiles, libraryFiles)
		logger.debug("Created cclyzer analysis: $analysis")

		ServerSideAnalysis serverSideAnalysis = new ServerSideAnalysis(
				AuthUtils.userContext.name,
				null, //orgName
				null, //projectName
				null, //projectVersion
				null, //rating
				null, //ratingCount
				analysis
		)

		return serverSideAnalysis
	}

	private static String writeFile(FileItem item, File dir, String name) {
		def f = new File(dir, name)
		item.write(f)
		FileOps.findFileOrThrow(f, "Could not save uploaded file: $name")
		return f as String
	}
}

