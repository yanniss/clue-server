package org.clyze.server.web.analysis

import groovy.transform.TupleConstructor
import groovy.util.logging.Log4j
import org.apache.commons.fileupload.FileItem
import org.clyze.analysis.AnalysisOption
import org.clyze.doop.core.Doop
import org.clyze.doop.core.DoopAnalysis
import org.clyze.doop.core.DoopAnalysisFactory
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.resource.BundleConfigurationManager
import org.clyze.utils.FileOps
import org.restlet.Request
import org.restlet.ext.fileupload.RestletFileUpload

import java.text.SimpleDateFormat

@Log4j
@TupleConstructor
class DoopServerSideAnalysisFactory extends DoopAnalysisFactory implements ServerSideAnalysisFactory<DoopAnalysis, DoopInputBundle> {

	static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd_HH.mm.ss")

	// Keep it as a field, in order to be able to parameterize analysis out dir
	DoopInputBundle bundle	

	/**
	 * Creates a "pre-analysis", a dummy DoopAnalysis that stops at facts and generates jimple.
	 */
	DoopAnalysis newAnalysis() {
		def options = initializeDoopInvocationOptions()
		options.USER_SUPPLIED_ID.value = "00-preanalysis"
		options.X_STOP_AT_FACTS.value = bundle.factsDir.canonicalPath
		options.GENERATE_ARTIFACTS_MAP.value = "true"
		options.GENERATE_JIMPLE.value = "true"
		options.SCAN_NATIVE_CODE.value = "true"
		options.X_SERVER_CHA.value = "true"
		return newAnalysis(options, bundle.inputResolutionContext)
	}

	@Override
	ServerSideAnalysis<DoopAnalysis> newAnalysis(Identifiers.AnalysisInfo info, Request request) {
		log.debug "Creating new doop analysis in $bundle.analysesDir"

		// The name of the configSet to use for the 'keep' spec
		String configSetName = null

		def ctx = bundle.inputResolutionContext
		def options = initializeDoopInvocationOptions()

		def upload = new RestletFileUpload(Config.instance.fileItemFactory)
		def items = upload.parseRequest(request)
		items.each { FileItem item ->
			def fieldName = item.fieldName

			if (item.isFormField()) {
				def value = item.getString()?.trim()

				if (fieldName == "set") {
					configSetName = value
				}
				else if (options.containsKey(fieldName)) {
					AnalysisOption option = options[fieldName]
					if (value) { //ignore empty or false values						

						def decorated = new DecoratedAnalysisOption(option)
						def processor = FormLogicHelper.ANALYSIS_FORM_FIELD_PROCESSORS.find {
							it.supportsOption(decorated)
						}
						if (processor) {
							def oldValue = option.value
							processor.process(bundle, ctx, decorated, value)
							log.debug "Set option $fieldName from $oldValue to ${option.value}"
						} else {
							log.debug "Ignoring form field: $fieldName = $value"
						}
					} else {
						log.debug "Ignoring empty form field: $fieldName"
					}
				} else {
					log.debug "Ignoring unknown form field: $fieldName"
				}
			} else {
				log.debug "Ignoring form file: $fieldName"
			}
		}

		/*
		def id = options.USER_SUPPLIED_ID.value
		if (id) {
			id = id + "-" + DATE_FORMAT.format(new Date())
		} else {
			id = generateServerSideAnalysisID(options.ANALYSIS.value as String)
		}
		*/
		def id = info.id
		File analysisDir = new File(bundle.analysesDir, id)
		FileOps.ensureDirExistsOrThrow(analysisDir, "Could not create analysis directory $id in ${bundle.analysesDir}")

		options.USER_SUPPLIED_ID.value = id
		options.GENERATE_OPTIMIZATION_DIRECTIVES.value = "true"
		options.X_SERVER_LOGIC.value = "true"
		options.X_EXTRA_METRICS.value = "true"
		options.X_EXTEND_FACTS.value = bundle.factsDir.canonicalPath
		options.SKIP_CODE_FACTGEN.value = "true"
		options.SANITY.value = "true"
		options.X_STATS_NONE.value = "true"

		//generate and set the 'keep' spec file
		if (configSetName) {
			File keepSpec = new File(analysisDir, DoopInputBundle.KEEP_SPEC_FILE)
			new BundleConfigurationManager(info.userId, bundle, configSetName).generateAnalysisKeepSpecFile(keepSpec)
			options.KEEP_SPEC.value = keepSpec.canonicalPath
		}

		def analysis = newAnalysis(options, ctx)
		log.debug("Created doop analysis: $analysis")
		
		def a = new ServerSideAnalysis(bundle.id, info.userId, analysis)
		//log.info a.toMap()
		return a
	}

	protected Map<String, AnalysisOption> initializeDoopInvocationOptions() {
		def options = Doop.createDefaultAnalysisOptions()
		bundle.options.each { String id, DecoratedAnalysisOption decOption ->
			if (options[id]) options[id].value = decOption.value
		}
		return options
	}

	protected String generateServerSideAnalysisID(String name) {
		"${DATE_FORMAT.format(new Date())}_${name}_${UUID.randomUUID().toString()}"
	}

	@Override
	protected File createOutputDirectory(Map<String, AnalysisOption> options) {
		def id = options.USER_SUPPLIED_ID.value as String
		def outDir = FileOps.ensureDirExistsOrThrow(new File(bundle.analysesDir, id), "Could not create analysis directory $id in ${bundle.analysesDir}")
		options.OUT_DIR.value = outDir
	}
}
