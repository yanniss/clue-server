package org.clyze.server.web.restlet.resource

import org.clyze.analysis.InputType
import org.clyze.server.web.Config
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.analysis.DecoratedAnalysisOption
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.JsonRepresentation
import org.codehaus.groovy.runtime.StackTraceUtils
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()

		new JsonRepresentation([inputBundle: publicViewOf(inputBundle)])
	}

	@Override
	protected Representation delete() throws ResourceException {

		ensureUserOwnsInputBundle()

		try {
			Config.instance.storage.removeInputBundle(inputBundle)
			return new JsonRepresentation(["msg": "OK"])
		} catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}

	static Map publicViewOf(DoopInputBundle b) {

		def formOptions = FormLogicHelper.BUNDLE_FORM_OPTIONS.collectEntries {
			[(it.id): it]
		} as Map<String, DecoratedAnalysisOption>
		def optionsToShow = b.options
				.findAll { formOptions[it.key] }
				.findAll { it.value.argInputType != InputType.INPUT && it.value.argInputType != InputType.LIBRARY }
				.collect { [label: formOptions[it.key].label, value: handleIfMultiple(it.value)] }

		def artifactMapper = { [name: it.name, sizeInBytes: it.sizeInBytes] }

		return [
				id          : b.id,
				displayName : b.displayName,
				created     : b.created,
				isPublic    : b.isPublic,
				projectId   : b.projectId,
				hasProguard : hasProguard(b),
				appArtifacts: b.appArtifacts.collect(artifactMapper),
				depArtifacts: b.depArtifacts.collect(artifactMapper),
				options     : optionsToShow
		]
	}

	static String handleIfMultiple(DecoratedAnalysisOption option) {
		option.multipleValues ? option.value.join(", ") : option.value as String
	}

	static boolean hasProguard(DoopInputBundle b) {
		return new File(b.inputsDir, DoopInputBundle.PROGUARD_FILE).exists()
	}
}
