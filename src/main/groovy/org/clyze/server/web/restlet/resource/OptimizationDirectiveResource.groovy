package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config

import org.clyze.server.web.persistent.model.doop.OptimizationDirective
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

import org.clyze.server.web.restlet.JsonRepresentation

import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class OptimizationDirectiveResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserOwnsInputBundle()

		def dirId = getReqAttributeDecoded("directive")
				
		def directive = Config.instance.storage.loadOptimizationDirectiveOfBundle(inputBundle, dirId)

		new JsonRepresentation(directive.toMap())
	}

	@Override
	protected Representation put(Representation entity) throws ResourceException {
		ensureUserOwnsInputBundle()

		def dirId = getReqAttributeDecoded("directive")
		
		def directive = Config.instance.storage.loadOptimizationDirectiveOfBundle(inputBundle, dirId)

		if (directive.originType == OptimizationDirective.OriginType.ANALYSIS) {
			throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, "Modification of analysis-generated directives is not allowed")
		}

		def form = getRequest().getResourceRef().getQueryAsForm()		
		def typeOfDirective = form.getFirstValue('typeOfDirective')

		if (!KeepRemoveMethodDirective.isValidTypeOfDirective(typeOfDirective)) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid directive type: $typeOfDirective")
		}

		directive.typeOfDirective = typeOfDirective
		Config.instance.storage.updateOptimizationDirectiveOfBundle(inputBundle, directive)

		new JsonRepresentation(directive.toMap())
	}

	@Override
	protected Representation delete() throws ResourceException {
		ensureUserOwnsInputBundle()

		def dirId = getReqAttributeDecoded("directive")

		def directive = Config.instance.storage.loadOptimizationDirectiveOfBundle(inputBundle, dirId)
		if (directive.originType == OptimizationDirective.OriginType.ANALYSIS) {
			throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, "Deletion of analysis-generated directives is not allowed")
		}
		Config.instance.storage.removeOptimizationDirectiveOfBundle(inputBundle, directive)

		new JsonRepresentation([msg:'OK'])
	}


}