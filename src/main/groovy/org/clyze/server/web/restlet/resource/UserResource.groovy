package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class UserResource extends AbstractServerResource {
	/**
	 * Deletes the user with the given id
	 */
	@Override
	protected Representation delete() throws ResourceException {

		ensureUserIsAdmin()

		def requestedUser = getReqAttributeDecoded("user")

		def authManager = Config.instance.authManager
		// Forbid admin user deletion
		if (authManager.isAdmin(requestedUser)) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Cannot delete admin user")
		}

		try {
			// Delete user if exists.
			authManager.removeUser(requestedUser)
			return new JsonRepresentation(["msg": "OK"])
		}
		catch (e) {
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}

	/**
	 * Updates the password of the user with the given id.
	 *
	 * At the moment, it works only for user password property.
	 */
	@Override
	protected Representation put(Representation entity) throws ResourceException {

		ensureUserIsAuthenticated()

		def requestedUser = getReqAttributeDecoded("user")
		def form = new Form(request.entity)
		def newPassword = form.getFirstValue("newPassword")
		if (!newPassword) {
			throw new RuntimeException("New password is required")
		}

		def authManager = Config.instance.authManager
		// For the requested user, allow only admin and that same user himself,
		// to modify his password.
		//
		// Authorises here instead of filter because in the filter,
		// cannot find the "requested user" to change password for.
		if (!authManager.isAdmin(userId) && userId != requestedUser) {
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}

		try {
			if (!authManager.userExists(requestedUser)) {
				throw new RuntimeException("User '$requestedUser' does not exist")
			}
			if (userId == requestedUser) {
				def oldPassword = form.getFirstValue('oldPassword')
				if (!oldPassword) {
					throw new RuntimeException("Current password is required")
				}
				// Checks if the user gave wrong "old/current" password.
				// "authenticateUser" throws exception in case of wrong password.
				authManager.authenticateUser(userId, oldPassword)
			}
			authManager.updateUserPassword(requestedUser, newPassword)
			return new JsonRepresentation(["mgs": "OK"])

		} catch (RuntimeException e) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}
}
