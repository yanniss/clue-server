package org.clyze.server.web.restlet.api

import org.clyze.server.web.Config
import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.auth.AuthUtils
import org.clyze.server.web.auth.IsAuthenticatedCookieFilter
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.resource.AbstractServerResource
import org.restlet.data.CookieSetting
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 *
 */
class SessionResource extends AbstractServerResource {

	/**
	 *
	 * @return
	 * @throws ResourceException
	 */
	@Override
	protected Representation get() throws ResourceException {

		String username

		try {
			// If the user is authenticated, either as normal user or as "demo" user, then do nothing

			username = new IsAuthenticatedCookieFilter().authenticateUser(getRequest())
		}
		catch (Exception e) {
			// If the user is not authenticated, then fake an auto login as "demo" user

			username = "demo"

			// Set the cookie
			getResponse().getCookieSettings().add(new CookieSetting(
					0,
					IsAuthenticatedCookieFilter.COOKIE_SESSION,
					Config.instance.authManager.createSessionToken(username),
					Config.instance.webContextPath,
					null
			))
			getResponse().getCookieSettings().add(new CookieSetting(
					0,
					"username",
					username,
					"/",
					getRequest().getHostRef().getHostDomain()
			))

			AuthUtils.initUserContext username + ""
		}

		return new JsonRepresentation(["username": username])
	}

	/**
	 * todo: what happens if already authenticated?
	 *
	 * @param entity
	 * @return
	 * @throws ResourceException
	 */
	@Override
	protected Representation post(Representation entity) throws ResourceException {

		AuthManager authManager = Config.instance.authManager

		Form form = new Form(entity)
		String username = form.getFirstValue("username")

		if (!authManager.userExists(username)) {
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}

		String password = form.getFirstValue("password")
		try {
			authManager.authenticateUser(username, password)
		} catch (e) {
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}

		String sessionToken = authManager.createSessionToken(username)

		getResponse().getCookieSettings().add(new CookieSetting(
				0,
				IsAuthenticatedCookieFilter.COOKIE_SESSION,
				authManager.createSessionToken(username),
				Config.instance.webContextPath,
				null
		))
		getResponse().getCookieSettings().add(new CookieSetting(
				0,
				"username",
				username,
                "/",
                getRequest().getHostRef().getHostDomain()
		))

		AuthUtils.initUserContext(username)

		log.debug("User: $username authenticated")

		return new JsonRepresentation(["nop": "nop"])
	}

	/**
	 *
	 * @param entity
	 * @return
	 * @throws ResourceException
	 */
	@Override
	protected Representation delete() throws ResourceException {

		String token = request.getCookies().getFirstValue(IsAuthenticatedCookieFilter.COOKIE_SESSION)

		Config.instance.authManager.removeSessionToken(token)

		// set the cookie
		getResponse().getCookieSettings().add(new CookieSetting(
				0,
				IsAuthenticatedCookieFilter.COOKIE_SESSION,
				null,
				Config.instance.webContextPath,
				null,
				null,
				0,
				false
		))
		getResponse().getCookieSettings().add(new CookieSetting(
				0,
				"username",
				null,
                "/",
                getRequest().getHostRef().getHostDomain(),
				null,
				0,
				false
		))

		return new JsonRepresentation(["nop": "nop"])
	}

}
