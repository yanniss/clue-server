package org.clyze.server.web.restlet.resource

import org.clyze.analysis.AnalysisFamilies
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.analysis.CClyzerServerSideAnalysisFactory
import org.clyze.server.web.analysis.DoopServerSideAnalysisFactory
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.analysis.ServerSideAnalysisFactory
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.JsonRepresentation
import org.codehaus.groovy.runtime.StackTraceUtils
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class AnalysesResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

        ensureUserCanReadInputBundle()		

		def list = []
		//TODO: Stream results
        Config.instance.storage.givenBundle(inputBundle).searchAnalysesOfBundle { String id ->
			try {
				list << AnalysisResource.fullViewOf(Config.instance.storage.loadAnalysis(id))
			} catch (Exception e) {
				e.printStackTrace()
				log.debug "Ignoring analysis ${id}, due to exception: ${e.message}"
			}
        }
        
        return new JsonRepresentation([results:list])
	}

	@Override
	protected Representation post(Representation entity) throws ResourceException {

		ensureUserOwnsInputBundle()

		try {
			String familyName
			if(inputBundle instanceof DoopInputBundle) {
				familyName = "doop"
			}
			else { // inputBundle instanceof CClyzerInputBundle
				familyName = "cclyzer"
			}

			assert familyName
			assert AnalysisFamilies.isRegistered(familyName)

			def family = AnalysisFamilies.get(familyName)

			Identifiers.AnalysisInfo info = Identifiers.instance.newAnalysisInfo(userId, project, inputBundle)

			ServerSideAnalysisFactory factory
			if(family?.name == 'doop') {
				factory = new DoopServerSideAnalysisFactory(inputBundle)
			}
			else { // family?.name == 'cclyzer'
				factory = new CClyzerServerSideAnalysisFactory(inputBundle)
			}

			ServerSideAnalysis analysis = factory.newAnalysis(info, request)
			Config.instance.storage.storeNewAnalysis(analysis, info)
			log.debug "Created $family.name analysis ${analysis.id}"

			new JsonRepresentation([id: analysis.id])
		} catch(e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}

}
