package org.clyze.server.web.restlet

import org.clyze.server.web.Config
import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.persistent.store.DataStoreConnector

import org.restlet.representation.Representation
import org.restlet.resource.ResourceException
import org.restlet.resource.ServerResource

class CleanDeploy extends ServerResource {

	static final  List<String> USERS = [
		"yannis",
		"gbalats",
		"george.kast",
		"gfour",
		"kferles",
		"anantoni",
		"saikos",
		"ppath",
		"admin",
		"neville",
		"itsatiris",
		"dhalatsis",
		"vsam"		
	]

	static final String PROJECT = "scrap"

	@Override
	protected Representation post(Representation entity) throws ResourceException {

		def storage = Config.instance.storage

		storage.cleanDeploy {
			AuthManager manager = Config.instance.authManager
			
			USERS.each {
				//Add the user
				manager.createUser it, "${it}123"									
				//Add the default project
				storage.storeNewProject(new Project(it, PROJECT))				
			}			

			//also create a demo user (to be removed)
			manager.createUser "demo", "demo123"

		}

		return new JsonRepresentation(["msg": "OK"])
	}
}
