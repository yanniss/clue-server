package org.clyze.server.web.restlet.api

import org.clyze.server.web.auth.AutoLoginManager
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.resource.AbstractServerResource
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class CreateAutoLoginTokenResource extends AbstractServerResource {
	/**
	 * Creates a new auto-login token and returns it.
	 */
	@Override
	protected Representation post(Representation entity) throws ResourceException {

		ensureUserIsAuthenticated()

		String token = AutoLoginManager.instance.createToken()
		log.debug "Auto-login token: $token"
		return new JsonRepresentation([token: token])
	}
}
