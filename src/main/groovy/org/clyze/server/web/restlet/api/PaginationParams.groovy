package org.clyze.server.web.restlet.api

import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.resource.ResourceException

/**
 * A utility class for processing the pagination params (start, count) from a web form.
 */
class PaginationParams {

	static final int DEFAULT_START = 0
	static final int DEFAULT_COUNT = 50
	static final int MAX_COUNT = 10000

	static final String START_PARAMETER = "_start"
	static final String COUNT_PARAMETER = "_count"

	int start = DEFAULT_START
	int count = DEFAULT_COUNT

	void process(Form form) {

		start = form.getFirstValue(START_PARAMETER)?.trim()?.toInteger() ?: DEFAULT_START
		count = form.getFirstValue(COUNT_PARAMETER)?.trim()?.toInteger() ?: DEFAULT_COUNT

		//Sanitize
		if (start < 0 || count < 0) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "start < 0 || count < 0")
		}

		count = Math.min(count, MAX_COUNT)
	}
}
