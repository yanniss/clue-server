package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.analysis.DoopServerSideAnalysisFactory
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 * Accepts analysis form input (identical to AnalysesResource.post()) and carries out all required steps
 * to calculate and store the effective directives.
 * Practically, it performs a dry run -- a full "evaluation" of the config/analysis settings to offer a WYSIWYG view to the user.
 */
class AnalyzeBundleConfigurationResource extends AbstractServerResource {

    @Override
    protected Representation post(Representation entity) throws ResourceException {

        try {
            ensureUserOwnsInputBundle()

            String bundleConfigName = getReqAttributeDecoded("name")

            log.debug("Creating new analysis in $bundleConfigName")
            Identifiers.AnalysisInfo info = Identifiers.instance.newAnalysisInfo(userId, project, inputBundle)
            ServerSideAnalysis a = new DoopServerSideAnalysisFactory(inputBundle).newAnalysis(info, request)

            Config.instance.storage.storeNewAnalysis(a, info)
            log.debug "Created new analysis ${a.id} in $bundleConfigName"

            BundleConfigurationManager manager = new BundleConfigurationManager(userId, inputBundle, bundleConfigName)
            manager.runAnalysisOnBundleConfiguration(a)

            new JsonRepresentation([id: a.id])
        } catch (all) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, all.getMessage())
        }
    }


}