package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.model.doop.MethodLookup
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.InvocationValues
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class InvocationResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()

		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def invo = getReqAttributeDecoded("invo")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]
		MethodInvocation i = searchLogic.findInvocation(invo)
		if (!i) return new JsonRepresentation(res)
		if (definition) res = conform(MethodInvocation, i.toMap(), searchLogic)

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("values") != null) {
			res.values = [] as Set
			searchLogic.searchInvocationValues(invo) { String json ->
				def t = new InvocationValues().fromJSON(json) as InvocationValues
				def m = searchLogic.findMethod(t.toMethodId)
				if (m) res.values << conform(Method, m.toMap(), searchLogic)
			}
			res.values = Searcher.filterPositionDuplicates(res.values)
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("cha") != null) {
			res.cha = [] as Set
			searchLogic.searchInvocationCHAValues(invo) { String json ->
				def t = new MethodLookup().fromJSON(json) as MethodLookup
				def m = searchLogic.findMethod(t.methodId)
				if (m) res.cha << conform(Method, m.toMap(), searchLogic)
			}
			res.cha = Searcher.filterPositionDuplicates(res.cha)
		}

		return new JsonRepresentation(res)
	}

	static JsonRepresentation getDefault(AbstractServerResource resource, MethodInvocation invo, boolean expand) {
		def r = new InvocationResource()
		r.setRequest(resource.request)
		r.setResponse(resource.response)
		r.setReqAttributeEncoded("invo", invo.doopId)
		if (expand) r.setQueryValue("values", "")
		r.get() as JsonRepresentation
	}
}
