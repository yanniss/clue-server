package org.clyze.server.web.restlet.resource

import org.clyze.analysis.AnalysisFamilies
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.auth.AuthUtils
import org.clyze.server.web.bundle.CClyzerInputBundleFactory
import org.clyze.server.web.bundle.DoopInputBundleFactory
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.bundle.InputBundle
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams
import org.clyze.utils.FileOps
import org.codehaus.groovy.runtime.StackTraceUtils
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 *
 */
class BundlesResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {
		
		loadProjectFromRequestParameter(true)
		ensureUserCanReadProject()

		def form = getRequest().getResourceRef().getQueryAsForm()
		def p = new PaginationParams()
		def o = new OrderByParams()
		p.process(form)
		o.process(form)		

		def list = []
		//TODO: Stream results
		SearchLimits limits = Config.instance.storage.givenProject(project).processInputBundlesOfProject(p.start, p.count, o.orderByList) {
			String id, String type, String json ->
				list << BundleResource.publicViewOf(new DoopInputBundle().fromJSON(json))
		}

		new JsonRepresentation([start: limits.start, count: limits.count, hits: limits.hits, results: list])
	}

	@Override
	protected Representation post(Representation entity) throws ResourceException {			

		ensureUserCanPostInputBundles()

		def familyName = getQueryValue("family")
		if (!familyName) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Family missing")
		}
		if (!AnalysisFamilies.isRegistered(familyName)) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Family unknown")
		}
		def family = AnalysisFamilies.get(familyName)		

		Identifiers.BundleInfo bundleInfo = Identifiers.instance.newBundleInfo(userId, project)
		
		File uploadDir
		def user = AuthUtils.userContext
		try {								
			uploadDir = new File(user.dir, bundleInfo.id)
			uploadDir.mkdir()
			uploadDir = FileOps.findDirOrThrow(uploadDir, "Could not create dir $uploadDir")			
		} catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage())
		}

		InputBundle inputBundle
		try {
			if (family?.name == "doop") {
				inputBundle = new DoopInputBundleFactory().newInputBundle(uploadDir, bundleInfo, request)
			} else if (family?.name == "cclyzer") {
				inputBundle = new CClyzerInputBundleFactory().newInputBundle(uploadDir, bundleInfo, request)
			} else {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Family: ${family?.name}")
			}
		}
		catch(ResourceException re) {
			re = StackTraceUtils.deepSanitize re
			log.error(re.getMessage(), re)
			throw re
		}
		catch(other) {
			other = StackTraceUtils.deepSanitize other
			log.error(other.getMessage(), other)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, other.getMessage() as String)
		}

		try {
			Config.instance.storage.storeNewInputBundle(inputBundle, bundleInfo)
		} catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage())
		}

		new JsonRepresentation([id: inputBundle.id])
	}
}
