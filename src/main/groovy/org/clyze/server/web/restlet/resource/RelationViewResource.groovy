package org.clyze.server.web.restlet.resource

import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class RelationViewResource extends AbstractServerResource {

    final int MAX_RELATION_SIZE = 20000

	@Override
	protected Representation get() throws ResourceException {

		ensureUserOwnsInputBundle()
		loadAnalysisFromRequestParameter()

		def bundleId = inputBundle.id
		def relation = getQueryValue("relation")
		if (!relation) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Relation missing")
		}

		log.debug "Request view for relation = ${relation}, bundleId = ${bundleId}, analysis = ${analysis}"

		def relationFile
		if (analysis) {
			relationFile = new File(analysis.analysis.outDir, "database/${relation}.csv")
		} else {
			// We cannot check against Doop's PredicateFile here, as
			// this would reject the fact files of HeapDL (which has a
			// separate PredicateFile).
			relationFile = new File(inputBundle.factsDir, "${relation}.facts")
		}
		if (!relationFile.exists()) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Relation file does not exist: ${relationFile.canonicalPath}")
		}

		int count = 0
		List tuples = []
		relationFile.withReader { reader ->
			String line
			while (line = reader.readLine()) {
				if (count++ < MAX_RELATION_SIZE) {
					tuples << line
				} else {
					log.debug "Limit reached (${MAX_RELATION_SIZE}) for relation view."
					break
				}
			}
		}

		def data = [ name: relation, tuples: tuples ]
		new JsonRepresentation(data)
	}
}
