package org.clyze.server.web.restlet.api

import org.clyze.server.web.Config
import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.resource.AbstractServerResource
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class LoginResource extends AbstractServerResource {

	@Override
	protected Representation post(Representation entity) throws ResourceException {

		AuthManager authManager = Config.instance.authManager

		Form form = new Form(entity)
		String username = form.getFirstValue("username")

		if (!authManager.userExists(username)) {
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}

		String password = form.getFirstValue("password")
		try {
			authManager.authenticateUser(username, password)
		} catch (e) {
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}

		String sessionToken = authManager.createSessionToken(username)
		return new JsonRepresentation(["token": sessionToken])
	}
}
