package org.clyze.server.web.restlet.resource

import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class RuntimeMonitorResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserOwnsInputBundle()
		loadAnalysisFromUriComponent()

		new JsonRepresentation(analysis.mostRecentMonitorValues)
	}
}
