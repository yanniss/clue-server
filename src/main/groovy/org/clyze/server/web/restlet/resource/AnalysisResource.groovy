package org.clyze.server.web.restlet.resource

import org.clyze.analysis.AnalysisOption
import org.clyze.persistent.model.Element
import org.clyze.persistent.model.doop.HeapAllocation
import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.persistent.model.doop.Usage
import org.clyze.persistent.model.doop.Variable
import org.clyze.server.web.AnalysisState
import org.clyze.server.web.Config
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.analysis.DecoratedAnalysisOption
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.JimpleImportBase
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.Searchable
import org.clyze.server.web.restlet.JsonRepresentation
import org.codehaus.groovy.runtime.StackTraceUtils
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class AnalysisResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromUriComponent()
		
		new JsonRepresentation([analysis: fullViewOf(analysis)])
	}

	@Override
	protected Representation delete() throws ResourceException {

		ensureUserOwnsInputBundle()
		loadAnalysisFromUriComponent()

		try {					
			AnalysisState state = analysis.getState()
			if (state.isRunning()) {
				throw new IllegalStateException("Analysis $id of user $userId is still running")
			}
			Config.instance.storage.removeAnalysis(analysis)
			return new JsonRepresentation(["msg": "OK"])
		}
		catch (e) {
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}


	private static final Map<String, String> ACTION_TO_METHOD = [
			'start'       : 'start',
			'stop'        : 'stop',
			'post_process': 'postProcess',
			'reset'       : 'reset',
			'restart'     : 'restart'
	]

	/**
	 * Executes an analysis action (start, stop, post_process, reset, restart).
	 */
	@Override
	protected Representation put(Representation entity) throws ResourceException {
		
		ensureUserOwnsInputBundle()
		loadAnalysisFromUriComponent()

		def action = getReqAttributeDecoded("action")
		def method = ACTION_TO_METHOD[action]
		if (!method)
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Invalid action: $action")
		try {

			//TODO: Implement this better
			//if this is the first analysis of the bundle, start a new thread to process the remaining jimple elements
			def store = Config.instance.storage.bundleDoopStore(inputBundle)
			if(method == 'start' && store.count(BooleanQueryBuilder.build('_type', Variable.simpleName)) == 0) {
				def bulk = new BulkProcessor(Config.instance.bulkSize, store)
				def typesToImport = [Variable.simpleName, Usage.simpleName, HeapAllocation.simpleName, MethodInvocation.simpleName] as Set
				def importer = new JimpleImportBase(inputBundle.jimpleDir, inputBundle.sourceClasses, typesToImport) {
					@Override
					void importElement(Element e) {
						bulk.scheduleSave(e)
					}
				}

				Config.instance.postProcessExecutor.submit(new Runnable() {
					@Override
					void run() {
						importer.importJimple()
					}
				})
			}

			analysis."$method"()
			return new JsonRepresentation(["state": analysis.state.name()])
		}
		catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.getMessage(), e)
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, e.getMessage())
		}
	}

	static Map shortViewOf(ServerSideAnalysis a) {
		return [
			id			: a.getId(),
			name		: a.get().name,
			displayName : a.displayName,
			created     : a.created,
			state		: a.getState().name()			
		]
	}

	static Map fullViewOf(ServerSideAnalysis a) {
		def formOptions = FormLogicHelper.ANALYSIS_FORM_OPTIONS.collectEntries { [(it.id): it] } as Map<String, DecoratedAnalysisOption>
		def optionsToShow = a.analysis.options
				.findAll { formOptions[it.key] }
				.collect { [label: formOptions[it.key].label, value: handleIfMultiple(it.value)] }
		def map = a.toMap()
		map.anOptions = optionsToShow
		return map
	}

	static String handleIfMultiple(AnalysisOption option) {
		option.multipleValues ? option.value.join(", ") : option.value as String
	}
}
