package org.clyze.server.web.restlet.resource

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.Config
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.auth.AuthUtils
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.AccessControl
import org.codehaus.groovy.runtime.StackTraceUtils
import org.restlet.resource.ResourceException
import org.restlet.resource.ServerResource

import org.restlet.data.Status

/**
 * The common ancestor of all clue server-side resources.
 * This class (as all restlet ServerResources) is meant to be used by a single thread.
 */
class AbstractServerResource extends ServerResource implements AccessControl {

	protected Log log = LogFactory.getLog(getClass())

	DoopInputBundle inputBundle

	ServerSideAnalysis analysis

	Project project

	String userId

	// This should be called after any ensure* or load* methods
	def getBaseFilter() { 
		def rootElemFilter = [inputBundle?.id, analysis?.id]
		return [rootElemId: rootElemFilter.findAll()] 
	}

	//////////////////////////////////////////////////////////////////
	// User-related public methods
	//////////////////////////////////////////////////////////////////

	@Override
	void ensureUserIsAuthenticated() throws ResourceException {
		if (userId) return

		if (AuthUtils.isAuthenticated()) {
			userId = AuthUtils.userContext.name
		} else {
			log.warn("User is not authenticated")
			throwUnauthorized()
		}
	}

	@Override
	void ensureUserIsAdmin() throws ResourceException {
		ensureUserIsAuthenticated()
		if (userId != "admin") {
			log.warn("User is not admin")
			throwUnauthorized()
		}
	}

	//////////////////////////////////////////////////////////////////
	// InputBundle-related methods
	//////////////////////////////////////////////////////////////////

	@Override
	void ensureUserCanPostInputBundles() throws ResourceException {
		ensureUserIsAuthenticated()		
		
		if (userId == "demo") {
			log.warn("User cannot post input bundle")
			throwUnauthorized()
		}

		loadProjectFromRequestParameter(true)

		doEnsureUserIsOwnerOrMemberOfProject()
	}

	@Override
	void ensureUserOwnsInputBundle() throws ResourceException {
		ensureUserIsAuthenticated()

		if (!inputBundle) loadInputBundle()		
		
		loadProject(inputBundle.projectId)

		doEnsureUserIsOwnerOrMemberOfProject()
	}

	@Override
	void ensureInputBundleIsPublic() throws ResourceException {
		if (!inputBundle) loadInputBundle()

		if (!inputBundle.isPublic) {
			log.warn("Input bundle is not public")
			throwUnauthorized()
		}
	}

	@Override
	void ensureUserCanReadInputBundle() throws ResourceException {
		ensureUserIsAuthenticated()

		try {
			ensureUserOwnsInputBundle()
		}
		catch (all) {
			ensureInputBundleIsPublic()
		}
	}

	@Override
	void ensureUserCanPostProjects() throws ResourceException {		
		ensureUserIsAuthenticated()		

		if (userId == "demo") {
			throwUnauthorized()
		}
	}

	@Override
	void ensureUserOwnsProject() throws ResourceException {
		ensureUserIsAuthenticated()

		if (!project) loadProjectFromUriComponent()

		doEnsureUserOwnsProject()
	}

	@Override
	void ensureUserCanReadProject() throws ResourceException {		
		ensureUserIsAuthenticated()

		if (!project) loadProjectFromUriComponent()

		doEnsureUserIsOwnerOrMemberOfProject()
	}

	protected void doEnsureUserIsOwnerOrMemberOfProject() {
		if (project.owner == userId || project.members.contains(userId)) {
			log.debug("User $userId is owner/member of project ${project.owner}/${project.name}")
		}
		else {
			throwUnauthorized()
		}
	}

	protected void doEnsureUserOwnsProject() {
		if (project.owner != userId) {
			throwUnauthorized()
		}
	}

	protected void loadInputBundle() {
		def id = getReqAttributeDecoded("bundle")
		try {
			inputBundle = Config.instance.storage.loadInputBundle(id)
		}
		catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.message, e)
			throwInvalidId("input bundle", id)
		}
	}

	protected void loadProjectFromUriComponent() {
		String id = getReqAttributeDecoded("project")				
		loadProject(id)		
	}

	protected void loadProjectFromRequestParameter(failIfNotPresent = false) {
		String id = getQueryValue("project")
		if (id) {
			//Check if the project has been given in the form "{userName}/{projectName}"	
			int slashIndex = id.indexOf('/')
			int len = id.length()
			if (slashIndex > 0 && slashIndex < len - 1){ 						
				String owner = id.substring(0, slashIndex)
				String name = id.substring(slashIndex + 1)
				project = Config.instance.storage.findProject(owner, name)				
				if (project == null) {
					throwNotFound("Project ${id}")
				}
			}
			else {
				loadProject(id)
			}
		}
		else {
			if (failIfNotPresent) {
				log.error("No project provided in query parameter but failIfNotPresent = true")
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Project is missing")
			}
			else {
				log.warn("No project provided in query parameter")
			}
		}
	}

	protected void loadProject(String id) {		
		try {
			project = Config.instance.storage.loadProject(id)
		}
		catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.message, e)
			throwInvalidId("project", id)
		}
	}

	//////////////////////////////////////////////////////////////////
	// Analysis-related methods
	//////////////////////////////////////////////////////////////////

	protected void loadAnalysisFromUriComponent() {
		String id = getReqAttributeDecoded("analysis")
		loadAnalysis(id)
	}

	protected void loadAnalysisFromRequestParameter(failIfNotPresent = false) {
		String id = getQueryValue("analysis")
		if (id) {
			loadAnalysis(id)
		}
		else {
			if (failIfNotPresent) {
				log.error("No analysis provided in query parameter but failIfNotPresent = true")
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Analysis is missing")
			}
			else {
				log.warn("No analysis provided in query parameter")
			}
		}
	}

	protected void loadAnalysis(String id) {		
		try {
			analysis = Config.instance.storage.loadAnalysis(id)
		}
		catch (e) {
			e = StackTraceUtils.deepSanitize e
			log.error(e.message, e)
			throwInvalidId("analysis", id)
		}
	}


	//////////////////////////////////////////////////////////////////
	// Other helper methods
	//////////////////////////////////////////////////////////////////

	protected void setReqAttributeEncoded(String name, Object value) {
		requestAttributes[name] = URLEncoder.encode(value, "UTF-8")
	}

	protected String getReqAttributeDecoded(String name) {
		String value = requestAttributes[name]
		return value ? URLDecoder.decode(value, "UTF-8") : null
	}

	protected void baseChecks() {
		if (!AuthUtils.isAuthenticated()) {
			throwUnauthorized()
		}
	}

	//////////////////////////////////////////////////////////////////
	// Common exceptions
	//////////////////////////////////////////////////////////////////

	static protected void throwUnauthorized() {
		throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
	}

	static protected void throwInvalidId(entityName, entityId) {
		throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST,
			"Invalid ${entityName} id: ${entityId}"
		)
	}

	static protected void throwMissingQueryParam(paramName, paramValidValues) {
		throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST,
			"Missing query parameter \"${paramName}\"\n" + 
			paramValidValues ? "Possible values: {${paramValidValues.join(",")}" : ""
		)
	}

	static protected void throwInvalidQueryParamValue(paramName, paramGivenValue, paramValidValues) {
		throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST,
			"Invalid query parameter \"${paramName}\" value: \"${paramGivenValue}\".\n" +
			"Possible values: {${paramValidValues.join(",")}}"
		)
	}

	static protected void throwNotFound(String message) {
		throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, message)
	}

}
