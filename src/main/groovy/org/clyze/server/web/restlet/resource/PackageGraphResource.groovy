package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.model.doop.PackageCallsPackage
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class PackageGraphResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()
        
        Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def res = [:].withDefault { [] as Set }
		searchLogic.searchPackageCallsPackage() { String json ->
			def p = new PackageCallsPackage().fromJSON(json) as PackageCallsPackage
			res[p.fromPackage] << p.toPackage
		}

		return new JsonRepresentation(["list": res.collect { from, to -> ["name": from, "size": 1, "imports": to] }])
	}

}
