package org.clyze.server.web.restlet.resource

import org.clyze.doop.common.android.AndroidSupport
import org.clyze.persistent.model.doop.Artifact
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class FileInArtifactResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		
		Searcher searchLogic = Config.instance.storage.givenBundle(inputBundle)

		def artifact = getValidatedArtifactFromURI()
		def file = getValidatedFileFromURI(artifact)
		def results

		//Check whether the file is actually a sub-artifact (e.g. a DEX file)				
		def dexArtifact = lookupForSubArtifact(artifact, file.name)

		if (dexArtifact) {
			results = getDexContents(searchLogic, dexArtifact)
		} else {
			if (file.isDirectory()) {
				results = getDirectoryContents(searchLogic, artifact, file)
			} else {
				results = getFileContents(artifact, file)
			}
		}

		new JsonRepresentation(results)
	}


	protected Artifact lookupForSubArtifact(Artifact parent, String fileName) {
		String dexArtifactName = "${parent.name}/${fileName}"
		return inputBundle.appArtifacts.find { it.name == dexArtifactName && it.parentArtifactId == parent.id }
	}

	//////////////////////////////////////////////////////////////////
	// Input validation methods
	//////////////////////////////////////////////////////////////////

	private Artifact getValidatedArtifactFromURI() {

		def artifactName = getReqAttributeDecoded("artifact") ?: ""
		def artifact = inputBundle.findArtifactByName(artifactName)	

		if (!artifact) {
			throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, artifactName)
		}

		return artifact
	}

	private File getValidatedFileFromURI(Artifact artifact) {

		String fileName = getRequestedFile()
		if (fileName.contains("..") || fileName.startsWith("/")) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "File path cannot contain '..' or start with '/'")
		}

		def file = new File(inputBundle.inputsExtractedDir, "${artifact.name}/${fileName}")
		if (!file.exists()) {
			throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, fileName)
		}

		return file
	}

	private String getRequestedFile() {

		return getReqAttributeDecoded("file") ?: ""
	}

	//////////////////////////////////////////////////////////////////
	// Response data methods
	//////////////////////////////////////////////////////////////////

	private Map getFileContents(Artifact artifact, File file) {

		def directory

		if (file.name.endsWith(".java") || file.name.endsWith(".kt")) {
			directory = inputBundle.sourcesDir
		} else if (file.name.endsWith(".shimple") || file.name.endsWith(".jimple")) {
			directory = inputBundle.jimpleDir
		} else if (file.name.endsWith(".smali")) {
			directory = new File(inputBundle.factsDir, AndroidSupport.DECODE_DIR)
		} else {
			directory = inputBundle.inputsExtractedDir
		}

		return [content: new File(directory, "${artifact.name}/${getRequestedFile()}").getText('UTF-8')]
	}

	private Map getDirectoryContents(Searcher searchLogic, Artifact artifact, File directory) {

		traverse(searchLogic) { def prefix, def nodes ->

			def typeNodeFileNames = []

			for (File child : directory.listFiles()) {
				if (child.isDirectory()) {
					nodes.directories << createSimpleNode(prefix, child.name)
				} else if (child.name.endsWith(".class")) {
					def nameWithoutExtension = child.name[0..-(".class".length() + 1)]
					def sourceFileName = prefix + nameWithoutExtension + (artifact.sourcesName ? ".java" : ".shimple")

					// "sourceFileName" is "GString". Conversion to "String" is required because otherwise elastic crashes.
					typeNodeFileNames << sourceFileName.toString()
				} else if (child.name.endsWith(".dex")) {
					def dexArtifact = lookupForSubArtifact(artifact, child.name)
					if (dexArtifact) {
						nodes.directories << createSimpleNode(prefix, child.name)
					} else {
						nodes.simpleFiles << createSimpleNode(prefix, child.name)
					}
				} else {
					nodes.simpleFiles << createSimpleNode(prefix, child.name)
				}
			}

			if (typeNodeFileNames.size() > 0) {
				searchLogic.searchSymbolsOfSourceFiles(typeNodeFileNames, Klass) { String json ->
					def klass = new Klass().fromJSON(json)
					def fileName = klass.sourceFileName.tokenize("/").last()
					def publicTypeName = fileName.take(fileName.lastIndexOf("."))

					if (klass.name == publicTypeName) {
						nodes.types << conform(Klass, klass.toMap(), searchLogic)
					}
				}
			}
		}
	}

	private Map getDexContents(Searcher searchLogic, Artifact dexArtifact) {

		traverse(searchLogic) { def prefix, def nodes ->

			searchLogic.searchClassesOfArtifact(dexArtifact.name) { String json ->
				def klass = new Klass().fromJSON(json)
				nodes.types << conform(Klass, klass.toMap(), searchLogic)
			}
		}
	}


	private Map traverse(Searcher searchLogic, Closure traverser) {
		def prefix = getRequestedFile() ? "${getRequestedFile()}/" : ""

		def nodes = [
				directories: [],
				types      : [],
				simpleFiles: [],
		]

		traverser.call(prefix, nodes)

		return nodes
	}

	private static Map createSimpleNode(String prefix, String name) {

		return [
				name          : name,
				sourceFileName: prefix + name
		]
	}

}
