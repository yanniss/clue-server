package org.clyze.server.web.restlet.resource

import org.clyze.server.web.bundle.BundleConfiguration
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleConfigurationResource extends AbstractServerResource {

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")

        BundleConfiguration bundleConfig = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).loadBundleConfiguration()

        return Responder.getBundleConfig(bundleConfig)

    }
}
