package org.clyze.server.web.restlet.resource

import org.clyze.server.web.persistent.model.doop.PackageCallsPackage
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.store.DataStore
import org.restlet.data.MediaType
import org.restlet.representation.Representation
import org.restlet.representation.OutputRepresentation
import org.restlet.resource.ResourceException

import net.sourceforge.plantuml.SourceStringReader

class UMLPackageDiagramResource extends AbstractServerResource {

	private static final boolean SHOW_SAME_PACKAGE_CALLS = false

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()

        Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		Set<String> uniquePackagePairs = [] as Set		

		//all package regex entries end with .*
		List<String> appPackages = analysis.get().options.APP_REGEX.value.split(":").collect { it.replaceAll("\\.\\*", "") }		
		int minPackageDepth = Integer.MAX_VALUE
		int maxPackageDepth = 0
		appPackages.each { String packagePrefix -> 
			def depth = packagePrefix.split("\\.").size() 
			if (depth <= minPackageDepth) {
				minPackageDepth = depth
			}
			if (depth >= maxPackageDepth) {
				maxPackageDepth = depth
			}
		}					

		int depth = minPackageDepth + 1
		Set<String> allowedPackages = appPackages.collect { 
			def parts = it.split("\\.")
			int sz = parts.size()
			if (sz <= depth) {
				return it
			}
			else {
				parts[(0..depth-1)].join(".")
			}
		} as Set
			
		PrefixBasedDiagramRenderer renderer = new PrefixBasedDiagramRenderer(allowedPackages:allowedPackages)
		searchLogic.searchPackageCallsPackage() { String json ->
			def p = new PackageCallsPackage().fromJSON(json) as PackageCallsPackage			
			renderer.process(p)
		}		

		final String uml = "@startuml\n" + renderer.render() + "\n@enduml"
		return new OutputRepresentation(MediaType.IMAGE_PNG) {
			@Override
			public void write(OutputStream outputStream) throws IOException {
		        SourceStringReader r = new SourceStringReader(uml)
		        r.outputImage(outputStream)
		    }
		}
	}

	static interface PackageDiagramRenderer {		

		void process(PackageCallsPackage p)

		String render()
	}

	static abstract class AbstractPackageDiagramRenderer implements PackageDiagramRenderer {
				
		Set<String> uniquePackagePairs = [] as Set		

		String render() {
			return uniquePackagePairs.join("\n")
		}		
	}

	static class PrefixBasedDiagramRenderer extends AbstractPackageDiagramRenderer {
				
		Collection allowedPackages

		void process(PackageCallsPackage p) {
			String fromPackagePrefix = allowedPackages.findAll { p.fromPackage.startsWith(it) }.max { it.length() }
			String toPackagePrefix   = allowedPackages.findAll { p.toPackage.startsWith(it) }.max { it.length() }			
			if (fromPackagePrefix && toPackagePrefix && (SHOW_SAME_PACKAGE_CALLS || fromPackagePrefix != toPackagePrefix)) {
				String pair = "[${fromPackagePrefix}] --> [${toPackagePrefix}]"
				if (!uniquePackagePairs.contains(pair)) {
					uniquePackagePairs.add pair
				}
			}
		}

		String render() {
			allowedPackages.collect { "[$it]"}.join("\n") + "\n" + super.render()
		}
	}
}
