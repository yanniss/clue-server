package org.clyze.server.web.restlet.api

import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.persistent.store.Ordering
import org.restlet.data.Form

/**
 * A utility class for processing the order by params from a web form.
 * The params are encoded in the _sort form field as follows:
 * _sort=field|ordering
 * where the _sort form field can occur zero or more times.
 */
class OrderByParams {

	static final String SORT_PARAMETER = "_sort"

	List<OrderBy> orderByList = []

	void process(Form form) {
		orderByList = form.getValuesArray(SORT_PARAMETER).collect { String s ->
			def (field, ordering) = s.tokenize('|')
			return new OrderBy(field: field, ordering: Enum.valueOf(Ordering, ordering.toUpperCase()))
		}
	}
}
