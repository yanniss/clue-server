package org.clyze.server.web.restlet.resource

import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Form
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleConfigurationRuleResource extends AbstractServerResource {

    @Override
    protected Representation put(Representation entity) throws ResourceException {

        ensureUserOwnsInputBundle()

        String configSetName = getReqAttributeDecoded("name")
        String ruleId = getReqAttributeDecoded("id")
        Form form = new Form(entity)
        String body = form.getFirstValue("ruleBody")
        String comment = form.getFirstValue("comment")

        log.debug("Update rule $ruleId with comment: $comment and body: $body")

        PersistentBundleConfigurationRule rule = new BundleConfigurationManager(userId, inputBundle, configSetName).
                updateRule(ruleId, body, comment)

        return new JsonRepresentation(rule.toMap())

    }

    @Override
    protected Representation delete() throws ResourceException {

        ensureUserOwnsInputBundle()

        String configSetName = getReqAttributeDecoded("name")
        String ruleId = getReqAttributeDecoded("id")

        new BundleConfigurationManager(userId, inputBundle, configSetName).removeRule(ruleId)

        return new JsonRepresentation([id: ruleId])
    }


}
