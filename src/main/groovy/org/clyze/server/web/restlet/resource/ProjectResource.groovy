package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.model.Project
import org.clyze.server.web.restlet.JsonRepresentation

import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class ProjectResource extends AbstractServerResource {	

	@Override
	protected Representation get() throws ResourceException {
		
		ensureUserCanReadProject()

		return new JsonRepresentation(publicViewOf(project))
	}

	@Override
	protected Representation put(Representation entity) throws ResourceException {
		
		ensureUserOwnsProject()

		Form form = new Form(entity)

		String name          = form.getFirstValue("name")
		List<String> members = form.getValuesArray("members").toList()

		if (!name && !members) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}

		if (name) {
			project.name = name
		}
		if (members) {
			project.members = members
		}

		Config.instance.storage.updateProject(project)

		return new JsonRepresentation(publicViewOf(project))
	}

	static Map publicViewOf(Project project) {
		return [
			id      : project.id,
			name    : project.name,
			created : project.created,			
			owner   : project.owner,
			members : project.members
		]
	}

}