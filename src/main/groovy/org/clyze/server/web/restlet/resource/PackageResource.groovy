package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Class
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class PackageResource extends AbstractServerResource {

	private static final VALID_CONTEXTS = [
			APPLICATION : "application",
			DEPENDENCIES: "dependencies",
			PLATFORM    : "platform",
	]

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		
		Searcher searchLogic = Config.instance.storage.givenBundle(inputBundle)

		def contextPackages = getContextPackages(getValidatedContextFromURI())		
		def packageName = getValidatedPackageFromURI(contextPackages)		

		def results = [
				packages: getPackages(packageName, contextPackages),
				types   : getTypes(searchLogic, packageName, contextPackages),
		]

		return new JsonRepresentation(results)
	}

	//////////////////////////////////////////////////////////////////
	// Input validation methods
	//////////////////////////////////////////////////////////////////

	private String getValidatedContextFromURI() {

		def context = getReqAttributeDecoded("context")

		if (context == null) {
			return null
		} else if (VALID_CONTEXTS.containsValue(context)) {
			return context
		} else {
			throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, context)
		}
	}

	private String getValidatedPackageFromURI(Set<String> contextPackages) {

		def packageName = getReqAttributeDecoded("package") ?: ""
		def exists = contextPackages.any { it == packageName || isDescendantPackage(it, packageName) }

		if (!isDefaultPackage(packageName) && !exists) {
			throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, packageName)
		}

		return packageName
	}

	private Set<String> getContextPackages(String context) {

		def contextPackages

		if (context == VALID_CONTEXTS.APPLICATION) {
			contextPackages = inputBundle.appArtifacts
		} else if (context == VALID_CONTEXTS.DEPENDENCIES) {
			contextPackages = inputBundle.depArtifacts
		} else if (context == VALID_CONTEXTS.PLATFORM) {
			contextPackages = inputBundle.platformArtifacts
		} else {
			contextPackages = inputBundle.with {
				inputBundle.appArtifacts + inputBundle.depArtifacts + inputBundle.platformArtifacts
			}
		}

		return contextPackages.inject([] as Set) { packages, artifact -> packages + artifact.packages }
	}

	//////////////////////////////////////////////////////////////////
	// Response data methods
	//////////////////////////////////////////////////////////////////

	private static Set<String> getPackages(String packageName, Set<String> contextPackages) {

		def partsNum = packageName.tokenize(".").size()
		def packages = contextPackages.findAll {
			isDescendantPackage(it, packageName)
		}.collect {
			it.tokenize(".")[partsNum]
		}.unique()

		return packages
	}

	private Collection<Class> getTypes(Searcher searchLogic, String packageName, Set<String> contextPackages) {

		def types = []

		searchLogic.searchClassesOfPackage(packageName) { String json ->
			def klass = new Class().fromJSON(json)

			if (contextPackages.contains(klass.packageName)) {
				types << conform(Class, klass.toMap(), searchLogic)
			}
		}

		return types
	}

	//////////////////////////////////////////////////////////////////
	// Helper methods
	//////////////////////////////////////////////////////////////////

	private static boolean isDefaultPackage(String packge) {

		return packge == ""
	}

	private static boolean isDescendantPackage(String descendant, String ancestor) {

		return isDefaultPackage(ancestor) ? !isDefaultPackage(descendant) : descendant.startsWith("${ancestor}.")
	}

}
