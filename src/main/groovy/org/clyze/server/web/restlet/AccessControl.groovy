package org.clyze.server.web.restlet

import org.restlet.resource.ResourceException

/**
 * A simple Access Control. 
 * Each method either succeeds or fails with an exception.
 */
interface AccessControl {

	void ensureUserIsAuthenticated() throws ResourceException

	void ensureUserOwnsInputBundle() throws ResourceException

	void ensureInputBundleIsPublic() throws ResourceException

	void ensureUserCanPostInputBundles() throws ResourceException	

	void ensureUserCanReadInputBundle() throws ResourceException

	void ensureUserIsAdmin() throws ResourceException

	void ensureUserCanPostProjects() throws ResourceException

	void ensureUserOwnsProject() throws ResourceException

	void ensureUserCanReadProject() throws ResourceException	
}
