package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.doop.Variable
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.Value
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class ClassResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()
		
		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def klass = getReqAttributeDecoded("class")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]
		Klass k = searchLogic.findClass(klass)
		if (!k) return new JsonRepresentation(res)
		if (definition) res = conform(Klass, k.toMap(), searchLogic)

		///////////////////////////////////////////////////////////////////////
		def hierarchy = getQueryValue("hierarchy")
		if (hierarchy) {
			res.hierarchy = [interfaces: [] as Set, classes: [] as Set]
			def processKlass = { Klass kl ->
				res.hierarchy[kl.isInterface ? "interfaces" : "classes"] << conform(Klass, kl.toMap(), searchLogic)
			}
			if (hierarchy == "subTypes") {
				searchLogic.findSubTypes(klass).each(processKlass)
			} else if (hierarchy == "superTypes") {
				searchLogic.findSuperTypes(klass).each(processKlass)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("fields") != null) {
			res.fields = searchLogic.findAllFieldsOfThis(klass).collect { conform(Field, it.toMap(), searchLogic) }
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("allocationsOfType") != null) {
			res.allocationsOfType = searchLogic.findAllocationsOfType(klass).collect {
				conform(Value, it.toMap(), searchLogic)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("fieldsOfType") != null) {
			res.fieldsOfType = searchLogic.findFieldsOfType(klass).collect {
				conform(Field, it.toMap(), searchLogic)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("varsOfType") != null) {
			res.varsOfType = searchLogic.findVariablesOfType(klass).collect {
				conform(Variable, it.toMap(), searchLogic)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("methods") != null) {
			res.methods = searchLogic.findDeclaringMethods(klass).collect {
				conform(Method, it.toMap(), searchLogic)
			}
		}

		new JsonRepresentation(res)
	}

	static JsonRepresentation getDefault(AbstractServerResource resource, Klass klass) {
		def r = new ClassResource()
		r.setRequest(resource.request)
		r.setResponse(resource.response)
		r.setReqAttributeEncoded("class", klass.doopId)
		r.get() as JsonRepresentation
	}
}
