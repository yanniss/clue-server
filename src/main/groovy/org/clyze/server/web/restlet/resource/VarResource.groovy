package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Usage
import org.clyze.persistent.model.doop.UsageKind
import org.clyze.persistent.model.doop.Variable
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.Value
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class VarResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()

		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)		

		def var = getReqAttributeDecoded("var")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]

		Variable v = searchLogic.findVariable(var)
		log.debug "Variable: ${v.toMap()}"
		if (!v) return new JsonRepresentation(res)
		if (definition) res = conform(Variable, v.toMap(), searchLogic)

		///////////////////////////////////////////////////////////////////////
		def accessedBy = getQueryValue("accessedBy")
		if (accessedBy)
			res.accessedBy = [] as Set
		if (accessedBy == "R")
			res.accessedBy = searchLogic.findUsages(var, UsageKind.DATA_READ).collect {
				conform(Usage, it.toMap(), searchLogic)
			}
		else if (accessedBy == "W")
			res.accessedBy = searchLogic.findUsages(var, UsageKind.DATA_WRITE).collect {
				conform(Usage, it.toMap(), searchLogic)
			}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("values") != null) {
			long line = getQueryValue("line")?.toInteger() ?: v.position.startLine
			Set<Value> values = searchLogic.findClosestValues(var, line).collect {
				conform(Value, it.toMap(), searchLogic)
			}
			res.values = Searcher.filterPositionDuplicates(values)
		}

		new JsonRepresentation(res)
	}

	static JsonRepresentation getDefault(AbstractServerResource resource, Variable var, boolean expand, Usage usage = null) {
		def r = new VarResource()
		r.setRequest(resource.request)
		r.setResponse(resource.response)
		r.setReqAttributeEncoded("var", var.doopId)
		if (expand) {
			r.setQueryValue("values", "")
			r.setQueryValue("line", usage?.position?.startLine as String)
		}
		r.get() as JsonRepresentation
	}
}
