package org.clyze.server.web.restlet.resource

import com.google.gson.stream.JsonWriter
import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.store.TextualSearch
import org.clyze.server.web.persistent.store.TextualSearchType
import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams
import org.restlet.data.Form
import org.restlet.data.MediaType
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class OptimizationDirectivesResource extends AbstractServerResource {

    static final int MAX_RESULTS_PER_FACET = 2000
    
    final static Map<String, String> COMMON_FACETS = [
        //fieldNameInResult : fieldNameInDatastore        
        "directiveType": "typeOfDirective",
        "originType"   : "originType",
        "origin"       : "origin",
        "package"      : "packageName",
        "class"        : "className" //,"method"        : "methodName"
    ]

    final static Map<String, String> CONFIG_ONLY_FACETS = [
        "ruleId": "ruleId"
    ]

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()
        loadAnalysisFromRequestParameter()

        Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

        def form = getRequest().getResourceRef().getQueryAsForm()
        def p = new PaginationParams()
        def o = new OrderByParams()
        p.process(form)
        o.process(form)

        def configSetName  = form.getFirstValue('set')
        boolean withFacets = Boolean.parseBoolean(form.getFirstValue('_facets'))

        /*
        if (typeOfDirective && !KeepRemoveMethodDirective.isValidTypeOfDirective(typeOfDirective)) {
        throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid directive type: $typeOfDirective")
        }		
        if (originType && !OptimizationDirective.OriginType.isValid(originType)){
        throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid originType: $originType")
        }
         */

        def facets = [:]
        facets.putAll(COMMON_FACETS)
        def configSets = []
        if (analysis || configSetName) {
            if (configSetName) {
                BundleConfiguration bundleConfig = new BundleConfigurationManager(userId, inputBundle, configSetName).loadBundleConfiguration()
                configSets << bundleConfig.id
                facets.putAll(CONFIG_ONLY_FACETS)
            }
            if (analysis) {
                configSets << analysis.id
            }					
        }
        //TODO: ensure that when both configSet & analysis are supplied we cannot also accept a ruleId

        def params = new Searcher.DirectivesSearchParams(configSet: configSets as Set)
        readSearchParamsFrom(form, params)        
		
        return streamSearchResults(searchLogic, params, p, o, withFacets ? facets : Collections.<String, String>emptyMap())
    }

    /*
     * POSTing a new directive is not supported (?)
     */
    /*
    @Override
    protected Representation post(Representation entity) throws ResourceException {

        ensureUserOwnsInputBundle()
		
        Searcher searchLogic = Config.instance.storage.givenBundle(inputBundle)	

        Form form = new Form(entity)

        String doopId          = form.getFirstValue("doopId")		
        String configSet       = form.getFirstValue("set") ?: DoopInputBundle.DEFAULT_CONFIG_SET
        String typeOfDirective = form.getFirstValue("typeOfDirective")

        if ([doopId, configSet, typeOfDirective].findAll().size() != 3) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
        }

        KeepRemoveMethodDirective.Type dirType = null
        try {
            dirType = KeepRemoveMethodDirective.Type.valueOf(typeOfDirective)
        }
        catch(any) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid directive type: $typeOfDirective")
        }		

        //MethodDirectiveContext ctx = MethodDirectiveContext.forDoopId(inputBundle, doopId)
        //KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective(inputBundle.id, configSet, ctx, USER, dirType)
        KeepRemoveMethodDirective directive = KeepRemoveMethodDirective.forDoopId(inputBundle, doopId, configSet, USER, dirType)
        Config.instance.storage.storeNewOptimizationDirective(inputBundle, directive)
		
        return new JsonRepresentation(directive.toMap())
    }
    */

    Representation streamSearchResults(Searcher searchLogic, 	
                                       Searcher.DirectivesSearchParams searchParams,
                                       PaginationParams p, 
                                       OrderByParams o,
                                       Map<String, String> facets) {

        return new WriterRepresentation(MediaType.APPLICATION_JSON) {
            @Override
            void write(Writer w) throws IOException {
                def writer = new JsonWriter(w)

                writer.beginObject()				
				
                writer.name("results")
                writer.beginArray()
                SearchLimits limits = searchLogic.searchMethodOptimizationDirectives(
                    searchParams, 
                    p.start, 
                    p.count, 
                    o.orderByList
                ) { String json ->                    
                    writer.jsonValue(json)
                    writer.flush()					
                }
                writer.endArray()
                
                if (!facets.isEmpty()) {
                    writer.name("facets")
                    writer.beginObject()
                    facets.each { fieldNameInResult, fieldNameInDatastore ->
                        Map<String, Long> facet = searchLogic.facetOfOptimizationDirectives(
                            searchParams.textualSearch ? searchParams : searchParams.createCopyWithNullifiedField(fieldNameInDatastore),
                            fieldNameInDatastore, 
                            MAX_RESULTS_PER_FACET
                        )
                        writer.name(fieldNameInResult)
                        writer.beginObject()
                        facet.each { k, v ->
                            writer.name(k).value(v)
                        }
                        writer.endObject()
                    }
                    writer.endObject()
                }

                writer.name("start").value(limits.start)
                writer.name("count").value(limits.count)
                writer.name("hits").value(limits.hits)								

                writer.endObject()
            }
        }	
    }

    //helper method
    static void readSearchParamsFrom(Form form, Searcher.DirectivesSearchParams params) {

        String text         = form.getFirstValue("text")
        String textType     = form.getFirstValue("textType")
        String[] textFields = form.getValuesArray("textField")

        def allowedTextFields = [
            "method" : "methodName",
            "class"  : "className",
            "package": "packageName"
        ]

        if (text) {
            TextualSearchType type = TextualSearchType.values().find { it.shortName == textType }
            if (!type) {
                type = TextualSearchType.WILDCARD
            }

            def textFieldsList = textFields?.collect { allowedTextFields.get(it) }?.findAll()?.unique()
            if (!textFieldsList) {
                textFieldsList = ['methodName']
            }
            params.textualSearch = new TextualSearch(type, text, textFieldsList as String[])
        }

        String[] typeOfDirective = form.getValuesArray('directiveType')
        String[] ruleId          = form.getValuesArray('ruleId')
        String[] originType      = form.getValuesArray('originType')
        String[] origin          = form.getValuesArray('origin')
        String[] packageName     = form.getValuesArray('package')
        String[] className       = form.getValuesArray('class')
        String[] methodName      = form.getValuesArray('method')

        if (typeOfDirective) params.typeOfDirective = typeOfDirective.toList() as Set
        if (ruleId) params.ruleId = ruleId.toList() as Set
        if (origin) params.origin = origin.toList() as Set
        if (originType) params.originType = originType.toList() as Set
        if (packageName) params.packageName = packageName.toList() as Set
        if (className) params.className = className.toList() as Set
        if (methodName) params.methodName = methodName.toList() as Set
    }

}