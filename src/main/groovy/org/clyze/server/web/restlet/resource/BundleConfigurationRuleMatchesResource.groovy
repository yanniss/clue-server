package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.bundle.BundleConfiguration
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleConfigurationRuleMatchesResource extends AbstractServerResource {

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")
        BundleConfiguration bundleConfig = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).loadBundleConfiguration()

        String ruleId = getReqAttributeDecoded("ruleId")
        if (!Config.instance.storage.bundleDoopStore(inputBundle).exists(ruleId, PersistentBundleConfigurationRule)) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "No such rule: $ruleId")
        }

        String pkg = getQueryValue("package")
        String cls = getQueryValue("class")

        if (pkg && cls) {
            return Responder.getMatchesOfClassInPackage(inputBundle, bundleConfig, ruleId, pkg, cls)
        }

        if (pkg) {
            return Responder.groupMatchesOfPackageByClass(inputBundle, bundleConfig, ruleId, pkg)
        }

        if (cls) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Parameter class without parameter package")
        }

        //Neither pkg, nor cls
        return Responder.groupMatchesByPackage(inputBundle, bundleConfig, ruleId)
    }

}
