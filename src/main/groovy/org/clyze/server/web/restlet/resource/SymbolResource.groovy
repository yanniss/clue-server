package org.clyze.server.web.restlet.resource

import groovy.json.JsonSlurper
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.persistent.model.doop.Usage
import org.clyze.persistent.model.doop.Variable
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class SymbolResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		
		Searcher searchLogic = Config.instance.storage.givenBundle(inputBundle)

		def res = [symbols: []]
		def file = getReqAttributeDecoded("file")
		def line = getReqAttributeDecoded("line")
		def column = getReqAttributeDecoded("column")

		// Explicitly check for null, since the empty string is a valid value
		def expand = (getQueryValue("noExpansion") == null)

		searchLogic.searchSymbolsAtPosition(file, line, column) {
			String id, String type, String json ->
				switch (type) {
					case MethodInvocation.simpleName:
						res.symbols << InvocationResource.getDefault(this, new MethodInvocation().fromJSON(json), expand).result
						break
					case Variable.simpleName:
						res.symbols << VarResource.getDefault(this, new Variable().fromJSON(json), expand).result
						break
					case Field.simpleName:
						res.symbols << FieldResource.getDefault(this, new Field().fromJSON(json), expand).result
						break
					case Usage.simpleName:
						def usage = new Usage().fromJSON(json)
						searchLogic.searchByDoopId(usage.doopId, [Klass.simpleName, Field.simpleName, Variable.simpleName]) {
							String id2, String type2, String json2 ->
								def result = null
								switch (type2) {
									case Klass.simpleName:
										result = ClassResource.getDefault(this, new Klass().fromJSON(json2)).result
										break
									case Field.simpleName:
										result = FieldResource.getDefault(this, new Field().fromJSON(json2), expand).result
										break
									case Variable.simpleName:
										result = VarResource.getDefault(this, new Variable().fromJSON(json2), expand, usage).result
										break
								}
								if (result) {
									result.usage = conform(Usage, usage.toMap(), searchLogic)
									res.symbols << result
								}
						}
						break
					default:
						res.symbols << conform(type, new JsonSlurper().parseText(json), searchLogic)
						break
				}
		}

		res.symbols = Searcher.filterPositionDuplicates(res.symbols)
		new JsonRepresentation(res)
	}
}
