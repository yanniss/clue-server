package org.clyze.server.web.restlet.resource

import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException
import org.restlet.resource.ServerResource

class Ping extends ServerResource {
	@Override
	protected Representation get() throws ResourceException {
		new JsonRepresentation(["msg": "OK"])
	}
}
