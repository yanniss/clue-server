package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 *
 */
class QueriesResource extends AbstractServerResource {

    @Override
    protected Representation get() throws ResourceException {

        ensureUserCanReadInputBundle()
        def hasAnalysisId = (getQueryValue("analysis") as boolean)

        def queries = Config.instance.readyMadeQueries
                .findAll { !hasAnalysisId ? !it.requiresAnalysis : true }
                .collect {
            //Manual map transformation of the query object (for clarity)
            return [
                id         : it.id,
                name       : it.name,
                description: it.description
            ]
        }

        new JsonRepresentation([ "results": queries ])
    }
}
