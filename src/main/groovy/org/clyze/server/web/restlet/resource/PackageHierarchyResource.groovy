package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 *
 */
class PackageHierarchyResource extends AbstractServerResource {	

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		
        def searchLogic = Config.instance.storage.givenBundle(inputBundle)

		def packages = [:]
		def classes = [:]
		def tree = [
			name: "",
			children: [],
		]

		Map<String, Long> methodCountPerClass = searchLogic.groupMethodsByDeclaringClass()		

		methodCountPerClass.each { String classDoopId, Long methodCount ->

			def parts = classDoopId.tokenize(".")
			def pNode = tree	// package node
			def pName = ""		// package name

			// ".init()" returns all elements but last in a collection.
			for(def part : parts.init()) {
				def name = pName ? "$pName.$part" : part
				def node = packages[name]

				if(!node) {
					node = [
						name: name,
						children: [],
					]
					pNode.children << node
					packages[name] = node
				}

				pNode = node
				pName = name
			}

			def classNode = addClass(classes, classDoopId, methodCount)
			if(classNode) {
				pNode.children << classNode
			}
		}

		return new JsonRepresentation([ results: tree ])
	}
	
	private static addClass(Map classes, String classDoopId, long methodCount) {

		def classNode = null

		if(classDoopId.contains('$')) { // nested class
			def outerClassDoopId = classDoopId.tokenize('$')[0]
			if(classes.containsKey(outerClassDoopId)) {
				classes[outerClassDoopId].size += methodCount
			}
			else {
				classes[outerClassDoopId] = [
					name: outerClassDoopId,
					size: methodCount,
				]
			}
		}
		else if(classes.containsKey(classDoopId)) {
			classes[classDoopId].size += methodCount
		}
		else {
			classes[classDoopId] = classNode = [
				name: classDoopId,
				size: methodCount,
			]
		}

		return classNode
	}

}
