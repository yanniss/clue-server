package org.clyze.server.web.restlet.resource


import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Disposition
import org.restlet.data.MediaType
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class BundleConfigurationActionResource extends AbstractServerResource {

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")
        String action = getReqAttributeDecoded("action")
        String target = getQueryValue("target")

        if (BundleConfigurationManager.BUNDLE_CONFIG_ACTION_EXPORT == action) {
            return exportBundleConfiguration(new BundleConfigurationManager(userId, inputBundle, bundleConfigName), target ?: bundleConfigName)
        }

        if (BundleConfigurationManager.BUNDLE_CONFIG_ACTION_CLONE == action) {
            println("*** CLONING $bundleConfigName ***")
            BundleConfiguration clone = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).cloneBundleConfigurationTo(target)
            println("*** CLONING result: $clone ***")
            return new JsonRepresentation(clone.toMap())
        }

        if (BundleConfigurationManager.BUNDLE_CONFIG_ACTION_RENAME == action) {
            BundleConfiguration renamed = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).renameBundleConfigurationTo(target)
            return new JsonRepresentation(renamed.toMap())
        }

        if (BundleConfigurationManager.BUNDLE_CONFIG_ACTION_IMPORT_DIRECTIVES == action) {
            loadAnalysis(target)
            new BundleConfigurationManager(userId, inputBundle, bundleConfigName).importDirectivesOf(analysis)
            return new JsonRepresentation([msg:'OK'])
        }

        throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Unknown action: $action")
    }

    static Representation exportBundleConfiguration(BundleConfigurationManager manager, String target) {
        WriterRepresentation r = new WriterRepresentation(MediaType.TEXT_PLAIN) {
            @Override
            void write(Writer w) throws IOException {
                manager.externalizeBundleConfiguration(w)
            }
        }

        Disposition d = new Disposition(Disposition.TYPE_ATTACHMENT)
        d.setFilename(target)
        r.setDisposition(d)
        return r
    }
}
