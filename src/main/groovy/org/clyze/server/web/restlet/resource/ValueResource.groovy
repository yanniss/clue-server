package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.ArrayValues
import org.clyze.server.web.persistent.model.doop.Value
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class ValueResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()

		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def valueId = getReqAttributeDecoded("value")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]
		Value v = null
		def vals = searchLogic.findValues(valueId)
		if (vals) {
			v = vals.first()
			if (definition) res = conform(Value, v.toMap(), searchLogic)
		}
		if (!v) return new JsonRepresentation(res)

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("arrayValues") != null) {
			res.arrayValues = [] as Set
			if (v.isArray) {
				searchLogic.searchArrayValues(valueId) { String json ->
					def t = new ArrayValues().fromJSON(json)
					res.arrayValues += searchLogic.findValues(t.valueId).collect {
						conform(Value, it.toMap(), searchLogic)
					}
				}
				res.arrayValues = Searcher.filterPositionDuplicates(res.arrayValues)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("fields") != null && v.allocatedTypeDoopId) {
			def r = new ClassResource()
			r.setRequest(request)
			r.setResponse(response)
			r.setReqAttributeEncoded("class", v.allocatedTypeDoopId)
			r.setQueryValue("noDef", "")
			r.setQueryValue("fields", "")
			res += (r.get() as JsonRepresentation).result
		}

		new JsonRepresentation(res)
	}
}
