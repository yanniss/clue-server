package org.clyze.server.web.restlet.resource

import com.android.tools.r8.shaking.BundleConfigurationRule
import com.google.common.collect.HashMultimap
import com.google.common.collect.Multimap
import groovy.transform.CompileStatic
import io.reactivex.BackpressureStrategy
import io.reactivex.Emitter
import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.Config
import org.clyze.server.web.Optimizer
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.BundleConfigurationContext
import org.clyze.server.web.bundle.ClueJsonFile
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.bundle.RuleExpansion
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.model.doop.EffectiveMethodDirective
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.model.doop.MethodDirectiveContext
import org.clyze.server.web.persistent.model.doop.OptimizationDirective
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.server.web.persistent.store.Searcher
import org.restlet.data.Status
import org.restlet.resource.ResourceException

/**
 * An entry point for all bundle configuration management operations exposed through the REST API.
 */
@CompileStatic
class BundleConfigurationManager {

    private static Log log = LogFactory.getLog(this)

    static final String BUNDLE_CONFIG_ACTION_EXPORT            = "export"
    static final String BUNDLE_CONFIG_ACTION_CLONE             = "clone"
    static final String BUNDLE_CONFIG_ACTION_RENAME            = "rename"
    static final String BUNDLE_CONFIG_ACTION_IMPORT_DIRECTIVES = "import_directives"

    static class RuleGenerator {

        static String generateRuleFrom(MethodDirectiveContext methodContext, KeepRemoveMethodDirective.Type type) {
            return generateRuleFrom(methodContext.klass, [methodContext.method], type)
        }

        static String generateRuleFrom(Klass klass,
                                       List<Method> ruleMethods,
                                       KeepRemoveMethodDirective.Type type,
                                       int declaredMethodsOfClass = -1) {

            /*
            //check for default constructor to make all dir. types (keep, remove, forceremove) behave uniformly
            if (ruleMethods.size() == 1) {
                Method m = ruleMethods.get(0)
                if (m.name == "<init>" && (m.params == null || m.params.length == 0)) {
                    return """${type.name().toLowerCase()} ${classType(klass)} ${klass.doopId}"""
                }
            }
             */

            if(ruleMethods.size() == declaredMethodsOfClass) {
                return """${type.name().toLowerCase()} ${classType(klass)} ${klass.doopId} { <methods>; }"""
            }


            Collection<String> methodSigs = ruleMethods.collect { methodSignature(it) }
            return """${type.name().toLowerCase()} ${classType(klass)} ${klass.doopId} {\n\t${methodSigs.join("\n\t")}\n}"""
        }

        static String fullyQualifiedClassName(String packageName, String name) {
            return packageName ? "${packageName}.${name}" : name
        }

        static String classType(Klass klass) {
            if (klass.isEnum()) return "enum"
            if (klass.isInterface()) return "interface"
            return "class"
        }

        static String methodSignature(Method method) {
            String modifiers = modifiers(method)
            String methodReturnTypeAndName = (method.name == "<init>" || method.name == "<clinit>") ?
                method.name :
                method.returnType + " " + method.name

            return modifiers ?
                "${modifiers} ${methodReturnTypeAndName}(${method.paramTypes.join(', ')});" :
                "${methodReturnTypeAndName}(${method.paramTypes.join(', ')});"
        }

        static String modifiers(Method method) {
            String otherModifiers = otherModifiers(method).trim()
            String accessModifiers = accessModifiers(method)
            if (accessModifiers && otherModifiers) return accessModifiers + " " + otherModifiers
            if (accessModifiers) return accessModifiers
            if (otherModifiers) return otherModifiers
            return null
        }

        static String accessModifiers(Method method) {
            if (method.isPublic()) return "public"
            if (method.isPrivate()) return "private"
            if (method.isProtected()) return "protected"
            return null
        }

        static String otherModifiers(Method method) {
            List<String> others = []
            if (method.isStatic()) others.add "static"
            if (method.isNative()) others.add "native"
            if (method.isAbstract()) others.add "abstract"
            if (method.isFinal()) others.add "final"
            if (method.isSynchronized()) others.add "synchronized"
            if (method.isSynthetic()) others.add "synthetic"
            return others.join(" ")
        }
    }

    private final String userId
    private final DoopInputBundle inputBundle
    private final String bundleConfigName

    BundleConfigurationManager(String userId, DoopInputBundle inputBundle, String bundleConfigName) {
        this.userId = userId
        this.inputBundle = inputBundle
        this.bundleConfigName = bundleConfigName
    }

    PersistentBundleConfigurationRule addNewRule(String body, String doopId, String type) {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            if (body) {

                if (doopId || type) {
                    throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "cannot give body along with doopId or type")
                }

                return addRule(bundleConfig, body, userId)
            }
            else {

                if (!doopId && !type) {
                    throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "cannot give empty doopId or type")
                }

                MethodDirectiveContext methodContext = null
                KeepRemoveMethodDirective directive = null
                KeepRemoveMethodDirective.Type dirType = null
                try {
                    dirType = KeepRemoveMethodDirective.Type.valueOf(type)
                }
                catch(any) {
                    throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid directive type: $type")
                }

                try {
                    methodContext = KeepRemoveMethodDirective.contextForDoopId(inputBundle, doopId)
                }
                catch(any) {
                    log.warn(any.getMessage(), any)
                    throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "Cannot load information for method $doopId: ${any.getMessage()}")
                }
                String ruleBody = RuleGenerator.generateRuleFrom(methodContext, dirType)
                return addRule(bundleConfig, ruleBody, userId)
            }
        }
    }


    List<PersistentBundleConfigurationRule> addNewRules(String extraOrigin, String... rules) {

        if (rules == null || rules.length == 0) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
        }

        String[] origin = extraOrigin ? [userId, extraOrigin] : [userId]

        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {
            //TODO: Revisit this to make it more efficient

            return rules?.collect { String rule ->
                return addRule(bundleConfig, rule, origin)
            }
        }
    }

    BundleConfiguration loadBundleConfiguration() {
        if (!bundleConfigName) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
        }

        BundleConfiguration configSet = Config.instance.storage.loadConfigSetByName(inputBundle, bundleConfigName)
        if (configSet == null) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "config $bundleConfigName")
        }

        return configSet
    }

    PersistentBundleConfigurationRule updateRule(String ruleId, String ruleBody, String comment) {

        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            //load the rule
            PersistentBundleConfigurationRule rule = loadRule(ruleId)

            String trimmedRuleBody = ruleBody?.trim()
            if (trimmedRuleBody) {
                log.debug("Updating comment and body")
                //clone the rule
                rule = new PersistentBundleConfigurationRule().fromJSON(rule.toJSON()) as PersistentBundleConfigurationRule
                rule.comment = comment
                RuleExpansion expansion = verifyAndParseRule(bundleConfig, rule, ruleBody)
                return expansion.expandAndUpdateIn(bundleConfig)
            }
            else {
                log.debug("Updating comment only")
                //update its comment
                rule.comment = comment
                Config.instance.storage.bundleDoopStore(inputBundle).updateExisting(rule)
                return rule
            }
        }
    }

    void removeRule(String ruleId) {

        /*
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            PersistentBundleConfigurationRule rule = loadRule(ruleId)

            DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
            store.delete(rule) {
                Config.instance.storage.removeDirectivesOfRule(inputBundle, bundleConfig, ruleId)
            }
        }
        */

        removeRules([ruleId] as Set)
    }

    void removeRules(Set<String> ruleIds) {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {
            Config.instance.storage.removeRulesOfBundleConfig(inputBundle, bundleConfig, ruleIds)

            bundleConfig.dirty = true
            Config.instance.storage.updateConfigSet(inputBundle, bundleConfig)
        }
    }

    void externalizeBundleConfiguration(Writer writer) {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {
            ClueJsonFile.externalize(inputBundle, bundleConfig, writer)
        }
    }

    void runAnalysisOnBundleConfiguration(ServerSideAnalysis a) {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {
            bundleConfig.currentAnalysisId = a.id
            bundleConfig.dirty = true
            Config.instance.storage.updateConfigSet(inputBundle, bundleConfig)

            log.debug "Updated $bundleConfigName"

            //clean up effective directives
            Config.instance.storage.removeEffectiveDirectivesOfBundleConfig(inputBundle, bundleConfig)

            a.onFinish = {
                calculateEffectiveDirectivesOfBundleConfiguration()
            }

            a.start()
        }
    }

    Collection<EffectiveMethodDirective> calculateEffectiveDirectivesOfBundleConfiguration() {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            log.debug "Calculating effective directives of $bundleConfigName"

            def storage = Config.instance.storage
            ServerSideAnalysis a = storage.loadAnalysis(bundleConfig.currentAnalysisId)

            DataStore store = storage.bundleDoopStore(inputBundle)
            BulkProcessor bulkProcessor = new BulkProcessor(Config.instance.bulkSize, store)

            store.deleteItems(BooleanQueryBuilder.build('_type', EffectiveMethodDirective.simpleName))

            def searcher = storage.givenBundle(inputBundle)

            Map<String, EffectiveMethodDirective> directives = [:]

            Closure processDirective = { String id, String type, String json ->
                KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
                EffectiveMethodDirective effective = new EffectiveMethodDirective(inputBundle.id, bundleConfig.id, directive)
                bulkProcessor.scheduleSave(effective)
                directives.put(effective.doopId, effective)
            }

            //read all force remove user-defined directives
            searcher.getMethodOptimizationDirectivesOfConfigSet(
                    bundleConfig.id,
                    [] as Set,
                    KeepRemoveMethodDirective.Type.FORCE_REMOVE,
                    processDirective
            )

            //read all other user-defined directives
            searcher.getMethodOptimizationDirectivesOfConfigSet(
                    bundleConfig.id,
                    directives.keySet(),
                    processDirective
            )

            //read all analysis directives that don't refer to already processed doop Ids
            storage.givenAnalysis(a).getMethodOptimizationDirectivesOfAnalysis(directives.keySet(), processDirective)

            bulkProcessor.finishUp()

            bundleConfig.dirty = false
            store.updateExisting(bundleConfig)

            log.debug "Calculated effective directives of $bundleConfigName: ${directives.keySet().size()} keep/remove directives"

            return directives.values()
        }
    }

    File optimizeBundleWithConfiguration() {

        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            Optimizer.Output optOut = Optimizer.optimize(
                inputBundle.getAPK(),
                "${inputBundle.outputsDir}/${inputBundle.dirForNewAPK(bundleConfig)}" as String,
                calculateEffectiveDirectivesOfBundleConfiguration()
            )

            return optOut.optimized
        }
    }

    BundleConfiguration cloneBundleConfigurationTo(String target) {

        BundleConfiguration original = loadBundleConfiguration()

        synchronized (original) {

            DataStoreStrategy storage = Config.instance.storage

            String cloneName = target ?: bundleConfigName - ".clue" + "-copy.clue"

            BundleConfiguration clone = storage.loadConfigSetByName(inputBundle, cloneName)
            if (clone) {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $cloneName")
            }
            else {
                Date now = new Date()
                clone = new BundleConfiguration(inputBundle.id, cloneName, now, now)
            }

            DataStore store = storage.bundleDoopStore(inputBundle)
            def bulk = new BulkProcessor(Config.instance.bulkSize, store)
            Map<String, String> newRuleIds = [:]

            storage.forEachBundleConfigurationRule(inputBundle, original) { String id, String type, String json ->
                PersistentBundleConfigurationRule rule = new PersistentBundleConfigurationRule().fromJSON(json) as PersistentBundleConfigurationRule
                PersistentBundleConfigurationRule copy = new PersistentBundleConfigurationRule(rule, clone.id)
                newRuleIds.put(rule.id, copy.id)
                bulk.scheduleSave copy
            }

            storage.givenBundle(inputBundle).getMethodOptimizationDirectivesOfConfigSet(original.id) { String id, String type, String json ->
                KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
                KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(inputBundle.id, clone.id, directive, directive.originType)
                copy.ruleId = newRuleIds.get(copy.ruleId)
                bulk.scheduleSave copy
            }
            bulk.finishUp()

            storage.storeNewConfigSet(inputBundle, clone)

            return clone
        }
    }

    BundleConfiguration renameBundleConfigurationTo(String target) {

        BundleConfiguration original = loadBundleConfiguration()
        synchronized (original) {
            if (!target) {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Empty target: $target")
            }

            DataStoreStrategy storage = Config.instance.storage

            BundleConfiguration renamed = storage.loadConfigSetByName(inputBundle, target)
            if (renamed) {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $target")
            }
            else {
                renamed = original
                renamed.nameOfSet = target
            }

            storage.updateConfigSet(inputBundle, renamed)

            return renamed
        }
    }

    void generateAnalysisKeepSpecFile(File keepSpecFile) {

        BundleConfiguration original = loadBundleConfiguration()
        synchronized (original) {

            def searcher = Config.instance.storage.givenBundle(inputBundle)
            Set<String> forceRemoveDoopIds = [] as Set<String>
            searcher.searchMethodOptimizationDirectives(original.id, KeepRemoveMethodDirective.Type.FORCE_REMOVE) { String json ->
                KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
                forceRemoveDoopIds.add(directive.doopId)
                null
            }
            keepSpecFile.withWriter { w ->
                searcher.getMethodOptimizationDirectivesOfConfigSet(original.id, forceRemoveDoopIds) { id, type, String json ->
                    KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
                    if (directive.typeOfDirective.isKeep()) {
                        w.write(directive.typeOfDirective.name())
                        w.write("\t")
                        w.write(directive.doopId)
                        w.write('\t')
                        w.write(directive.ruleId) // all keep directives have a rule id
                        w.write('\n')
                    }
                }
            }
        }
    }

    void importDirectivesOf(ServerSideAnalysis a) {
        BundleConfiguration bundleConfig = loadBundleConfiguration()
        synchronized (bundleConfig) {

            def storage = Config.instance.storage
            long ruleCount = storage.givenBundle(inputBundle).countConfigSetRules(bundleConfig.id)

            Multimap<String, KeepRemoveMethodDirective> classToDirectives = HashMultimap.create()

            storage.givenAnalysis(a).getMethodOptimizationDirectivesOfAnalysis([] as Set) { id, type, String json ->
                KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
                KeepRemoveMethodDirective copy      = new KeepRemoveMethodDirective(inputBundle.id, bundleConfig.id, directive, directive.originType)
                String fqClassName = RuleGenerator.fullyQualifiedClassName(copy.packageName, copy.className)
                classToDirectives.put(fqClassName, copy)
            }

            Set<String> classIds = classToDirectives.keySet()
            log.debug("Total classes ${classIds.size()}")

            Searcher searcher = storage.givenBundle(inputBundle)
            BulkProcessor bulk = new BulkProcessor(Config.instance.bulkSize, storage.bundleDoopStore(inputBundle))
            Collection<Klass> classes = new ArrayList<Klass>(classIds.size())
            searcher.searchByDoopIds(classIds, [Klass.simpleName]) { id, type, String json ->
                Klass k = new Klass().fromJSON(json) as Klass
                classes.add(k)
            }
            if (classIds.size() != classes.size()) {
                log.warn("Class count in directives (${classIds.size()}) differs from resolved class count (${classes.size()}) in rule generation")
            }

            //Map<String, Long> declaredMethodsPerClass = searcher.groupMethodsByDeclaringClass(10_000)

            classes.each { Klass klass ->
                Collection<KeepRemoveMethodDirective> directives = classToDirectives.get(klass.doopId)
                Collection<String> methodDoopIds = directives.collect { it.doopId }
                List<Method> methods = new ArrayList<>(directives.size())
                searcher.searchByDoopIds(methodDoopIds, [Method.simpleName]) { id, type, String json ->
                    methods.add(new Method().fromJSON(json) as Method)
                }
                if (directives.size() != methods.size()) {
                    log.warn("Directive count (${directives.size()}) differs from method count (${methods.size()}) in rule generation for ${klass.doopId}")
                }
                //int declaredMethods = (declaredMethodsPerClass.get(klass.doopId) ?: -1).intValue()
                String ruleBody = RuleGenerator.generateRuleFrom(
                        klass,
                        methods,
                        KeepRemoveMethodDirective.Type.REMOVE,
                        -1 //declaredMethods
                )
                PersistentBundleConfigurationRule rule = addExpandedRule(
                        bulk,
                        bundleConfig,
                        ruleBody,
                        ruleCount++,
                        KeepRemoveMethodDirective.Type.REMOVE,
                        origin(inputBundle, a),
                        OptimizationDirective.OriginType.ANALYSIS,
                        directives
                )
                //log.debug("Added rule (${methods.size()} / $declaredMethods): $ruleBody")
            }
            bulk.finishUp()
        }
    }

    private PersistentBundleConfigurationRule loadRule(String ruleId) {

        DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
        try {
            return store.load(ruleId, PersistentBundleConfigurationRule)
        }
        catch(e) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
        }
    }

    private PersistentBundleConfigurationRule addRule(BundleConfiguration bundleConfig, String ruleBody, String... origin) {
        PersistentBundleConfigurationRule rule = new PersistentBundleConfigurationRule(
            bundleId: inputBundle.id,
            configSet: bundleConfig.id,
            originType: OptimizationDirective.OriginType.USER,
            origin: origin as Set,
            userIndex: bundleConfig.getRuleIndex()
        )
        RuleExpansion expansion = verifyAndParseRule(bundleConfig, rule, ruleBody)
        return expansion.expandAndSaveTo(bundleConfig)
    }

    private PersistentBundleConfigurationRule addExpandedRule(BulkProcessor bulk,
                                                              BundleConfiguration bundleConfig,
                                                              String ruleBody,
                                                              long userIndex,
                                                              KeepRemoveMethodDirective.Type type,
                                                              String origin,
                                                              OptimizationDirective.OriginType originType,
                                                              Collection<KeepRemoveMethodDirective> directives) {
        PersistentBundleConfigurationRule rule = new PersistentBundleConfigurationRule(
            id: UUID.randomUUID().toString(),
            checksum: BundleConfigurationRule.checksumFor(ruleBody),
            bundleId: inputBundle.id,
            configSet: bundleConfig.id,
            ruleBody: ruleBody,
            ruleType: type.ruleType,
            originType: originType,
            origin: [origin] as Set,
            matchingMethods: directives.size(),
            userIndex: userIndex
        )

        directives.each { KeepRemoveMethodDirective directive ->
            directive.ruleId = rule.id
            bulk.scheduleSave(directive)
        }

        bulk.scheduleSave(rule)

        return rule
    }

    private RuleExpansion verifyAndParseRule(BundleConfiguration bundleConfig, PersistentBundleConfigurationRule rule, String ruleBody) {

        String trimmedRuleBody = ruleBody?.trim()

        if (!trimmedRuleBody) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "empty rule")
        }
        String checksum = BundleConfigurationRule.checksumFor(trimmedRuleBody)
        if (Config.instance.storage.existsRuleInBundleConfig(inputBundle, bundleConfig, checksum)) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "duplicate rule: $trimmedRuleBody")
        }

        rule.checksum = checksum
        rule.ruleBody = trimmedRuleBody

        log.debug("Verified rule body: $trimmedRuleBody")

        RuleExpansion expansion = new RuleExpansion(new BundleConfigurationContext(userId), inputBundle, rule)
        try {
            expansion.parseRule()
            log.debug("Parsed rule body")
        }
        catch(e) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
        }

        return expansion
    }


    static BundleConfiguration createNewBundleConfiguration(DoopInputBundle inputBundle, String name) {
        if (!name) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
        }

        try {
            name = BundleConfiguration.validateUserSuppliedName(name)
        }
        catch(all) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, all.getMessage())
        }

        BundleConfiguration bundleConfig = Config.instance.storage.loadConfigSetByName(inputBundle, name)
        if (bundleConfig) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $name")
        }
        else {
            Date now = new Date()
            bundleConfig = new BundleConfiguration(inputBundle.id, name, now, now)
        }

        Config.instance.storage.storeNewConfigSet(inputBundle, bundleConfig)

        return bundleConfig
    }

    static String origin(DoopInputBundle bundle, ServerSideAnalysis a) {
        return "$bundle.displayName / ${a.displayName}"
    }
}
