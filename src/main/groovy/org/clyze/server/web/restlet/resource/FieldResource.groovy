package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.Usage
import org.clyze.persistent.model.doop.UsageKind
import org.clyze.persistent.model.doop.Variable
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.InstanceFieldValues
import org.clyze.server.web.persistent.model.doop.StaticFieldValues
import org.clyze.server.web.persistent.model.doop.Value
import org.clyze.server.web.persistent.model.doop.VarValues
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class FieldResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()

		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def field = getReqAttributeDecoded("field")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]
		Field f = searchLogic.findField(field)
		if (!f) return new JsonRepresentation(res)
		if (definition) res = conform(Field, f.toMap(), searchLogic)

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("shadowedBy") != null) {
			res.shadowedBy = searchLogic.findFieldsShadowedBy(field).collect { conform(Field, it.toMap(), searchLogic) }
		}

		///////////////////////////////////////////////////////////////////////
		def accessedBy = getQueryValue("accessedBy")
		if (accessedBy)
			res.accessedBy = [] as Set
		if (accessedBy == "R")
			res.accessedBy = searchLogic.findUsages(field, UsageKind.DATA_READ).collect {
				conform(Usage, it.toMap(), searchLogic)
			}
		else if (accessedBy == "W")
			res.accessedBy = searchLogic.findUsages(field, UsageKind.DATA_WRITE).collect {
				conform(Usage, it.toMap(), searchLogic)
			}

		// Common filters for both cases below
		def kindClass = f.isStatic ? StaticFieldValues : InstanceFieldValues
		def filters = [_type: kindClass.simpleName, fieldId: field] << baseFilter
		def baseValue = getQueryValue("baseValue")
		if (baseValue && !f.isStatic) filters << [baseValueId: baseValue]

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("values") != null) {
			res.values = [] as Set
			if (f.isStatic) {
				res.values = searchLogic.findStaticFieldValues(field).collect {
					conform(Value, it.toMap(), searchLogic)
				}
			} else {
				res.values = searchLogic.findInstanceFieldValues(field, baseValue).collect {
					conform(Value, it.toMap(), searchLogic)
				}
			}
			res.values = Searcher.filterPositionDuplicates(res.values)
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("vars") != null) {
			res.vars = [] as Set
			def varIds = [] as Set
			def processVarValues = { String json ->
				def varId = new VarValues().fromJSON(json).varId
				if (!varId.contains("/@parameter") &&
						!varId.endsWith("/this") &&
						!varId.endsWith("/this#_0") &&
						!varId.endsWith("/@this"))
					varIds << varId
			}
			if (f.isStatic) {
				searchLogic.searchStaticFieldValues(field) { String json ->
					def t = new StaticFieldValues().fromJSON(json)
					searchLogic.searchVarValues(t.valueId, processVarValues)
				}
			} else {
				searchLogic.searchInstanceFieldValues(field, baseValue) { String json ->
					def t = new InstanceFieldValues().fromJSON(json)
					searchLogic.searchVarValues(t.valueId, processVarValues)
				}
			}
			res.vars << searchLogic.findVariables(varIds).collect {
				conform(Variable, new Variable().fromJSON(json2).toMap(), searchLogic)
			}
		}

		new JsonRepresentation(res)
	}

	static JsonRepresentation getDefault(AbstractServerResource resource, Field field, boolean expand) {
		def r = new FieldResource()
		r.setRequest(resource.request)
		r.setResponse(resource.response)
		r.setReqAttributeEncoded("field", field.doopId)
		if (expand) r.setQueryValue("values", "")
		r.get() as JsonRepresentation
	}
}
