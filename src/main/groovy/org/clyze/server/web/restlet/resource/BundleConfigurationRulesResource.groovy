package org.clyze.server.web.restlet.resource

import com.google.gson.stream.JsonWriter
import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.persistent.store.*
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams
import org.restlet.data.Form
import org.restlet.data.MediaType
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class BundleConfigurationRulesResource extends AbstractServerResource {

    final static def FACETS = [
        //fieldNameInResult : fieldNameInDatastore
        "ruleType" : "ruleType",
        "origin"   : "origin",
        "package"  : "packageName",
        "class"    : "className"
    ]

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")

        BundleConfiguration configSet = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).loadBundleConfiguration()
        log.debug("Loaded configSet: ${configSet.toMap()}")

        Searcher searcher = Config.instance.storage.givenBundle(inputBundle)

        def form = getRequest().getResourceRef().getQueryAsForm()

        def p = new PaginationParams()
        p.process(form)
        //don't process the pagination params from the request
        //always give ranges from 0 to ruleCount
        //long ruleCount = searcher.countConfigSetRules(configSet.id)
        //log.debug("Rules of configSet: ${ruleCount}")
        //p.start = 0
        //p.count = ruleCount.intValue()

        def o = new OrderByParams()
        o.process(form)
        if (o.orderByList.isEmpty()) {
            o.orderByList = [new OrderBy(field: 'matchingMethods', ordering: Ordering.DESC)]
        }

        boolean withFacets = Boolean.parseBoolean(form.getFirstValue('_facets'))

        def params = new Searcher.RulesSearchParams(configSet: [configSet.id] as Set)
        readSearchParamsFrom(form, params)

        return streamSearchResults(searcher, params, p, o, withFacets)
    }

    @Override
    protected Representation post(Representation entity) throws ResourceException {

        ensureUserOwnsInputBundle()
        String bundleConfigName = getReqAttributeDecoded("name")
        Form form = new Form(entity)
        String body = form.getFirstValue("body")
        String doopId = form.getFirstValue("doopId")
        String type   = form.getFirstValue("type")

        PersistentBundleConfigurationRule rule = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).
                addNewRule(body, doopId, type)
        return new JsonRepresentation(rule.toMap())
    }

    @Override
    protected Representation delete() throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")
        Form form = getRequest().getResourceRef().getQueryAsForm()
        String[] ruleIds = form.getValuesArray('ids')

        if (ruleIds == null || ruleIds.length == 0) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "no ids")
        }

        new BundleConfigurationManager(userId, inputBundle, bundleConfigName).removeRules(ruleIds.toList().toSet())

        return new JsonRepresentation([msg:"OK"])
    }

    Representation streamSearchResults(Searcher searcher,
                                       Searcher.RulesSearchParams searchParams,
                                       PaginationParams p,
                                       OrderByParams o,
                                       boolean withFacets) {

        return new WriterRepresentation(MediaType.APPLICATION_JSON) {
            @Override
            void write(Writer w) throws IOException {
                def writer = new JsonWriter(w)

                writer.beginObject()

                writer.name("results")
                writer.beginArray()
                SearchLimits limits = searcher.searchConfigSetRules(searchParams, p.start, p.count, o.orderByList) { String json ->
                    writer.jsonValue(json)
                    writer.flush()
                }
                writer.endArray()

                if (withFacets) {
                    writer.name("facets")
                    writer.beginObject()
                    FACETS.each { fieldNameInResult, fieldNameInDatastore ->
                        Map<String, Long> facet = searcher.facetOfConfigSetRules(
                            searchParams.textualSearch ? searchParams : searchParams.createCopyWithNullifiedField(fieldNameInDatastore),
                            fieldNameInDatastore,
                            OptimizationDirectivesResource.MAX_RESULTS_PER_FACET
                        )
                        writer.name(fieldNameInResult)
                        writer.beginObject()
                        facet.each { k, v ->
                            writer.name(k).value(v)
                        }
                        writer.endObject()
                    }
                    writer.endObject()
                }

                writer.name("start").value(limits.start)
                writer.name("count").value(limits.count)
                writer.name("hits").value(limits.hits)

                writer.endObject()
            }
        }
    }

    //helper method
    static void readSearchParamsFrom(Form form, Searcher.RulesSearchParams params) {

        String text = form.getFirstValue("text")

        if (text) {
            TextualSearchType type = TextualSearchType.FULL_TEXT
            params.textualSearch = new TextualSearch(type, text, ['origin', 'comment'] as String[])
        }

        String[] ruleType    = form.getValuesArray('ruleType')
        String[] origin      = form.getValuesArray('origin')
        String[] originType  = form.getValuesArray('originType')
        String[] packageName = form.getValuesArray('package')
        String[] className   = form.getValuesArray('class')

        if (ruleType) params.ruleType = ruleType.toList() as Set
        if (origin) params.origin = origin.toList() as Set
        if (originType) params.originType = originType.toList() as Set
        if (packageName) params.packageName = packageName.toList() as Set
        if (className) params.className = className.toList() as Set
    }
}