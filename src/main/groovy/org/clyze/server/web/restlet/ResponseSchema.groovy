package org.clyze.server.web.restlet

import org.clyze.persistent.model.doop.*
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.Value

class ResponseSchema {

	static def conform(java.lang.Class klass, def rawMap, Searcher sh) {
		conform(klass.simpleName, rawMap, sh)
	}

	static def conform(String klass, def rawMap, Searcher sh) {
		rawMap?.with {
			def base = [
					doopId        : doopId,
					sourceFileName: sourceFileName,
					position      : position,
					kind          : klass,
			]

			switch (klass) {
				case Klass.simpleName:
					return [
							declaringSymbolDoopId: declaringSymbolDoopId,
							name                 : name,
							packageName          : packageName,
							isInterface          : isInterface,
							isAbstract           : isAbstract,
							isEnum               : isEnum,
							isReachable          : sh.isSymbolReachable(doopId),
							sizeInBytes          : sizeInBytes,
					] << base

				case HeapAllocation.simpleName:
					return [
							allocatingMethodDoopId: allocatingMethodDoopId,
							allocatedTypeDoopId   : allocatedTypeDoopId,
							isArray               : isArray,
					] << base + [kind: Value.simpleName]

				case Value.simpleName:
					return [
							allocatedTypeDoopId: allocatedTypeDoopId,
							description        : description,
							isArray            : isArray,
							valueKind          : kind,
					] << base

				case Method.simpleName:
					return [
							declaringClassDoopId: declaringClassDoopId,
							isStatic            : isStatic,
							isInterface         : isInterface,
							isAbstract          : isAbstract,
							isNative            : isNative,
							name                : name,
							outerPosition       : outerPosition,
							paramTypes          : paramTypes,
							returnType          : returnType,
							isReachable         : sh.isSymbolReachable(doopId)							
					] << base

				case MethodInvocation.simpleName:
					return [
							name                : name,
							invokingMethodDoopId: invokingMethodDoopId,
					] << base

				case Field.simpleName:
					return [
							name                : name,
							type                : type,
							isStatic            : isStatic,
							declaringClassDoopId: declaringClassDoopId,
					] << base

				case Variable.simpleName:
					return [
							name: name,
							type: type,
					] << base

				case Usage.simpleName:
					return [
							doopId        : doopId,
							sourceFileName: sourceFileName,
							position      : position,
							usageKind     : usageKind,
					]

				default:
					return rawMap
			}
		}
	}
}
