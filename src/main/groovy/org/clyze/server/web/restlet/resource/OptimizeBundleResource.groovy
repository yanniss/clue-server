package org.clyze.server.web.restlet.resource


import org.restlet.data.Disposition
import org.restlet.data.MediaType
import org.restlet.data.Status
import org.restlet.representation.FileRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class OptimizeBundleResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserOwnsInputBundle()		

		String configSetName = getQueryValue("set")

		try {
			log.debug("Optimize bundle ${inputBundle.id} using $configSetName")

			File apk = new BundleConfigurationManager(userId, inputBundle, configSetName).optimizeBundleWithConfiguration()

			FileRepresentation result = new FileRepresentation(apk, new MediaType("application/vnd.android.package-archive"))
			Disposition d = new Disposition(Disposition.TYPE_ATTACHMENT)
			d.setFilename(apk.getName())
			d.setSize(apk.length())
			result.setDisposition(d)
			return result
		}
		catch(all) {
			log.error(all.getMessage(), all)
			throw new ResourceException(Status.SERVER_ERROR_INTERNAL, all.getMessage())
		}
	}

}