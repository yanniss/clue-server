package org.clyze.server.web.restlet

import groovy.json.JsonOutput
import org.restlet.data.MediaType
import org.restlet.representation.WriterRepresentation

class JsonRepresentation extends WriterRepresentation {

	private final Object result

	JsonRepresentation(Object result) {
		super(MediaType.APPLICATION_JSON)
		this.result = result
	}

	@Override
	void write(Writer writer) throws IOException {
		// Explicitly check for null, since an empty map/list is a valid value
		if (result != null) {
			if (result instanceof Map) {
				writer.write(JsonOutput.toJson(result))
			} else {
				writer.write(result.toString())
			}
		}
	}
}
