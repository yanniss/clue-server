package org.clyze.server.web.restlet.resource

import org.apache.commons.io.FilenameUtils
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.restlet.api.PaginationParams
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class FileResource extends AbstractServerResource {

    /**
     * Accepts only requests on code files; that is files with extension .java, .kt, .smali, .shimple or .jimple.
     *
     * @return
     * @throws ResourceException
     */
    @Override
    protected Representation get() throws ResourceException {

        ensureUserCanReadInputBundle()
        loadAnalysisFromRequestParameter()		
		
        def searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

        def filePath = getReqAttributeDecoded("file")
        // Disallow traversing the file tree "upwards"
        if (filePath.contains("../")) {
            throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, filePath)
        } else if (!(filePath ==~ /.*\.(java|kt|smali|jimple|shimple)$/)) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "$filePath: Must be code file (i.e. .java, .kt, .smali, .shimple, .jimple)")
        }

        def res = [:]

        ///////////////////////////////////////////////////////////////////////
        // Explicitly check for null, since the empty string is a valid value
        if (getQueryValue("noContent") == null) {
            def parentDir =
            //Check for both .jimple and .shimple
            (filePath.endsWith(".jimple") || filePath.endsWith(".shimple")) ?
            inputBundle.jimpleDir?.absolutePath :
            inputBundle.sourcesMergedDir?.absolutePath
            if (!parentDir)
            throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, filePath)

            def file = new File(parentDir, filePath)
            if (!file.exists()) {
                log.error "Source file not found: $file"
                throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, filePath)
            }

            if (file.isDirectory()) {
                res.files = []
                file.eachFile {
                    def ext = FilenameUtils.getExtension(it.name)
                    if (!"class".equalsIgnoreCase(ext))
                    res.files << [name: it.name, isDirectory: it.isDirectory()]
                }
                // The rest of the parameters do not apply to directories
                return new JsonRepresentation(res)
            } else
            res.content = file.getText('UTF-8')
        }

        ///////////////////////////////////////////////////////////////////////
        // Explicitly check for null, since the empty string is a valid value
        if (getQueryValue("classes") != null || getQueryValue("symbols") != null) {
            res.classes = []
            searchLogic.searchSymbolsOfSourceFiles([filePath], Klass) { String json ->
                res.classes << conform(Klass, new Klass().fromJSON(json).toMap(), searchLogic)
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // Explicitly check for null, since the empty string is a valid value
        if (getQueryValue("fields") != null || getQueryValue("symbols") != null) {
            res.fields = []
            searchLogic.searchSymbolsOfSourceFiles([filePath], Field) { String json ->
                res.fields << conform(Field, new Field().fromJSON(json).toMap(), searchLogic)
            }
        }

        ///////////////////////////////////////////////////////////////////////
        // Explicitly check for null, since the empty string is a valid value
        if (getQueryValue("methods") != null || getQueryValue("symbols") != null) {
            res.methods = []
            searchLogic.searchSymbolsOfSourceFiles([filePath], Method) { String json ->
                Method m = new Method().fromJSON(json)
                res.methods << conform(Method, m.toMap(), searchLogic)
            }
        }

        String configSetName = getQueryValue("set")

        if (analysis || configSetName) {
            def configSets = []
            if (configSetName) {
                BundleConfiguration bundleConfig = new BundleConfigurationManager(userId, inputBundle, configSetName).loadBundleConfiguration()
                configSets << bundleConfig.id
            }
            if (analysis) {
                configSets << analysis.id
            }					
            res.directives = []
            def orderByList = [
                new OrderBy(field:'configSet'),
                new OrderBy(field:'className'),
                new OrderBy(field:'methodName'),
            ]
            searchLogic.searchMethodOptimizationDirectives(configSets as Set, filePath, 0, PaginationParams.MAX_COUNT, orderByList) {
                String json ->
                KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json)
                res.directives << directive.toMap()
            }
        }


        new JsonRepresentation(res)
    }
}
