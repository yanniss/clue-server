package org.clyze.server.web.restlet.resource

import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class ArtifactsResource extends AbstractServerResource {

	/**
	 * Returns the ids of the artifacts (jars, aars, etc) composing the input bundle
	 * organized in application and dependency artifacts.
	 */
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		def filter = { it.parentArtifactId == null }
		def collector = { it.name }
		def responseData = [
				application: inputBundle.appArtifacts.findAll(filter).collect(collector),
				dependency : inputBundle.depArtifacts.collect(collector),
				platform   : inputBundle.platformArtifacts.collect(collector),
		]

		return new JsonRepresentation(responseData)
	}

}
