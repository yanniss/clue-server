package org.clyze.server.web.restlet.resource


import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Form
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException


/** Add multiple rules in a bundle config (batch) */
class BundleConfigurationRulesBatchResource extends AbstractServerResource {

    @Override
    protected Representation post(Representation entity) throws ResourceException {

        ensureUserOwnsInputBundle()

        String bundleConfigName = getReqAttributeDecoded("name")

        Form form = new Form(entity)
        String origin  = form.getFirstValue("origin")
        String[] rules = form.getValuesArray("rules")

        List<PersistentBundleConfigurationRule> newRules = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).
                addNewRules(origin, rules)
        return new JsonRepresentation([rules:newRules])
    }


}