package org.clyze.server.web.restlet

import org.clyze.server.web.auth.ClearAuthFilter
import org.clyze.server.web.auth.IsAuthenticatedCookieOrHeaderFilter
import org.clyze.server.web.auth.IsAuthenticatedHeaderFilter
import org.clyze.server.web.restlet.api.CreateAutoLoginTokenResource
import org.clyze.server.web.restlet.api.ExecuteDatastoreQueryResource
import org.clyze.server.web.restlet.api.LoginResource
import org.clyze.server.web.restlet.api.SessionResource
import org.clyze.server.web.restlet.resource.*
import org.restlet.Application
import org.restlet.Restlet
import org.restlet.engine.application.CorsFilter
import org.restlet.engine.application.Encoder
import org.restlet.resource.Directory
import org.restlet.resource.ServerResource
import org.restlet.routing.Filter
import org.restlet.routing.Router
import org.restlet.service.EncoderService

import static org.restlet.data.Method.*

/**
 * The web app realized as a restlet application.
 */
class WebApp extends Application {

	@Override
	synchronized Restlet createInboundRoot() {

		def router = new Router(getContext())

		// GET
		// POST
		router.attach "/api/v1/analyses/{analysis}/query/datastore",
				chain(IsAuthenticatedCookieOrHeaderFilter, ExecuteDatastoreQueryResource)

		// To be removed when admin UI is complete
		// POST
		router.attach "/clean/deploy", CleanDeploy

		// GET
		router.attach "/api/v1/ping", Ping

		//////////////////////////////////////////////////////////////////
		// Auth
		//////////////////////////////////////////////////////////////////

		// GET, POST, DELETE
		router.attach "/auth/v1/session", SessionResource

		// POST
		router.attach "/api/v1/authenticate", LoginResource

		// POST
		router.attach "/api/v1/token",
			chain(IsAuthenticatedHeaderFilter, CreateAutoLoginTokenResource)

		//////////////////////////////////////////////////////////////////
		// User(s)
		//////////////////////////////////////////////////////////////////

		// GET, POST
		router.attach "/api/v1/users",
				chain(IsAuthenticatedCookieOrHeaderFilter, UsersResource)

		// DELETE, PUT
		router.attach "/api/v1/users/{user}",
				chain(IsAuthenticatedCookieOrHeaderFilter, UserResource)

		// PUT
		router.attach "/api/v1/users/{user}/password",
				chain(IsAuthenticatedCookieOrHeaderFilter, UserResource)


		//////////////////////////////////////////////////////////////////
		// Project(s)
		//////////////////////////////////////////////////////////////////
		router.attach "/api/v1/projects",
				chain(IsAuthenticatedCookieOrHeaderFilter, ProjectsResource)

		router.attach "/api/v1/projects/{project}",
				chain(IsAuthenticatedCookieOrHeaderFilter, ProjectResource)


		//////////////////////////////////////////////////////////////////
		// Bundle(s)
		//////////////////////////////////////////////////////////////////

		// GET
		// Query parameters:
		// 1) family={doop,cclyzer} 	OPTIONAL. If omitted, returns bundles of any family.
		//
		// POST
		// Query parameters:
		// 1) family={doop,cclyzer}     REQUIRED
		// 2) project=projectId       REQUIRED 
		//    ^-- This param should be removed once we re-arrange the endpoints to include the projects
		router.attach "/api/v1/bundles",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundlesResource)

		// GET, DELETE
		// GET Query Parameters:
		// 1) project=projectId       REQUIRED 
		//    ^-- This param should be removed once we re-arrange the endpoints to include the projects
		router.attach "/api/v1/bundles/{bundle}",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleResource)

		// GET
		// Query parameters:
		// 1) prefix={the prefix}
		// 2) types=[list of symbol types] -- optional
		router.attach "/api/v1/bundles/{bundle}/symbols/startWith",
			chain(IsAuthenticatedCookieOrHeaderFilter, SearchSymbolByPrefixResource)

		// GET
		router.attach "/api/v1/bundles/{bundle}/artifacts",
			chain(IsAuthenticatedCookieOrHeaderFilter, ArtifactsResource)

		// GET
		// Query parameters:
		// 1) noExpansion=				OPTIONAL
		router.attach "/api/v1/bundles/{bundle}/symbols/{file}/{line}/{column}",
			chain(IsAuthenticatedCookieOrHeaderFilter, SymbolResource)		
		

		//////////////////////////////////////////////////////////////////
		// Package(s)
		//////////////////////////////////////////////////////////////////

		// GET
		router.attach "/api/v1/bundles/{bundle}/packages/",
			chain(IsAuthenticatedCookieOrHeaderFilter, PackageResource)
		router.attach "/api/v1/bundles/{bundle}/packages/{package}",
			chain(IsAuthenticatedCookieOrHeaderFilter, PackageResource)
		router.attach "/api/v1/bundles/{bundle}/contexts/{context}/packages/",
			chain(IsAuthenticatedCookieOrHeaderFilter, PackageResource)
		router.attach "/api/v1/bundles/{bundle}/contexts/{context}/packages/{package}",
			chain(IsAuthenticatedCookieOrHeaderFilter, PackageResource)

		//////////////////////////////////////////////////////////////////
		// File(s)
		//////////////////////////////////////////////////////////////////

		// The following two resources, "FileResource" and "FileInArtifactResource", both provide data
		// on input bundle files.
		//
		// Where do they differ?
		// * One difference is that the former does not require the artifact that contains the requested
		//   file while the latter requires it.
		// * Another difference is that they return different data and maybe even different format. For
		//   example "FileResource" returns the contents of the requested file and the symbols contained
		//   in it, while "FileInArtifactResource" returns only the file contents.
		//
		// Why do the differ?
		// * Actually, it might be a good idea to merge these two resources into one.
		// * If not merged, then common logic should probably be moved in "Searcher".

		// GET
		// Query parameters:
		// 1) classes=					OPTIONAL
		// 2) fields=					OPTIONAL
		// 3) methods=					OPTIONAL
		// 4) symbols=					OPTIONAL. Alias for (1)&(2)&(3)
		// 5) noContent=				OPTIONAL
		// 6) analysis=<analysis id>	OPTIONAL
		// 7) set=name-of-config-set    OPTIONAL. 
		// ^-- This is the name of the current bundle clue file (config-set). The analysis
		// config set is implied by the analysis parameter (if present, the analysis config-set
		// will be added in the results automatically). 
		router.attach "/api/v1/bundles/{bundle}/files/{file}",
			chain(IsAuthenticatedCookieOrHeaderFilter, FileResource)

		router.attach "/api/v1/bundles/{bundle}/artifacts/{artifact}/files/",
			chain(IsAuthenticatedCookieOrHeaderFilter, FileInArtifactResource)
		router.attach "/api/v1/bundles/{bundle}/artifacts/{artifact}/files/{file}",
			chain(IsAuthenticatedCookieOrHeaderFilter, FileInArtifactResource)

		//////////////////////////////////////////////////////////////////
		// Analysis/es
		//////////////////////////////////////////////////////////////////

		// GET
		router.attach "/api/v1/families/{family}",
			chain(IsAuthenticatedCookieOrHeaderFilter, AnalysisFamilyResource)

		// GET
		// Query parameters:
		// 1) what =					MANDATORY (bundle or analysis)
		// Allows anonymous access in order to facilitate the new clue-client dynamic option discovery
		router.attach "/api/v1/options", CreateBundleOrAnalysisOptionsResource

		// GET, POST
		router.attach "/api/v1/bundles/{bundle}/analyses",
				chain(IsAuthenticatedCookieOrHeaderFilter, AnalysesResource)

		// GET, DELETE
		router.attach "/api/v1/bundles/{bundle}/analyses/{analysis}",
				chain(IsAuthenticatedCookieOrHeaderFilter, AnalysisResource)	

		// PUT
		router.attach "/api/v1/bundles/{bundle}/analyses/{analysis}/action/{action}",
				chain(IsAuthenticatedCookieOrHeaderFilter, AnalysisResource)

		// GET
		// Query parameters:
		// 1) hierarchy=<subTypes|superTypes>
		// 2) fields=
		// 3) methods=
		// 4) allocationsOfType=
		// 5) fieldsOfType=
		// 6) varsOfType=
		// 7) noDef=
		// 8) analysis=<analysis id> -- optional
		router.attach "/api/v1/bundles/{bundle}/classes/{class}",
				chain(IsAuthenticatedCookieOrHeaderFilter, ClassResource)

		// GET
		// Query parameters:
		// 1) shadowedBy=
		// 2) accessedBy=<R|W>
		// 3) values=Z
		// 3.b) baseValue=<baseValue> -- optional
		// 4) noDef=
		// 5) vars=
		// 6) analysis=<analysis id> -- optional
		router.attach "/api/v1/bundles/{bundle}/fields/{field}",
				chain(IsAuthenticatedCookieOrHeaderFilter, FieldResource)

		// GET
		// Query parameters:
		// 1) accessedBy=<R|W>
		// 2) values=
		// 2.b) line=<NUM> -- optional
		// 3) noDef=
		// 4) analysis=<analysis id> -- optional
		router.attach "/api/v1/bundles/{bundle}/vars/{var}",
				chain(IsAuthenticatedCookieOrHeaderFilter, VarResource)

		// GET
		// Query parameters:
		// 1) hierarchy=<subTypes|superTypes>
		// 2) reverseInvocations=
		// 3) this=
		// 4) noDef=
		// 5) returnValues=
		// 6) analysis=<analysis id> -- optional
		// 7) reverseChaInvocations= -- optional
		router.attach "/api/v1/bundles/{bundle}/methods/{method}",
				chain(IsAuthenticatedCookieOrHeaderFilter, MethodResource)

		// GET
		// Query parameters:
		// 1) values=
		// 2) noDef=
		// 3) analysis=<analysis id> -- optional
		// 4) cha= -- optional
		router.attach "/api/v1/bundles/{bundle}/invocations/{invo}",
				chain(IsAuthenticatedCookieOrHeaderFilter, InvocationResource)

		// GET
		// Query parameters:
		// 1) arrayValues=
		// 2) noDef=
		// 3) fields=
		// 4) analysis=<analysis id> -- optional
		router.attach "/api/v1/bundles/{bundle}/values/{value}",
				chain(IsAuthenticatedCookieOrHeaderFilter, ValueResource)

		// GET
		router.attach "/api/v1/bundles/{bundle}/queries",
				chain(IsAuthenticatedCookieOrHeaderFilter, QueriesResource)

		// GET
		router.attach "/api/v1/bundles/{bundle}/queries/{query}",
				chain(IsAuthenticatedCookieOrHeaderFilter, QueryResource)

		// GET
		router.attach "/api/v1/bundles/{bundle}/packageGraph",
				chain(IsAuthenticatedCookieOrHeaderFilter, PackageGraphResource)

		// GET
		router.attach "/api/v1/bundles/{bundle}/packageHierarchy",
				chain(IsAuthenticatedCookieOrHeaderFilter, PackageHierarchyResource)

		// GET
		// Relation view query parameters
		// 1) relation=
		// 2) analysis=<analysis id> -- optional (nothing = input bundle facts)
		router.attach "/api/v1/bundles/{bundle}/relationView",
				chain(IsAuthenticatedCookieOrHeaderFilter, RelationViewResource)

		// GET (relations)
		router.attach "/api/v1/bundles/{bundle}/relations",
				chain(IsAuthenticatedCookieOrHeaderFilter, RelationsResource)

		// GET (Runtime monitoring)
		router.attach "/api/v1/bundles/{bundle}/analyses/{analysis}/runtime",
				chain(IsAuthenticatedCookieOrHeaderFilter, RuntimeMonitorResource)

		// GET (Experimental UML generation)
		router.attach "/api/v1/bundles/{bundle}/uml",
				chain(IsAuthenticatedCookieOrHeaderFilter, UMLPackageDiagramResource)

		// GET (Invoke Redex repackager)
		// Query parameters:		
		// 1) set=<name-of-config-set> -- optional
		// TODO: use a better HTTP method, using GET for convenience (UI will deal with it)
		router.attach "/api/v1/bundles/{bundle}/optimize",
				chain(IsAuthenticatedCookieOrHeaderFilter, OptimizeBundleResource)

		//GET
		//Query parameters for GET:
		//1)  typeOfDirective=KEEP, REMOVE, (multi-valued) optional
		//2)  set=<name-of-config-set>
		//3)  analysis=<analysis-id>, optional (this can be given along with the set)
		//4)  ruleId=<ruleId>, (multi-valued), optional (this can be given only with the set and not with the analysis)
		//5)  originType=USER, ANALYSIS (multi-valued), optional
		//6)  origin=user names or analyses (multi-valued), optional
		//7)  package=<packageName> (multi-valued), optional
		//8)  class=<className> (multi-valued), optional
		//9)  method=<methodName>, (multi-valued) optional
		//10)  text=<user-input>
		//11)  textType = W|R|FT (Wildcard|Regex|FullText), default W
		//12) textField = <method|class|package> (multi-valued), search for text in the given fields, default method
		//13) _start=int
		//14) _count=int
		//15) _sort=FIELD|ASC (multi-valued)
		//16) _facets=boolean (default false)
		router.attach "/api/v1/bundles/{bundle}/directives",
				chain(IsAuthenticatedCookieOrHeaderFilter, OptimizationDirectivesResource)

		//GET, PUT, DELETE
		//Query parameters for PUT:
		//typeOfDirective (for changing the directive's type -- KEEP or REMOVE)
		router.attach "/api/v1/bundles/{bundle}/directives/{directive}",
				chain(IsAuthenticatedCookieOrHeaderFilter, OptimizationDirectiveResource)

		//GET
		// Query parameters:
		//1)  action=<EXPORT|CLONE|MERGE|RENAME|DELETE_FILTERED|EXPORT_FILTERED|CLONE_FILTERED|MERGE_FILTERED> -- mandatory
		//2)  target=name-of-config-set -- mandatory only for merge, rename
		//3)  typeOfDirective=KEEP, REMOVE, (multi-valued) optional, for *_FILTERED actions only
		//4)  originType=USER, ANALYSIS (multi-valued), optional, for *_FILTERED actions only
		//5)  package=<packageName> (multi-valued), optional, for *_FILTERED actions only
		//6)  class=<className> (multi-valued), optional, for *_FILTERED actions only
		//7)  method=<methodName>, (multi-valued) optional, for *_FILTERED actions only
		//8)  text=<user-input>, optional, for *_FILTERED actions only
		//9)  textType = W|R|FT (Wildcard|Regex|FullText), default W, optional, for *_FILTERED actions only
		//10) textField = <method|class|package> (multi-valued), search for text in the given fields, default method,
        //                optional, for *_FILTERED actions only
		//Manage the directives of a bundle config set, identified by
		//its clue file name (e.g. optimize.clue)		
		router.attach "/api/v1/bundles/{bundle}/sets/{set}", 
			chain(IsAuthenticatedCookieOrHeaderFilter, ManageConfigSetDirectivesResource)

		// Query parameters:
		// 1) action=<EXPORT|CLONE|MERGE> -- mandatory
		// 2) target=name-of-config-set -- optional (mandatory only for merge)
		//Manage the directives of an analyis, identified by its id		
		router.attach "/api/v1/bundles/{bundle}/analyses/{analysis}/directives",
				chain(IsAuthenticatedCookieOrHeaderFilter, ManageAnalysisDirectivesResource)


		//*************
		//NEW endpoints
		//*************

		//GET
		//Query parameters for GET:
		//1)  typeOfDirective=KEEP, REMOVE, (multi-valued) optional
		//2)  set=<name-of-config-set>
		//3)  analysis=<analysis-id>, optional (this can be given along with the set)
		//4)  ruleId=<ruleId>, (multi-valued), optional
		//5)  originType=USER, ANALYSIS (multi-valued), optional
		//6)  origin=user names or analyses (multi-valued), optional
		//7)  package=<packageName> (multi-valued), optional
		//8)  class=<className> (multi-valued), optional
		//9)  method=<methodName>, (multi-valued) optional
		//10)  text=<user-input>
		//11)  textType = W|R|FT (Wildcard|Regex|FullText), default W
		//12) textField = <method|class|package> (multi-valued), search for text in the given fields, default method
		//13) _start=int
		//14) _count=int
		//15) _sort=FIELD|ASC (multi-valued)
		//16) _facets=boolean (default false)
		router.attach "/api/v1/bundles/{bundle}/configs/{name}/directives",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationEffectiveDirectivesResource)

		//GET:  List bundle configs.
		// Query parameters:
		// none
		//POST: Create new bundle config
		// Query parameters:
		// 1) name={name-of-new-config}
		router.attach "/api/v1/bundles/{bundle}/configs",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationsResource)

		//GET: Get info for bundle config
		router.attach "/api/v1/bundles/{bundle}/configs/{name}",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationResource)

		//GET: List rules of bundle config
		// Query parameters:
		//1)  typeOfRule=KEEP, REMOVE, etc. (multi-valued) optional
		//2)  set=<name-of-config-set>
		//3)  originType=USER, ANALYSIS (multi-valued), optional
		//4)  origin=user names or analyses (multi-valued), optional
		//5)  text=<user-input> for searching in origin and/or comment
		//6) _start=int
		//7) _count=int
		//8) _sort=FIELD|ASC (multi-valued)
		//9) _facets=boolean (default false)
		//POST: Add new rule in config:
		// (a) add a new rule, given its body (param 1)
		// (b) add a new rule, given a directive (a doopId and type, params 2 & 3)
		// Query parameters:
		// 1) body, the text/body of the new rule, optional
		// 2) doopId, the method identifier (to create the rule for this particular method), optional
		// 3) type, the type of the directive (becomes the type of the rule), optional
		//DELETE: Remove rules from config:
		// Query parameters:
		// 1) ids=<list-of-rule-ids>
		//The bundle config is identified by its clue file name (e.g. optimize.clue)
		router.attach "/api/v1/bundles/{bundle}/configs/{name}/rules",
			chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationRulesResource)

		router.attach "/api/v1/bundles/{bundle}/configs/{name}/rulesBatch",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationRulesBatchResource)

		router.attach "/api/v1/bundles/{bundle}/configs/{name}/matches/{ruleId}",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationRuleMatchesResource)

		//POST: the create analysis form
		router.attach "/api/v1/bundles/{bundle}/configs/{name}/analyze",
				chain(IsAuthenticatedCookieOrHeaderFilter, AnalyzeBundleConfigurationResource)

		//PUT: Update a bundle config rule
		// Query parameters:
		// 1) body=text of rule, mandatory
		//DELETE: Delete a bundle config rule
		// Query parameters:
		// none
		//The rule is identified by its id
		router.attach "/api/v1/bundles/{bundle}/configs/{name}/rules/{id}",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationRuleResource)

		//PUT: execute a UI action on an existing bundle config. Supported actions: export, clone, rename, import_directives.
		// Query parameters:
		// 1) target=for clone and rename, the name of new config, for export it is ignored, for import_directives the analysis id
		router.attach "/api/v1/bundles/{bundle}/configs/{name}/actions/{action}",
				chain(IsAuthenticatedCookieOrHeaderFilter, BundleConfigurationActionResource)

		router.attach "/api/v1/proguard",
				chain(IsAuthenticatedCookieOrHeaderFilter, PreviewProguardFileResource)

		// If put before the above routes, then they do not work
		def dir = new Directory(getContext(), "war:///static/")
		dir.setIndexName "index.html"
		router.attach "", dir
		router.attach "/", dir

		//////////////////////////////////////////////////////////////////
		// Filters
		//////////////////////////////////////////////////////////////////

		def clearAuthFilter = new ClearAuthFilter()
		clearAuthFilter.setNext(router)

		def logFilter = new LogEverythingFilter()
		logFilter.setNext(clearAuthFilter)

		def encodeFilter = new Encoder(getContext(), false, true, new EncoderService(true))
		encodeFilter.setNext(logFilter)

		// Add a CORS filter to allow cross-domain requests
		CorsFilter corsFilter = new CorsFilter(getContext(), encodeFilter)
		corsFilter.setAllowedOrigins(["*"] as Set)
		corsFilter.setAllowedCredentials(true)
		corsFilter.setAllowedHeaders([] as Set)
		corsFilter.setDefaultAllowedMethods([GET, POST, PUT, DELETE] as Set)
		corsFilter.setAllowingAllRequestedHeaders(true)
		corsFilter.setSkippingResourceForCorsOptions(true)
		corsFilter.setMaxAge(10)

		return corsFilter
	}

	static Restlet chain(Class<Filter> filterClass, Class<ServerResource> resourceClass) {
		def filter = filterClass.newInstance()
		filter.setNext(resourceClass)
		return filter
	}
}
