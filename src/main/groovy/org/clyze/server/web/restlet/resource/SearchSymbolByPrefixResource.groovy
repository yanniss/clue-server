package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher

import com.google.gson.stream.JsonWriter

import org.restlet.data.Form
import org.restlet.data.MediaType
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class SearchSymbolByPrefixResource extends AbstractServerResource {

	@Override
    protected Representation post(Representation entity) throws ResourceException {

		ensureUserCanReadInputBundle()

		Searcher searcher = Config.instance.storage.givenBundle(inputBundle)
		
		def form = new Form(entity)
		String prefix = form.getFirstValue("prefix")

		if (prefix == null)  {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "The prefix is empty")
		}

		String[] types = form.getValuesArray("types")

		//start streaming
		return new WriterRepresentation(MediaType.APPLICATION_JSON) {
			@Override
			void write(Writer w) throws IOException {
				def writer = new JsonWriter(w)

				writer.beginObject()				

				//No pagination (start, count) yet

				//the results array
				writer.name("results")
				writer.beginArray()
				writer.flush()
				
				searcher.autoComplete(prefix, types as Set<String>) { String id, String type, String json ->
					writer.beginObject()
					writer.name("type").value(type)
					writer.name("data").jsonValue(json)
					writer.endObject()
					writer.flush()
				}

				writer.endArray()
				
				writer.endObject()
			}
		}
	}
}
