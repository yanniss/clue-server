package org.clyze.server.web.restlet.resource

import org.clyze.cclyzer.CClyzerAnalysisFamily
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class AnalysisFamilyResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserIsAuthenticated()

		def familyName = getReqAttributeDecoded("family")

		def res = [:]
		if (familyName.equalsIgnoreCase("doop")) {
			res["options"] = FormLogicHelper.BUNDLE_FORM_OPTIONS.collect { it.option }
			res["analyses"] = FormLogicHelper.ANALYSIS_FORM_OPTIONS.collect { it.option }
		} else if (familyName.equalsIgnoreCase("cclyzer")) {
			res["options"] = new CClyzerAnalysisFamily()
					.supportedOptions()
					.sort { it.name }
		} else
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Invalid analysis family: ${familyName}")

		new JsonRepresentation(res)
	}

}
