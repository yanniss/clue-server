package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.store.*
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import static org.clyze.server.web.persistent.model.doop.OptimizationDirective.OriginType.*
import org.clyze.server.web.restlet.JsonRepresentation

import org.restlet.data.Disposition
import org.restlet.data.Status
import org.restlet.data.MediaType
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class ManageConfigSetDirectivesResource extends AbstractServerResource {

	static final String EXPORT_ACTION = "EXPORT"
	static final String CLONE_ACTION  = "CLONE"
	static final String MERGE_ACTION  = "MERGE"
	static final String RENAME_ACTION = "RENAME"	
	static final String DELETE_FILTERED_ACTION = "DELETE_FILTERED"
	static final String EXPORT_FILTERED_ACTION = "EXPORT_FILTERED"
	static final String CLONE_FILTERED_ACTION  = "CLONE_FILTERED"
	static final String MERGE_FILTERED_ACTION  = "MERGE_FILTERED"

	@Override
	protected Representation get() throws ResourceException {
		
		ensureUserCanReadInputBundle()

		String configSetName = getReqAttributeDecoded("set")		

		if (!configSetName) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}

		BundleConfiguration configSet = Config.instance.storage.loadConfigSetByName(inputBundle, configSetName)

		if (!configSet) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not found: $configSet")
		}

		String action = getQueryValue("action")
		String target = getQueryValue("target")

		if (!action) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}

		if (action == EXPORT_ACTION) {
			return exportDirectivesTo(
				Config.instance.storage.givenBundle(inputBundle).&getMethodOptimizationDirectivesOfConfigSet.curry(configSet.id),
				target ?: configSet.nameOfSet
			)
		}
		else if (action == EXPORT_FILTERED_ACTION) {

			def form = getRequest().getResourceRef().getQueryAsForm()
			def params = new Searcher.DirectivesSearchParams(configSet: [configSet.id] as Set)
			OptimizationDirectivesResource.readSearchParamsFrom(form, params)
			DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)

			return exportDirectivesTo(
				store.&scan.curry(params.textualSearch, BooleanQueryBuilder.buildAND(params.asSearchFilters()), null),
				target ?: configSet.nameOfSet
			)
		}
		else if (action == CLONE_ACTION || action == CLONE_FILTERED_ACTION) {

			String cloneName = target ?: configSet.nameOfSet - ".clue" + "-copy.clue"

			BundleConfiguration targetConfigSet = Config.instance.storage.loadConfigSetByName(inputBundle, cloneName)
			if (targetConfigSet) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $cloneName")
			}			


			if (action == CLONE_ACTION) {
				return cloneDirectivesTo(
					Config.instance.storage.givenBundle(inputBundle).&getMethodOptimizationDirectivesOfConfigSet.curry(configSet.id),
					inputBundle,
					cloneName
				)
			}
			else { //action == CLONE_FILTERED_ACTION

				def form = getRequest().getResourceRef().getQueryAsForm()
				def params = new Searcher.DirectivesSearchParams(configSet: [configSetName] as Set)
				OptimizationDirectivesResource.readSearchParamsFrom(form, params)
				DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)

				return cloneDirectivesTo(
					store.&scan.curry(params.textualSearch, BooleanQueryBuilder.buildAND(params.asSearchFilters()), null),
					inputBundle,
					cloneName
				)
			}

		}
		else if (action == MERGE_ACTION || action == MERGE_FILTERED_ACTION) {

			BundleConfiguration targetConfigSet = Config.instance.storage.loadConfigSetByName(inputBundle, target)
			if (!targetConfigSet) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not found: $target")
			}

			if (action == MERGE_ACTION) {
				return mergeDirectivesTo(inputBundle, configSet, targetConfigSet)
			}
			else {
				def form = getRequest().getResourceRef().getQueryAsForm()
				def params = new Searcher.DirectivesSearchParams(configSet: [configSetName] as Set)
				OptimizationDirectivesResource.readSearchParamsFrom(form, params)
				return mergeDirectivesTo(inputBundle, params, targetConfigSet)
			}

		}
		else if (action == RENAME_ACTION) {

			if (!target) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
			}

			BundleConfiguration targetConfigSet = Config.instance.storage.loadConfigSetByName(inputBundle, target)
			if (targetConfigSet) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $target")
			}

			return renameConfigSet(inputBundle, configSet, target)
		}
		else if (action == DELETE_FILTERED_ACTION) {

			def form = getRequest().getResourceRef().getQueryAsForm()
			def params = new Searcher.DirectivesSearchParams(configSet: [configSetName] as Set)
			OptimizationDirectivesResource.readSearchParamsFrom(form, params)

			return deleteDirectivesOf(inputBundle, params)
		}
		else {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Unknown action: $action")
		}

	}	

	@Override
	protected Representation delete() throws ResourceException {

		ensureUserOwnsInputBundle()

		String configSetName = getReqAttributeDecoded("set")		

		if (!configSetName) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}

		BundleConfiguration configSet = Config.instance.storage.loadConfigSetByName(inputBundle, configSetName)

		if (!configSet) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not found: $configSet")
		}

		Config.instance.storage.removeConfigSet(inputBundle, configSet)

		return new JsonRepresentation(["msg": "OK"])
	}


	static Representation exportDirectivesTo(Closure directivesProvider, String fileName) {
		//log.debug("Exporting ${configSet.nameOfSet} to $fileName")
		WriterRepresentation r = new WriterRepresentation(MediaType.TEXT_PLAIN) {
			@Override
			void write(Writer w) throws IOException {
				exportDirectives(directivesProvider, w)
			}
		}

		Disposition d = new Disposition(Disposition.TYPE_ATTACHMENT)
		d.setFilename(fileName)
		r.setDisposition(d)
		return r
	}

	static Representation cloneDirectivesTo(DoopInputBundle inputBundle, BundleConfiguration configSet, String configSetName) {
		
		BundleConfiguration copyConfigSet = Config.instance.storage.createAndStoreNewConfigSet(configSetName, inputBundle)
		DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)
		Config.instance.storage.givenBundle(inputBundle).getMethodOptimizationDirectivesOfConfigSet(configSet.id) { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(inputBundle.id, copyConfigSet.id, directive, USER)
			bulk.scheduleSave copy
		}
		bulk.finishUp()
		return new JsonRepresentation(copyConfigSet.toMap())		
	}

	static Representation cloneDirectivesTo(Closure directivesProvider, DoopInputBundle inputBundle, String configSetName) {

		BundleConfiguration copyConfigSet = Config.instance.storage.createAndStoreNewConfigSet(configSetName, inputBundle)
		DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)
		directivesProvider.call() { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(inputBundle.id, copyConfigSet.id, directive, USER)
			bulk.scheduleSave copy
		}
		bulk.finishUp()
		return new JsonRepresentation(copyConfigSet.toMap())
	}

	static Representation mergeDirectivesTo(DoopInputBundle inputBundle, BundleConfiguration src, BundleConfiguration target) {
		inputBundle.mergeDirectives(src, target)		
		return new JsonRepresentation([msg:"OK"])
	}

	static Representation mergeDirectivesTo(DoopInputBundle inputBundle, Searcher.DirectivesSearchParams params, BundleConfiguration target) {
		DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
		inputBundle.mergeSourceDirectivesTo(
			store.&scan.curry(params.textualSearch, BooleanQueryBuilder.buildAND(params.asSearchFilters()), null),
			target
		)
		return new JsonRepresentation([msg:"OK"])
	}

	static Representation renameConfigSet(DoopInputBundle inputBundle, BundleConfiguration configSet, String newConfigSet) {
		Representation resultOfClone = cloneDirectivesTo(inputBundle, configSet, newConfigSet)
		Config.instance.storage.removeConfigSet(inputBundle, configSet)
		return resultOfClone
	}

	static Representation deleteDirectivesOf(DoopInputBundle inputBundle, Searcher.DirectivesSearchParams params) {
		DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
		store.deleteItems(params.textualSearch, BooleanQueryBuilder.buildAND(params.asSearchFilters()))
		return new JsonRepresentation([msg:"OK"])
	}


	static void exportDirectives(Closure directivesProvider, Writer writer) {
		directivesProvider.call() { id, type, String json ->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			directive.export(writer)
		}
	}
}