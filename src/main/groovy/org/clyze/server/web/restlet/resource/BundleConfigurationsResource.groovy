package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.persistent.store.OrderBy
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleConfigurationsResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {
		
		ensureUserOwnsInputBundle()

		def list = []
		Config.instance.storage.givenBundle(inputBundle).getConfigSetsOfBundle([new OrderBy(field:'nameOfSet')]) { id, type, String json->
			list << new BundleConfiguration().fromJSON(json)
		}

		return new JsonRepresentation([
			start  : 0, 
			count  : list.size(), 
			hits   : list.size(), 
			results: list
		])
	}	

	@Override
	protected Representation post(Representation entity) throws ResourceException {
		
		ensureUserOwnsInputBundle()

		String name = getQueryValue("name")

		BundleConfiguration bundleConfig = BundleConfigurationManager.createNewBundleConfiguration(inputBundle, name)
		
		return Responder.getBundleConfig(bundleConfig)
	}

}