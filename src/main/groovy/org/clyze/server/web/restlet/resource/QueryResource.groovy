package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.query.ReadyMadeQuery

import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 *
 */
class QueryResource extends AbstractServerResource {

    @Override
    protected Representation get() throws ResourceException {

        ensureUserCanReadInputBundle()
        loadAnalysisFromRequestParameter()
        
        Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

        def queryId = getReqAttributeDecoded("query")

        // Check if "queryId" is valid
        ReadyMadeQuery q = Config.instance.readyMadeQueries.find { it.id == queryId }
        if (!q) {
            throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, queryId)
        }

        if (q.requiresAnalysis && !analysis) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Missing analysis")
        }

        return q.fetch(searchLogic, inputBundle, analysis)
    }

}
