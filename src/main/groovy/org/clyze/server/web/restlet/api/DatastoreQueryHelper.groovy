package org.clyze.server.web.restlet.api

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.store.SearchLimits

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import com.google.gson.stream.JsonWriter

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

import org.restlet.data.Form
import org.restlet.data.MediaType
import org.restlet.representation.WriterRepresentation

/**
 * A helper for querying the datastore about persistent elements (symbols and symbol relations).
 * The parameters match the parameters of Datastore.search() method with the additional capability to
 * resolve the specified symbol relations.
 *
 * Examples:
 *
 * 1. The following request fetches all the symbols of the source file name extras.Test.java:
 *
 * _txt=sourceFileName:extras.Test.java
 *
 * 2. Fetching only the Class and Variable symbols of the above file name:
 * _txt=sourceFileName:extras.Test.java
 * _type=Class
 * _type=Variable
 *
 * 3. Fetching only the symbols of the above file name at a given position (e.g. line):
 * _txt=sourceFileName:extras.Test.java
 * position.startLine:20
 *
 * 4. Fetching the VarPointTo symbol relations of the above file name for the given position (the symbols at this position are also returned):
 * _txt=sourceFileName:extras.Test.java
 * position.startLine:20
 * _resolve:VarPointTo
 */
class DatastoreQueryHelper {

	static Log log = LogFactory.getLog(this)

	private final String analysisId
	private final String userId

	DatastoreQueryHelper(String analysisId, String userId) {
		this.analysisId = analysisId
		this.userId = userId
	}

	WriterRepresentation fetch(Form form) {
		//process form
		def params = new DatastoreQueryParams().process(analysisId, userId, form)

		Searcher searcher = Config.instance.storage.given(userId, null, analysisId)

		//start streaming
		return new WriterRepresentation(MediaType.APPLICATION_JSON) {
			@Override
			void write(Writer w) throws IOException {
				def writer = new JsonWriter(w)

				writer.beginObject()

				//start & count
				writer.name("start").value(params.pagination.start)
				writer.name("count").value(params.pagination.count)

				//the results array
				writer.name("results")
				writer.beginArray()
				writer.flush()

				//fetch the query
				def limits = new SearchLimits(start: params.pagination.start, count: params.pagination.count)
				searcher.search(params.text, BooleanQueryBuilder.buildAND(params.filters), params.ordering.orderByList, limits) {
					String id, String type, String json ->
						def res = JsonOutput.toJson(new JsonSlurper().parseText(json) << [resolvedType: type])
						writer.jsonValue(res)
						writer.flush()
				}
				writer.endArray()

				//total hits
				writer.name("hits").value(limits.hits)
				writer.endObject()

				// NOTE: Don't close the writer, restlet takes care of it!
			}
		}
	}
}
