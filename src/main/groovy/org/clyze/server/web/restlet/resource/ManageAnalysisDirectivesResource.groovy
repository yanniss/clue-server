package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.store.*
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import static org.clyze.server.web.persistent.model.doop.OptimizationDirective.OriginType.*
import org.clyze.server.web.restlet.JsonRepresentation

import org.restlet.data.Disposition
import org.restlet.data.Status
import org.restlet.data.MediaType
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation
import org.restlet.resource.ResourceException

class ManageAnalysisDirectivesResource extends AbstractServerResource {	

	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromUriComponent()

		String action = getQueryValue("action")
		String target = getQueryValue("target")

		if (!action) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}

		if (action == ManageConfigSetDirectivesResource.EXPORT_ACTION) {
			return exportDirectivesTo(inputBundle, analysis, target ?: analysis.displayName + ".clue")			
		}
		else if (action == ManageConfigSetDirectivesResource.CLONE_ACTION) {

			String configSetName = target ?: analysis.displayName + "-copy.clue"

			BundleConfiguration configSet = Config.instance.storage.loadConfigSetByName(inputBundle, configSetName)
			if (configSet) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Already exists: $configSetName")
			}			
				
			return cloneDirectivesTo(inputBundle, analysis, configSetName)			
		}
		else if (action == ManageConfigSetDirectivesResource.MERGE_ACTION) {

			BundleConfiguration configSet = Config.instance.storage.loadConfigSetByName(inputBundle, target)
			if (!configSet) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not found: $configSetName")
			}

			return mergeDirectivesTo(inputBundle, analysis, configSet)
		}		
		else {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Unknown action: $action")
		}
		
	}

	Representation exportDirectivesTo(DoopInputBundle inputBundle, ServerSideAnalysis analysis, String fileName) {
		log.debug("Exporting analysis directives to $fileName")
		WriterRepresentation r = new WriterRepresentation(MediaType.TEXT_PLAIN) {
			@Override
			void write(Writer w) throws IOException {
				inputBundle.exportAsClueFile(analysis, w)
			}
		}

		Disposition d = new Disposition(Disposition.TYPE_ATTACHMENT)
		d.setFilename(fileName)
		r.setDisposition(d)
		return r
	}

	Representation cloneDirectivesTo(DoopInputBundle inputBundle, ServerSideAnalysis analysis, String configSetName) {
		BundleConfiguration configSet = Config.instance.storage.createAndStoreNewConfigSet(configSetName, inputBundle)
		DataStore store = Config.instance.storage.bundleDoopStore(inputBundle)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)
		Config.instance.storage.givenAnalysis(analysis).getMethodOptimizationDirectivesOfAnalysis { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(inputBundle.id, configSet.id, directive, USER)
			bulk.scheduleSave copy
		}
		bulk.finishUp()
		return new JsonRepresentation(configSet.toMap())
	}

	Representation mergeDirectivesTo(DoopInputBundle inputBundle, ServerSideAnalysis analysis, BundleConfiguration configSet) {
		inputBundle.mergeDirectives(analysis, configSet)		
		return new JsonRepresentation([msg:"OK"])
	}
}