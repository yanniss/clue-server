package org.clyze.server.web.restlet.resource

import com.android.tools.r8.position.TextPosition
import com.android.tools.r8.position.TextRange
import com.android.tools.r8.shaking.BundleConfigurationRule
import com.android.tools.r8.shaking.ProguardConfiguration
import com.android.tools.r8.shaking.ProguardKeepRule
import org.apache.commons.fileupload.FileItem
import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfigurationContext
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.Request
import org.restlet.data.Status
import org.restlet.ext.fileupload.RestletFileUpload
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class PreviewProguardFileResource extends AbstractServerResource {

    @Override
    protected Representation post(Representation entity) throws ResourceException {

        ensureUserIsAuthenticated()

        def (String name, InputStream data) = readUploadedProguardFile(request)
        def rules = []

        Map<String, String> result = [
            origin: name,
            text  : null,
            rules : rules
        ]
        ProguardConfiguration proConfig =
                new BundleConfigurationContext(userId).
                parseProguard(data).
                forEachSupportedProguardRule { ProguardKeepRule rule ->

                    BundleConfigurationRule clueRule = BundleConfigurationRule.copyFrom(rule)
                    TextPosition start = ((TextRange) rule.getPosition()).getStart()
                    rules.add([
                        rule  : clueRule.getSource(),
                        coords: "${start.line},$start.column"
                    ])
                }

        result.text = proConfig.getParsedConfiguration()

        return new JsonRepresentation(result)
    }

    static Tuple2<String, InputStream> readUploadedProguardFile(Request request) {
        def upload = new RestletFileUpload(Config.instance.fileItemFactory)
        def items = upload.parseRequest(request)

        InputStream input = null
        String filename = null
        items.each { FileItem item ->
            if (item.fieldName == 'file' && !item.isFormField()) {
                if (!input) {
                    input = item.inputStream
                    filename = item.name
                }
                else {
                    throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "A single proguard file can be processed at a time")
                }
            }
        }

        if (input == null) {
            throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "No proguard files")
        }

        return new Tuple2(filename, input)
    }

}
