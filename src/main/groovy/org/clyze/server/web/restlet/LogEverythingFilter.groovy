package org.clyze.server.web.restlet

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.restlet.Request
import org.restlet.Response
import org.restlet.routing.Filter

/**
 * Wrap all requests/responses and log the duration of completion.
 */
class LogEverythingFilter extends Filter {

	Log log = LogFactory.getLog(getClass())

	private static final ThreadLocal<Long> startedAt = new ThreadLocal<Long>() {
		@Override protected Long initialValue() { return 0 }
	}

	@Override
	protected int beforeHandle(Request request, Response response) {
		log.debug("New ${request.getMethod()} request: ${request.getResourceRef().getPath()}")
		startedAt.set(System.currentTimeMillis())
		return CONTINUE
	}

	@Override
	protected void afterHandle(Request request, Response response) {
		long duration = System.currentTimeMillis() - startedAt.get()
		log.debug("Spent $duration millis on processing ${request.getMethod()} request: ${request.getResourceRef().getPath()}")
	}

}
