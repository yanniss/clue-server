package org.clyze.server.web.restlet.resource

import org.clyze.persistent.model.doop.Method
import org.clyze.persistent.model.doop.MethodInvocation
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.model.doop.InvocationParts
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.persistent.model.doop.InvocationValues
import org.clyze.server.web.persistent.model.doop.Value
import org.clyze.server.web.persistent.model.doop.VarReturn
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.restlet.ResponseSchema.conform

class MethodResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserCanReadInputBundle()
		loadAnalysisFromRequestParameter()
		
		Searcher searchLogic = Config.instance.storage.given(userId, inputBundle, analysis)

		def method = getReqAttributeDecoded("method")

		// Explicitly check for null, since the empty string is a valid value
		def definition = (getQueryValue("noDef") == null)

		def res = [:]
		Method m = searchLogic.findMethod(method)
		if (!m) return new JsonRepresentation(res)
		if (definition) res = conform(Method, m.toMap(), searchLogic)

		///////////////////////////////////////////////////////////////////////
		def hierarchy = getQueryValue("hierarchy")
		if (hierarchy) {
			res.hierarchy = [] as Set
			if (hierarchy == "subTypes") {
				res.hierarchy = searchLogic.findMethodSubTypes(method).collect {
					conform(Method, it.toMap(), searchLogic)
				}
			} else if (hierarchy == "superTypes")
				res.hierarchy = searchLogic.findMethodSuperTypes(method).collect {
					conform(Method, it.toMap(), searchLogic)
				}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("reverseInvocations") != null) {
			res.reverseInvocations = [] as Set
			searchLogic.searchInvocationValuesTo(method) { String json ->
				def t = new InvocationValues().fromJSON(json)
				MethodInvocation i = searchLogic.findInvocation(t.invocationId)
				if (i) res.reverseInvocations << conform(MethodInvocation, i.toMap(), searchLogic)
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("this") != null) {
			res.this = [] as Set
			if (!m.isStatic && !m.isInterface && !m.isAbstract) {
				res.this = searchLogic.findClosestValues("$method/@this", 0).collect {
					conform(Value, it.toMap(), searchLogic)
				}
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("returnValues") != null) {
			res.returnValues = [] as Set
			searchLogic.searchVarReturn(method) { String json ->
				String varId = new VarReturn().fromJSON(json).varId
				res.returnValues += searchLogic.findClosestValues(varId, 0).collect {
					conform(Value, it.toMap(), searchLogic)
				}
			}
		}

		///////////////////////////////////////////////////////////////////////
		// Explicitly check for null, since the empty string is a valid value
		if (getQueryValue("reverseChaInvocations") != null) {
			res.reverseChaInvocations = [] as Set
			searchLogic.searchInvocationCHAValuesTo(method) { String json ->
				def t = new InvocationParts().fromJSON(json) as InvocationParts
				def invo = searchLogic.findInvocation(t.invocationId)
				if (invo) res.reverseChaInvocations << conform(MethodInvocation, invo.toMap(), searchLogic)
			}
		}

		new JsonRepresentation(res)
	}
}
