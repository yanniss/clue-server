package org.clyze.server.web.restlet.api

import org.clyze.server.web.restlet.resource.AbstractServerResource
import org.restlet.data.Form
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

/**
 * An endpoint for querying the datastore about persistent elements (symbols and symbol relations) of a single analysis.
 */
class ExecuteDatastoreQueryResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {
		return post(request.getEntity())
	}

	@Override
	protected Representation post(Representation entity) throws ResourceException {

		ensureUserCanReadInputBundle()

		return new DatastoreQueryHelper(analysis.id, analysis.userId).fetch(new Form(request.getEntity()))
	}
}
