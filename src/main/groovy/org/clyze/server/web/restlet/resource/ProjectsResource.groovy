package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config

import org.clyze.server.web.persistent.model.Project

import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams

import org.clyze.server.web.restlet.JsonRepresentation

import org.clyze.server.web.persistent.store.SearchLimits

import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class ProjectsResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {

		ensureUserIsAuthenticated()

		def form = getRequest().getResourceRef().getQueryAsForm()
		def p = new PaginationParams()
		def o = new OrderByParams()
		p.process(form)
		o.process(form)
		def text = form.getFirstValue("text")

		def list = []
		//TODO: Stream results
		SearchLimits limits = Config.instance.storage.givenUser(userId).processProjectsOfUser(text, p.start, p.count, o.orderByList) {
			String id, String type, String json ->			
				list << ProjectResource.publicViewOf(new Project().fromJSON(json))				
		}

		new JsonRepresentation([start: limits.start, count: limits.count, hits: limits.hits, results: list])
	}

	@Override
	protected Representation post(Representation entity) throws ResourceException {
		
		ensureUserCanPostProjects()

		Form form = new Form(entity)

		String name = form.getFirstValue("name")

		if (!name) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Required data missing")
		}
		//TODO: Check that the project's name is unique for this user		

		Project project = new Project(userId, name)		
		Config.instance.storage.storeNewProject(project)

		return new JsonRepresentation(ProjectResource.publicViewOf(project))
	}

}