package org.clyze.server.web.restlet.resource

import static groovy.io.FileType.DIRECTORIES
import static groovy.io.FileType.FILES
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class RelationsResource extends AbstractServerResource {
	@Override
	protected Representation get() throws ResourceException {

		ensureUserOwnsInputBundle()

        def bundleRelations = [] as List
        inputBundle.factsDir.eachFileMatch FILES, ~/.*\.facts/, { File f ->
            bundleRelations << f.name[0..-7]
        }

        def analysisRelations = [:]
        inputBundle.analysesDir.eachFile DIRECTORIES, { File aDir ->
            def db = new File(aDir, "database")
            if (db.exists()) {
                def rs = [] as List
                db.eachFileMatch FILES, ~/.*\.csv/, { File f -> rs << f.name[0..-5] }
                if (rs.size() > 0)
                    analysisRelations[aDir.name] = rs
            }
        }

        def data = [ bRelations : bundleRelations, aRelations : analysisRelations ]
        new JsonRepresentation(data)
	}
}
