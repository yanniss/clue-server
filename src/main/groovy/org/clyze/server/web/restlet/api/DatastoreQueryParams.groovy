package org.clyze.server.web.restlet.api

import org.clyze.server.web.persistent.model.doop.InstanceFieldValues
import org.clyze.server.web.persistent.model.doop.InvocationValues
import org.clyze.server.web.persistent.model.doop.StaticFieldValues
import org.clyze.server.web.persistent.model.doop.VarValues
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.resource.ResourceException

/**
 * A utility class for processing the datastore query params from a web form.
 */
class DatastoreQueryParams {

	static final Set<String> VALID_RESOLVE_VALUES = [
			InstanceFieldValues.simpleName,
			StaticFieldValues.simpleName,
			VarValues.simpleName,
			InvocationValues.simpleName
	] as Set

	static final String TEXT_PARAMETER = "_txt"
	static final String FIELDS_PARAMETER = "_fields"
	static final String RESOLVE_PARAMETER = "_resolve"

	private static List<String> IGNORED_PARAMS = [
			PaginationParams.START_PARAMETER,
			PaginationParams.COUNT_PARAMETER,
			OrderByParams.SORT_PARAMETER
	]

	String text = null
	Map<String, Object> filters = null
	List<String> fields = null
	List<String> resolve = null
	PaginationParams pagination = new PaginationParams()
	OrderByParams ordering = new OrderByParams()

	boolean resolveInstanceFieldValues = false
	boolean resolveStaticFieldValues = false
	boolean resolveVarValues = false
	boolean resolveInvocationValues = false
	boolean resolveEnabled = false

	DatastoreQueryParams process(String analysisId, String userId, Form form) {

		//process the pagination params
		pagination.process(form)
		//process the order by params
		ordering.process(form)
		//process the rest of the form
		Map<String, List<String>> terms = [:]

		def putTerm = { String key, String[] values ->
			List<String> termValues = terms.get(key)
			if (!termValues) {
				termValues = []
				terms.put(key, termValues)
			}
			termValues.addAll(values)
		}

		form.getNames().findAll { !(it in IGNORED_PARAMS) }.each { String param ->
			switch (param) {
				case TEXT_PARAMETER:
					text = form.getFirstValue(param)?.trim()
					break
				case FIELDS_PARAMETER:
					fields = form.getValuesArray(param)
					break
				case RESOLVE_PARAMETER:
					resolve = form.getValuesArray(param)
					break
				default:
					putTerm(param, form.getValuesArray(param))
					break
			}
		}

		//Sanitize
		if (!text && !terms) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Empty parameters")
		}

		if (resolve) {
			String firstInvalidResolve = resolve.find { !(it in VALID_RESOLVE_VALUES) }
			if (firstInvalidResolve) {
				throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Not a valid resolve value: $firstInvalidResolve")
			} else {
				resolveEnabled = true
				resolveInstanceFieldValues = resolve.contains(InstanceFieldValues.simpleName)
				resolveStaticFieldValues = resolve.contains(StaticFieldValues.simpleName)
				resolveVarValues = resolve.contains(VarValues.simpleName)
				resolveInvocationValues = resolve.contains(InvocationValues.simpleName)
			}
		}

		//process terms to generate the filters
		filters = terms.collectEntries { String key, List<String> value ->
			value.size() == 1 ?
					[(key): value[0]] :
					[(key): value]
		}

		if (analysisId) {
			filters.rootElemId = analysisId
		}
		/*
		if (userId) {
			filters.userId = userId
		}
		*/

		return this
	}
}
