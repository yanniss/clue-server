package org.clyze.server.web.restlet.resource

import org.clyze.analysis.InputType
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.analysis.DecoratedAnalysisOption
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

import static org.clyze.server.web.analysis.DecoratedAnalysisOption.NO_DESCRIPTION

class CreateBundleOrAnalysisOptionsResource extends AbstractServerResource {

	@Override
	protected Representation get() throws ResourceException {
		def what = getQueryValue("what")
		if ("BUNDLE".equalsIgnoreCase(what)) {
			return new JsonRepresentation([options: FormLogicHelper.BUNDLE_FORM_OPTIONS.collect(this.&toMap)])
		} else if ("ANALYSIS".equalsIgnoreCase(what)) {
			return new JsonRepresentation([options: FormLogicHelper.ANALYSIS_FORM_OPTIONS.collect(this.&toMap)])
		} else {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Invalid what parameter: ${what}")
		}
	}

	/* 
	 * The basic processing logic for each option is the following:
	 * a.  option.argInputType == null:
	 * a1. option.validValues is empty: render the option using a text field.
	 * a2. option.validValues is non-empty: render the option using a drop-down. 
	 *     Use the (optional) option.defaultValue field to determine the default selected value of the drop-down.
	 * b.  option.argInputType != null:
 	 *     option.multipleValues == true: support multiple files.
 	 *     option.fileTypes contains a list of the supported types of "file inputs": 
     *     LOCAL_FILE, MAVEN_ARTIFACT, ZIP, URL.
     *     The LOCAL_FILE and ZIP input types should be handled as identical by the UI.
	 */

	static final Map<String, Object> toMap(DecoratedAnalysisOption option) {
		def formDesc = option.formDescription
		def description = formDesc ? (formDesc != NO_DESCRIPTION ? formDesc : null) : option.description
		return [
				id            : option.id,
				name          : option.label,
				group         : option.group,
				description   : description,
				isFile        : option.argInputType,
				fileTypes     : getFileTypes(option.argInputType),
				defaultValue  : option.value ?: null,
				validValues   : option.validValues ?: [], //the allowed values (drop-down)
				multipleValues: option.multipleValues,
				isBoolean     : (!option.argName),
				isMandatory   : option.isMandatory
		]
	}

	static List<String> getFileTypes(InputType inputType) {
		inputType ? inputType.resolverTypes.collect { it.name() } : []
	}
}
