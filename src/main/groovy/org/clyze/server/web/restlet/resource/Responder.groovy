package org.clyze.server.web.restlet.resource

import com.google.gson.stream.JsonWriter
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.server.web.Config
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.JsonRepresentation
import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams
import org.restlet.data.MediaType
import org.restlet.representation.Representation
import org.restlet.representation.WriterRepresentation

class Responder {

    static Representation getEffectiveDirectives(Searcher searcher,
                                                 Searcher.DirectivesSearchParams searchParams,
                                                 PaginationParams p,
                                                 OrderByParams o,
                                                 Map<String, String> facets) {

        return new WriterRepresentation(MediaType.APPLICATION_JSON) {

            @Override
            void write(Writer w) throws IOException {
                def writer = new JsonWriter(w)

                writer.beginObject()

                writer.name("results")
                writer.beginArray()
                SearchLimits limits = searcher.searchMethodOptimizationDirectives(
                        searchParams,
                        p.start,
                        p.count,
                        o.orderByList
                ) { String json ->
                    writer.jsonValue(json)
                    writer.flush()
                }
                writer.endArray()

                if (!facets.isEmpty()) {
                    writer.name("facets")
                    writer.beginObject()
                    facets.each { fieldNameInResult, fieldNameInDatastore ->
                        Map<String, Long> facet = searcher.facetOfOptimizationDirectives(
                                searchParams.textualSearch ? searchParams : searchParams.createCopyWithNullifiedField(fieldNameInDatastore),
                                fieldNameInDatastore,
                                OptimizationDirectivesResource.MAX_RESULTS_PER_FACET
                        )
                        writer.name(fieldNameInResult)
                        writer.beginObject()
                        facet.each { k, v ->
                            writer.name(k).value(v)
                        }
                        writer.endObject()
                    }
                    writer.endObject()
                }

                writer.name("start").value(limits.start)
                writer.name("count").value(limits.count)
                writer.name("hits").value(limits.hits)

                writer.endObject()
            }
        }

    }

    static Representation getBundleConfig(BundleConfiguration bundleConfig) {

        Map json = bundleConfig.toMap()

        String analysisId = bundleConfig.getCurrentAnalysisId()
        if (analysisId) {
            ServerSideAnalysis a = Config.instance.storage.loadAnalysis(analysisId)
            json.put('currentAnalysis', a?.toMap())
        }

        return new JsonRepresentation(json)
    }

    static Representation groupMatchesByPackage(DoopInputBundle bundle, BundleConfiguration bundleConfig, String ruleId) {

        Searcher.DirectivesSearchParams searchParams = new Searcher.DirectivesSearchParams(
            configSet: [bundleConfig.id] as Set,
            ruleId: [ruleId] as Set
        )
        Searcher searcher = Config.instance.storage.givenBundle(bundle)
        Map<String, Long> facet = searcher.facetOfOptimizationDirectives(
            searchParams,
            'packageName',
            OptimizationDirectivesResource.MAX_RESULTS_PER_FACET
        )

        def listOfObjects = facet.collect { String pkg, Long cnt ->
            return [name: pkg, matchingMethods: cnt]
        }

        return new JsonRepresentation([results:listOfObjects])
    }

    static Representation groupMatchesOfPackageByClass(DoopInputBundle bundle,
                                                       BundleConfiguration bundleConfig,
                                                       String ruleId,
                                                       String packageName) {

        Searcher.DirectivesSearchParams searchParams = new Searcher.DirectivesSearchParams(
            configSet: [bundleConfig.id] as Set,
            ruleId: [ruleId] as Set,
            packageName: [packageName] as Set
        )
        Searcher searcher = Config.instance.storage.givenBundle(bundle)
        Map<String, Long> facet = searcher.facetOfOptimizationDirectives(
            searchParams,
            'className',
            OptimizationDirectivesResource.MAX_RESULTS_PER_FACET
        )

        Set<Klass> classes = [] as Set
        searcher.searchByDoopIds(facet.keySet().collect { packageName + "." + it }, [Klass.simpleName]) { String id, String type, String json ->
            classes.add(new Klass().fromJSON(json) as Klass)
        }

        def listOfObjects = classes.collect { Klass kl ->
            def map = kl.toMap()
            map.matchingMethods = facet.get(kl.name)
            return map
        }

        return new JsonRepresentation([results:listOfObjects])
    }

    static Representation getMatchesOfClassInPackage(DoopInputBundle bundle,
                                                     BundleConfiguration bundleConfig,
                                                     String ruleId,
                                                     String packageName,
                                                     String className) {

        Searcher.DirectivesSearchParams searchParams = new Searcher.DirectivesSearchParams(
            configSet: [bundleConfig.id] as Set,
            ruleId: [ruleId] as Set,
            packageName: [packageName] as Set,
            className: [className] as Set
        )

        Searcher searcher = Config.instance.storage.givenBundle(bundle)
        def listOfObjects = []
        searcher.searchMethodOptimizationDirectives(searchParams, 0, PaginationParams.MAX_COUNT, []) { String json ->
            listOfObjects.add(new KeepRemoveMethodDirective().fromJSON(json))
        }

        return new JsonRepresentation([results:listOfObjects])
    }
}
