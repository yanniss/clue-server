package org.clyze.server.web.restlet.resource

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.SearchLimits
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.restlet.JsonRepresentation
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class UsersResource extends AbstractServerResource {
	/**
	 * Returns a list of the available user objects
	 */
	@Override
	protected Representation get() throws ResourceException {

		ensureUserIsAdmin()

		def res = []
		def limits = new SearchLimits(start: 0, count: 100)
		Config.instance.storage.accountStore().search(BooleanQueryBuilder.build("_type", "User"), null, limits) {
			String id, String type, String json -> res << id
		}
		return new JsonRepresentation([list: res])
	}

	/**
	 * Creates a new user and returns its id
	 */
	@Override
	protected Representation post(Representation entity) throws ResourceException {

		ensureUserIsAdmin()

		def form = new Form(request.entity)
		def username = form.getFirstValue("username")
		def password = form.getFirstValue("password")

		log.debug("Attempting to create new user $username")
		try {
			def authManager = Config.instance.authManager

			if (!username) { //non-empty (plus whatever constraint we should also enforce, e.g. length)
				throw new RuntimeException("The username is invalid")
			}
			if (!password) { //non-empty (plus whatever constraint we should also enforce, e.g. length, special chars)
				throw new RuntimeException("The password is invalid")
			}
			if (authManager.userExists(username)) {
				throw new RuntimeException("The username already exists")
			}

			authManager.createUser(username, password)
			return new JsonRepresentation([id: username])
		} catch (RuntimeException e) {
			throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, e.getMessage())
		}
	}
}
