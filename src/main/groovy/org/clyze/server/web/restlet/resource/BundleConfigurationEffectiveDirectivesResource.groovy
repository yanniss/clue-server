package org.clyze.server.web.restlet.resource


import org.clyze.server.web.Config
import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.api.OrderByParams
import org.clyze.server.web.restlet.api.PaginationParams
import org.restlet.data.Form
import org.restlet.representation.Representation
import org.restlet.resource.ResourceException

class BundleConfigurationEffectiveDirectivesResource extends AbstractServerResource {

    final static Map<String, String> FACETS_OF_EFFECTIVE_DIRECTIVES = [
            //fieldNameInResult : fieldNameInDatastore
            "directiveType": "typeOfDirective",
            "originType"   : "originType",
            "origin"       : "origin",
            "package"      : "packageName",
            "class"        : "className",
            "ruleId"       : "ruleId"
            //,"method"        : "methodName"
    ]

    @Override
    protected Representation get() throws ResourceException {

        ensureUserOwnsInputBundle()

        Searcher searchLogic = Config.instance.storage.givenBundle(inputBundle)

        def form = getRequest().getResourceRef().getQueryAsForm()
        def p = new PaginationParams()
        def o = new OrderByParams()
        p.process(form)
        o.process(form)

        def bundleConfigName = getReqAttributeDecoded("name")
        BundleConfiguration bundleConfig = new BundleConfigurationManager(userId, inputBundle, bundleConfigName).loadBundleConfiguration()

        def params = new Searcher.DirectivesSearchParams(configSet: [bundleConfig.id] as Set)
        readSearchParamsFrom(form, params)

        boolean withFacets = Boolean.parseBoolean(form.getFirstValue('_facets'))

        return Responder.getEffectiveDirectives(searchLogic, params, p, o, withFacets ? FACETS_OF_EFFECTIVE_DIRECTIVES : Collections.<String, String>emptyMap())
    }

    //helper method
    static void readSearchParamsFrom(Form form, Searcher.DirectivesSearchParams params) {

        OptimizationDirectivesResource.readSearchParamsFrom(form, params)
        params.effectiveDirectives = true
    }

}
