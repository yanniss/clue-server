package org.clyze.server.web

import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger

class MonitorThreadFactory implements ThreadFactory {

	private final MonitorThreadGroup group = new MonitorThreadGroup()
	private final AtomicInteger cnt = new AtomicInteger(1)

	Thread newThread(Runnable r) {
		Thread t = new Thread(group, r, group.getName() + "-" + cnt.getAndIncrement())
		t.setDaemon(true)
		return t
	}

	void startNewThread(Runnable r) {
		newThread(r).start()
	}
}