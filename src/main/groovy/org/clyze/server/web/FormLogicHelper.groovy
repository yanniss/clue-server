package org.clyze.server.web

import groovy.util.logging.Log4j
import org.apache.commons.fileupload.FileItem
import org.clyze.analysis.AnalysisOption
import org.clyze.analysis.BooleanAnalysisOption
import org.clyze.analysis.InputType
import org.clyze.analysis.IntegerAnalysisOption
import org.clyze.doop.core.DoopAnalysisFamily
import org.clyze.doop.input.InputResolutionContext
import org.clyze.server.web.analysis.DecoratedAnalysisOption
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.utils.FileOps

import static groovy.io.FileType.FILES
import static org.clyze.server.web.analysis.DecoratedAnalysisOption.NO_DESCRIPTION

/**
 * Helper methods for preparing or processing the bundle / analysis forms.
 *
 * The getAnalysisFormOptions() and getBundleFormOptions() methods return the list of AnalysisOption objects that drive both:
 * (a) the form generation UI,
 * (b) the form data processors.
 */
@Log4j
class FormLogicHelper {

	private static final String DEFAULT_PLATFORM = "android_25_fulljars"

	static final List<DecoratedAnalysisOption> BUNDLE_FORM_OPTIONS
	static final List<DecoratedAnalysisOption> ANALYSIS_FORM_OPTIONS

	static {
		def options = DoopAnalysisFamily.instance.supportedOptionsAsMap()

		def platform = new AnalysisOption<String>(options.PLATFORM)
		platform.value = DEFAULT_PLATFORM

		BUNDLE_FORM_OPTIONS = [				
				new DecoratedAnalysisOption(platform, "Platform",
						"The platform on which to perform the analysis"),
				new DecoratedAnalysisOption(options.INPUTS, "Input file(s)", null, true),
				new DecoratedAnalysisOption(options.LIBRARIES, "Library file(s)"),
				new DecoratedAnalysisOption(options.MAIN_CLASS, "Main class(es)", NO_DESCRIPTION),
				new DecoratedAnalysisOption(options.HEAPDLS, "HeapDL file(s)"),
				new DecoratedAnalysisOption(options.DACAPO, "DaCapo 2006 benchmark"),
				new DecoratedAnalysisOption(options.DACAPO_BACH, "DaCapo Bach benchmark"),
				new DecoratedAnalysisOption(options.TAMIFLEX, "TamiFlex file", NO_DESCRIPTION),
				new DecoratedAnalysisOption(options.APP_REGEX, "Application regex"),
				new DecoratedAnalysisOption(options.EXTRACT_MORE_STRINGS, "Extract more strings"),
				new DecoratedAnalysisOption(options.THOROUGH_FACT_GEN, "Thorough fact generation"),
				new DecoratedAnalysisOption(new AnalysisOption<String>(
						id: "SOURCES_JAR",
						description: "The source files of the application",
						argName: "JAR",
						argInputType: InputType.MISC
				), "Source files (JAR)"),
				new DecoratedAnalysisOption(new AnalysisOption<String>(
						id: "JCPLUGIN_METADATA",
						description: "The metadata of the source files of the application (generated with jcPlugin)",
						argName: "JAR",
						argInputType: InputType.MISC
				), "JCPlugin metadata (JAR)"),
				new DecoratedAnalysisOption(new AnalysisOption<File>(
						id: "CLUE_FILE",
						description: "The clue file containing the optimization directives",
						argName: "FILE",
						argInputType: InputType.MISC
				), "Clue file", "The clue file containing existing optimization directives"),
				/*
				new DecoratedAnalysisOption(new AnalysisOption<File>(
						id: "SEEDS_FILE",
						description: "The proguard seeds file",
						argName: "FILE",
						argInputType: InputType.MISC
				), "Seeds file", "The proguards seeds file")
				*/
				new DecoratedAnalysisOption(new AnalysisOption<File>(
						id: "PROGUARD_FILE",
						description: "The proguard file",
						argName: "FILE",
						argInputType: InputType.MISC
				), "Proguard file", "The proguard file")
		]

		def analysisOption = options.ANALYSIS.clone()
		analysisOption.validValues = [
				"types-only",
				"context-insensitive",
				"context-insensitive-plus",
				"1-call-site-sensitive",
				"1-object-sensitive",
				"2-object-sensitive+heap",
				"2-type-sensitive+heap",
		]
		def simulateNativeReturns = new BooleanAnalysisOption(options.SIMULATE_NATIVE_RETURNS)
		simulateNativeReturns.value = true

		def scanNativeCode = new BooleanAnalysisOption(options.SCAN_NATIVE_CODE)
		scanNativeCode.value = true

		ANALYSIS_FORM_OPTIONS = [
				new DecoratedAnalysisOption(analysisOption, "The analysis to run", NO_DESCRIPTION, true),
				new DecoratedAnalysisOption(options.TIMEOUT, "Execution timeout"),
				new DecoratedAnalysisOption(options.DISCOVER_MAIN_METHODS, "Discover 'main' method(s)",
						"Discover method with the classic signature of main()."),
				new DecoratedAnalysisOption(options.IGNORE_MAIN_METHOD, "Ignore 'main' method(s)"),
				new DecoratedAnalysisOption(options.DISCOVER_TESTS, "Discover test code"),
				new DecoratedAnalysisOption(options.REFLECTION_CLASSIC, "Classic reflection analysis"),
				new DecoratedAnalysisOption(options.REFLECTION_SUBSTRING_ANALYSIS, "Reflection substring analysis"),
				new DecoratedAnalysisOption(options.REFLECTION_CONTEXT_SENSITIVITY,
						"Use context-sensitivity in reflection handling"),
				new DecoratedAnalysisOption(options.REFLECTION_HIGH_SOUNDNESS_MODE, "Reflection high-soundness mode"),
				new DecoratedAnalysisOption(options.REFLECTION_SPECULATIVE_USE_BASED_ANALYSIS,
						"Reflection speculative-use-based analysis", NO_DESCRIPTION),
				new DecoratedAnalysisOption(options.REFLECTION_INVENT_UNKNOWN_OBJECTS,
						"Reflection invent-unknown-objects analysis", NO_DESCRIPTION),
				new DecoratedAnalysisOption(options.REFLECTION_METHOD_HANDLES, "Analyze method handles using reflection"),
				new DecoratedAnalysisOption(options.REFLECTION_REFINED_OBJECTS, "Refine objects in reflection", NO_DESCRIPTION),
				new DecoratedAnalysisOption(scanNativeCode, "Scan JNI native code for callbacks to Java"),
				new DecoratedAnalysisOption(options.LIGHT_REFLECTION_GLUE, "Only support simple reflective patterns"),
				new DecoratedAnalysisOption(options.NO_MERGES, "Don't merge string constants", NO_DESCRIPTION),
				new DecoratedAnalysisOption(options.DISTINGUISH_REFLECTION_ONLY_STRING_CONSTANTS,
						"Distinguish reflection string constants"),
				new DecoratedAnalysisOption(options.DISTINGUISH_ALL_STRING_CONSTANTS,
						"Distinguish all string constants"),
				new DecoratedAnalysisOption(options.DISTINGUISH_ALL_STRING_BUFFERS,
						"Distinguish all string buffers"),
				new DecoratedAnalysisOption(options.DISTINGUISH_STRING_BUFFERS_PER_PACKAGE,
						"Distinguish string buffers per package"),
				new DecoratedAnalysisOption(options.EXCLUDE_IMPLICITLY_REACHABLE_CODE,
						"Exclude implicitly reachable code"),
				new DecoratedAnalysisOption(options.COARSE_GRAINED_ALLOCATION, "Coarse-grained allocations"),
				new DecoratedAnalysisOption(options.NO_MERGE_LIBRARY_OBJECTS, "Don't merge library objects"),
				new DecoratedAnalysisOption(options.CONTEXT_SENSITIVE_LIBRARY_ANALYSIS,
						"Context-sensitive analysis of libraries"),
				new DecoratedAnalysisOption(simulateNativeReturns, "Simulate native neturns"),
				new DecoratedAnalysisOption(options.REFLECTION_DYNAMIC_PROXIES, "Analyze uses of the dynamic proxies API"),
				new DecoratedAnalysisOption(options.OPEN_PROGRAMS, "Treat as open program"),
				new DecoratedAnalysisOption(options.X_SERVER_LOGIC_THRESHOLD, "Points-to sets threshold"),
		]
	}

	static final List<FormFileProcessor> BUNDLE_FORM_FILE_PROCESSORS = [
			new InputOrLibraryFormFileProcessor(),
			new HeapDLFormFileProcessor(),
			new SourcesJarFormFileProcessor(),
			new JcPluginMetadataFormFileProcessor(),
			new TamiflexFormFileProcessor(),
			new ClueFileProcessor(),
			//new SeedsFileProcessor()
			new ProguardFileProcessor()
	]

	static final List<FormFieldProcessor> BUNDLE_FORM_FIELD_PROCESSORS = [
			new InputOrLibraryFormFieldProcessor(),
			new GenericBundleOptionFormFieldProcessor(),
	]

	static final List<FormFieldProcessor> ANALYSIS_FORM_FIELD_PROCESSORS = [
			new GenericAnalysisOptionFormFieldProcessor()
	]

	private static File writeFile(FileItem item, File dir, String name) {
		def f = new File(dir, name)
		item.write(f)
		return FileOps.findFileOrThrow(f, "Could not save uploaded file: $name")
	}

	static interface OptionAware {
		boolean supportsOption(DecoratedAnalysisOption option)
	}

	static interface FormFileProcessor extends OptionAware {
		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item)
	}

	static class InputOrLibraryFormFileProcessor implements FormFileProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			option.argInputType in [InputType.INPUT, InputType.LIBRARY]
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			writeFile(item, bundle.inputsDir, item.name)
			ctx.add item.name, option.argInputType
			log.debug "Added ${option.argInputType} ${item.name}"
		}
	}

	static class HeapDLFormFileProcessor implements FormFileProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			option.argInputType == InputType.HEAPDL
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			if (item.name.endsWith(".zip")) {
				def zip = writeFile(item, bundle.heapdlDir, item.name)
				FileOps.unzip(zip, bundle.heapdlDir)
				bundle.heapdlDir.eachFileMatch FILES, ~/.*\.hprof/, { File f ->
					log.debug("Found .hprof file $f")
					ctx.add f.name, InputType.HEAPDL
					log.debug "Added ${InputType.HEAPDL} ${item.name}"
				}
			} else {
				writeFile(item, bundle.heapdlDir, item.name)
				ctx.add item.name, InputType.HEAPDL
				log.debug "Added ${InputType.HEAPDL} ${item.name}"
			}
		}
	}

	static class SourcesJarFormFileProcessor implements FormFileProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "SOURCES_JAR"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			def sourcesJar = writeFile(item, bundle.inputsDir, item.name)
			def sourcesSubDir = FileOps.ensureDirExistsOrThrow(new File(bundle.sourcesDir, item.name), "Could not create sources sub-dir")
			FileOps.unzip(sourcesJar, sourcesSubDir)
			FileOps.unzip(sourcesJar, bundle.sourcesMergedDir)
			log.debug("Added sources jar ${item.name} in ${bundle.sourcesDir}}")
		}
	}

	static class JcPluginMetadataFormFileProcessor implements FormFileProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "JCPLUGIN_METADATA"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			def jcPluginMetadataJar = writeFile(item, bundle.inputsDir, item.getName())
			FileOps.unzip(jcPluginMetadataJar, bundle.jcPluginMetadataDir)
			bundle.hasSrcMetadata = true
			log.debug("Added sources metadata ${item.name} in ${bundle.jcPluginMetadataDir}")
		}
	}

	static class TamiflexFormFileProcessor implements FormFileProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "TAMIFLEX"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			def tamiflexFile = writeFile(item, bundle.etcDir, item.getName())
			bundle.options.TAMIFLEX.value = tamiflexFile.canonicalPath
			log.debug("Added tamiflex file ${item.name} in ${bundle.etcDir}")
		}
	}

	static class ClueFileProcessor implements FormFileProcessor {
		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "CLUE_FILE"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			writeFile(item, bundle.inputsDir, DoopInputBundle.DEFAULT_CONFIG_SET)
			log.debug("Added clue file ${DoopInputBundle.DEFAULT_CONFIG_SET} in ${bundle.inputsDir}")
		}
	}

	static class SeedsFileProcessor implements FormFileProcessor {
		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "SEEDS_FILE"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			writeFile(item, bundle.inputsDir, DoopInputBundle.SEEDS_FILE)
			log.debug("Added seeds file ${DoopInputBundle.SEEDS_FILE} in ${bundle.inputsDir}")
		}
	}

	static class ProguardFileProcessor implements FormFileProcessor {
		boolean supportsOption(DecoratedAnalysisOption option) {
			option.id == "PROGUARD_FILE"
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, FileItem item) {
			writeFile(item, bundle.inputsDir, DoopInputBundle.PROGUARD_FILE)
			log.debug("Added proguard file ${DoopInputBundle.PROGUARD_FILE} in ${bundle.inputsDir}")
		}
	}

	static interface FormFieldProcessor extends OptionAware {
		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, String value)
	}

	static class InputOrLibraryFormFieldProcessor implements FormFieldProcessor {

		private final InputOrLibraryFormFileProcessor formFileProcessor = new InputOrLibraryFormFileProcessor()

		boolean supportsOption(DecoratedAnalysisOption option) {
			formFileProcessor.supportsOption(option)
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, String value) {
			String jars = value
			jars.split(",").each { String jar ->
				if (jar) {
					ctx.add jar.trim(), option.argInputType
					log.debug "Added ${option.argInputType} ${jar.trim()}"
				}
			}
		}
	}

	static class GenericBundleOptionFormFieldProcessor implements FormFieldProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			BUNDLE_FORM_OPTIONS.any { it.id == option.id }
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, String value) {
			handleOption(option, value)
			bundle.options[option.id] = option
		}
	}

	static class GenericAnalysisOptionFormFieldProcessor implements FormFieldProcessor {

		boolean supportsOption(DecoratedAnalysisOption option) {
			ANALYSIS_FORM_OPTIONS.any { it.id == option.id }
		}

		void process(DoopInputBundle bundle, InputResolutionContext ctx, DecoratedAnalysisOption option, String value) {
			handleOption(option, value)
		}
	}

	static void handleOption(DecoratedAnalysisOption option, String value) {
		// If the option has an arg, the value of this arg defines the value of the respective analysis option
		if (option.argName) {
			if (option.multipleValues)
				value.split(",").each {
					option.value << (parseValue(option, it.trim()))
				}
			else
				option.value = parseValue(option, value)
		}
		// The option has no arg and thus it is a boolean flag, enabling
		// the respective analysis option (boolean options are checkboxes in the form)
		// NOTE: All boolean doop options that are presented to the UIs should be false by default
		else
			option.value = true

	}

	static def parseValue(DecoratedAnalysisOption decoratedOpt, String value) {
		if (decoratedOpt.option instanceof BooleanAnalysisOption)
			return value.toBoolean()
		else if (decoratedOpt.option instanceof IntegerAnalysisOption)
			return value.toInteger()
		else
			return value
	}
}
