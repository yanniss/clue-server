package org.clyze.server.web
/**
 * Created by saiko on 28/4/2015.
 */
class UserContext {
	final String name
	final File dir

	UserContext(String name, File dir) {
		this.name = name
		this.dir = dir
	}
}
