package org.clyze.server.web

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

class MonitorThreadGroup extends ThreadGroup {

	private static final Log log = LogFactory.getLog(this)

	MonitorThreadGroup() {
		super("Status Monitor")
	}

	void uncaughtException(Thread t, Throwable e) {
		log.error("Uncaught exception in status monitor thread ${t.getName()}:")
		log.error(e.getMessage(), e)
	}

}