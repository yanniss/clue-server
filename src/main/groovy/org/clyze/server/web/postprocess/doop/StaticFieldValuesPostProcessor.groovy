package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.StaticFieldValues

@InheritConstructors
class StaticFieldValuesPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_SField_Values", { line ->
			processedLines++
			def (fieldId, valueId) = line.split(", ")
			save new StaticFieldValues(aId, fieldId, valueId)
		}
	}
}
