package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.InstanceFieldValues

@InheritConstructors
class InstanceFieldValuesPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_IField_Values", { line ->
			processedLines++
			def (baseValueId, fieldId, valueId) = line.split(", ")
			save new InstanceFieldValues(aId, baseValueId, fieldId, valueId)
		}
	}
}
