package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.ArrayValues

@InheritConstructors
class ArrayValuesPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Array_Values", { line ->
			processedLines++
			def (arrayValueId, valueId) = line.split(", ")
			save new ArrayValues(aId, arrayValueId, valueId)
		}
	}
}
