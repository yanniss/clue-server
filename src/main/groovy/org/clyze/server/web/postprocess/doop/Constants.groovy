package org.clyze.server.web.postprocess.doop

import org.clyze.persistent.model.doop.*
import org.clyze.persistent.model.doop.Class as Klass

class Constants {
	static final List<java.lang.Class> JC_PLUGIN_SYMBOLS = [
			Klass,
			Field,
			Method,
			Variable
	]

	static final List<java.lang.Class> JC_PLUGIN_BASIC_ELEMENTS = [
			HeapAllocation,
			MethodInvocation,
			Usage,
			StringConstant
	]
}
