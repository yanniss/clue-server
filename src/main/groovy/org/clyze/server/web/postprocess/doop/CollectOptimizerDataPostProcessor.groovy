package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import groovy.util.logging.Log4j
import org.clyze.server.web.Config
import org.clyze.server.web.bundle.DoopInputBundle
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.server.web.persistent.store.Searcher
import org.clyze.server.web.restlet.resource.BundleConfigurationManager
import org.clyze.utils.FileOps

import static org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective.Type.REMOVE
import static org.clyze.server.web.persistent.model.doop.OptimizationDirective.OriginType.ANALYSIS

@InheritConstructors
@Log4j
class CollectOptimizerDataPostProcessor extends DoopPostProcessor {

	@Override
	void process() {

		DataStoreStrategy storage = Config.instance.storage

		//Only methods_to_remove are currently supported

		File methods_to_remove = FileOps.findFileOrThrow(
			new File(serverSideAnalysis.analysis.outDir, "database/methods_to_remove.csv"),
			"File methods_to_remove not found"
		)

		DoopInputBundle bundle = storage.loadInputBundle(serverSideAnalysis.bundleId)
		String origin = BundleConfigurationManager.origin(bundle, serverSideAnalysis)

		methods_to_remove.eachLine { line ->
			try {
				def (jvmDescriptor, doopId) = line.split("\t")
				save KeepRemoveMethodDirective.forDoopId(bundle, doopId as String, serverSideAnalysis.id, origin, ANALYSIS, REMOVE)
			}
			catch(e) {
				log.warn(e.getMessage(), e)
			}
			finally {
				processedLines++
			}
		}
	}

}
