package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import groovy.util.logging.Log4j
import org.clyze.server.web.persistent.model.doop.EntryPoint
import org.clyze.server.web.persistent.model.doop.InvocationZeroTargets
import org.clyze.server.web.persistent.model.doop.Metrics

import static org.clyze.server.web.persistent.model.doop.Metrics.Kind.*

@InheritConstructors
@Log4j
class MetricsPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Metrics", { String line ->
			def (metric, value) = line.split(", ")
			log.debug "Metric: ${metric} Value: ${value}"
			switch (metric) {
				case "#AppClass":
					serverSideAnalysis.metrics.appClasses = value.toLong()
					break
				case "#Class":
					serverSideAnalysis.metrics.totalClasses = value.toLong()
					break
				case "#AppMethod":
					serverSideAnalysis.metrics.appMethods = value.toLong()
					break
				case "#Method":
					serverSideAnalysis.metrics.totalMethods = value.toLong()
					break
				case "#Field":
					serverSideAnalysis.metrics.totalFields = value.toLong()
					break
				case "#Var":
					serverSideAnalysis.metrics.totalVariables = value.toLong()
					break
				case "#HeapAllocation":
					serverSideAnalysis.metrics.totalHeapAllocations = value.toLong()
					break
				case "#MethodInvocation":
					serverSideAnalysis.metrics.totalMethodInvocations = value.toLong()
					break

				case "#AppVirtual (static)":
					serverSideAnalysis.metrics.appVirtuals = value.toLong()
					break
				case "#AppVirtual (resolved)":
					serverSideAnalysis.metrics.appVirtualsResolved = value.toLong()
					break
				case "#AppMethod (reachable)":
					serverSideAnalysis.metrics.appMethodsReachable = value.toLong()
					break
			}
		}
		def appVirtualsNum = serverSideAnalysis.metrics.appVirtuals
		serverSideAnalysis.metrics.appVirtualsPercent = appVirtualsNum ? serverSideAnalysis.metrics.appVirtualsResolved.div(appVirtualsNum) as double : 0
		log.debug "Metric: AppVirtuals% Value: ${serverSideAnalysis.metrics.appVirtualsPercent}"

		def appMethodsNum = serverSideAnalysis.metrics.appMethods
		serverSideAnalysis.metrics.appMethodsPercent = appMethodsNum ? serverSideAnalysis.metrics.appMethodsReachable.div(appMethodsNum) as double : 0
		log.debug "Metric: AppMethods% Value: ${serverSideAnalysis.metrics.appMethodsPercent}"

		store.updateExisting(serverSideAnalysis)


		serverSideAnalysis.analysis.processRelation "Metrics_AppEntryPoint", { String methodId ->
			processedLines++
			save new EntryPoint(aId, methodId)
		}

		serverSideAnalysis.analysis.processRelation "Metrics_InvocationTargets_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, INVOCATION_TARGETS, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Metrics_Method_IncomingInvo_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, INCOMING_INVO, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Metrics_Method_IncomingMethods_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, INCOMING_METHODS, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Metrics_Method_OutgoingInvo_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, OUTGOING_INVO, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Metrics_Method_OutgoingMethods_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, OUTGOING_METHODS, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Metrics_Value_AsReceiver_Count", { String line ->
			processedLines++
			def (String doopId, String counter) = line.split(", ")
			save new Metrics(aId, VALUE_AS_RECEIVER, doopId, Long.parseLong(counter))
		}
		serverSideAnalysis.analysis.processRelation "Server_IField_Values_Count", { String line ->
			processedLines++
			def (String doopId, fld, String counter) = line.split(", ")
			save new Metrics(aId, IFIELD_VALUES, doopId, Long.parseLong(counter))
		}

		serverSideAnalysis.analysis.processRelation "Metrics_InvocationZeroTargets", { String invocationId ->
			processedLines++
			save new InvocationZeroTargets(aId, invocationId)
		}
	}
}
