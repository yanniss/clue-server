package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.ReachableSymbol

@InheritConstructors
class ReachableSymbolPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "ReachableClass", { String doopId ->
			processedLines++
			save new ReachableSymbol(aId, doopId)
		}

		serverSideAnalysis.analysis.processRelation "Reachable", { String doopId ->
			processedLines++
			save new ReachableSymbol(aId, doopId)
		}
	}
}
