package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.FieldShadowedBy

@InheritConstructors
class FieldShadowedByPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Field_ShadowedBy", { line ->
			processedLines++
			def (fieldId, shadowFieldId) = line.split(", ")
			save new FieldShadowedBy(aId, fieldId, shadowFieldId)
		}
	}
}
