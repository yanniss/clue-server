package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.InvocationValues

@InheritConstructors
class InvocationValuesPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Invocation_Values", { line ->
			processedLines++
			def (fromMethodId, invocationId, toMethodId) = line.split(", ")
			save new InvocationValues(aId, fromMethodId, invocationId, toMethodId)
		}
	}
}
