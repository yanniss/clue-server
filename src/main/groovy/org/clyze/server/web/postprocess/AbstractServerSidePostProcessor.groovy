package org.clyze.server.web.postprocess

import groovy.util.logging.Log4j
import org.clyze.analysis.Analysis
import org.clyze.persistent.model.Element
import org.clyze.server.web.Config
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.store.DataStore

/**
 * A starting point for server-side post processors, ones
 * that operate in the context of a user (a uId is present),
 * supports logging/timing and access to server-side logic.
 */
@Log4j
abstract class AbstractServerSidePostProcessor<A extends Analysis> {

	protected ServerSideAnalysis<A> serverSideAnalysis
	protected BulkProcessor bulkProcessor
	protected DataStore store
	protected String aId
	protected String uId
	long processedLines = 0L

	AbstractServerSidePostProcessor(ServerSideAnalysis<A> serverSideAnalysis, BulkProcessor bulkProcessor) {
		this.serverSideAnalysis = serverSideAnalysis
		this.bulkProcessor = bulkProcessor
		this.store = bulkProcessor.getDataStore()
		this.aId = serverSideAnalysis.analysis.id
		this.uId = serverSideAnalysis.userId
	}

	abstract void process()

	protected void importElementToDataStore(Element el) {
		el.rootElemId = serverSideAnalysis.analysis.id		
		bulkProcessor.scheduleSave el
		export el
	}

	protected void export(Element el) {
		if (Config.instance.isExportEnabled) {
			def dir = new File("${serverSideAnalysis.analysis.outDir.canonicalPath}/debug/")
			if (!dir.exists()) dir.mkdir()
			def file = new File(dir, "${getClass().simpleName}.txt")
			if (!file.exists()) file.createNewFile()
			file << el.toJSON() << "\n"
		}
	}
}
