package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.TypeCanAccessField

@InheritConstructors
class TypeCanAccessFieldPostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Type_Can_Access_Field", { line ->
			processedLines++
			def (classId, fieldId) = line.split(", ")
			save new TypeCanAccessField(aId, classId, fieldId)
		}
	}
}
