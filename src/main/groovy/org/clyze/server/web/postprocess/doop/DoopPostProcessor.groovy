package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.doop.core.DoopAnalysis
import org.clyze.persistent.model.Item
import org.clyze.server.web.postprocess.AbstractServerSidePostProcessor

@InheritConstructors
abstract class DoopPostProcessor extends AbstractServerSidePostProcessor<DoopAnalysis> {
	Set<String> sourceClasses = [] as Set

	protected void save(Item item) {
		bulkProcessor.scheduleSave item
		export item
	}
}
