package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.PackageCallsPackage

@InheritConstructors
class PackageCallsPackagePostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_ClassCallsClass", { line ->
			processedLines++
			def (fromClass, toClass) = line.split(", ")
			def pos = fromClass.lastIndexOf(".")
			def fromPackage = pos != -1 ? fromClass.substring(0, pos) : "default"
			pos = toClass.lastIndexOf(".")
			def toPackage = pos != -1 ? toClass.substring(0, pos) : "default"
			if (fromPackage != toPackage) save new PackageCallsPackage(aId, fromPackage, toPackage)
		}
	}
}
