package org.clyze.server.web.postprocess.doop

// import groovy.transform.CompileStatic
import groovy.transform.InheritConstructors
import groovy.util.logging.Log4j
import org.clyze.server.web.persistent.model.doop.VarReturn
import org.clyze.server.web.persistent.model.doop.VarValues
import org.clyze.utils.DoopConventions;

@InheritConstructors
@Log4j
// @CompileStatic
class VarValuesPostProcessor extends DoopPostProcessor {

	Map cache
	String currentClass

	@Override
	void process() {

		Map<String, String> varToFilteredVar = [:]
		Map<String, String> escapedValues = [:]

		serverSideAnalysis.analysis.processRelation "Server_Var_Values", { String line ->
			processedLines++
			String[] lineParts = line.split(", ")
			def declClass = lineParts[0]
			def varId = lineParts[1]
			def valueId = lineParts[2]
			def lineNumber = 0

			if (declClass in sourceClasses && varId.contains("#_")) {
				def pieces = varId.split("#_")
				def varSSAName = pieces[0]
				try {
					lineNumber = pieces[1].toInteger()
				} catch (NumberFormatException e1) {
					log.debug "**** Fallback for line: $line"
					try {
						lineNumber = pieces[1].split("/")[0].toInteger()
					} catch (NumberFormatException e2) {
						log.debug "**** Ignoring line: $line"
						return
					}
				}

				def ssaDelimiter = DoopConventions.getSeparator()
				def index = varSSAName.indexOf(ssaDelimiter)
				varId = index >= 0 ? varSSAName.substring(0, index) : varSSAName
				varId = omitHashSuffix(varId)
			}
			varToFilteredVar[lineParts[1]] = varId

			// NOTE: Results come sorted by declClass (from LB) so we
			// can exploit the locality to keep a small cache
			if (declClass != currentClass) {
				currentClass = declClass
				cache = [:]
			}
			def anId = serverSideAnalysis.analysis.id			
			// NOTE: Should be a String and not a GString so that equals works correctly
			//def key = "$varId|$lineNumber|$valueId|$anId" as String
			def key = "VV$processedLines-$anId" as String

			// CLUE-238: value ids containing slashes or single/double
			// quotation marks make post-processing fail.
			if (valueId.contains('\\') || valueId.contains('\'') || valueId.contains('"')) {
				String valueId2 = escapedValues.get(valueId)
				if (valueId2 == null) {
					valueId2 = valueId.replace('\\\"', "&quot;").replace('\\\'', "&lsquo;").replace('\\', '&#47;')
					println "Escaping JSON.valueId: ${valueId} -> ${valueId2}"
					escapedValues.put(valueId, valueId2)
				}
				valueId = valueId2
			}

			if (!cache[key]) {
				/* save new VarValues(anId, varId, lineNumber, valueId) */
				/* Manual json generation (GString) */
				String json = """{
					"id": "$key",
					"rootElemId": "$anId",					
					"varId": "$varId",
					"line": $lineNumber,
					"valueId": "$valueId"}""".toString()
				bulkProcessor.scheduleSave VarValues.simpleName, key, json
				cache[key] = true
			}
		}

		serverSideAnalysis.analysis.processRelation "Server_Var_Return", { String line ->
			String[] lineParts = line.split(", ")
			def varId = lineParts[0]
			def methodId = lineParts[1]
			save new VarReturn(aId, varToFilteredVar[varId], methodId)
		}
	}

	// Given a 'var#n' name, return 'var'. Needed by some Soot versions.
	static String omitHashSuffix(String s) {
		   int index = s.indexOf("#")
		   return index > 0 ? s.substring(0, index) : s
	}
}
