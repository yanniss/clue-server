package org.clyze.server.web.postprocess.doop

import groovy.transform.InheritConstructors
import org.clyze.server.web.persistent.model.doop.MethodSubtype

@InheritConstructors
class MethodSubtypePostProcessor extends DoopPostProcessor {
	@Override
	void process() {
		serverSideAnalysis.analysis.processRelation "Server_Method_Subtype", { line ->
			processedLines++
			def (methodId, subMethodId) = line.split(", ")
			save new MethodSubtype(aId, methodId, subMethodId)
		}
	}
}
