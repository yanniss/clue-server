package org.clyze.server.web

import org.apache.commons.fileupload.disk.DiskFileItemFactory
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

import org.clyze.server.web.MonitorThreadFactory
import org.clyze.server.web.auth.AuthManager
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.server.web.postprocess.doop.DoopPostProcessor
import org.clyze.server.web.query.ReadyMadeQuery

import org.clyze.utils.FileOps

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * The web app config.
 *
 * @author: Kostas Saidis (saiko@di.uoa.gr)
 * Date: 1/10/2014
 */
@Singleton
class Config {

	private static final Log logger = LogFactory.getLog(this)

	static int DEFAULT_TIMEOUT_MINUTES = 120
	static int DEFAULT_MAX_CACHE_SIZE = 1000

	static int DEFAULT_THREAD_POOL_SIZE = 2
	static int DEFAULT_POST_PROCESS_THREAD_POOL_SIZE = 4

	static int DEFAULT_BULK_SIZE = 10000
	static int DEFAULT_DELETION_BULK_SIZE = 1000

	/** The doop platforms lib */
	File platformsLib

	/** The upload directory */
	File uploadDir

	/** The export directory */
	File exportDir

	/** The Redex base directory */
	File redexDir

	/** Redex keystore configuration */
	File redexKeystorePath
	String redexKeystoreAlias
	String redexKeystorePass

	/** Redex requires android sdk */
	File redexAndroidSdkDir

	/** Is export enabled? */
	boolean isExportEnabled = false

	/** The datastore bulk size */
	int bulkSize = DEFAULT_BULK_SIZE

	/** The datastore deletion bulk size */
	int deletionBulkSize = DEFAULT_DELETION_BULK_SIZE

	/** The path of the web app */
	String webContextPath

	/** The DiskFileItemFactory used for the uploads */
	DiskFileItemFactory fileItemFactory

	/** The AuthManager */
	AuthManager authManager

	/** The DataStore Strategy **/
	DataStoreStrategy storage

	/** The analysis timeout in minutes */
	int timeoutInMinutes

	/** The main executor service (for analyses) */
	ExecutorService executor

	/** The post-processing executor service */
	ExecutorService postProcessExecutor

	/**The server side analysis post processors */
	List<Class> serverSideAnalysisPostProcessors = []	

	/** The ready-made queries */
	List<ReadyMadeQuery> readyMadeQueries = []

	/** A factory for all the monitor threads */
	MonitorThreadFactory monitorThreads = new MonitorThreadFactory()

	protected void setupUpload(String uploadDir) {
		this.uploadDir = FileOps.findDirOrThrow(uploadDir, "Invalid uploadDir: $uploadDir")
		fileItemFactory = new DiskFileItemFactory(524288, this.uploadDir) //size threshold 0.5MB
	}

	protected void setupAuthManager(String authClass) {
		authManager = Class.forName(authClass).invokeMethod("getInstance", null) as AuthManager
	}

	protected void setupExecutor(int threadPoolSize) {
		executor = Executors.newFixedThreadPool(threadPoolSize)
	}

	protected void setupPostProcessExecutor(int threadPoolSize) {
		postProcessExecutor = Executors.newFixedThreadPool(threadPoolSize)
	}

	protected void setupAnalysisPostProcessors(String commaSeparatedListOfClassNames) {
		List<String> classNames = commaSeparatedListOfClassNames?.tokenize(",")
		classNames?.each { String className ->
			Class cl = Class.forName(className.trim())
			if (DoopPostProcessor.isAssignableFrom(cl)) {
				//log.debug("DoopPostProcessor $cl")
				serverSideAnalysisPostProcessors.push cl
			}
		}
	}

	protected void setupReadyMadeQueries(String commaSeparatedListOfClassNames) {
		//Ready-made queries expect no input, so we can instantiate them here and 
		//reuse the objects throughout the server's lifecycle
		List<String> classNames = commaSeparatedListOfClassNames?.tokenize(",")
		classNames?.each { String className ->			
			Class cl = Class.forName(className.trim())			
			if (ReadyMadeQuery.isAssignableFrom(cl)) {				
				ReadyMadeQuery q = cl.newInstance() as ReadyMadeQuery
				//log.debug("ReadyMadeQuery $q")
				readyMadeQueries.push q
			}
		}
	}

	protected void setupPlatformsLib(String platformsLib) {
		this.platformsLib = FileOps.findDirOrThrow(platformsLib as String, "The path to the DOOP_PLATFORMS_LIB is invalid: $platformsLib")
	}

	protected void setupExportDir(String exportDir) {		
		this.exportDir = FileOps.ensureDirExistsOrThrow(exportDir as String, "The export directory is invalid: $exportDir")
	}

	protected void setupRedexDir(String redexDir) {
        if (!redexDir) {
            logger.warn "WARNING: The Redex base directory is empty"
            return
        }
		this.redexDir = new File(redexDir)
        if (!this.redexDir.exists())
            logger.warn "WARNING: The Redex base directory is invalid: $redexDir"
	}

	protected void setupRedexAndroidSdkDir(String androidSdkDir) {
        if (!androidSdkDir) {
            logger.warn "WARNING: The Android SDK directory is empty"
            return
        }
		this.redexAndroidSdkDir = new File(androidSdkDir)
        if (!this.redexAndroidSdkDir.exists())
            logger.warn "WARNING: The Android SDK directory is invalid: $androidSdkDir"
	}

	protected void setupRedexKeystore(String redexKeystorePath, String redexKeystoreAlias, String redexKeystorePass) {
        if (!redexKeystorePath) {
            logger.warn "WARNING: The Redex keystore path is empty"
            return
        } else if (!redexKeystoreAlias) {
            logger.warn "WARNING: The Redex keystore alias is empty"
            return
        } else if (!redexKeystorePass) {
            logger.warn "WARNING: The Redex keystore pass is empty"
            return
        }
		this.redexKeystorePath = new File(redexKeystorePath)
        if (!this.redexKeystorePath.exists())
            logger.warn "WARNING: The Redex keystore path is invalid: $redexKeystorePath"
		this.redexKeystoreAlias = redexKeystoreAlias
		this.redexKeystorePass = redexKeystorePass
	}
}
