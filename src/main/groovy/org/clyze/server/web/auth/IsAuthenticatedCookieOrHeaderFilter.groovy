package org.clyze.server.web.auth

import org.restlet.Request

/**
 *
 */
class IsAuthenticatedCookieOrHeaderFilter extends IsAuthenticatedBaseFilter {

	@Override
	String authenticateUser(Request request) {

		try {
			return new IsAuthenticatedCookieFilter().authenticateUser(request)
		}
		catch(Exception ex1) {
			try {
				return new IsAuthenticatedHeaderFilter().authenticateUser(request)
			}
			catch(Exception ex2) {
				throw new RuntimeException("No cookie ${ex1.getMessage()} or header ${ex2.getMessage()} tokens")
			}
		}
	}

}
