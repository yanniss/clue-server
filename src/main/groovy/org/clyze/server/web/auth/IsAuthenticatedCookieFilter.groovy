package org.clyze.server.web.auth

import org.clyze.server.web.Config
import org.restlet.Request

/**
 * Created by saiko on 28/4/2015.
 */
class IsAuthenticatedCookieFilter extends IsAuthenticatedBaseFilter {

	static final String COOKIE_SESSION = "SESSION_ID"

	@Override
	String authenticateUser(Request request) {
		String token = request.getCookies().getFirstValue(COOKIE_SESSION)
		log.debug "cookie session token: $token"
		if (token) {
			return Config.instance.authManager.getUserOfToken(token)
		} else {
			throw new RuntimeException("No session token in cookie")
		}
	}
}
