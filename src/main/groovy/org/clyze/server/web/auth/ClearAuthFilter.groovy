package org.clyze.server.web.auth

import org.restlet.Request
import org.restlet.Response
import org.restlet.routing.Filter

/**
 * Created by saiko on 28/4/2015.
 *
 * Clean up the user context from the thread local.
 */
class ClearAuthFilter extends Filter {

	@Override
	protected void afterHandle(Request request, Response response) {
		AuthUtils.clearUserContext()
	}
}
