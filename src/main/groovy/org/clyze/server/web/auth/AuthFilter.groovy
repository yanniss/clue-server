package org.clyze.server.web.auth

import org.restlet.Request
import org.restlet.routing.Filter

/**
 * Created by saiko on 28/4/2015.
 */
abstract class AuthFilter extends Filter {
	abstract String authenticateUser(Request request) throws RuntimeException;
}
