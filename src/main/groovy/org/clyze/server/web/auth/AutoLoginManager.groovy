package org.clyze.server.web.auth

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Created by saiko on 6/23/16.
 */
@Singleton
class AutoLoginManager {

	private final Log log = LogFactory.getLog(getClass())

	/** Scheduled executor service */
	private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor()

	/** Map of auto login tokens (token to username) */
	private final ConcurrentHashMap<String, String> tokenMap = new ConcurrentHashMap<>()

	String createToken() {
		String userName = AuthUtils.userContext.name
		String token = UUID.randomUUID().toString()

		tokenMap.put(token, userName)

		def autoRemoveToken = {
			String user = tokenMap.remove(token)
			if (user) {
				log.debug "Removed $user auto-login token: $token"
			} else {
				log.warn "No user for auto-login token: $token"
			}
		} as Runnable

		scheduledExecutor.schedule(autoRemoveToken, 5, TimeUnit.MINUTES)

		return token
	}

	String getUserOfToken(String token) {
		return tokenMap.remove(token)
	}
}
