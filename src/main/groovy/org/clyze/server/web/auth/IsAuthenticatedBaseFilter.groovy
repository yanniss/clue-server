package org.clyze.server.web.auth

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.restlet.Request
import org.restlet.Response
import org.restlet.data.Status

/**
 * Created by saiko on 28/4/2015.
 */
abstract class IsAuthenticatedBaseFilter extends AuthFilter {

	protected Log log = LogFactory.getLog(getClass())

	@Override
	protected int beforeHandle(Request request, Response response) {
		log.debug("Before handling: ${request.getResourceRef()}")
		try {
			String user = authenticateUser(request)
			AuthUtils.initUserContext(user)
			log.info("User $user authenticated")
			return CONTINUE
		} catch (e) {
			log.error(e.getMessage(), e)
			response.setStatus(Status.CLIENT_ERROR_UNAUTHORIZED)
			return STOP
		}
	}
}
