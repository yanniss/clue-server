package org.clyze.server.web.auth

import org.restlet.Request


/**
 * Created by saiko on 6/23/16.
 */
class IsAuthenticatedCookieOrAutoLoginFilter extends IsAuthenticatedBaseFilter {

	@Override
	String authenticateUser(Request request) {

		try {
			return new IsAuthenticatedCookieFilter().authenticateUser(request)
		}
		catch (Exception e) {
			try {
				return new IsAuthenticatedAutoLoginTokenParamFilter().authenticateUser(request)
			}
			catch (Exception ex) {
				throw new RuntimeException("No cookie (${e.getMessage()}) or auto-login (${ex.getMessage()}) tokens")
			}
		}
	}
}
