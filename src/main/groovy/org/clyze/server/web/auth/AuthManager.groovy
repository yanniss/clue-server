package org.clyze.server.web.auth

/**
 * Created by saiko@di.uoa.gr on 27/4/2015.
 */
interface AuthManager {

	void createUser(String username, String password) throws RuntimeException

	void removeUser(String username) throws RuntimeException

	void authenticateUser(String username, String password) throws RuntimeException

	boolean userExists(String username)

	boolean isAdmin(String username)

	String createSessionToken(String username) throws RuntimeException

	String getUserOfToken(String token) throws RuntimeException

	void removeSessionToken(String token) throws RuntimeException

	void updateUserPassword(String username, String password) throws RuntimeException	
}
