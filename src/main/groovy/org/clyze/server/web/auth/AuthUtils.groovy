package org.clyze.server.web.auth

import org.clyze.doop.core.Doop
import org.clyze.server.web.UserContext
import org.clyze.utils.FileOps

/**
 * Created by saiko on 27/4/2015.
 */
class AuthUtils {

	private static final ThreadLocal<UserContext> USER_CONTEXT = new ThreadLocal<UserContext>()

	static void initUserContext(String username) {
		String dir = "${Doop.doopOut}/$username"
		File f = new File(dir)
		f.mkdirs()
		FileOps.findDirOrThrow(f, "Could not create user dir: $f")
		USER_CONTEXT.set(new UserContext(username, f))
	}

	static void clearUserContext() {
		USER_CONTEXT.remove()
	}

	static UserContext getUserContext() {
		return USER_CONTEXT.get()
	}

	static boolean isAuthenticated() {
		return getUserContext() != null
	}
}
