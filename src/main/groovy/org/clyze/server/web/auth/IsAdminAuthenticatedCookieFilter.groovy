package org.clyze.server.web.auth

import org.clyze.server.web.Config
import org.restlet.Request
import org.restlet.data.Status
import org.restlet.resource.ResourceException

class IsAdminAuthenticatedCookieFilter extends IsAuthenticatedCookieFilter {

	@Override
	String authenticateUser(Request request) {
		String userId = super.authenticateUser(request)

		if (Config.instance.authManager.isAdmin(userId)) {
			return userId
		} else {
			throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED)
		}
	}
}
