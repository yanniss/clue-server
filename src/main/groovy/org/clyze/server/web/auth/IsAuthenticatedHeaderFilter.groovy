package org.clyze.server.web.auth

import org.clyze.server.web.Config
import org.restlet.Request
import org.restlet.data.Header
import org.restlet.util.Series

/**
 * Created by saiko on 28/4/2015.
 */
class IsAuthenticatedHeaderFilter extends IsAuthenticatedBaseFilter {

	static final String HEADER_TOKEN = "x-clue-token"

	@Override
	String authenticateUser(Request request) {
		Series<Header> headers = request.getHeaders()
		String token = headers.getFirstValue(HEADER_TOKEN)
		log.debug "header session token: $token"
		if (token) {
			return Config.instance.authManager.getUserOfToken(token)
		} else {
			throw new RuntimeException("No session token in header")
		}
	}
}
