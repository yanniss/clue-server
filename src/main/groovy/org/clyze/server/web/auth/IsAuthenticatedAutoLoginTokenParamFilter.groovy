package org.clyze.server.web.auth

import org.restlet.Request

/**
 * Created by saiko on 6/23/16.
 */
class IsAuthenticatedAutoLoginTokenParamFilter extends IsAuthenticatedBaseFilter {

	static final String AUTO_LOGIN_TOKEN_PARAM = "t"

	@Override
	String authenticateUser(Request request) {
		String token = request.getResourceRef().getQueryAsForm().getFirstValue(AUTO_LOGIN_TOKEN_PARAM)
		log.debug "auto-login token: $token"
		if (token) {
			return AutoLoginManager.instance.getUserOfToken(token)
		} else {
			throw new RuntimeException("No auto-login token in request params")
		}
	}

}
