package org.clyze.server.web

import org.clyze.utils.Executor
import org.clyze.utils.FileOps

import java.util.concurrent.Callable

import groovy.util.logging.Log4j

@Log4j
class RedexRepackager implements Callable<File> {

    private static final String PREFIX = "APK optimization failed:"

    String apkIn
    String apkOut
    String removeDirectives

    File call() {
        String config = "${Config.instance.redexDir}/config/remover.config"
        runRedexOn(apkIn, apkOut, config, removeDirectives)
        return FileOps.findFileOrThrow(apkOut, "$PREFIX output file not found.")        
    }    

    /**
     * Run Redex on an .apk.
     *
     * @param apkIn           the input .apk
     * @param apkOut          the output .apk
     * @param config          an optional configuration file for Redex
     * @param methodsToRemove the file containing the methods to remove     
     */
    private void runRedexOn(String apkIn, String apkOut, String config, String methodsToRemove) {

        FileOps.findFileOrThrow(apkIn,  "$PREFIX invalid input apk: $apkIn")
        FileOps.findFileOrThrow(config, "$PREFIX invalid config: $config")
        FileOps.findFileOrThrow(config, "$PREFIX invalid methodsToRemove: $config")
        
        if (!apkOut?.trim()) {
            throw new RuntimeException("$PREFIX no path to output .apk.")            
        }
        
        def command = [ "${Config.instance.redexDir}/bin/credex", apkIn ]
        command.addAll(['--config', config])        
        command.addAll(['-o', apkOut])
        command << "-SRemoverPass.rmethods=${methodsToRemove}"
        // if (methodsToMakeAbstract) {
        //     command.addAll(['-S', "RemoverPass.amethods=${methodsToMakeAbstract}"])
        // }        

        // Default Android SDK 'debug' keystore and keyalias.
        command << "--keystore"
        command << Config.instance.redexKeystorePath
        command << "--keyalias"
        command << Config.instance.redexKeystoreAlias
        command << "--keypass"
        command << Config.instance.redexKeystorePass
        command << "--sign"

        // Run Redex with ANDROID_SDK placed in the environment.
        Map<String, String> env = [:]        
        env.putAll(System.getenv())
        env.put("ANDROID_SDK", Config.instance.redexAndroidSdkDir.canonicalPath)
        log.debug "Redex env: $env"
        Executor executor = new Executor(environment: env)
        List<String> cmd = command.collect { it as String }
        log.debug "Redex invocation: ${cmd.join(' ')}"

        File temp = File.createTempFile("redex-output", ".tmp")
        executor.executeWithRedirectedOutput(cmd, temp)
        temp.delete()

        log.debug "Redex finished!"        
    }    
}
