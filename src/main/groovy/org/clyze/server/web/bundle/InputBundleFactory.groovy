package org.clyze.server.web.bundle

import org.clyze.server.web.persistent.Identifiers

import org.restlet.Request

/**
 * The input bundle factory. 
 */
interface InputBundleFactory<B extends InputBundle> {
	
	B newInputBundle(File baseDir, Identifiers.BundleInfo info, Request request)
}