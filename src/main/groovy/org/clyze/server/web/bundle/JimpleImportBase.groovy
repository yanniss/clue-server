package org.clyze.server.web.bundle

import groovy.transform.CompileStatic
import groovy.util.logging.Log4j
import org.apache.commons.io.FilenameUtils
import org.clyze.jimple.JimpleListenerImpl
import org.clyze.persistent.model.Element

import java.util.concurrent.atomic.AtomicLong

import static groovy.io.FileType.FILES

@Log4j @CompileStatic
abstract class JimpleImportBase {

    File jimpleDir
    Set<String> sourceClasses
    Set<String> typesToImport

    JimpleImportBase(File jimpleDir, Set<String> sourceClasses, Set<String> typesToImport) {
        this.jimpleDir = jimpleDir
        this.sourceClasses = sourceClasses
        this.typesToImport = typesToImport
    }

    void importJimple() {

        def ignoreFiles = 0L
        def totalElements = 0L

        Map<String, ElementSummary> summaryMap = [:].withDefault { new ElementSummary() }

        log.debug "Generating jimple metadata for ${typesToImport} elements excluding ${sourceClasses.size()} classes from source"
        def now = System.currentTimeMillis()

        AtomicLong filesError = new AtomicLong(0l)
        AtomicLong filesOK = new AtomicLong(0l)

        long filesParsed = 0

        String baseDir = jimpleDir.canonicalPath
        if (!baseDir.endsWith("/")) baseDir += "/"

        jimpleDir.eachFileRecurse(FILES) { File f ->
            def className = javaClassNameOf(f)

            if (className in sourceClasses) {
                //log.debug "Excluding $className ($f)"
                ignoreFiles++
            }
            else {

                JimpleListenerImpl.Walker walker = null

                try {
                    String jimpleFileName = f.canonicalPath - baseDir
                    walker = JimpleListenerImpl.parseJimpleText(jimpleFileName, f.text)
                    filesParsed++
                    if (filesParsed % 1000 == 0) {
                        log.debug("${Thread.currentThread().getName()} - Parsed $filesParsed jimple files")
                    }
                }
                catch(any) {
                    log.error("Error parsing $className: ${any.message}", any)
                    filesError.getAndIncrement()
                }

                if (walker) {
                    try {
                        totalElements += walk(walker)
                        long filesNow = filesOK.getAndIncrement()
                        if (filesNow % 1000 == 0) {
                            log.debug("${Thread.currentThread().getName()} - Processed ${totalElements} ${typesToImport} elements from ${filesNow} jimple files")
                        }
                    }
                    catch(err) {
                        log.error("Error processing elements of $className: ${err.message}", err)
                        filesError.getAndIncrement()
                    }
                }

                /*
                if (walker) {
                    CompletableFuture<Long> future = CompletableFuture.runAsync({ ->
                        walk(walker)
                    }, executor).handle({ Long elements, Throwable err ->
                        if (err) {
                            log.error("Error processing elements of $className: ${err.message}", err)
                            filesError.getAndIncrement()
                            return 0l
                        } else {
                            long filesNow = filesOK.getAndIncrement()
                            if (filesNow % 1000 == 0) {
                                report("Processed ${filesNow} jimple files")
                            }
                            return elements
                        }
                    })
                    futures.add future
                }
                */
            }
        }

        /*
        def futureWait = System.currentTimeMillis()
        totalElements = futures.collect { CompletableFuture<Long> f -> return f.get() }.sum()
        report("Calculated sum of elements (${totalElements}) from ${futures.size()} futures in ${System.currentTimeMillis() - futureWait} ms")
        */

        log.debug "Processed ${filesOK.get()} jimple files with ${totalElements} ${typesToImport} elements in ${System.currentTimeMillis() - now} ms (ignored $ignoreFiles files, failures in ${filesError.get()} files)"
    }

    protected long walk(JimpleListenerImpl.Walker walker) {
        //def time = System.currentTimeMillis()
        def fileElements = 0
        walker.walk { Element e ->
            if (shouldImportElement(e)) {
                importElement(e)
                fileElements++
            }
        }
        //report("Processed ${fileElements} in ${System.currentTimeMillis() - time} ms")
        return fileElements
    }

    protected boolean shouldImportElement(Element e) {
        typesToImport.contains(e.getClass().getSimpleName())
    }


    protected String javaClassNameOf(File jimpleFile) {
        String parent = jimpleDir.canonicalPath
        String jimple = FilenameUtils.removeExtension(jimpleFile.canonicalPath)

        String relative = jimple - parent
        relative.replace(File.separatorChar, '.'.charAt(0)).substring(1)
    }

    abstract void importElement(Element e)

    static class ElementSummary {
        long count
        long imported
    }
}
