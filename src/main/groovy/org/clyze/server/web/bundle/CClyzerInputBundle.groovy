package org.clyze.server.web.bundle

import org.clyze.analysis.AnalysisFamilies
import org.clyze.analysis.AnalysisFamily
import org.clyze.analysis.AnalysisOption
import org.clyze.cclyzer.CClyzerAnalysis
import org.clyze.doop.input.InputResolutionContext
import org.clyze.server.web.analysis.CClyzerServerSideAnalysisFactory
import org.clyze.server.web.analysis.ServerSideAnalysis

class CClyzerInputBundle extends InputBundle<CClyzerAnalysis> {

	@Override
	CClyzerInputBundle fromJSON(String json) {

	}

	@Override
	Map<String, Object> toMap() {
		
	}

	@Override
	InputResolutionContext getInputResolutionContext() {
		return null
	}

	@Override
	CClyzerAnalysis initializeAnalysis(Object jsonMap) {
		assert AnalysisFamilies.isRegistered('cclyzer')
		AnalysisFamily family = AnalysisFamilies.get('cclyzer')

		String id = jsonMap.anId
		String name = family.name //the "name" of cclyzer analysis is always cclyzer

		//Read the inputFiles from the jsonMap, verifying they are still accessible
		List<File> inputFiles = jsonMap.anInputs.collect { String file ->
			FileOps.findFileOrThrow(file as String, "File is invalid: $file").canonicalPath
		}

		//Read the inputFiles from the jsonMap, verifying they are still accessible
		List<File> libraryFiles = jsonMap.anLibraries.collect { String file ->
			FileOps.findFileOrThrow(file as String, "File is invalid: $file").canonicalPath
		}

		//Initialize the default options
		Map<String, AnalysisOption> options = [:]
		family.supportedOptions().each { AnalysisOption option ->
			options.put(option.id, AnalysisOption.newInstance(option))
		}

		//override them with the options held in the jsonMap
		jsonMap.anOptions.each { String key, Object value ->
			options.get(key)?.value = value
		}

		return new CClyzerServerSideAnalysisFactory().newAnalysis(family, id, name, options, inputFiles, libraryFiles)
	}

	@Override
	void postProcess(ServerSideAnalysis<CClyzerAnalysis> serverSideAnalysis) {
		return null
	}
}