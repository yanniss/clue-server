package org.clyze.server.web.bundle

import org.clyze.server.web.persistent.Identifiers

import org.restlet.Request

/**
 * The input bundle factory. 
 */
class CClyzerInputBundleFactory implements InputBundleFactory<CClyzerInputBundle> {
	
	CClyzerInputBundle newInputBundle(File baseDir, Identifiers.BundleInfo info, Request request) {
		return null
	}
}