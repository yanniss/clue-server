package org.clyze.server.web.bundle


import io.reactivex.parallel.ParallelFlowable
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

class DummyProguardRuleExpander implements ReactiveBundleConfigurationRuleExpander {

    /*
    @Override
    ParallelFlowable<KeepRemoveMethodDirective> expand(BundleConfigurationContext ctx, BundleConfiguration config) {
        return null
    }
     */

    @Override
    ParallelFlowable<KeepRemoveMethodDirective> expandRule(BundleConfigurationContext ctx, BundleConfiguration config, PersistentBundleConfigurationRule rule) {
        return null
    }
}
