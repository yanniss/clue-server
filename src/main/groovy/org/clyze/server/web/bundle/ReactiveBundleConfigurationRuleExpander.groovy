package org.clyze.server.web.bundle

import io.reactivex.parallel.ParallelFlowable
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

interface ReactiveBundleConfigurationRuleExpander {

    ParallelFlowable<KeepRemoveMethodDirective> expandRule(BundleConfigurationContext ctx, BundleConfiguration config, PersistentBundleConfigurationRule rule)

}
