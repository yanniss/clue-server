package org.clyze.server.web.bundle

import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import org.clyze.analysis.*
import org.clyze.doop.core.Doop
import org.clyze.doop.core.DoopAnalysis
import org.clyze.doop.core.DoopAnalysisFamily
import org.clyze.doop.input.InputResolutionContext
import org.clyze.doop.input.ResolvedInputResolutionContext
import org.clyze.persistent.model.doop.Artifact
import org.clyze.persistent.model.doop.ArtifactKind
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.analysis.DecoratedAnalysisOption
import org.clyze.server.web.analysis.DoopServerSideAnalysisFactory
import org.clyze.server.web.analysis.ServerSideAnalysis
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.store.DataStore
import org.clyze.server.web.persistent.store.Dates
import org.clyze.server.web.postprocess.doop.DoopPostProcessor
import org.clyze.utils.FileOps
import org.clyze.utils.DoopConventions;
import org.clyze.server.web.persistent.model.doop.OptimizationDirective
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective

import static org.clyze.utils.Helper.timingWithLogging

@Log4j
class DoopInputBundle extends InputBundle<DoopAnalysis> {

	static final String DEFAULT_CONFIG_SET = "optimize.clue"
	static final String SEEDS_FILE         = "seeds.txt"
	static final String PROGUARD_FILE      = "rules.pro"
	static final String KEEP_SPEC_FILE     = "keep-spec.txt"

	static final Map<String, DecoratedAnalysisOption> OPTIONS_TEMPLATE

	static {
		def options = DoopAnalysisFamily.instance.supportedOptionsAsMap()

		OPTIONS_TEMPLATE = (FormLogicHelper.BUNDLE_FORM_OPTIONS + [
				new DecoratedAnalysisOption(options.PLATFORMS),
				new DecoratedAnalysisOption(options.PLATFORMS_LIB),
		]).collectEntries { [(it.id): it] }
	}

	InputResolutionContext inputResolutionContext

	Date created

	Map<String, DecoratedAnalysisOption> options = OPTIONS_TEMPLATE.collectEntries { [(it.key): it.value.clone()] }

	Map<InputType, List<Artifact>> artifacts = [:].withDefault { [] }

	boolean hasSrcMetadata = false

	Set<String> sourceClasses = [] as Set

	Long analysesCnt = 0
	Long optimizationsCnt = 0

	// Containing all inputs
	File inputsDir
	// Containing all inputs extracted in separate sub-directories
	File inputsExtractedDir
	// Containing a different dir per artifact, e.g. foo-sources.jar/source files
	File sourcesDir
	// Containing all sources extracted and merged under a single directory
	File sourcesMergedDir
	// Containing the json files
	File jcPluginMetadataDir
	// Containing the doop facts
	File factsDir
	// Containing the jimple/shimple files
	File jimpleDir
	// Containing the platform files
	File platformDir
	// Containing all outputs, such as optimized APKs
	File outputsDir
	// Containing the HeapDL files
	File heapdlDir
	// Containing any other files, e.g. tamiflex
	File etcDir
	// The parent directory of all analyses
	File analysesDir

	List<Artifact> getAppArtifacts() { artifacts[InputType.INPUT] }

	List<Artifact> getDepArtifacts() { artifacts[InputType.LIBRARY] }

	List<Artifact> getPlatformArtifacts() { artifacts[InputType.PLATFORM] }

	List<Artifact> getHeapDLArtifacts() { artifacts[InputType.HEAPDL] }	

	DoopInputBundle() {}

	DoopInputBundle(Identifiers.BundleInfo info, Date created) {
		this.id          = info.id
		this.userId      = info.userId
		this.projectId   = info.project.id
		this.created     = created
	}

	void setupDirectories(File baseDir) {
		this.baseDir = FileOps.ensureDirExistsOrThrow(baseDir, "Base dir $baseDir invalid")
		inputsDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "inputs"), "Inputs dir invalid")
		inputsExtractedDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "inputsExtracted"), "inputsExtracted dir invalid")
		sourcesDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "sources"), "Sources dir invalid")
		sourcesMergedDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "sourcesMerged"), "SourcesMerged dir invalid")
		jcPluginMetadataDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "jcPluginMetadata"), "jcPluginMetadata dir invalid")
		factsDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "facts"), "Facts dir invalid")
		jimpleDir = FileOps.ensureDirExistsOrThrow(DoopConventions.jimpleDir(factsDir.canonicalPath), "Jimple dir invalid")
		platformDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "platform"), "Platform dir invalid")
		heapdlDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "heapdl"), "HeapDL dir invalid")
		etcDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "etc"), "Etc dir invalid")
		analysesDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "analyses"), "Analyses dir invalid")
		outputsDir = FileOps.ensureDirExistsOrThrow(new File(baseDir, "outputs"), "Outputs dir invalid")
	}

	private static final Closure<Artifact> artifactMapDecoder = { jsonMap ->
		new Artifact(
				jsonMap.id as String,
				jsonMap.name as String,
				Enum.valueOf(ArtifactKind, jsonMap.kind),
				jsonMap.isDependency as boolean,
				jsonMap.sourcesName as String,
				jsonMap.checksum as String,
				jsonMap.sizeInBytes as long,
				jsonMap.packages as Set<String>,
				jsonMap.parentArtifactId as String
		)
	}	

	@Override
	DoopInputBundle fromJSON(String json) {
		def map = new JsonSlurper().parseText(json)
		this.id = map.id
		this.userId = map.userId
		this.projectId = map.projectId
		this.displayName = map.displayName
		this.created = Dates.parse(map.created)
		this.hasSrcMetadata = map.hasSrcMetadata
		this.sourceClasses = map.sourceClasses as Set
		this.analysesCnt = map.analysesCnt as Long
		this.optimizationsCnt = map.optimizationCnt as Long

		this.options = [:]
		map.options.each {
			def optionTemplate = OPTIONS_TEMPLATE[it.key as String]
			if (optionTemplate) {
				def newOption = optionTemplate.clone()
				newOption.value = it.value
				this.options[(newOption.id)] = newOption
			}
		}

		setupDirectories(new File(map.baseDir as String))

		artifacts[InputType.INPUT] = map.appArtifacts.collect(artifactMapDecoder)
		artifacts[InputType.LIBRARY] = map.depArtifacts.collect(artifactMapDecoder)
		artifacts[InputType.PLATFORM] = map.platformArtifacts.collect(artifactMapDecoder)		

		return this
	}

	@Override
	Map<String, Object> toMap() {
		return [
				id               : id,
				userId           : userId,
				displayName      : displayName,
				projectId        : projectId,
				baseDir          : baseDir.canonicalPath,
				created          : Dates.format(created),
				hasSrcMetadata   : hasSrcMetadata,
				sourceClasses    : sourceClasses,
				analysesCnt      : analysesCnt,
				optimizationsCnt : optimizationsCnt,
				options          : options.collectEntries { [(it.key): it.value.value] },
				appArtifacts     : appArtifacts.collect { it.toMap() },
				depArtifacts     : depArtifacts.collect { it.toMap() },
				platformArtifacts: platformArtifacts.collect { it.toMap() }
		]
	}

	void exportAsClueFile(ServerSideAnalysis analysis, Writer writer) {
		exportDirectivesAsClueFile(Config.instance.storage.givenAnalysis(analysis).&getMethodOptimizationDirectivesOfAnalysis, writer)
	}

	void exportDirectivesAsClueFile(Closure directivesProvider, Writer writer) {
		directivesProvider.call() { id, type, String json ->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			directive.export(writer)
		}
	}

	void mergeDirectives(ServerSideAnalysis analysis, BundleConfiguration target) {

		DataStore store = Config.instance.storage.bundleDoopStore(this)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)

		def alreadyProcessedDoopIds = [] as Set		

		Config.instance.storage.givenBundle(this).getMethodOptimizationDirectivesOfConfigSet(target.id) { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			alreadyProcessedDoopIds.add directive.doopId						
		}

		Config.instance.storage.givenAnalysis(analysis).getMethodOptimizationDirectivesOfAnalysis(alreadyProcessedDoopIds) { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(this.getId(), target.id, directive, OptimizationDirective.OriginType.USER)
			bulk.scheduleSave copy
		}

		bulk.finishUp()
	}

	void mergeDirectives(BundleConfiguration source, BundleConfiguration target) {

		mergeSourceDirectivesTo(
			Config.instance.storage.givenBundle(this).&getMethodOptimizationDirectivesOfConfigSet.curry(source.id),
			target
		)

	}

	void mergeSourceDirectivesTo(Closure sourceDirectivesProvider, BundleConfiguration target) {
		DataStore store = Config.instance.storage.bundleDoopStore(this)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)

		def alreadyProcessedDirectives = [:] //doopId -> directive

		def searcher = Config.instance.storage.givenBundle(this)

		searcher.getMethodOptimizationDirectivesOfConfigSet(target.id) { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective
			alreadyProcessedDirectives.put(directive.doopId, directive)
		}

		sourceDirectivesProvider.call() { id, type, String json->
			KeepRemoveMethodDirective directive = new KeepRemoveMethodDirective().fromJSON(json) as KeepRemoveMethodDirective

			if (alreadyProcessedDirectives.containsKey(directive.doopId)) {
				log.debug("Ignoring directive ${directive.typeOfDirective} ${directive.doopId}")
			}

			KeepRemoveMethodDirective copy = new KeepRemoveMethodDirective(this.getId(), target.id, directive, OptimizationDirective.OriginType.USER)
			bulk.scheduleSave copy
		}

		bulk.finishUp()
	}


	protected String dirForNewAPK(ServerSideAnalysis an) {
		return "redex-out-${System.currentTimeMillis()}"
	}

	String dirForNewAPK(BundleConfiguration configSet) {
		return "redex-out-${System.currentTimeMillis()}"
	}

	File getAPK() {
		Artifact artifact = appArtifacts.find {
			it.kind == ArtifactKind.APK
		}

		if (artifact) {
			return FileOps.findFileOrThrow(new File(inputsDir, artifact.name), "Path to apk ${artifact.name} is invalid")
		} else {
			throw new RuntimeException("No APK in bundle $id")
		}
	}

	@Override
	InputResolutionContext getInputResolutionContext() {
		if (!inputResolutionContext) {
			def rootAppArtifacts = appArtifacts.findAll { !it.parentArtifactId }
			inputResolutionContext = new ResolvedInputResolutionContext(
					rootAppArtifacts.collect { it.name } as List<String>, // inputs
					depArtifacts.collect { it.name } as List<String>, // libraries
					platformArtifacts.collect { it.name } as List<String>, // platformFiles
					[] as List<String>, // heapDLs
					rootAppArtifacts.collect(artifactFileResolver) as List<File>, // allInputs
					depArtifacts.collect(artifactFileResolver) as List<File>, // allLibraries
					platformArtifacts.collect { new File(platformDir, it.name) } as List<File>, // allPlatformFiles
					(options.HEAPDLS.value as List<File>).collect {
						FileOps.findFileOrThrow(it.canonicalPath as String, "HeapDL not found: $it")
					} as List<File>) // allHeapDLs
			log.debug "Input resolution context created for bundle: ${inputResolutionContext}"
		}
		return inputResolutionContext
	}

	private final Closure<File> artifactFileResolver = { Artifact artifact ->
		FileOps.findFileOrThrow(new File(inputsDir, artifact.name), "${artifact.name} is invalid")
	}

	@Override
	DoopAnalysis initializeAnalysis(Object jsonMap) {
		def options = Doop.createDefaultAnalysisOptions()
		jsonMap.anOptions.each { String key, Object value -> 			
			def o = options.get(key)
			if (o) {
				if (o.argName) {					
					o.value = value 
				}				
				else {
					o.value = Boolean.parseBoolean(value)
				}
				
			}
		}
		log.debug("Loading analysis with options: " + options.collect { k, v -> "$k = $v.value (${v.value?.class})" }.join("\n"))
		new DoopServerSideAnalysisFactory(this).newAnalysis(options, getInputResolutionContext())
	}

	/*
	 * Post processing entry-point.
	 */

	void postProcess(ServerSideAnalysis<DoopAnalysis> serverSideAnalysis) {
		timingWithLogging("******** Post-Processing") {
			DataStore store = Config.instance.storage.analysisDoopStore(serverSideAnalysis)
			// Should disable and re-enable but the latter is hard to do in a multi-analysis setting
			// where each analysis messes up the others. Should pick a reasonable global default (e.g., 90s) instead.
			// store.updateSetting("refresh_interval", -1)

			def commonBulkProcessor = new BulkProcessor(Config.instance.bulkSize, store)
		
			Config.instance.serverSideAnalysisPostProcessors.each { Class cl ->
				log.debug "Instantiating $cl"
				def processor = cl.newInstance([serverSideAnalysis, commonBulkProcessor] as Object[]) as DoopPostProcessor<DoopAnalysis>
				processor.sourceClasses = this.sourceClasses
				log.debug "Invoking process on $processor"
				timingWithLogging("**** (Partial Time) ${processor.getClass().name}") {
					processor.process()
					log.debug "Processed ${processor.processedLines} lines"
				}				
			}

			timingWithLogging("**** BulkProcessor finish up") {
				commonBulkProcessor.finishUp()
			}
			//store.updateSetting("refresh_interval", '1s')
		}

		log.info "Finished doop analysis ${serverSideAnalysis.analysis.id}"
	}

	Artifact findArtifactByName(String artifactName) {
		def closure = { it.name == artifactName }
		return appArtifacts.find(closure) ?:
			   depArtifacts.find(closure) ?:
			   platformArtifacts.find(closure)
	}    
}
