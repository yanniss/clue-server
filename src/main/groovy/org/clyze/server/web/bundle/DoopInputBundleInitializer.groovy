package org.clyze.server.web.bundle


import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Log4j
import org.apache.commons.io.FilenameUtils
import org.clyze.analysis.InputType
import org.clyze.doop.core.DoopAnalysis
import org.clyze.persistent.model.Element
import org.clyze.persistent.model.Position
import org.clyze.persistent.model.doop.AnnotateableSymbolWithDoopId
import org.clyze.persistent.model.doop.Artifact
import org.clyze.persistent.model.doop.ArtifactKind
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Field
import org.clyze.persistent.model.doop.HeapAllocation
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.analysis.DoopServerSideAnalysisFactory
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.model.doop.*
import org.clyze.server.web.postprocess.doop.Constants
import org.clyze.utils.CheckSum
import org.clyze.utils.FileOps

import java.nio.file.Files
import java.nio.file.Path
import java.util.concurrent.ConcurrentHashMap

import static groovy.io.FileType.FILES

@Log4j @CompileStatic @PackageScope
class DoopInputBundleInitializer {

	static final String ARTIFACT_CLASS_FACTS    = "Class-Artifact.facts"
	static final String TYPE_ANNOTATION_FACTS   = "Type-Annotation.facts"
	static final String METHOD_ANNOTATION_FACTS = "Method-Annotation.facts"
	static final String FIELD_ANNOTATION_FACTS  = "Field-Annotation.facts"

	static final long sum(long l1, long l2) {
		return l1 + l2
	}

	private final DoopInputBundle bundle
	private final BulkProcessor bulk
	private DoopAnalysis analysis

	// We expect that artifact names are unique
	private final Map<String, Artifact> artifactsByName = new ConcurrentHashMap<>()
	private final Map<String, Klass> classes            = new ConcurrentHashMap<>()
	private final Map<String, Method> methods           = new ConcurrentHashMap<>()
	private final Map<String, Field> fields             = new ConcurrentHashMap<>()
	private final Map<String, Set<String>> annotationsByDoopId = new ConcurrentHashMap<>()

	private DoopInputBundleInitializer(DoopInputBundle bundle, BulkProcessor bulk) {
		this.bundle = bundle
		this.bulk   = bulk
	}

	static DoopInputBundleInitializer forBundle(DoopInputBundle bundle) {
		def store = Config.instance.storage.initializeBundleDoopStore(bundle)
		def bulk = new BulkProcessor(Config.instance.bulkSize, store)

		return new DoopInputBundleInitializer(bundle, bulk)
	}

	void finishUp() {
		bulk.finishUp()
	}

	void initializeArtifacts() {
		log.debug "Processing artifacts"
		int index = 0		
		def handleFileClosure = { Path inDir, File file, InputType inputType ->
			index++
			def theFile = ensureFileIsInDir(inDir, file)
			extractFile(bundle, theFile)
			def fileName = theFile.name
			def artifact = new Artifact(
					"${bundle.id}-art-$index",
					fileName,
					determineArtifactKind(fileName),
					inputType != InputType.INPUT, // isDependency
					sourcesName(bundle, fileName),
					CheckSum.checksum(file, Artifact.CHECKSUM_ALGORITHM),
					theFile.length()
			)
			bundle.artifacts[inputType] << artifact
			artifactsByName[fileName] = artifact
		}

		def inputsPath = bundle.inputsDir.toPath()
		bundle.options.INPUTS.value.each { File f -> handleFileClosure(inputsPath, f, InputType.INPUT) }

		bundle.options.LIBRARIES.value.each { File f -> handleFileClosure(inputsPath, f, InputType.LIBRARY) }

		def platformPath = bundle.platformDir.toPath()
		bundle.options.PLATFORMS.value.each { File f -> handleFileClosure(platformPath, f, InputType.PLATFORM) }		
	}	

	void generateFacts() {
		log.debug "Generating facts"
		analysis = new DoopServerSideAnalysisFactory(bundle).newAnalysis()
		// Update options in bundle
		analysis.options.each { key, option -> bundle.options[key]?.value = option.value }
		analysis.run()
	}


	void connectClassesPackagesAndArtifacts() {		
		log.debug "Connecting classes, packages and artifacts"

		def facts = FileOps.findFileOrThrow(new File(bundle.factsDir, ARTIFACT_CLASS_FACTS), "$ARTIFACT_CLASS_FACTS not found")
		facts.eachLine { String line ->
			try {
				//The fqClassName is the doop id
				String[] lineParts  = line.split()
				String artifactName = lineParts[0]
				String fqClassName  = lineParts[1]
				String dexName      = lineParts[2]
				String sizeInBytes  = lineParts[3]
				def artifact = artifactsByName.get(artifactName)
				if (!artifact) {
					throw new RuntimeException("Unknown artifact: $artifactName")
				}

				Artifact dexArtifact = null
				if (artifact.kind == ArtifactKind.APK && dexName) {
					def dexQualifiedName = "$artifactName/$dexName" as String
					dexArtifact = artifactsByName.get(dexQualifiedName)
					if (!dexArtifact) {
						dexArtifact = new Artifact(
							dexQualifiedName,
							dexQualifiedName,
							ArtifactKind.DEX,
							artifact.isDependency(),
							null, // sources
							null, // checksum
							0, // length,
							[] as Set, // packages
							artifact.id // parent
						)
						// Add the dexArtifact in the bundle inputs
						bundle.artifacts[InputType.INPUT] << dexArtifact
						// Also keep it in the map
						artifactsByName[dexQualifiedName] = dexArtifact
					}
				}
				def parts = fqClassName.tokenize(".")
				Klass kl = new Klass()
				kl.name = parts[-1].replaceAll('\\$', '.')
				kl.packageName = parts.size() > 1 ? parts[0..-2].join('.') : ''
				kl.doopId = fqClassName
				kl.artifactName = (dexArtifact ?: artifact).name
				kl.sizeInBytes = lineParts[3] as Integer
				//hold the packages in both parent (apk) and child (dex) artifacts
				artifact.packages.add(kl.packageName)
				dexArtifact?.packages?.add(kl.packageName)

				classes.put(fqClassName as String, kl)
			} catch (any) {
				log.warn "Ignoring $ARTIFACT_CLASS_FACTS line: $line"
				log.warn(any.getMessage(), any)
			}
		}		
	}

	void processCHA(){

		log.debug "Processing CHA info"

		analysis.processRelation "Class_InterestingSubtype", { String line ->
			String[] parts    = line.split(", ")
			String classId    = parts[0]
			String subClassId = parts[1]
			bulk.scheduleSave new ClassSubtype(bundle.id, classId, subClassId)
		}
		analysis.processRelation "MethodLookupExt", { String line ->
			String[] parts = line.split(", ")
			String simplename       = parts[0]
			String descriptor       = parts[1]
			String typeId           = parts[2]
			String methodId         = parts[3]
			String isSingleDispatch = parts[4]
			bulk.scheduleSave new MethodLookup(bundle.id, "$simplename $descriptor" as String, typeId, methodId, isSingleDispatch == "1")
		}
		analysis.processRelation "Invocation_Parts", { String line ->
			String[] parts          = line.split(", ")
			String invocationId     = parts[0]
			String simplename       = parts[1]
			String descriptor       = parts[2]
			String baseTypeId       = parts[3]
			String isSingleDispatch = parts[4]
			bulk.scheduleSave new InvocationParts(bundle.id, invocationId, "$simplename $descriptor" as String, baseTypeId, isSingleDispatch == "1")
		}

		//TODO: Check whether both Class_InterestingSubtype and SubtypeOf are actually needed
		long count = 0
		analysis.processRelation "SubtypeOf", { String line ->
			String[] parts = line.split(", ")
			String classId = parts[0]
			String superTypeId = parts[1]

			if (!IgnoreTypes.shouldIgnoreType(classId)) {
				Klass kl = classes.get(classId)
				if (!kl) {
					log.warn("Class ${classId} is contained in SubtypeOf but is not found in the bundle")
				}
				else {
					kl.superTypes.add(superTypeId)
					count++
				}
			}
		}
		log.debug("${Thread.currentThread().getName()} - Added $count superTypes")
	}

	void processAnnotations() {

		[TYPE_ANNOTATION_FACTS, METHOD_ANNOTATION_FACTS, FIELD_ANNOTATION_FACTS].each { String factsFileName ->

			def facts = FileOps.findFileOrThrow(new File(bundle.factsDir, factsFileName), "$factsFileName not found")
			long annotationCount = 0
			facts.eachLine { String line ->
				try {
					String[] lineParts    = line.split('\t')
					String doopId         = lineParts[0]
					String annotationType = lineParts[1]

					if (annotationsByDoopId.containsKey(doopId)) {
						annotationsByDoopId.get(doopId).add(annotationType)
					}
					else {
						annotationsByDoopId.put(doopId, [annotationType] as Set)
					}
					annotationCount++
				}
				catch(any) {
					log.warn "Ignoring $factsFileName line: $line"
					log.warn(any.getMessage(), any)
				}
			}
			log.debug("Processed $annotationCount lines from $factsFileName")
		}
	}

	void processJCPluginMetadata() {		
		def jsonFiles = 0L

		def bundleId = bundle.id

		Set<String> sourceClasses = bundle.sourceClasses

		log.debug "Importing json files from ${bundle.jcPluginMetadataDir}"
		bundle.jcPluginMetadataDir.eachFileMatch FILES, ~/.*\.json/, { File f ->
			def json = new JsonSlurper().parse(f, "UTF-8")
			(Constants.JC_PLUGIN_SYMBOLS + Constants.JC_PLUGIN_BASIC_ELEMENTS).each { Class cl ->
				json[cl.simpleName].each { Map<String, Object> elementMap ->
					// Static final String fields are inlined by javac
					// We get that information back from JCPlugin
					if (cl.simpleName == 'StringConstant') {
						//def userId = elementMap['userId'] as String
						def fieldId = elementMap['fieldDoopId'] as String
						def valueId = "$fieldId (${elementMap['value']})" as String
						def ha = new HeapAllocation(
								elementMap['position'] as Position,
								elementMap['sourceFileName'] as String,
								valueId,
								"java.lang.String",
								null,
								false,
								false)
						ha.rootElemId = bundleId
						bulk.scheduleSave ha
						bulk.scheduleSave new StaticFieldValues(bundleId, fieldId, valueId)
					} else {
						def el = cl.newInstance() as Element
						if (el instanceof Klass) {
							sourceClasses << (el as Klass).doopId
						}
						saveElement(el)
					}
				}
			}
			jsonFiles++
		}
		log.debug "Submitted bulk import of $jsonFiles json files, ${sourceClasses.size()} classes"
	}

	void processJimple() {

		Set<String> typesToImport = [Klass.simpleName, Field.simpleName, Method.simpleName] as Set
		def importer = new JimpleImportBase(bundle.jimpleDir, bundle.sourceClasses, typesToImport) {
			@Override
			void importElement(Element e) {
				saveElement(e)
			}
		}

		importer.importJimple()
	}

	void processBundleConfiguration() {
		//always add the default configSet to the bundle
		BundleConfiguration configSet = new BundleConfiguration(bundle.id, DoopInputBundle.DEFAULT_CONFIG_SET, bundle.getCreated(), bundle.getCreated())

		File clueFile  = new File(bundle.inputsDir, DoopInputBundle.DEFAULT_CONFIG_SET)
		if (clueFile?.exists()) {
			log.debug "Processing clue file"
			BundleConfigurationContext proguardCtx = new BundleConfigurationContext(bundle.userId)
			Map<String, PersistentBundleConfigurationRule> persistentRulesMap = ClueJsonFile.parse(clueFile, proguardCtx, configSet)
			Collection<PersistentBundleConfigurationRule> persistentRules = persistentRulesMap.values()
			log.debug("Parsed clue file rules:\n ${persistentRules.collect { it.toMap() }.join('\n')}")
			methods.values().groupBy { it.declaringClassDoopId }.each { String classId, List<Method> methods ->
				try {

					//verify the klass/methods
					Tuple2<Klass, Artifact> tuple2 = verify(classId, methods)
					Klass kl = tuple2.first
					Artifact a = tuple2.second

					//find all the rules that match the klass
					Collection<PersistentBundleConfigurationRule> rules = DefaultRuleMatchers.findAllClassRulesThatMatch(proguardCtx, persistentRules, kl)
					if (rules) {
						rules.each { PersistentBundleConfigurationRule rule ->
							//find the methods that match its member rules
							Set<Method> matchingMethods = DefaultRuleMatchers.filterMethodsThatMatchRule(proguardCtx, rule, methods)
							int cnt = matchingMethods.size()
							if (cnt > 0) {
								matchingMethods.each { Method m ->
									MethodDirectiveContext mdctx = new MethodDirectiveContext(a, kl, m, null)
									KeepRemoveMethodDirective.Type dirType = KeepRemoveMethodDirective.Type.fromBundleConfigurationRuleType(rule.parsedRule.getType())
									bulk.scheduleSave(new KeepRemoveMethodDirective(
											bundle.id,
											configSet.id,
											rule.getId(),
											mdctx,
											rule.origin,
											rule.originType,
											dirType
									))
									rule.packageName.add(mdctx.klass.packageName)
									rule.className.add(mdctx.klass.name)
								}
								rule.matchingMethods += cnt
							}
						}
					}
				}
				catch(e) {
					log.warn(e.getMessage(), e)
				}
			}

			persistentRules.each { PersistentBundleConfigurationRule persRule ->
				bulk.scheduleSave(persRule)
			}
		}

		Config.instance.storage.storeNewConfigSet(bundle, configSet)
	}

	private Tuple2<Klass, Artifact> verify(String classId, List<Method> methods) {
		Klass kl = classes.get(classId)
		if (!kl) {
			throw new RuntimeException("Class not found $classId (for methods ${methods.collect{ it.doopId }.join(', ')})")
		}
		Artifact a = artifactsByName.get(kl.artifactName)
		if (!a) {
			throw new RuntimeException("Artifact ${kl.artifactName} not found for class $classId (with methods ${methods.collect{ it.doopId }.join(', ')})" )
		}
		return new Tuple2<Klass, Artifact>(kl, a)
	}

	private static File ensureFileIsInDir(Path dirPath, File file) {
		def filePath = file.toPath()
		if (Files.isSameFile(filePath.getParent(), dirPath)) {
			return file
		} else {
			//copy the file in the given directory
			return Files.copy(filePath, dirPath.resolve(file.getName())).toFile()
		}
	}

	private static void extractFile(DoopInputBundle bundle, File zipFile) {
		def name = zipFile.name
		def dir = new File(bundle.inputsExtractedDir, name)
		dir.mkdir()
		FileOps.findDirOrThrow(dir, "Could not unzip $name")
		FileOps.unzip(zipFile, dir)
	}

	private static ArtifactKind determineArtifactKind(String fileName) {
		def ext = FilenameUtils.getExtension(fileName)
		try {
			return ArtifactKind.valueOf(ext?.toUpperCase())
		} catch (any) {
			return ArtifactKind.OTHER
		}
	}

	private static String sourcesName(DoopInputBundle bundle, String jarName) {
		def ext = ".jar"
		def baseName = jarName.substring(0, jarName.length() - ext.length())
		try {
			File sourcesJar = FileOps.findDirOrThrow(new File(bundle.sourcesDir, baseName + "-sources.jar"), "No sources")
			log.debug "Found sources for $jarName -> $sourcesJar"
			return sourcesJar.getName()
		} catch (all) {
			log.debug "No sources for $jarName"
			return null
		}
	}

	private void saveElement(Element e) {
		e.rootElemId = bundle.id
		if (e instanceof AnnotateableSymbolWithDoopId) {
			setupAnnotations(e as AnnotateableSymbolWithDoopId)
			if (e instanceof Klass) {
				setupClass(e as Klass)
			}
			else if (e instanceof Method) {
				setupMethod(e as Method)
			}
			else if (e instanceof Field) {
				setupField(e as Field)
			}
		}
		bulk.scheduleSave(e)
	}

	private void setupClass(Klass kl) {
		Klass klassInMap = classes.get(kl.doopId)
		if (klassInMap) {
			kl.artifactName = klassInMap.artifactName
			kl.sizeInBytes = klassInMap.sizeInBytes
			kl.superTypes = klassInMap.superTypes
		}
	}

	private void setupMethod(Method m) {
		methods.put(m.doopId, m)
	}

	private void setupField(Field f) {
		fields.put(f.doopId, f)
	}

	private void setupAnnotations(AnnotateableSymbolWithDoopId s) {
		def annotations = annotationsByDoopId.get(s.doopId)
		if (annotations) {
			s.setAnnotationTypes(annotations)
		}
	}

	/*
	private static String[] append(String[] array, String element) {
		if (array) {
			String[] newArray = new String[array.length + 1]
			System.arraycopy(array, 0, newArray, 0, array.length)
			newArray[array.length] = element
			return newArray
		}
		else {
			String[] newArray = new String[1]
			newArray[0] = element
			return newArray
		}
	}
	*/

	private static class IgnoreTypes {
		private static final Set<String> IGNORED_TYPES = ["boolean",
														  "byte",
														  "char",
														  "short",
														  "int",
														  "long",
														  "float",
														  "double",
														  "void",
														  "null_type"] as Set

		//ignore all primitives, the null type and all array types
		private static boolean shouldIgnoreType(String typeId) {
			return IGNORED_TYPES.contains(typeId) || typeId?.endsWith("[]")
		}
	}

}