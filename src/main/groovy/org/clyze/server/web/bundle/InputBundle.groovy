package org.clyze.server.web.bundle

import groovy.json.JsonOutput
import org.clyze.analysis.Analysis
import org.clyze.doop.input.InputResolutionContext
import org.clyze.persistent.model.Item
import org.clyze.server.web.analysis.ServerSideAnalysis

/**
 * The user-supplied input bundle (artifacts, sources, metadata, etc), along with its
 * custom behavior (e.g. data serialization or post-processing logic).
 * 
 * A container for all clyze-related inputs.
 */
abstract class InputBundle<A extends Analysis> implements Item {
	
	String id
	String userId
	String projectId
	String displayName
	File baseDir
	boolean isPublic = false	

	@Override
	String toJSON() {
		return JsonOutput.toJson(toMap())
	}	

	/**
	 * Get the input resolution context that "knows" the inputs of this bundle.
	 */
	abstract InputResolutionContext getInputResolutionContext()

	/**
	 * Initialize the analysis object from a JSON map (originating from the underlying datastore).
	 */
	abstract A initializeAnalysis(Object jsonMap)

	/**
	 * Post processing entry-point.
	 */
	abstract void postProcess(ServerSideAnalysis<A> serverSideAnalysis)
}