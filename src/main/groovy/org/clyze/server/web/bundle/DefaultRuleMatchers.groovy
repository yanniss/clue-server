package org.clyze.server.web.bundle

import org.clyze.persistent.model.doop.Class
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.PersistentBundleConfigurationRule

class DefaultRuleMatchers {

    static Set<Method> filterMethodsThatMatchRule(BundleConfigurationContext ctx, PersistentBundleConfigurationRule rule, List<Method> methods) {
        return new DefaultMethodMatcher(ctx, methods).matchingMethods(rule.parsedRule)
    }

    static boolean matchesClassRule(BundleConfigurationContext ctx, PersistentBundleConfigurationRule rule, Class klass) {
        return R8BundleConfigurationRuleMatcher.matchesClassRule(ctx, rule.parsedRule, klass)
    }

    static Collection<PersistentBundleConfigurationRule> findAllClassRulesThatMatch(BundleConfigurationContext ctx, Collection<PersistentBundleConfigurationRule> rules, Class klass) {
        rules.findAll { PersistentBundleConfigurationRule rule ->
            R8BundleConfigurationRuleMatcher.matchesClassRule(ctx, rule.parsedRule, klass)
        }
    }
}
