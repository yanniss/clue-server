package org.clyze.server.web.bundle


import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableEmitter
import io.reactivex.FlowableOnSubscribe
import io.reactivex.annotations.NonNull
import io.reactivex.parallel.ParallelFlowable
import io.reactivex.schedulers.Schedulers
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.doop.Class
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.model.doop.MethodDirectiveContext
import org.clyze.server.web.persistent.model.doop.OptimizationDirective

abstract class R8BundleConfigurationRuleEngine implements ReactiveBundleConfigurationRuleExpander {

    private static final Log log = LogFactory.getLog(R8BundleConfigurationRuleEngine.class)

    protected final DoopInputBundle bundle;

    R8BundleConfigurationRuleEngine(DoopInputBundle bundle) {
        this.bundle = bundle
    }

    @Override
    ParallelFlowable<KeepRemoveMethodDirective> expandRule(BundleConfigurationContext ctx, BundleConfiguration config, PersistentBundleConfigurationRule rule) {
        allClassesOfBundle().parallel().runOn(Schedulers.from(Config.instance.postProcessExecutor)).concatMap{ Class klass ->
            boolean matchesClassRule = DefaultRuleMatchers.matchesClassRule(ctx, rule, klass)
            Flowable.create(matchesClassRule ? new MethodMatcher(ctx, config, rule, klass) : new ClassNotMatched(), BackpressureStrategy.BUFFER)
        }
    }

    abstract Flowable<Class> allClassesOfBundle();

    abstract List<Method> allDeclaredMethodsOf(Class klass);

    class MethodMatcher implements FlowableOnSubscribe<KeepRemoveMethodDirective> {

        private final BundleConfigurationContext ctx;
        private final PersistentBundleConfigurationRule rule;
        private final BundleConfiguration config;
        private final Class klass;

        MethodMatcher(BundleConfigurationContext ctx, BundleConfiguration config, PersistentBundleConfigurationRule rule, Class klass) {
            this.ctx = ctx;
            this.rule = rule;
            this.config = config;
            this.klass = klass;
        }

        @Override
        void subscribe(@NonNull final FlowableEmitter<KeepRemoveMethodDirective> emitter) throws Exception {

            findMatchingMethodsOfClass().each { methodCtx ->
                    emitter.onNext(
                        new KeepRemoveMethodDirective(
                                bundle.getId(),
                                config.getId(),
                                rule.getId(),
                                methodCtx,
                                [ctx.userId] as Set,
                                OptimizationDirective.OriginType.USER,
                                KeepRemoveMethodDirective.Type.fromBundleConfigurationRuleType(rule.parsedRule.getType())
                        )
                    )
            }
            emitter.onComplete()
        }

        Collection<MethodDirectiveContext> findMatchingMethodsOfClass() {
            List<Method> declaredMethods = allDeclaredMethodsOf(klass)
            DefaultRuleMatchers.filterMethodsThatMatchRule(ctx, rule, declaredMethods).collect { Method method ->
                return new MethodDirectiveContext(null, klass, method, rule.parsedRule)
            }
        }
    }

    class ClassNotMatched implements FlowableOnSubscribe<KeepRemoveMethodDirective> {
        @Override
        void subscribe(@NonNull FlowableEmitter<KeepRemoveMethodDirective> emitter) throws Exception {
            emitter.onComplete()
        }
    }
}
