package org.clyze.server.web.bundle

import com.android.tools.r8.shaking.BundleConfigurationRule
import com.google.gson.stream.JsonWriter
import groovy.json.JsonSlurper
import groovy.transform.CompileStatic
import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.persistent.model.doop.OptimizationDirective

@CompileStatic
class ClueJsonFile {

    static Map<String, PersistentBundleConfigurationRule> parse(File clueJsonFile, BundleConfigurationContext ctx, BundleConfiguration bundleConfiguration) {

        BundleConfigurationBuilder configurationBuilder = ctx.newBundleConfigurationBuilder()

        Map<String, PersistentBundleConfigurationRule> persistentRules = [:]

        Map json = new JsonSlurper().parse(clueJsonFile) as Map
        Map jsonSteps = json.steps as Map

        boolean isShrinkEnabled    = isBooleanTrue(jsonSteps, 'shrink')
        boolean isObfuscateEnabled = isBooleanTrue(jsonSteps, 'obfuscate')
        boolean isOptimizeEnabled  = isBooleanTrue(jsonSteps, 'optimize')

        if (isShrinkEnabled) bundleConfiguration.enableStep(BundleConfiguration.Step.SHRINK)
        if (isObfuscateEnabled) bundleConfiguration.enableStep(BundleConfiguration.Step.OBFUSCATE)
        if (isOptimizeEnabled) bundleConfiguration.enableStep(BundleConfiguration.Step.OPTIMIZE)

        long index = 0
        json.rules.each { Map jsonRule ->

            String rule = jsonRule.rule as String
            String checksum = BundleConfigurationRule.checksumFor(rule)
            String comment = jsonRule.comment as String
            String originType = jsonRule.originType as String

            PersistentBundleConfigurationRule persistentRule = new PersistentBundleConfigurationRule(
                id: UUID.randomUUID().toString(),
                checksum: checksum,
                bundleId: bundleConfiguration.bundleId,
                configSet: bundleConfiguration.id,
                ruleBody: rule,
                comment: comment,
                originType: OptimizationDirective.OriginType.valueOf(originType),
                origin: readOriginFromJson(jsonRule.origin),
                userIndex: index++
            )

            ctx.parseClueRule(rule, configurationBuilder)
            persistentRules.put(persistentRule.checksum, persistentRule)
        }

        List<BundleConfigurationRule> parsedRules = configurationBuilder.buildClueRules()
        assert parsedRules.size() == persistentRules.size() : "Parsed rules size mismatch"

        //iterate over rules again to match persistent rules with parsed ones
        parsedRules.each { BundleConfigurationRule rule ->
            PersistentBundleConfigurationRule persRule = persistentRules.get(rule.id)
            assert persRule != null : "Parsed rule checksum/id mismatch for: ${rule.source}"
            persRule.ruleType = rule.type
            persRule.parsedRule = rule
        }

        return persistentRules
    }

    static Set<String> readOriginFromJson(def origin) {
        if (origin instanceof String) {
            return [origin] as Set
        }
        if (origin instanceof Collection) {
            return origin as Set
        }
        return Collections.emptySet()
    }

    static void externalize(DoopInputBundle bundle, BundleConfiguration bundleConfiguration, Writer w) {

        def writer = new JsonWriter(w)

        writer.beginObject() // {
        writer.name('steps')
        writer.beginObject() // {
        writer.name('shrink').value(bundleConfiguration.isStepEnabled(BundleConfiguration.Step.SHRINK))
        writer.name('obfuscate').value(bundleConfiguration.isStepEnabled(BundleConfiguration.Step.OBFUSCATE))
        writer.name('optimize').value(bundleConfiguration.isStepEnabled(BundleConfiguration.Step.OPTIMIZE))
        writer.endObject() // }
        writer.name("rules")
        writer.beginArray() // [
        Config.instance.storage.forEachBundleConfigurationRule(bundle, bundleConfiguration) { String id, String type, String json ->

            PersistentBundleConfigurationRule rule = new PersistentBundleConfigurationRule().fromJSON(json) as PersistentBundleConfigurationRule

            writer.beginObject() // {
            writer.name("rule").value(rule.ruleBody)
            writer.name("comment").value(rule.comment)
            writer.name("originType").value(rule.originType.name())
            writer.name("origin")
            writer.beginArray() // [
            rule.origin.each { String origin -> writer.value(origin)}
            writer.endArray() // ]
            writer.endObject() // }
        }
        writer.endArray() // ]
        writer.endObject() // }
    }

    private static boolean isBooleanTrue(Map map, String field) {
        return  Boolean.parseBoolean(map?.get(field)?.toString())
    }
}
