package org.clyze.server.web.bundle

import com.android.tools.r8.shaking.BundleConfigurationRule
import com.android.tools.r8.shaking.ProguardRuleParserException
import io.reactivex.parallel.ParallelFlowable
import org.clyze.server.web.Config
import org.clyze.server.web.PersistentBundleConfigurationRule
import org.clyze.server.web.persistent.BulkProcessor
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.clyze.server.web.persistent.store.DataStore

class RuleExpansion {

    private final BundleConfigurationContext ctx
    private final DoopInputBundle bundle
    final PersistentBundleConfigurationRule rule
    final boolean isNew

    RuleExpansion(BundleConfigurationContext ctx, DoopInputBundle bundle, PersistentBundleConfigurationRule rule) {
        this.ctx = ctx
        this.bundle = bundle
        this.rule = rule
        this.isNew = (rule.getId() == null)
    }

    RuleExpansion parseRule() throws ProguardRuleParserException {
        List<BundleConfigurationRule> parsedRules = ctx.parseClueRule(rule.ruleBody).buildClueRules()
        assert parsedRules.size() == 1: "Rules count mismatch! Are you adding multiple rules in a context where a single rule is expected?"
        rule.parsedRule = parsedRules.get(0)
        rule.ruleType = rule.parsedRule.type
        return this
    }

    protected ParallelFlowable<KeepRemoveMethodDirective> expandTo(BundleConfiguration target) {
        ElasticBundleConfigurationRuleEngine engine = new ElasticBundleConfigurationRuleEngine(bundle)
        //if rule is new, we need to set its id before expanding it, so that the directives will reference it correctly
        if (isNew) rule.setId(UUID.randomUUID().toString())
        engine.expandRule(ctx, target, rule)
    }

    /*
    long expandAndCount() {
        expandTo(null).reduce( { ->
            0l
        }, { Long counter, KeepRemoveMethodDirective directive ->
            return counter + 1
        }).sequential().reduce( 0l, { long l1, long l2 ->
            return l1 + l2
        }).blockingGet()
    }
     */

    PersistentBundleConfigurationRule expandAndSaveTo(BundleConfiguration target) {

        DataStore store = Config.instance.storage.bundleDoopStore(bundle)
        BulkProcessor bulkProcessor = new BulkProcessor(Config.instance.bulkSize, store)
        RuleMetadata ruleMetadata = expandTo(target).reduce( {-> new RuleMetadata()}, { RuleMetadata struct, KeepRemoveMethodDirective directive ->
            bulkProcessor.scheduleSave(directive)
            struct.matchingMethods++
            struct.packageNames.add(directive.packageName)
            struct.classNames.add(directive.className)
            return struct
        }).sequential().reduce( new RuleMetadata(), { RuleMetadata rm1, RuleMetadata rm2 ->
            rm1.matchingMethods += rm2.matchingMethods
            rm1.packageNames.addAll(rm2.packageNames)
            rm1.classNames.addAll(rm2.classNames)
            return rm1
        }).doAfterTerminate {
            bulkProcessor.finishUp()
        }.blockingGet()

        rule.matchingMethods = ruleMetadata.matchingMethods
        rule.packageName = ruleMetadata.packageNames
        rule.className = ruleMetadata.classNames
        if (isNew) {
            store.saveNew(rule)
            target.setRuleIndex(target.getRuleIndex() + 1)
        }
        else {
            store.updateExisting(rule)
        }

        target.dirty = true
        Config.instance.storage.updateConfigSet(bundle, target)

        return rule
    }

    PersistentBundleConfigurationRule expandAndUpdateIn(BundleConfiguration target) {

        //Remove the directives of the rule
        Config.instance.storage.removeDirectivesOfRule(bundle, target, rule.id)

        return expandAndSaveTo(target)
    }

    private static final class RuleMetadata {
        long matchingMethods
        Set<String> packageNames = new HashSet<>()
        Set<String> classNames = new HashSet<>()
    }

}
