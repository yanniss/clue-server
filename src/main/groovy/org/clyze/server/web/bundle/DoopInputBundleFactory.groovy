package org.clyze.server.web.bundle

import org.apache.commons.fileupload.FileItem

import org.clyze.utils.Helper
import org.clyze.doop.input.*
import org.clyze.analysis.InputType

import org.clyze.server.web.Config
import org.clyze.server.web.persistent.Identifiers
import org.clyze.server.web.FormLogicHelper
import org.clyze.server.web.analysis.DecoratedAnalysisOption

import org.restlet.Request
import org.restlet.ext.fileupload.RestletFileUpload

import groovy.util.logging.Log4j

@Log4j
class DoopInputBundleFactory implements InputBundleFactory<DoopInputBundle> {	

	DoopInputBundle newInputBundle(File baseDir, Identifiers.BundleInfo info, Request request) {

		String id = info.id

		Date now = new Date()		

		def bundle = new DoopInputBundle(info, now)
		bundle.setupDirectories(baseDir) //this creates all dirs

		log.debug "Creating new input bundle ${bundle.id} for ${bundle.userId} in ${bundle.baseDir}"

		def platformsDir = new File(bundle.options.PLATFORMS_LIB.value as String)
		def ctx = new DefaultInputResolutionContext(
				new ChainResolver(
						new SecurePlatformResolver(platformsDir),
						new FileResolver(bundle.inputsDir),
						new FileResolver(bundle.heapdlDir),
						new URLResolver(bundle.inputsDir),
						new RecursiveBenchmarksResolver(platformsDir),
						new IvyResolver()
				)
		)
		bundle.inputResolutionContext = ctx

		def upload = new RestletFileUpload(Config.instance.fileItemFactory)
		def items = upload.parseRequest(request)
		def options = FormLogicHelper.BUNDLE_FORM_OPTIONS.collectEntries { [(it.id): it.clone()] } as Map<String, DecoratedAnalysisOption>

		items.each { FileItem item ->

			def fieldName = item.fieldName
			// Lookup the option using its id. If that fails, look it up using its name.
			def option = options[fieldName] ?: options.find { it.value.name == fieldName }.value as DecoratedAnalysisOption
			if (option) {
				if (item.isFormField()) {
					def value = item.getString()?.trim()

					if (value) { //ignore empty or false values											
						def processor = FormLogicHelper.BUNDLE_FORM_FIELD_PROCESSORS.find { it.supportsOption(option) }
						if (processor) {
							log.debug "Processing form field: $fieldName = $value"
							processor.process(bundle, ctx, option, value)
						} else {
							log.debug "Ignoring form field: $fieldName = $value"
						}
					} else {
						log.debug "Ignoring empty form field: $fieldName"
					}
				} else if (item.name == "" && item.size == 0) { // "item.size == 0" may be redundant.
					log.debug "Ignoring empty form file: $fieldName"
				} else {
					def processor = FormLogicHelper.BUNDLE_FORM_FILE_PROCESSORS.find { it.supportsOption(option) }
					if (processor) {
						log.debug "Processing form file: $fieldName = ${item.name}"
						processor.process(bundle, ctx, option, item)
					} else {
						log.debug "Ignoring form file: $fieldName = ${item.name}"
					}
				}
			} else {
				log.debug "Ignoring unknown form field: $fieldName"
			}
		}

		def platform = bundle.options.PLATFORM.value as String
		def platformsLib = bundle.options.PLATFORMS_LIB.value as String
		log.debug "Getting platform files: $platform - $platformsLib"
		new PlatformManager(platformsLib).find(platform).each { ctx.add(it, InputType.PLATFORM) }

		// Always resolve the inputs/libraries in order to be able to gather proper Artifact information
		ctx.resolve()
		def allInputs = ctx.allInputs
		if (!allInputs)
			throw new RuntimeException("Bundle is empty")
		bundle.options.INPUTS.value = allInputs
		bundle.options.LIBRARIES.value = ctx.allLibraries
		bundle.options.PLATFORMS.value = ctx.allPlatformFiles
		bundle.options.HEAPDLS.value = ctx.allHeapDLs

		Helper.timingWithLogging("BUNDLE - Initialization") {

			def initializer = DoopInputBundleInitializer.forBundle(bundle)
			Helper.timingWithLogging("BUNDLE - Initialize artifacts") {
				initializer.initializeArtifacts()
			}

			Helper.timingWithLogging("BUNDLE - Generate facts") {
				initializer.generateFacts()
			}

			Helper.timingWithLogging("BUNDLE - Connect class/packages/artifacts") {
				initializer.connectClassesPackagesAndArtifacts()
			}

			Helper.timingWithLogging("BUNDLE - Process CHA") {
				initializer.processCHA()
			}

			Helper.timingWithLogging("BUNDLE - Process annotations") {
				initializer.processAnnotations()
			}

			if (bundle.hasSrcMetadata) {
				Helper.timingWithLogging("BUNDLE - Process sources") {
					initializer.processJCPluginMetadata()
				}
			} else {
				log.warn "No jcPluginMetadata available"
			}
			Helper.timingWithLogging("BUNDLE - Process jimple") {
				initializer.processJimple()
			}

			Helper.timingWithLogging("BUNDLE - Process bundle config") {
				initializer.processBundleConfiguration()
			}

			Helper.timingWithLogging("BUNDLE - Finish up") {
				initializer.finishUp() //wait for the initializer to finish
			}
		}

		return bundle
	}		

}
