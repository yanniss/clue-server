package org.clyze.server.web.bundle


import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.clyze.persistent.model.doop.Class as Klass
import org.clyze.persistent.model.doop.Method
import org.clyze.server.web.Config
import org.clyze.server.web.persistent.store.BooleanQuery
import org.clyze.server.web.persistent.store.BooleanQueryBuilder
import org.clyze.server.web.persistent.store.Searchable

class ElasticBundleConfigurationRuleEngine extends R8BundleConfigurationRuleEngine {

    private static final Log log = LogFactory.getLog(ElasticBundleConfigurationRuleEngine.class)

    protected final Searchable searchable

    ElasticBundleConfigurationRuleEngine(DoopInputBundle bundle) {
        super(bundle)
        searchable = Config.instance.storage.bundleDoopStore(bundle)
    }

    @Override
    Flowable<Klass> allClassesOfBundle() {
        Flowable.<Klass>create ({ emitter ->
            BooleanQuery q = BooleanQueryBuilder.build('_type', Klass.simpleName)
            searchable.scan(q, null) { id, type, String json ->
                emitter.onNext(new Klass().fromJSON(json) as Klass)
            }
            emitter.onComplete()
        }, BackpressureStrategy.BUFFER)
    }


    @Override
    List<Method> allDeclaredMethodsOf(Klass klass) {
        BooleanQuery q = BooleanQueryBuilder.build {
            AND (
                EQ('_type', Method.simpleName),
                EQ('declaringClassDoopId', klass.doopId)
            )
        }
        List<Method> methods = []
        searchable.scan(null, q, null) { id, type, String json ->
            methods.add(new Method().fromJSON(json) as Method)
        }
        return methods
    }
}
