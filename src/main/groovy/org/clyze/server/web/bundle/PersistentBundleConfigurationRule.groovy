package org.clyze.server.web

import com.android.tools.r8.shaking.BundleConfigurationRule
import groovy.transform.CompileStatic
import org.clyze.persistent.model.Element
import org.clyze.server.web.bundle.ClueJsonFile
import org.clyze.server.web.persistent.model.doop.OptimizationDirective

/**
 * Our model of a Bundle config rule (richer than proguard/r8 ProguardConfigurationRule).
 * Instances of this class are generated from the processing of the clue JSON clue file and are also 
 * persisted in the datastore.
 */
@CompileStatic
class PersistentBundleConfigurationRule extends Element {

    String checksum
    String bundleId
    String configSet
    String ruleBody
    BundleConfigurationRule.Type ruleType
    String comment
    OptimizationDirective.OriginType originType
    Set<String> origin
    Set<String> packageName = new HashSet<>()
    //TODO: Class names may clash. Is this an issue?
    Set<String> className = new HashSet<>()
    long matchingMethods
    long userIndex //the index of the rule in the config, as inserted by the user

    BundleConfigurationRule parsedRule //hold the parsed rule transiently

    PersistentBundleConfigurationRule() {}

    PersistentBundleConfigurationRule(PersistentBundleConfigurationRule source, String configSet) {
        this.id = UUID.randomUUID().toString()
        this.checksum = source.checksum
        setBundleId(source.bundleId)
        this.configSet = configSet
        this.ruleBody = source.ruleBody
        this.ruleType = source.ruleType
        this.comment = source.comment
        this.originType = source.originType
        this.origin = source.origin
        this.packageName.addAll(source.packageName)
        this.className.addAll(source.className)
        this.matchingMethods = source.matchingMethods
        this.userIndex = source.userIndex
    }

    void setBundleId(String bundleId) {
        this.bundleId = bundleId
        this.rootElemId = bundleId
    }

    @Override
    protected void loadFrom(Map<String, Object> map) {
        super.loadFrom(map)
        this.id = (String) map.get("id")
        this.checksum = (String) map.get("checksum")
        setBundleId((String) map.get("bundleId"))
        this.configSet = (String) map.get("configSet")
        this.ruleBody = (String) map.get("ruleBody")
        this.ruleType = BundleConfigurationRule.Type.valueOf( (String) map.get("ruleType") )
        this.comment = (String) map.get("comment")
        this.originType = OptimizationDirective.OriginType.valueOf( (String) map.get("originType") )
        this.origin = ClueJsonFile.readOriginFromJson(map.get("origin"))
        this.packageName.addAll(map.get("packageName") as Collection<String>)
        this.className.addAll(map.get("className") as Collection<String>)
        this.matchingMethods = (Long) map.get("matchingMethods")
        this.userIndex = (Long) map.get("userIndex")
    }

    @Override
    protected void saveTo(Map<String, Object> map) {
        super.saveTo(map)
        map.put("id", id)
        map.put("checksum", checksum)
        map.put("bundleId", bundleId)
        map.put("configSet", configSet)
        map.put("rootElemId", rootElemId)
        map.put("ruleBody", ruleBody)
        map.put("ruleType", ruleType.name())
        map.put("comment", comment)
        map.put("originType", originType.name())
        map.put("origin", origin)
        map.put("packageName", packageName)
        map.put("className", className)
        map.put("matchingMethods", matchingMethods)
        map.put("userIndex", userIndex)
    }
}
