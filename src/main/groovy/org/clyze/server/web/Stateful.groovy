package org.clyze.server.web

/**
 * An interface that models a stateful object or process (an item that has a state), supporting the transition
 * from one state to another.
 * Created by saiko on 1/12/2015.
 */
interface Stateful<S> {
	/**
	 * Get the current state.
	 * @return
	 */
	S getState()

	/**
	 * Performs the transition from the current state to the given newState, if this transition is supported,
	 * otherwise it throws an IllegalStateException.
	 * The supplied transition callback is executed only after a successful change of state.
	 *
	 * @param newState - the new state to change to
	 * @param callback - a void closure that accepts no arguments (ala Runnable.run())
	 */
	void changeStateTo(S newState, Closure callback) throws IllegalStateException
}
