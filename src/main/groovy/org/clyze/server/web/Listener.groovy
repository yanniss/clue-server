package org.clyze.server.web

import org.apache.log4j.Logger
import org.clyze.analysis.AnalysisFamilies
import org.clyze.analysis.AnalysisOption
import org.clyze.cclyzer.CClyzerAnalysisFamily
import org.clyze.doop.core.Doop
import org.clyze.doop.core.DoopAnalysisFamily
import org.clyze.server.web.persistent.store.DataStoreConnector
import org.clyze.server.web.persistent.store.DataStoreStrategy
import org.clyze.utils.Helper
import org.codehaus.groovy.runtime.StackTraceUtils

import javax.servlet.ServletContext
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.ServletException

import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * A servlet context listener providing the configuration entry point for the web app.
 *
 * @author: Kostas Saidis (saiko@di.uoa.gr)
 * Date: 3/9/2014
 */
class Listener implements ServletContextListener {

	@Override
	void contextInitialized(ServletContextEvent sce) {

		try {

			ServletContext sc = sce.getServletContext()

			String contextRealPath = sc.getRealPath("/")

			//Init doop
			String homePath = contextRealPath
			String outputPath = sc.getInitParameter("DOOP_OUT")
			String cachePath = sc.getInitParameter("DOOP_CACHE")
			String tmpPath = sc.getInitParameter("DOOP_TMP") ?: System.getProperty("java.io.tmpdir")
			Doop.initDoop(homePath, outputPath, cachePath, "$outputPath/logs" as String, tmpPath)
			Helper.initLogging("DEBUG", Doop.doopLog, true)

			//Init the analysis families
			if (!AnalysisFamilies.isRegistered('doop')) {
				AnalysisFamilies.register(DoopAnalysisFamily.instance)
			}
			if (!AnalysisFamilies.isRegistered('cclyzer')) {
				AnalysisFamilies.register(new CClyzerAnalysisFamily())
			}

			Config config = Config.instance
			Logger logger = Logger.getRootLogger()
			logger.debug "Doop initialized - Home: ${Doop.doopHome} - Out: ${Doop.doopOut} - Cache: ${Doop.doopCache} - Log: ${Doop.doopLog}"

			//set the web context path
			config.webContextPath = sc.getContextPath()

			//init the auth manager
			config.setupAuthManager sc.getInitParameter("org.clyze.server.web.auth.AuthManager")

			//init doop platforms lib
			String platformsLib = sc.getInitParameter("DOOP_PLATFORMS_LIB")
			if (!platformsLib) {
				platformsLib = System.getProperty("DOOP_PLATFORMS_LIB")
			}
			config.setupPlatformsLib platformsLib
			setDefaultValueOfAnalysisOption("doop", "PLATFORMS_LIB", config.platformsLib.canonicalPath)
			logger.debug "Option doop.PLATFORMS_LIB: ${config.platformsLib.canonicalPath}"

			//init upload handling components
			String uploadDir = sc.getInitParameter("upload")
			if (!uploadDir) {
				uploadDir = System.getProperty("java.io.tmpdir")
			}
			config.setupUpload uploadDir
			logger.debug "Option upload dir: ${config.uploadDir.canonicalPath}"

			//init data export directory
			String exportDir = sc.getInitParameter("CLUE_EXPORT")
			if (!exportDir) {
				exportDir = "$homePath/export"
			}
			config.setupExportDir exportDir
			logger.debug "Option export dir: ${config.exportDir.canonicalPath}"

			//init redex base directory
			String redexDir = sc.getInitParameter("REDEX_PATH")
			config.setupRedexDir redexDir
			logger.debug "Option redex dir: ${config.redexDir.canonicalPath}"

			String redexKeystorePath = sc.getInitParameter("REDEX_KEYSTORE_PATH")
			String redexKeystoreAlias = sc.getInitParameter("REDEX_KEYSTORE_KEYALIAS")
			String redexKeystorePass = sc.getInitParameter("REDEX_KEYSTORE_KEYPASS")
			config.setupRedexKeystore(redexKeystorePath, redexKeystoreAlias, redexKeystorePass)
			logger.debug "Option Redex keystore path: ${config.redexKeystorePath.canonicalPath}"
			logger.debug "Option Redex keystore alias: ${config.redexKeystoreAlias}"
			logger.debug "Option Redex keystore pass: ${config.redexKeystorePass}"

			String androidSdk = sc.getInitParameter("REDEX_ANDROID_SDK")
			config.setupRedexAndroidSdkDir androidSdk
			logger.debug "Option android sdk: ${config.redexAndroidSdkDir.canonicalPath}"

			config.isExportEnabled = Boolean.parseBoolean(sc.getInitParameter("isExportEnabled"))
			logger.debug "Option export enabled: ${config.isExportEnabled}"

			String bulkSize = sc.getInitParameter("bulk_size")
			config.bulkSize = toInteger(bulkSize, Config.DEFAULT_BULK_SIZE)
			logger.debug "Option bulk size: ${config.bulkSize}"

			String deletionBulkSize = sc.getInitParameter("deletion_bulk_size")
			config.deletionBulkSize = toInteger(deletionBulkSize, Config.DEFAULT_DELETION_BULK_SIZE)
			logger.debug "Option deletion bulk size: ${config.deletionBulkSize}"
			
			//init the datastore strategy
			String storeStrategyClass = sc.getInitParameter("org.clyze.server.web.persistent.store.DataStoreStrategy")
			config.storage = Class.forName(storeStrategyClass).newInstance() as DataStoreStrategy

			//init the analysis timeout
			String timeoutMinutes = sc.getInitParameter("timeout_minutes")
			config.timeoutInMinutes = toInteger(timeoutMinutes, Config.DEFAULT_TIMEOUT_MINUTES)
			logger.debug "Option timeout in minutes: ${config.timeoutInMinutes}"

			//init the main thread pool (for the analyses executor)
			String threadPoolSize = sc.getInitParameter("thread_pool_size")
			Integer tpSize = toInteger(threadPoolSize, Config.DEFAULT_THREAD_POOL_SIZE)
			logger.debug "Option thread pool size: $tpSize"
			config.setupExecutor tpSize

			//init the post-process thread pool
			threadPoolSize = sc.getInitParameter("post_process_thread_pool_size")
			Integer pptpSize = toInteger(threadPoolSize, Config.DEFAULT_POST_PROCESS_THREAD_POOL_SIZE)
			logger.debug "Option post processing thread pool size: $pptpSize"
			config.setupPostProcessExecutor pptpSize

			//init the ServerSideAnalysis cache of the datastore strategy
			String maxCacheSize = sc.getInitParameter("max_cache_size")
			Integer mcSize = toInteger(maxCacheSize, Config.DEFAULT_MAX_CACHE_SIZE)
			logger.debug "Option max cache size: $mcSize"			
			config.storage.setupCache config.timeoutInMinutes, mcSize

			//init the server side analysis post processors
			config.setupAnalysisPostProcessors sc.getInitParameter("analysis_post_processors")
			logger.debug "Option analysis post processors: \n${config.serverSideAnalysisPostProcessors.join('\n')}"

			//init the datastore connector			
			Properties datastoreConnectorProps = new Properties()
			String pathToDataStoreConnectorProps = sc.getInitParameter("datastore_connector_properties")
			new File("$contextRealPath/$pathToDataStoreConnectorProps").withReader { datastoreConnectorProps.load it }
			String storeConnectorClass = sc.getInitParameter("org.clyze.server.web.persistent.store.DataStoreConnector")
			logger.debug "Option datastore connector class: $storeConnectorClass"
			logger.debug datastoreConnectorProps.collect { k, v -> "Option datastore connector $k: $v" }.join("\n")
			DataStoreConnector connector = Class.forName(storeConnectorClass).invokeMethod("getInstance", null) as DataStoreConnector
			connector.configure(datastoreConnectorProps)
			config.storage.connector = connector	

			//startup the store machinery
			logger.debug "Starting up the datastore machinery"
			config.storage.startup()

			//init the ready-made queries			
			config.setupReadyMadeQueries sc.getInitParameter("ready_made_queries")
			logger.debug """Option ready made queries: \n${config.readyMadeQueries.collect{ it.id }.join('\n')}"""

			logger.info("##############")
			logger.info("server started")
			logger.info("##############")            

		} catch (e) {
			e = StackTraceUtils.deepSanitize e
			System.err.println("Error: ${e.getMessage()}")
			e.printStackTrace(System.err)
			throw new ServletException(e.getMessage(), e)
		}
	}

	@Override
	void contextDestroyed(ServletContextEvent sce) {	

		Logger logger = Logger.getRootLogger()	

		logger.debug "Stopping the server"

		Config.instance.with {
			logger.debug "Stopping analysis executor"
			executor.shutdownNow()
			logger.debug "Stopping post-processing executor"
			postProcessExecutor.shutdownNow()
		}
	
		//Don't wait indefinitely for a graceful elastic client shutdown
		int timeout = 90 //in seconds
		def executorService = Executors.newSingleThreadExecutor()
		try {
			executorService.submit(new Runnable() {
				@Override
				void run() {					
					logger.debug "Stopping elastic client"
					Config.instance.storage?.shutdown()							
				}
			}).get(timeout, TimeUnit.SECONDS)
		} catch (TimeoutException te) {
			log.error "Timeout has expired ($timeout min)."			
		}
		finally {			
			logger.debug "Stopping elastic client (force)"
			executorService.shutdownNow()			
		}
	
		logger.info("##############")
		logger.info("server stopped")
		logger.info("##############")            
	}

	private static int toInteger(String value, int defaultValue) {
		try {
			return Integer.parseInt(value)
		} catch (all) {
			Logger.getRootLogger().warn("Using default value: $defaultValue instead of: $value")
			return defaultValue
		}
	}

	private static void setDefaultValueOfAnalysisOption(String family, String optionId, def value) throws RuntimeException {
		AnalysisOption option = AnalysisFamilies.supportedOptionsOf(family).find { it.id == optionId }
		if (option) {
			option.value = value
		} else {
			throw new RuntimeException("No such analysis option: $optionId in family: $family")
		}
	}
}
