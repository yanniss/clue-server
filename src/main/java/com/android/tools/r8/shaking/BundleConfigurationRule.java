package com.android.tools.r8.shaking;

import com.android.tools.r8.origin.Origin;
import com.android.tools.r8.position.Position;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.clyze.persistent.model.doop.Method;
import org.clyze.server.web.bundle.BundleConfiguration;
import org.clyze.utils.CheckSum;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class BundleConfigurationRule extends ProguardConfigurationRule {

    private static final Log log = LogFactory.getLog(ProguardConfigurationRule.class);

    private final String id;
    protected final Type type;

    //TODO: Support proguard keep modifiers

    public BundleConfigurationRule(
            Origin origin,
            Position position,
            String source,
            ProguardTypeMatcher classAnnotation,
            ProguardAccessFlags classAccessFlags,
            ProguardAccessFlags negatedClassAccessFlags,
            boolean classTypeNegated,
            ProguardClassType classType,
            ProguardClassNameList classNames,
            ProguardTypeMatcher inheritanceAnnotation,
            ProguardTypeMatcher inheritanceClassName,
            boolean inheritanceIsExtends,
            List<ProguardMemberRule> memberRules,
            Type type) {
        super(origin, position, source, classAnnotation, classAccessFlags, negatedClassAccessFlags,
                classTypeNegated, classType, classNames, inheritanceAnnotation, inheritanceClassName,
                inheritanceIsExtends, memberRules);
        this.type = type;
        this.id = checksumFor(source);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static String checksumFor(String ruleSource) {
        return CheckSum.checksum(ruleSource, "SHA1");
    }

    public Type getType() { return type; }

    public String getId() { return id; }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BundleConfigurationRule)) {
            return false;
        }
        BundleConfigurationRule other = (BundleConfigurationRule) o;
        if (this.type != other.type) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        return 3 * result + type.hashCode();
    }

    @Override
    protected StringBuilder append(StringBuilder builder, boolean includeMemberRules) {
        builder.append(type.toString());
        builder.append(' ');
        super.append(builder, includeMemberRules);
        return builder;
    }

    @Override
    String typeString() {
        return type.toString();
    }

    @Override
    public String toShortString() {
        return append(new StringBuilder(), false).toString();
    }

    @Override
    public String toString() {
        //return append(new StringBuilder(), true).toString();
        return getSource();
    }

    public static BundleConfigurationRule copyFrom(ProguardKeepRule keepRule) {
        //TODO We transform the rule's source here but we don't update its position (most probably, it will not be used)
        String src = keepRule.getSource(); //all proguard keep rules start with a -
        String newSrc = src.startsWith("-") ? src.substring(1) : src;
        log.debug("Copying rule: " + keepRule.getSource() + " -> " + newSrc);
        return new BundleConfigurationRule(
            keepRule.getOrigin(),
            keepRule.getPosition(),
            newSrc,
            keepRule.getClassAnnotation(),
            keepRule.getClassAccessFlags(),
            keepRule.getNegatedClassAccessFlags(),
            keepRule.getClassTypeNegated(),
            keepRule.getClassType(),
            keepRule.getClassNames(),
            keepRule.getInheritanceAnnotation(),
            keepRule.getInheritanceClassName(),
            keepRule.getInheritanceIsExtends(),
            keepRule.getMemberRules(),
            Type.fromProguardKeepRuleType(keepRule.getType())
        );
    }

    public static final boolean isSupported(ProguardConfigurationRule rule) {
        // Currently, only proguard keep rules are supported (with the if rule exception)
        if (rule instanceof ProguardKeepRule) {
            ProguardKeepRule keepRule = (ProguardKeepRule) rule;
            ProguardKeepRuleType keepRuleType = keepRule.getType();
            return (keepRuleType != ProguardKeepRuleType.CONDITIONAL);
        }

        return false;
    }

    /**
     * Returns the methods that match the given rule.
     */
    public interface MethodMatcher {

        Set<Method> matchingMethods(BundleConfigurationRule rule);

    }

    public static class Builder extends ProguardConfigurationRule.Builder<BundleConfigurationRule, Builder> {

        protected Type type;

        protected Builder() {
            super();
        }

        public BundleConfigurationRule.Builder setType(Type type) {
            this.type = type;
            return this;
        }

        @Override
        public BundleConfigurationRule.Builder self() {
            return this;
        }

        public BundleConfigurationRule.Builder copyFrom(ProguardKeepRule.Builder keepBuilder) {
            this.setClassAccessFlags(keepBuilder.getClassAccessFlags());
            this.setNegatedClassAccessFlags(keepBuilder.getNegatedClassAccessFlags());
            this.setClassAnnotation(keepBuilder.getClassAnnotation());
            this.setClassNames(keepBuilder.getClassNames());
            this.setClassType(keepBuilder.getClassType());
            this.setClassTypeNegated(keepBuilder.getClassTypeNegated());
            this.setInheritanceAnnotation(keepBuilder.getInheritanceAnnotation());
            this.setInheritanceClassName(keepBuilder.getInheritanceClassName());
            this.setInheritanceIsExtends(keepBuilder.getInheritanceIsExtends());
            this.setMemberRules(keepBuilder.getMemberRules());
            return this;
        }

        @Override
        public BundleConfigurationRule build() {
            return new BundleConfigurationRule(origin, getPosition(), source, classAnnotation, classAccessFlags,
                    negatedClassAccessFlags, classTypeNegated, classType, classNames, inheritanceAnnotation,
                    inheritanceClassName, inheritanceIsExtends, memberRules, type);
        }
    }

    public enum Type {

        KEEP("keep", EnumSet.of(BundleConfiguration.Step.SHRINK, BundleConfiguration.Step.OBFUSCATE)),
        KEEP_CLASS_MEMBERS("keepclassmembers", EnumSet.of(BundleConfiguration.Step.SHRINK, BundleConfiguration.Step.OBFUSCATE)),
        KEEP_CLASSES_WITH_MEMBERS("keepclasseswithmembers", EnumSet.of(BundleConfiguration.Step.SHRINK, BundleConfiguration.Step.OBFUSCATE)),
        KEEP_NAMES("keepnames", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        KEEP_CLASS_MEMBER_NAMES("keepclassmembernames", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        KEEP_CLASSES_WITH_MEMBER_NAMES("keepclasseswithmembernames", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        // Should we also support the following?
        // KEEP_PACKAGE_NAMES("keeppackagenames", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        // KEEP_ATTRIBUTES("keepattributes", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        // KEEP_PARAMETER_NAMES("keepparameternames", EnumSet.of(BundleConfiguration.Step.OBFUSCATE)),
        REMOVE("remove", EnumSet.of(BundleConfiguration.Step.SHRINK)),
        FORCE_REMOVE("forceremove", EnumSet.of(BundleConfiguration.Step.SHRINK));

        private final String typeString;
        private final EnumSet<BundleConfiguration.Step> appliesToSteps;

        Type(String text, EnumSet<BundleConfiguration.Step> appliesToSteps) {
            this.typeString = text;
            this.appliesToSteps = appliesToSteps;
        }

        @Override
        public String toString() {
            return typeString;
        }

        public EnumSet<BundleConfiguration.Step> stepsAppliedTo() {
            return appliesToSteps;
        }

        public boolean isAppliedToStep(BundleConfiguration.Step step) {
            return appliesToSteps.contains(step);
        }

        public boolean isKeep() {
            return (this == KEEP || this == KEEP_CLASS_MEMBERS || this == KEEP_CLASSES_WITH_MEMBERS ||
                    this == KEEP_NAMES || this == KEEP_CLASS_MEMBER_NAMES || this == KEEP_CLASSES_WITH_MEMBER_NAMES);
        }

        public static Type fromProguardKeepRuleType(ProguardKeepRuleType type) {
            if (type == ProguardKeepRuleType.KEEP) {
                return KEEP;
            }

            if (type == ProguardKeepRuleType.KEEP_CLASS_MEMBERS) {
                return KEEP_CLASS_MEMBERS;
            }

            if (type == ProguardKeepRuleType.KEEP_CLASSES_WITH_MEMBERS) {
                return KEEP_CLASSES_WITH_MEMBERS;
            }

            throw new RuntimeException("Invalid Proguard keep rule type: " + type);
        }
    }
}
