package com.android.tools.r8.shaking;

import com.android.tools.r8.graph.DexField;
import com.android.tools.r8.graph.DexItemFactory;
import com.android.tools.r8.utils.LongInterval;
import com.android.tools.r8.utils.Reporter;
import com.android.tools.r8.utils.StringDiagnostic;
import com.google.common.collect.ImmutableList;
import org.clyze.server.web.bundle.ClueRuleParser;

import java.util.ArrayList;
import java.util.List;

public class ClueRuleParserHelper {

    public static ProguardConfigurationParser.IdentifierPatternWithWildcards withoutWildcards(String pattern) {
        return new ProguardConfigurationParser.IdentifierPatternWithWildcards(pattern, ImmutableList.of());
    }

    public static Iterable<ProguardWildcard> getWildcards(ProguardIfRule ifRule) {
        return ifRule.getWildcards();
    }

    public static void verifyAndLinkBackReferences(Iterable<ProguardWildcard> wildcards, ClueRuleParser parser) {
        List<ProguardWildcard.Pattern> patterns = new ArrayList<>();
        boolean backReferenceStarted = false;
        for (ProguardWildcard wildcard : wildcards) {
            if (wildcard.isBackReference()) {
                backReferenceStarted = true;
                ProguardWildcard.BackReference backReference = wildcard.asBackReference();
                if (patterns.size() < backReference.referenceIndex) {
                    throw parser.getReporter().fatalError(new StringDiagnostic(
                            "Wildcard <" + backReference.referenceIndex + "> is invalid "
                                    + "(only seen " + patterns.size() + " at this point).",
                            parser.getOrigin(), parser.getPosition()));
                }
                backReference.setReference(patterns.get(backReference.referenceIndex - 1));
            } else {
                assert wildcard.isPattern();
                if (!backReferenceStarted) {
                    patterns.add(wildcard.asPattern());
                }
            }
        }
    }

    public static ProguardWildcard newProguardWildcardBackReference(int backreference) {
        return new ProguardWildcard.BackReference(backreference);
    }

    public static ProguardWildcard newProguardWildcardPattern(String pattern) {
        return new ProguardWildcard.Pattern(pattern);
    }

    public static ProguardTypeMatcher newClassProguardTypeMatcher(String pattern, List<ProguardWildcard> wildcards, DexItemFactory factory) {
        return ProguardTypeMatcher.create(
            new ProguardConfigurationParser.IdentifierPatternWithWildcards(pattern, wildcards),
            ProguardTypeMatcher.ClassOrType.CLASS,
            factory
        );
    }

    public static ProguardTypeMatcher newTypeProguardTypeMatcher(String pattern, List<ProguardWildcard> wildcards, DexItemFactory factory) {
        return ProguardTypeMatcher.create(
            new ProguardConfigurationParser.IdentifierPatternWithWildcards(pattern, wildcards),
            ProguardTypeMatcher.ClassOrType.TYPE,
            factory
        );
    }

    public static void setMemberRuleName(ProguardMemberRule.Builder ruleBuilder, String pattern, List<ProguardWildcard> wildcards) {
        ruleBuilder.setName(new ProguardConfigurationParser.IdentifierPatternWithWildcards(pattern, wildcards));
    }

    public static ProguardMemberRuleReturnValue newProguardMemberRuleReturnValue(boolean value) {
        return new ProguardMemberRuleReturnValue(value);
    }

    public static ProguardMemberRuleReturnValue newProguardMemberRuleReturnValue() {
        return new ProguardMemberRuleReturnValue();
    }

    public static ProguardMemberRuleReturnValue newProguardMemberRuleReturnValue(LongInterval interval) {
        return new ProguardMemberRuleReturnValue(interval);
    }

    public static ProguardMemberRuleReturnValue newProguardMemberRuleReturnValue(DexField field) {
        return new ProguardMemberRuleReturnValue(field);
    }

    public static ProguardClassNameList build(ProguardClassNameList.Builder builder) {
        return builder.build();
    }

    public static ProguardPathList build(ProguardPathList.Builder builder) {
        return builder.build();
    }

    public static void validate(ProguardConfiguration.Builder configurationBuilder, Reporter reporter) {
        if (configurationBuilder.isKeepParameterNames() && configurationBuilder.isObfuscating()) {
            // The flag -keepparameternames has only effect when minifying, so ignore it if we
            // are not.
            throw reporter.fatalError(new StringDiagnostic(
                    "-keepparameternames is not supported",
                    configurationBuilder.getKeepParameterNamesOptionOrigin(),
                    configurationBuilder.getKeepParameterNamesOptionPosition()));
        }
        if (configurationBuilder.isOverloadAggressively()
                && configurationBuilder.isUseUniqueClassMemberNames()) {
            reporter.warning(
                    new StringDiagnostic(
                            "The -overloadaggressively flag has no effect if -useuniqueclassmembernames"
                                    + " is also specified."));
        }
    }
}
