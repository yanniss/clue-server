package org.clyze.server.web.bundle;

import com.android.tools.r8.graph.AccessFlags;
import com.android.tools.r8.graph.ClassAccessFlags;
import com.android.tools.r8.graph.DexType;
import com.android.tools.r8.graph.MethodAccessFlags;
import com.android.tools.r8.shaking.*;
import com.android.tools.r8.utils.DescriptorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.clyze.persistent.model.doop.Class;
import org.clyze.persistent.model.doop.Method;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class R8BundleConfigurationRuleMatcher implements BundleConfigurationRule.MethodMatcher {

    private static final Log log = LogFactory.getLog(R8BundleConfigurationRuleMatcher.class);

    protected final BundleConfigurationContext proguardContext;

    public R8BundleConfigurationRuleMatcher(BundleConfigurationContext proguardContext) {
        this.proguardContext = proguardContext;
    }

    //The matches methods that follow mimic the behavior of R8.
    // Start with the following method:
    // com.android.tools.r8.shaking.RootSetBuilder.process(DexClass, ProguardConfigurationRule, ProguardIfRule)
    public static boolean matchesClassRule(BundleConfigurationContext proguardContext, BundleConfigurationRule rule, Class klass) {
        if (!satisfyClassType(rule, klass)) {
            //log.debug("satisfyClassType FALSE for class: " + klass.getDoopId() + " - rule: " + rule.getClassType());
            return false;
        }

        if (!satisfyAccessFlag(rule, klass)) {
            //log.debug("satisfyAccessFlag FALSE for class: " + klass.getDoopId() + " - rule: " + rule.getClassAccessFlags());
            return false;
        }

        if (!satisfyAnnotation(proguardContext, rule, klass)) {
            return false;
        }

        if (rule.hasInheritanceClassName() && !satisfyInheritanceRule(proguardContext, rule, klass)) {
            //log.debug("satisfyInheritanceRule FALSE for class: " + klass.getDoopId() + " - rule: " + rule.getInheritanceClassName());
            return false;
        }

        DexType dexType = toDexType(proguardContext, klass.getDoopId());

        /*
        boolean matched = rule.getClassNames().matches(dexType);
        log.debug("getClassNames matches: [" + matched + "] - class: " + klass.getDoopId() + " - rule: " + rule.getClassNames());
        return matched;
        */

        return rule.getClassNames().matches(dexType);
    }

    public static boolean matchesMemberRule(BundleConfigurationContext proguardContext, ProguardMemberRule memberRule, Method method) {

        switch(memberRule.getRuleType()) {
            case ALL_METHODS:
                if ("<clinit>".equals(method.getName())) {
                    break;
                }
            case ALL:
                AccessFlags methodFlags = accessFlagsOfMethod(method);
                if (!memberRule.getAccessFlags().containsAll(methodFlags) ||
                        !memberRule.getNegatedAccessFlags().containsNone(methodFlags)) {
                    //log.debug("FALSE (ALL access flags) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    break;
                }

                return containsAnnotation(proguardContext, memberRule.getAnnotation(), method.getAnnotationTypes());
            case METHOD:
                DexType dexType = toDexType(proguardContext, method.getReturnType());
                if (!memberRule.getType().matches(dexType)) {
                    //log.debug("FALSE (type) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    break;
                }
            case CONSTRUCTOR:
            case INIT:
                //name check
                if (!memberRule.getName().matches(method.getName())) {
                    //log.debug("FALSE (name) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    break;
                }

                //access flags check
                AccessFlags mFlags = accessFlagsOfMethod(method);
                if (!memberRule.getAccessFlags().containsAll(mFlags) ||
                        !memberRule.getNegatedAccessFlags().containsNone(mFlags)) {
                    //log.debug("FALSE (access flags) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    break;
                }

                if (!containsAnnotation(proguardContext, memberRule.getAnnotation(), method.getAnnotationTypes())) {
                    break;
                }

                //param types check
                List<ProguardTypeMatcher> arguments = memberRule.getArguments();
                if (arguments.size() == 1 && arguments.get(0).isTripleDotPattern()) {
                    //log.debug("TRUE (triple dot) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    return true;
                }
                DexType[] parameters = Arrays.
                        stream(method.getParamTypes()).
                        map((s) -> toDexType(proguardContext, s)).
                        collect(Collectors.toList()).
                        toArray(new DexType[method.getParamTypes().length]);
                if (parameters.length != arguments.size()) {
                    //log.debug("FALSE (params/args sizes differ) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                    break;
                }
                for (int i = 0; i < parameters.length; i++) {
                    if (!arguments.get(i).matches(parameters[i])) {
                        //log.debug("FALSE (param/arg types differ) method: " + method.getDoopId() + " matches rule: " + memberRule.toString());
                        return false;
                    }
                }
                // All parameters matched.
                //log.debug("match: [true] - constructor/init: " + method.getDoopId() + " - rule: " + memberRule.toString());
                return true;
            case ALL_FIELDS:
            case FIELD:
                break;
        }
        return false;
    }

    private static boolean satisfyClassType(ProguardConfigurationRule rule, Class klass) {
        return classTypeMatches(rule.getClassType(), klass) != rule.getClassTypeNegated();
    }

    private static boolean satisfyAccessFlag(ProguardConfigurationRule rule, Class klass) {
        AccessFlags accessFlags = accessFlagsOfClass(klass);
        return rule.getClassAccessFlags().containsAll(accessFlags) && rule.getNegatedClassAccessFlags().containsNone(accessFlags);
    }

    private static boolean satisfyAnnotation(BundleConfigurationContext ctx, ProguardConfigurationRule rule, Class klass) {
        return containsAnnotation(ctx, rule.getClassAnnotation(), klass.getAnnotationTypes());
    }

    private static boolean containsAnnotation(BundleConfigurationContext ctx, ProguardTypeMatcher annotation, Set<String> annotationTypes) {
        if (annotation == null) {
            return true;
        }

        if (annotationTypes.isEmpty()) {
            return false;
        }

        for (String annotationType : annotationTypes) {
            if (annotation.matches(toDexType(ctx, annotationType))) {
                return true;
            }
        }
        return false;
    }

    private static boolean satisfyInheritanceRule(BundleConfigurationContext proguardContext, ProguardConfigurationRule rule, Class klass) {
        //NOTE: We treat extends / implements rules in the same manner
        ProguardTypeMatcher inheritanceMatcher = rule.getInheritanceClassName();
        //log.debug("satisfy inheritance rule (" + inheritanceMatcher + ") for class? " + klass.getDoopId());
        for (String superType : klass.getSuperTypes()) { //superTypes includes self
            DexType dexType = toDexType(proguardContext, superType);
            if (inheritanceMatcher.matches(dexType)) {
                //log.debug("--- " + superType + " matches: TRUE");
                //log.debug("TRUE satisfy inheritance rule (" + inheritanceMatcher + ") for class: " + klass.getDoopId());
                return true;
            }
            else {
                //log.debug("--- " + superType + " matches: FALSE");
            }
        }
        //log.debug("FALSE satisfy inheritance rule (" + inheritanceMatcher + ") for class: " + klass.getDoopId());
        return false;
    }

    private static boolean classTypeMatches(ProguardClassType classType, Class klass) {
        switch (classType) {
            case ANNOTATION_INTERFACE:
                //TODO: Correct code is return klass.isInterface() && klass.isAnnotation()
                return false;
            case CLASS:
                //TODO: Correct code is return !klass.isInterface() && !klass.isEnum() && !klass.isAnnotation()
                return !klass.isInterface() && !klass.isEnum();
            case ENUM:
                return klass.isEnum();
            case INTERFACE:
                //TODO: Correct code is return klass.isInterface() && !klass.isAnnotation()
                return klass.isInterface();
            //case UNSPECIFIED:
            default:
                return true;
        }
    }

    private static AccessFlags accessFlagsOfClass(Class klass) {
        ClassAccessFlags accessFlags = ClassAccessFlags.fromSharedAccessFlags(0);
        if (klass.isPublic()) {
            accessFlags.setPublic();
        }
        else if (klass.isProtected()) {
            accessFlags.setProtected();
        }
        else if (klass.isPrivate()) {
            accessFlags.setPrivate();
        }
        else if(klass.isStatic()) {
            accessFlags.setStatic();
        }
        else if(klass.isFinal()) {
            accessFlags.setFinal();
        }
        //else if(klass.isSynthetic()) {
        //    TODO: accessFlags.setSynthetic()
        //}
        return accessFlags;
    }

    private static AccessFlags accessFlagsOfMethod(Method method) {
        MethodAccessFlags accessFlags = MethodAccessFlags.fromSharedAccessFlags(0, "<init>".equals(method.getName()));
        if(method.isStatic()) {
            accessFlags.setStatic();
        }
        else if (method.isAbstract()) {
            accessFlags.setAbstract();
        }
        else if (method.isNative()) {
            accessFlags.setNative();
        }
        else if (method.isSynchronized()) {
            accessFlags.setSynchronized();
        }
        else if(method.isFinal()) {
            accessFlags.setFinal();
        }
        else if(method.isSynthetic()) {
            accessFlags.setSynthetic();
        }
        else if (method.isPublic()) {
            accessFlags.setPublic();
        }
        else if (method.isProtected()) {
            accessFlags.setProtected();
        }
        else if (method.isPrivate()) {
            accessFlags.setPrivate();
        }
        //TODO: strictfp
        return accessFlags;
    }

    private static DexType toDexType(BundleConfigurationContext ctx, String type) {
        return ctx.getFactory().createType(DescriptorUtils.javaTypeToDescriptor(type));
    }
}
