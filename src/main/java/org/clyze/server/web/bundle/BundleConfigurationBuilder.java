package org.clyze.server.web.bundle;

import com.android.tools.r8.origin.Origin;
import com.android.tools.r8.position.Position;
import com.android.tools.r8.shaking.*;
import com.android.tools.r8.utils.InternalOptions;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class BundleConfigurationBuilder {

    private static final Log log = LogFactory.getLog(BundleConfigurationBuilder.class);

    private final ProguardConfiguration.Builder proBuilder;
    private final List<BundleConfigurationRule> clueRules = new ArrayList<>();
    private final Predicate<ProguardConfigurationRule> isProguardRuleSupported;

    public BundleConfigurationBuilder(Predicate<ProguardConfigurationRule> isProguardRuleSupported, ProguardConfiguration.Builder proBuilder) {
        this.isProguardRuleSupported = isProguardRuleSupported;
        this.proBuilder = proBuilder;
    }

    ProguardConfiguration.Builder getUnderlyingBuilder() {
        return proBuilder;
    }

    public void addParsedConfiguration(String source) {
        //do nothing
        //proBuilder.addParsedConfiguration(source);
    }

    public void addInjars(List<FilteredClassPath> injars) {
        proBuilder.addInjars(injars);
    }

    public void addLibraryJars(List<FilteredClassPath> libraryJars) {
        proBuilder.addLibraryJars(libraryJars);
    }

    public InternalOptions.PackageObfuscationMode getPackageObfuscationMode() { return proBuilder.getPackageObfuscationMode(); }

    public void setPackagePrefix(String packagePrefix) { proBuilder.setPackagePrefix(packagePrefix); }

    public void setFlattenPackagePrefix(String packagePrefix) { proBuilder.setFlattenPackagePrefix(packagePrefix); }

    public void setAllowAccessModification(boolean allowAccessModification) { proBuilder.setAllowAccessModification(allowAccessModification); }

    public void setIgnoreWarnings(boolean ignoreWarnings) { proBuilder.setIgnoreWarnings(ignoreWarnings); }

    public void disableOptimization() { proBuilder.disableOptimization(); }

    public void disableObfuscation() { proBuilder.disableObfuscation(); }

    public boolean isOptimizing() { return proBuilder.isOptimizing(); }

    public boolean isShrinking() { return proBuilder.isShrinking(); }

    public void disableShrinking() { proBuilder.disableShrinking(); }

    public void setPrintConfiguration(boolean printConfiguration) { proBuilder.setPrintConfiguration(printConfiguration); }

    public void setPrintConfigurationFile(Path file) { proBuilder.setPrintConfigurationFile(file); }

    public void setPrintUsage(boolean printUsage) { proBuilder.setPrintUsage(printUsage); }

    public void setPrintUsageFile(Path printUsageFile) { proBuilder.setPrintUsageFile(printUsageFile); }

    public void setPrintMapping(boolean printMapping) { proBuilder.setPrintMapping(printMapping); }

    public void setPrintMappingFile(Path file) { proBuilder.setPrintUsageFile(file); }

    public void setApplyMappingFile(Path file) { proBuilder.setApplyMappingFile(file); }

    public void setVerbose(boolean verbose) { proBuilder.setVerbose(verbose); }

    public void setRenameSourceFileAttribute(String renameSourceFileAttribute) { proBuilder.setRenameSourceFileAttribute(renameSourceFileAttribute); }

    public void addKeepAttributePatterns(List<String> keepAttributePatterns) { proBuilder.addKeepAttributePatterns(keepAttributePatterns); }

    public void addRule(ProguardConfigurationRule rule) {
        log.debug("Adding pro rule: " + rule.getSource());
        if (isProguardRuleSupported.test(rule)) {
            proBuilder.addRule(rule);
        }
        else {
            throw new RuntimeException("Rule not supported: " + rule);
        }
    }

    public void addRule(BundleConfigurationRule rule) {
        clueRules.add(rule);
    }

    public void addDontWarnPattern(ProguardClassNameList pattern) { proBuilder.addDontWarnPattern(pattern); }

    public void addDontNotePattern(ProguardClassNameList pattern) { proBuilder.addDontNotePattern(pattern); }

    public void setSeedFile(Path seedFile) { proBuilder.setSeedFile(seedFile); }

    public void setPrintSeeds(boolean printSeeds) { proBuilder.setPrintSeeds(printSeeds); }

    public void setObfuscationDictionary(Path obfuscationDictionary) { proBuilder.setObfuscationDictionary(obfuscationDictionary); }

    public void setClassObfuscationDictionary(Path classObfuscationDictionary) { proBuilder.setClassObfuscationDictionary(classObfuscationDictionary); }

    public void setPackageObfuscationDictionary(Path packageObfuscationDictionary) { proBuilder.setPackageObfuscationDictionary(packageObfuscationDictionary); }

    public void setUseUniqueClassMemberNames(boolean useUniqueClassMemberNames) { proBuilder.setUseUniqueClassMemberNames(useUniqueClassMemberNames); }

    public void setKeepParameterNames(boolean keepParameterNames, Origin optionOrigin, Position optionPosition) {
        proBuilder.setKeepParameterNames(keepParameterNames, optionOrigin, optionPosition);
    }

    public void addAdaptClassStringsPattern(ProguardClassNameList pattern) { proBuilder.addAdaptClassStringsPattern(pattern); }

    public void enableAdaptResourceFilenames() { proBuilder.enableAdaptResourceFilenames(); }

    public void addAdaptResourceFilenames(ProguardPathList pattern) { proBuilder.addAdaptResourceFilenames(pattern); }

    public void enableAdaptResourceFileContents() { proBuilder.enableAdaptResourceFileContents(); }

    public void addAdaptResourceFileContents(ProguardPathList pattern) { proBuilder.addAdaptResourceFileContents(pattern); }

    public void enableKeepDirectories() { proBuilder.enableKeepDirectories(); }

    public void addKeepDirectories(ProguardPathList pattern) { proBuilder.addKeepDirectories(pattern); }

    public void setForceProguardCompatibility(boolean forceProguardCompatibility) { proBuilder.setForceProguardCompatibility(forceProguardCompatibility); }

    public void setOverloadAggressively(boolean overloadAggressively) { proBuilder.setOverloadAggressively(overloadAggressively); }

    public ProguardConfiguration forEachSupportedProguardRule(Consumer<ProguardKeepRule> consumer) {
        ProguardConfiguration proConfig = proBuilder.build();
        if (!hasDefaultKeepAllRuleOnly(proConfig)) {
            for (ProguardConfigurationRule r : proConfig.getRules()) {
                if(isProguardRuleSupported.test(r)) { //only proguard keep rules are currently supported
                    consumer.accept((ProguardKeepRule) r);
                }
            }
        }
        return proConfig;
    }

    //this is used only for testing/debugging
    public BundleConfiguration buildTo(BundleConfiguration existing) {
        buildClueRules().forEach( existing::addOrUpdateRule );
        return existing;
    }

    //this is used only for testing/debugging
    public BundleConfiguration build() {
        return buildTo(new BundleConfiguration());
    }

    public List<BundleConfigurationRule> buildClueRules() {
        List<BundleConfigurationRule> rules = new ArrayList<>();

        //build proguard config and get its rules
        forEachSupportedProguardRule( (r) -> rules.add(BundleConfigurationRule.copyFrom(r)) );

        //get all clue rules
        rules.addAll(clueRules);

        return rules;
    }

    // R8 adds a default keep all rule in empty proguard configs -- see method
    // com.android.tools.r8.shaking.ProguardConfiguration.build()
    private static boolean hasDefaultKeepAllRuleOnly(ProguardConfiguration proConfig) {
        List<ProguardConfigurationRule> rules = proConfig.getRules();
        return (
            (!proConfig.isShrinking() || !proConfig.isObfuscating() || !proConfig.isOptimizing())
            && rules.size() == 1
        );
    }
}
