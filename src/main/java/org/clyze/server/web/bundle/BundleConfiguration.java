package org.clyze.server.web.bundle;

import com.android.tools.r8.shaking.BundleConfigurationRule;
import com.android.tools.r8.shaking.ProguardConfigurationRule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.clyze.persistent.model.ItemImpl;
import org.clyze.server.web.persistent.store.Dates;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;



/* A bundle optimization configuration set (config set) */
public class BundleConfiguration extends ItemImpl {

    public enum Step {
        SHRINK,
        OBFUSCATE,
        OPTIMIZE
    }

    private static final Log log = LogFactory.getLog(BundleConfiguration.class);
    private static final char[] EXTRA_NAME_CHARACTERS = new char[] {'_', '-', '+'};

    private String bundleId;
    private String nameOfSet;
    private Date   created;
    private Date   modified;
    private long   ruleIndex = 0;
    private String currentAnalysisId = null;
    private boolean dirty;
    private Map<String, BundleConfigurationRule> rules = new LinkedHashMap<>(); //this is used only for testing/debugging
    private final EnumSet<Step> enabledSteps = EnumSet.noneOf(Step.class);

    public BundleConfiguration() {
        //constructor used when loading the object from elastic
    }

    public BundleConfiguration(String bundleId, String nameOfSet, Date created, Date modified) {
        // constructor used when creating a new empty bundle (before saving it to elastic)
        this.id       = UUID.randomUUID().toString();
        this.bundleId = bundleId;
        this.nameOfSet= nameOfSet;
        this.created  = created;
        this.modified = modified;
        this.dirty    = true;
    }

    //this is used only for testing/debugging
    public BundleConfigurationRule getRuleById(String id) {
        return rules.get(id);
    }

    //this is used only for testing/debugging
    public Collection<BundleConfigurationRule> getRules(){
        return Collections.unmodifiableCollection(rules.values());
    }

    //this is used only for testing/debugging
    public void addOrUpdateRule(BundleConfigurationRule rule) {
        this.rules.put(rule.getId(), rule);
    }

    //this is used only for testing/debugging
    public void removeRule(BundleConfigurationRule rule) {
        this.rules.remove(rule.getId());
    }

    public String getBundleId() {
        return bundleId;
    }

    public String getNameOfSet() {
        return nameOfSet;
    }

    public void setNameOfSet(String newName) {
        nameOfSet = newName;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void touch() { modified = new Date(); }

    public long getRuleIndex() { return ruleIndex; }

    public void setRuleIndex(long ruleIndex) { this.ruleIndex = ruleIndex; }

    public String getCurrentAnalysisId() { return currentAnalysisId; }

    public void setCurrentAnalysisId(String currentAnalysisId) {
        this.currentAnalysisId = currentAnalysisId;
    }

    public boolean isDirty() { return dirty; }

    public void setDirty(boolean dirty) { this.dirty = dirty; }

    public void enableStep(Step step) {
        enabledSteps.add(step);
    }

    public void disableStep(Step step) {
        enabledSteps.remove(step);
    }

    public boolean isStepEnabled(Step step) {
        return enabledSteps.contains(step);
    }

    public Set<Step> enabledSteps() {
        return Collections.unmodifiableSet(enabledSteps);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof BundleConfiguration)) return false;
        return Objects.equals(id, ((BundleConfiguration)object).id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    protected void loadFrom(Map<String, Object> map) {

        this.id           = (String) map.get("id");
        this.bundleId     = (String) map.get("bundleId");
        this.nameOfSet    = (String) map.get("nameOfSet");
        this.created      = Dates.parse((String) map.get("created"));
        this.modified     = Dates.parse((String) map.get("modified"));
        this.ruleIndex    = ((Number) map.get("ruleIndex")).longValue();
        this.dirty        = (Boolean) map.get("dirty");
        this.currentAnalysisId = (String) map.get("currentAnalysisId");
        readEnabledSteps((String) map.get("enabledSteps"));
    }

    private void readEnabledSteps(String commaSeparatedSteps) {
        if (commaSeparatedSteps == null) {
            return;
        }

        String[] steps = commaSeparatedSteps.split(", ");
        for (String step : steps) {
            String trimmed = step.trim();
            if (trimmed.length() > 0) {
                try {
                    enableStep(Step.valueOf(trimmed));
                } catch (Exception e) {
                    log.warn("Unknown step: " + step);
                }
            }
        }
    }

    @Override
    protected void saveTo(Map<String, Object> map) {
        map.put("id", id);
        map.put("bundleId", bundleId);
        map.put("nameOfSet", nameOfSet);
        map.put("created", Dates.format(created));
        map.put("modified", Dates.format(modified));
        map.put("ruleIndex", ruleIndex);
        map.put("dirty", dirty);
        map.put("currentAnalysisId", currentAnalysisId);
        map.put("enabledSteps", enabledSteps.stream().map(Step::name).collect(Collectors.joining(", ")));
    }

    public static String validateUserSuppliedName(String name) {

        String suffix =".clue";

        if (name == null) {
            throw new RuntimeException("Invalid name: " + name);
        }

        String trimmed = name.trim();

        if (!trimmed.endsWith(suffix))
            trimmed = trimmed + suffix;

        String prefix = trimmed.substring(0, trimmed.length() - suffix.length());

        for (int i = 0, len = prefix.length(); i < len; i ++) {
            char c = prefix.charAt(i);
            if (!Character.isLetter(c) && !Character.isDigit(c) && !isExtraNameChar(c))
                throw new RuntimeException("Invalid name: " + name);
        }

        return trimmed;
    }

    private static boolean isExtraNameChar(char c) {
        for (char extraNameCharacter : EXTRA_NAME_CHARACTERS) {
            if (c == extraNameCharacter) {
                return true;
            }
        }
        return false;
    }

    public static Predicate<ProguardConfigurationRule> isSupportedProguardRulePredicate() {
        return BundleConfigurationRule::isSupported;
    }
}
