package org.clyze.server.web.bundle;

import com.android.tools.r8.DiagnosticsHandler;
import com.android.tools.r8.graph.DexItemFactory;
import com.android.tools.r8.origin.Origin;
import com.android.tools.r8.shaking.*;
import com.android.tools.r8.utils.Reporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class BundleConfigurationContext {

    private final String userId;

    private final DexItemFactory factory = new DexItemFactory();
    private final Reporter reporter = new Reporter(new DiagnosticsHandler() {});

    public BundleConfigurationContext(String userId) {
        this.userId = userId;
    }

    public final DexItemFactory getFactory() {
        return factory;
    }

    public final Reporter getReporter() {
        return reporter;
    }

    public final String getUserId() { return userId; }

    public BundleConfigurationBuilder parseProguardFile(String pathToProFile) {
        return _parseProguard(new ProguardConfigurationSourceFile(Paths.get(pathToProFile)));
    }

    public BundleConfigurationBuilder parseProguardText(String proguardText) throws IOException {
        return parseProguard(new ByteArrayInputStream(proguardText.getBytes("UTF-8")));
    }

    public BundleConfigurationBuilder parseProguardRule(String textOfProRule) {
        return _parseProguard(new ProguardConfigurationSourceStrings(Arrays.asList(textOfProRule), Paths.get(""), Origin.unknown()));
    }

    public BundleConfigurationBuilder parseProguard(InputStream is) throws IOException {
        return _parseProguard(new ProguardConfigurationSourceBytes(is, Origin.unknown()));
    }

    private BundleConfigurationBuilder _parseProguard(ProguardConfigurationSource src) {
        ProguardConfigurationParser parser = new ProguardConfigurationParser(factory, reporter);
        parser.parse(src);
        return new BundleConfigurationBuilder(
            BundleConfiguration.isSupportedProguardRulePredicate(),
            parser.getConfigurationBuilder()
        );
    }

    public BundleConfigurationBuilder parseClueRules(String pathToClueRulesFile) throws IOException, ProguardRuleParserException {
        BundleConfigurationBuilder configurationBuilder = newBundleConfigurationBuilder();
        ClueRuleParser.newInstance(
            factory,
            reporter,
            new ProguardConfigurationSourceFile(Paths.get(pathToClueRulesFile)),
            configurationBuilder
        ).parse();
        return configurationBuilder;
    }

    public BundleConfigurationBuilder parseClueRules(Path pathToClueRulesFile) throws IOException, ProguardRuleParserException {
        return _parseClueRules(new ProguardConfigurationSourceFile(pathToClueRulesFile));
    }

    public BundleConfigurationBuilder parseClueRule(String textOfClueRule) throws IOException, ProguardRuleParserException {
        return _parseClueRules(new ProguardConfigurationSourceStrings(Arrays.asList(textOfClueRule), Paths.get(""), Origin.unknown()));
    }

    public BundleConfigurationBuilder parseClueRules(InputStream is) throws IOException, ProguardRuleParserException {
        return _parseClueRules(new ProguardConfigurationSourceBytes(is, Origin.unknown()));
    }

    //Don't create a new bundle configuration builder for holding the parsed rules, but reuse the given one
    public void parseClueRule(String textOfClueRule, BundleConfigurationBuilder configurationBuilder) throws IOException, ProguardRuleParserException {
        ProguardConfigurationSource src = new ProguardConfigurationSourceStrings(
            Arrays.asList(textOfClueRule),
            Paths.get(""),
            Origin.unknown()
        );
        ClueRuleParser.newInstance(factory, reporter, src, configurationBuilder).parse();
    }

    private BundleConfigurationBuilder _parseClueRules(ProguardConfigurationSource src) throws IOException, ProguardRuleParserException {
        BundleConfigurationBuilder configurationBuilder = newBundleConfigurationBuilder();
        ClueRuleParser.newInstance(factory, reporter, src, configurationBuilder).parse();
        return configurationBuilder;
    }

    public BundleConfigurationBuilder newBundleConfigurationBuilder() {
        return new BundleConfigurationBuilder(
            BundleConfiguration.isSupportedProguardRulePredicate(),
            ProguardConfiguration.builder(factory, reporter)
        );
    }
}
