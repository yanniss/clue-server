package org.clyze.server.web.bundle;

import com.android.tools.r8.shaking.BundleConfigurationRule;
import com.android.tools.r8.shaking.ProguardMemberRule;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.clyze.persistent.model.doop.Method;

import java.util.*;
import java.util.stream.Collectors;

public class DefaultMethodMatcher implements BundleConfigurationRule.MethodMatcher {

    private static final Log log = LogFactory.getLog(DefaultMethodMatcher.class);

    private final BundleConfigurationContext proguardContext;
    private final List<Method> declaredMethods;

    public DefaultMethodMatcher(BundleConfigurationContext proguardContext, List<Method> declaredMethods) {
        this.proguardContext = proguardContext;
        this.declaredMethods = declaredMethods;
    }

    @Override
    public Set<Method> matchingMethods(BundleConfigurationRule rule) {

        Set<Method> matchingMethods = new HashSet<>();
        log.debug("Filtering methods for rule: " + rule);

        BundleConfigurationRule.Type ruleType = rule.getType();
        if (ruleType == BundleConfigurationRule.Type.KEEP_CLASSES_WITH_MEMBERS ||
            ruleType == BundleConfigurationRule.Type.KEEP_CLASSES_WITH_MEMBER_NAMES) {

            for (ProguardMemberRule memberRule: rule.getMemberRules()) {
                Set<Method> methods = findSatisfyingMethods(memberRule);
                if (methods.isEmpty()) {
                    //don't proceed any further, we need to match them all
                    return Collections.emptySet();
                }
                else {
                    matchingMethods.addAll(methods);
                }
            }
        }
        else {
            for (ProguardMemberRule memberRule: rule.getMemberRules()) {
                Set<Method> methods = findSatisfyingMethods(memberRule);
                if (!methods.isEmpty()) {
                    matchingMethods.addAll(methods);
                }
            }
        }
        log.debug(matchingMethods.size() + " methods matching rule: " + rule);
        log.debug(matchingMethods.stream().map(Method::getDoopId).collect(Collectors.joining(", ")));
        return matchingMethods;
    }

    private Set<Method> findSatisfyingMethods(ProguardMemberRule rule) {
        Set<Method> matchingMethods = new HashSet<>();
        if (rule.getRuleType().includesMethods()) {
            for (Method method : declaredMethods) {
                if (R8BundleConfigurationRuleMatcher.matchesMemberRule(proguardContext, rule, method)) {
                    log.debug("Method " + method.getDoopId() + " matches member rule: " + rule);
                    matchingMethods.add(method);
                }
            }
        }
        return matchingMethods;
    }
}
