package org.clyze.server.web.persistent.model.doop;

import com.android.tools.r8.shaking.BundleConfigurationRule;
import org.clyze.persistent.model.doop.Artifact;
import org.clyze.persistent.model.doop.Class;
import org.clyze.persistent.model.doop.Method;

public class MethodDirectiveContext {

	private Artifact artifact;
	private Class klass;
	private Method method;
	private BundleConfigurationRule rule;

	public MethodDirectiveContext(Artifact artifact, Class klass, Method method, BundleConfigurationRule rule) {
		this.artifact = artifact;
		this.klass = klass;
		this.method = method;
		this.rule = rule;
	}

	public Artifact getArtifact() {
		return artifact;
	}

	public void setArtifact(Artifact artifact) {
		this.artifact = artifact;
	}

	public Class getKlass() {
		return klass;
	}

	public void setKlass(Class klass) {
		this.klass = klass;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public BundleConfigurationRule getRule() {
		return rule;
	}

	public void setRule(BundleConfigurationRule rule) {
		this.rule = rule;
	}
}
