package org.clyze.server.web.persistent.store;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dates {

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat(DATE_FORMAT);

    public static Date parse(String date) {
        try {
            return FORMAT.parse(date);
        } catch (Exception all) {
            return null;
        }

    }

    public static String format(Date date) {
        try {
            return FORMAT.format(date);
        } catch (Exception all) {
            return null;
        }

    }
}
