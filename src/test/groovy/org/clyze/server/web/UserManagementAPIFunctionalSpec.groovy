package org.clyze.server.web

import spock.lang.*

import groovy.json.JsonSlurper

import org.clyze.client.web.api.Remote

@Stepwise
class UserManagementAPIFunctionalSpec extends Specification {

	private static final String HOST               = System.getProperty("gretty.host")
	private static final String PORT               = System.getProperty("gretty.port")			
	private static final Set<String> DEFAULT_USERS = org.clyze.server.web.restlet.CleanDeploy.USERS as Set<String>

	@Shared Remote api = Remote.at(HOST, PORT as Integer)
	
	def "Server is up and running"() {		
		expect:
		api.ping()
	}
	
	def "Clean deploy succeeds"() {
		expect:		
		api.cleanDeploy()
		Thread.sleep(5000) //Wait 5 secs for elastic refresh to complete
	}

	def "Admin user logins successfully"() {		

		when:
		api.login("admin", "admin123")		

		then:
		api.isLoggedIn()
		
	}

	def "Default list of users is correct"() {

		when:	
		def json = api.listUsers()

		then:
		def users = json.list
		(DEFAULT_USERS - users).isEmpty()
	}
}