package org.clyze.server.web

import spock.lang.*

import groovy.json.JsonSlurper

import org.clyze.client.web.api.Remote

@Stepwise
class ProjectManagementAPIFunctionalSpec extends Specification {

	private static final String HOST     = System.getProperty("gretty.host")
	private static final String PORT     = System.getProperty("gretty.port")			
	private static final String PROJECT  = "test-project"
	private static final String PROJECT2 = "renamed-" + PROJECT

	@Shared Remote api = Remote.at(HOST, PORT as Integer)
	@Shared String projectId = "ef93f216-ec87-4b89-886b-20dfe61b9e2e"
	@Shared String bundleId = "6db93186-9daf-47ee-bff8-56fccc9bd879"
	
	def "Server is up and running"() {		
		expect:
		api.ping()
	}
	
	def "Clean deploy succeeds"() {
		expect:		
		api.cleanDeploy()
		Thread.sleep(1000) //Wait a while for elastic refresh to complete
	}

	def "User logins successfully"() {

		when:
		api.login("yannis", "yannis123")		

		then:
		api.isLoggedIn()
	}

	def "User creates a project"() {
		when:
		def json = api.createProject(PROJECT)
		projectId = json.id
		Thread.sleep(1000) //Wait a while for elastic refresh to complete

		then:		
		json.name == PROJECT
	}

	def "User lists the projects"() {
		when:
		def json  = api.listProjects()			

		then:		
		json.start == 0 &&
		json.hits == 2 &&
		json.results.find { it.name == PROJECT}				
	}

	def "User reads project"() {
		when: 
		def json = api.getProject(projectId)

		then:		
		json.id && json.name == PROJECT
	}

	def "User updates the name of the project"() {
		when: 
		def json = api.updateProject(projectId, PROJECT2, null)
		Thread.sleep(1000) //Wait a while for elastic refresh to complete

		then:
		json.name == PROJECT2
	}

	def "User adds a member to the project"() {
		when: 
		def json = api.updateProject(projectId, null, ["vsam"])
		Thread.sleep(1000) //Wait a while for elastic refresh to complete

		then:
		json.members == ["vsam"]
	}	

	def "Member logins"() {
		when:
		api.login("vsam", "vsam123")		

		then:
		api.isLoggedIn()
	}
	
	def "Member lists the projects"() {
		when:
		def json  = api.listProjects()	
		println json	

		then:		
		json.start == 0 &&
		json.hits == 2 &&
		json.results.find { it.name == PROJECT2}				
	}
	
	def "Member posts the fb-lite bundle in the project"() {
		when:		
		bundleId = api.createDoopBundle(
			projectId,
			"android_25_fulljars", 
			"com.facebook.lite_85.0.0.10.176-96581738_minAPI9(x86)(nodpi)_apkmirror.com.apk"
		)		

		then:
		bundleId
	}

	def "Member lists the bundles of the project"() {
		when:		
		def json = api.listBundles(projectId)		

		then:
		json.start == 0 &&
		json.hits == 1 &&
		json.results.find { it.id == bundleId }				
	}

	def "Member creates a context-insensitive analysis on the fb-lite bundle"() {

		when:
		def analysisId = api.createAnalysis(bundleId, "context-insensitive")		

		then:
		analysisId
	}	

	def "Member posts the fb-lite bundle in his scrap project"() {
		when:		
		bundleId = api.createDoopBundle(
			"vsam/scrap",
			"android_25_fulljars", 
			"com.facebook.lite_85.0.0.10.176-96581738_minAPI9(x86)(nodpi)_apkmirror.com.apk"
		)		

		then:
		bundleId
	}

	def "Member lists the bundles of his scrap project"() {
		when:		
		def json = api.listBundles("vsam/scrap")		

		then:
		json.start == 0 &&
		json.hits == 1 &&
		json.results.find { it.id == bundleId }				
	}	
	
}
