package org.clyze.server.web.bundle

import com.android.tools.r8.shaking.BundleConfigurationRule
import com.android.tools.r8.shaking.ProguardConfigurationRule
import spock.lang.Specification
import spock.lang.Unroll

class ClueFileParsingSpec extends Specification {

    private static final String PHONOGRAPH_PRO =
"""
-keep class retrofit.** { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser\$** {
    **[] \$VALUES;
    public *;
}

-keep class butterknife.** { *; }
-keep class **\$\$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

-keep class !android.support.v7.internal.view.menu.**,** {*;}
"""

    private static final String PHONOGRAPH_PRO_AS_CLUE_FILE =
"""
keep class retrofit.** { *; }

keep public class * implements com.bumptech.glide.module.GlideModule
keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser\$** {
    **[] \$VALUES;
    public *;
}

keep class butterknife.** { *; }
keep class **\$\$ViewBinder { *; }
keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

keep class !android.support.v7.internal.view.menu.**,** {*;}
"""

    private static final List<String> TEST_RULES = [
        'keep class retrofit.** { *; }',
        'remove class butterknife.** { *; }',
        'forceremove public class * implements com.bumptech.glide.module.GlideModule'
    ]

    private static final String TEST_FILE = TEST_RULES.join('\n')

    private static final asStream(String s) {
        new ByteArrayInputStream(s.getBytes("UTF-8"))
    }

    def "Clue file parser works for phonograph"() {
        given:
        Collection<ProguardConfigurationRule> r8Rules   = BundleConfigurationContext.parseProguard(asStream(PHONOGRAPH_PRO)).build().rules
        Collection<ProguardConfigurationRule> clueRules = BundleConfigurationContext.parseClueRules(asStream(PHONOGRAPH_PRO_AS_CLUE_FILE)).build().rules

        expect:
        r8Rules.size() == clueRules.size() &&
        //r8Rules.keySet().join('\n') == clueRules.keySet().join('\n') &&
        r8Rules.collect{ it.source.substring(1) }.join('\n') == clueRules.collect{ it.source }.join('\n')
    }


    @Unroll
    def "Clue bundle configuration correctly processes proguard rule #rule"() {
        given:
        BundleConfiguration config = BundleConfigurationContext.parseProguardRule(rule).build()
        String id = BundleConfigurationRule.checksumFor(rule)

        expect:
        config.getRuleById(id).source == rule

        where:
        rule << ["-keep class foo.** ",
                 "-keep public class bar.** { *; }"]
    }

    @Unroll
    def "Clue bundle configuration correctly processes clue rule #rule"() {
        given:
        BundleConfiguration config = BundleConfigurationContext.parseClueRule(rule).build()
        String id = BundleConfigurationRule.checksumFor(rule)

        expect:
        config.getRuleById(id).source == rule

        where:
        rule << ["forceremove class foo.** ",
                 "remove public class bar.** { *; }"]
    }

    @Unroll
    def "Clue bundle configuration correctly processes clue file"() {
        given:
        BundleConfiguration config = BundleConfigurationContext.parseClueRules(asStream(TEST_FILE)).build()

        expect:
        config.rules.collect { it.source } == TEST_RULES
    }
}
