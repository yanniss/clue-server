package org.clyze.server.web

import org.clyze.server.web.bundle.BundleConfiguration
import org.clyze.server.web.bundle.DummyProguardRuleExpander
import org.clyze.server.web.bundle.BundleConfigurationContext
import spock.lang.Specification
import spock.lang.Unroll

class ProguardProcessingSpec extends Specification {

    @Unroll
    def "dummy proguard rule expander processes #rule"() {
        /*
        given:
        TestSubscriber subscriber = new TestSubscriber()
        BundleConfigurationContext ctx = new BundleConfigurationContext()
        BundleConfiguration config = ctx.parseClueRule(rule).build()
        new DummyProguardRuleExpander().expand()

        expect:
        subscriber.subscribed
        subscriber.kc.proguardRule.classAccessFlags.toString() == accessFlags
        subscriber.completed
        subscriber.subscription

        where:
        rule | accessFlags
        "keep class foo.** " | ""
        "keep public class bar.** { *; }" | "public"
        */
    }

}
