package org.clyze.server.web

import spock.lang.*

import groovy.json.JsonSlurper

import org.clyze.client.web.api.Remote

@Stepwise
class APIFunctionalSpec extends Specification {

	/*
	 Invoke the test using ./gradlew test [-Pdeploy=...] [-DBUNDLE=... -DANALYSIS=...]
	*/

	private static final String HOST                  = System.getProperty("gretty.host")
	private static final String PORT                  = System.getProperty("gretty.port")
	private static final String USE_EXISTING_BUNDLE   = System.getProperty("BUNDLE")
	private static final String USE_EXISTING_ANALYSIS = System.getProperty("ANALYSIS")
	private static final String PROJECT               = org.clyze.server.web.restlet.CleanDeploy.PROJECT
	
	@Shared String bundleId   = USE_EXISTING_BUNDLE
	@Shared String analysisId = USE_EXISTING_ANALYSIS	
	@Shared Remote api        = Remote.at(HOST, PORT as Integer)
	
	def "Server is up and running"() {		
		expect:
		api.ping()
	}

	@IgnoreIf({USE_EXISTING_BUNDLE})
	def "Clean deploy succeeds"() {
		expect:
		api.cleanDeploy()
		Thread.sleep(1000) //Wait a while for elastic refresh to complete
	}

	def "User logins successfully"() {		

		when:
		api.login("yannis", "yannis123")		

		then:
		api.isLoggedIn()
		
	}
	
	@IgnoreIf({USE_EXISTING_BUNDLE})
	def "User bundles list is empty"() {

		setup:
		def json = null

		when:
		json = api.listBundles("yannis/$PROJECT")

		then:
		with (json) {
			//validateListBundlesResponse(delegate)
			assert start == 0		
			assert hits  == 0
			assert results.isEmpty()
		}		
	}

	@IgnoreIf({USE_EXISTING_BUNDLE})
	def "User submits the fb-lite bundle"() {		

		when:
		bundleId = api.createDoopBundle(
			"yannis/$PROJECT",
			"android_25_fulljars", 
			"com.facebook.lite_85.0.0.10.176-96581738_minAPI9(x86)(nodpi)_apkmirror.com.apk"
		)		

		then:
		bundleId
	}
	
	@IgnoreIf({USE_EXISTING_BUNDLE})
	def "User creates a context-insensitive analysis on the fb-lite bundle"() {

		when:
		analysisId = api.createAnalysis(bundleId, "context-insensitive")		

		then:
		analysisId
	}
	
	@IgnoreIf({USE_EXISTING_BUNDLE})
	def "User starts the analysis and waits for its completion"() {

		when: 
		api.executeAnalysisAction(bundleId, analysisId, 'start')

		then:
		def terminalStates = ["FINISHED", "ERROR"] as Set
		api.waitForAnalysisStatus(terminalStates, bundleId, analysisId, 120) == "FINISHED"		
		
	}		

	@Unroll
	def "Local variable in #file@#line,#col is verified"() {
		expect:
		TestHelper.verifyLocalVar(
			api.getSymbolAt(bundleId, analysisId, file, line, col),
			file,
			line,
			col
		)

		where:
		file                                           | line| col
		'com/facebook/lite/ClientApplication.shimple'  | 232 | 50
		'com/facebook/lite/MainActivity.shimple'       | 31  | 42

	}
}