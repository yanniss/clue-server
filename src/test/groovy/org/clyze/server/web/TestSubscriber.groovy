package org.clyze.server.web

import io.reactivex.FlowableSubscriber
import io.reactivex.annotations.NonNull
import org.clyze.server.web.persistent.model.doop.KeepRemoveMethodDirective
import org.reactivestreams.Subscription

class TestSubscriber implements FlowableSubscriber<KeepRemoveMethodDirective> {

    boolean completed = false
    boolean hasError = false
    boolean subscribed = false
    KeepRemoveMethodDirective directive = null
    Subscription subscription = null

    @Override
    void onSubscribe(@NonNull Subscription s) {
        subscribed = true
        subscription = s
        s.request(100L)
    }

    @Override
    void onNext(@NonNull KeepRemoveMethodDirective directive) {
        this.directive = directive
    }

    @Override
    void onError(@NonNull Throwable e) {
        hasError = true
    }

    @Override
    void onComplete() {
        completed = true
    }
}
