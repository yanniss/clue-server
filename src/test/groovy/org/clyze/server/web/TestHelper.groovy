package org.clyze.server.web

import groovy.json.JsonSlurper

class TestHelper {

	static boolean validateListBundlesResponse(def json) {
		["start", "count", "hits", "results"].every {
			json.hasProperty it
		}		
		//TODO: Validate the format of the results array (bundle items)	
	}

	static String fileNameOfLocalVarSpec(String sourceFileName, int line, int col) {
		String fileName = sourceFileName.replaceAll('/', '-')
		return "local-var-in-${fileName}@${line},${col}.json"
	}

	static def loadLocalVarSpec(String sourceFileName, int line, int col) {
		String jsonFileName = fileNameOfLocalVarSpec(sourceFileName, line, col)		
		new JsonSlurper().parse(openResource(jsonFileName), "UTF-8")
	}

	static boolean verifyLocalVar(def json, String file, int line, int col) {
		def jsonSpec = loadLocalVarSpec(file, line, col)
		
		assert jsonSpec.symbols.size() == json.symbols.size()
		int i = 0
		jsonSpec.symbols.each { def symbolSpec ->
			def symbol = json.symbols[i]
			assertPositionsAreEqual(symbolSpec, symbol)
			assertPropertiesAreEqual(symbolSpec, symbol, 'kind')
			assertPropertiesAreEqual(symbolSpec, symbol, 'sourceFileName')
			i++
		}

		true
	}

	static void assertPositionsAreEqual(def symbol1, def symbol2) {
		assert symbol1.position.startLine   == symbol2.position.startLine
		assert symbol1.position.endLine     == symbol2.position.endLine
		assert symbol1.position.startColumn == symbol2.position.startColumn
		assert symbol1.position.endColumn   == symbol2.position.endColumn
	}

	static void assertPropertiesAreEqual(def symbol1, def symbol2, String property) {
		assert symbol1[(property)] == symbol2[(property)]
	}

	static InputStream openResource(String res) {
		return TestHelper.getClassLoader().getResourceAsStream(res)
	}
}