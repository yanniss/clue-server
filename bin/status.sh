#!/usr/bin/bash

potentialElasticDirs=`find ../../ -name "elasticsearch*" -type d`
echo "############################################"
echo "###   Apparent configuration of elastic  ###"
echo "############################################"
for dir in $potentialElasticDirs
do
  # --with-filename
  egrep --no-messages 'cluster.name|network.host|http.port|transport.tcp.port' $dir/config/elasticsearch.yml
done
echo

potentialServerDirs=`find ../../ -name "*-server" -type d`
echo "############################################"
echo "###   Server's configuration of elastic  ###"
echo "############################################"
for dir in $potentialServerDirs
do
  potentialServerConfigDirs=`find $dir -name "config" -type d`
  for configdir in $potentialServerConfigDirs
  do
    echo "Configuration directory: " $configdir
    egrep --no-messages 'indexName|clusterName|host|port' $configdir/elastic_remote.properties
  done
done
echo

echo "############################################"
echo "###   Apparent elastic process id        ###"
echo "############################################"
elasticPid=`ps -x | grep elastic | grep java | awk '{print $1}'`
echo $elasticPid
echo

elasticProcess=$elasticPid"/java"
echo "############################################"
echo "###   Apparent elastic process port      ###"
echo "############################################"
netstat -p 2>/dev/null | grep "$elasticProcess" | grep localhost | awk '{print $4;}' | sort -u
echo

echo "############################################"
echo "###   Apparent Clue server process id    ###"
echo "############################################"
serverPid=`ps -x | grep server | egrep "clue|doop" | awk '{print $1}'`
echo $serverPid
echo

serverProcess=$serverPid"/java"
echo "############################################"
echo "###   Apparent Clue server ports         ###"
echo "############################################"
netstat -pa 2>/dev/null | grep "$serverProcess" | awk '{print $4;}' | grep '\[::\]' | sort -u

