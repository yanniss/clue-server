#!/usr/bin/python

# Checks that some analysis outputs have been correctly imported into Elastic.
#
# Example usage:
#
#   import-checker ${HOST}:${PORT} ${USER} ${ANALYSIS_ID} ,,,/analysisId/database
#

import csv
import os
import sys
from elastic_lib import *

def checkImport(relation, filters, tupleDescription):
    for row in schema.findAll(relation, filters):
        return True
    schema.failCheck(tupleDescription + " was not imported correctly")
    return False

def importedClassSubtypeExists(data, tupleDescription):
    filters = [Q("match", classId = data[0]), Q("match", subClassId = data[1])]
    return checkImport("ClassSubtype", filters, tupleDescription)

def importedIFieldValueExists(data, tupleDescription):
    filters = [Q("match", baseValueId = data[0]), Q("match", fieldId = data[1]), Q("match", valueId = data[2])]
    return checkImport("InstanceFieldValues", filters, tupleDescription)

def importedMethodSubtypeExists(data, tupleDescription):
    filters = [Q("match", methodId = data[0]), Q("match", subMethodId = data[1])]
    return checkImport("MethodSubtype", filters, tupleDescription)

def importedSFieldValueExists(data, tupleDescription):
    filters = [Q("match", fieldId = data[0]), Q("match", valueId = data[1])]
    return checkImport("StaticFieldValues", filters, tupleDescription)

def importedTypeCanAccessFieldExists(data, tupleDescription):
    filters = [Q("match", classId = data[0]), Q("match", fieldId = data[1])]
    return checkImport("TypeCanAccessField", filters, tupleDescription)

def importedFieldShadowedByExists(data, tupleDescription):
    filters = [Q("match", fieldId = data[0]), Q("match", shadowFieldId = data[1])]
    return checkImport("FieldShadowedBy", filters, tupleDescription)

def importedInvocationValueExists(data, tupleDescription):
    filters = [Q("match", fromMethodId = data[0]), Q("match", invocationId = data[1]), Q("match", toMethodId = data[2]), Q("match", isCHA = data[3])]
    return checkImport("InvocationValues", filters, tupleDescription)

def importedVarValueExists(data, tupleDescription):
    # data[0] is not used in the stored tuples
    filters = [Q("match", varId = data[1]), Q("match", valueId = data[2])]
    return checkImport("VarValues", filters, tupleDescription)

def importedArrayValueExists(data, tupleDescription):
    filters = [Q("match", arrayValueId = data[0]), Q("match", valueId = data[1])]
    return checkImport("ArrayValues", filters, tupleDescription)

def importedVarReturnExists(data, tupleDescription):
    filters = [Q("match", varId = data[0]), Q("match", methodId = data[1])]
    return checkImport("VarReturn", filters, tupleDescription)

def importedClassExists(data, tupleDescription):
    classId = data[0]
    if classId == "null_type":
        return True
    if classId.startswith("("):
        # These are classes that come from approximations in the
        # analysis and can be safely ignored.
        print("Ignoring invalid class: " + classId)
        return True
    return schema.classExists(classId, tupleDescription)

def importedMethodExists(data, tupleDescription):
    methodId = data[0]
    # We ignore class/instance initializers, since they may be skipped
    # when importing jcplugin facts into Elasticsearch.
    if (": void <clinit>()>" in methodId) or \
       (": void <init>(" in methodId) or \
       (schema.methodExists(methodId, tupleDescription)):
        return True
    # If the method was not found, maybe its signature got mangled
    # (e.g., covariant return types). If a method with the same name
    # and arity is found, report a warning to the console.
    klassPrefix = methodId.split(":")[0]
    parts = methodId.split()
    if (len(parts) > 2):
        parts = parts[2].split("(")
        name = parts[0]
        parts = parts[1].split(")")
        arity = 0 if "()" in methodId else len(parts[0].split(","))
        print(methodId + " does not exist, doing approximate method lookup (to detect covariant instances): " + klassPrefix[1:] + "." + name + "/" + str(arity))
        if schema.methodNameExists(klassPrefix, name, arity, tupleDescription):
            print("Checked OK: " + name + "/" + str(arity))
            return True
    return False

def checkFactsFile(csvName, maxTuples, checkFunc, databaseDir):
    csvPath = databaseDir + '/' + csvName
    if not os.path.isfile(csvPath):
        print("File does not exist: {}".format(csvPath))
        return
    facts = open(csvPath, 'rb')
    reader = csv.reader(facts, delimiter="\t")
    count = 0
    failed = 0
    print("== Checking {} ==".format(csvName))
    for row in reader:
        if count == maxTuples:
            break
        count = count + 1
        if not checkFunc(row, csvName + " entry @ line " + str(count)):
            failed = failed + 1
    print("{} failed facts: {}/{}".format(csvName, str(failed), str(count)))

def usage():
    print("Usage: import-checker.py hostname:port userId bundleId outDir max [relation]")
    print("  hostname:port    the Elastic connection point")
    print("  userId           the user ID")
    print("  bundleId         the input bundle ID")
    print("  outDir           the output directory of the server")
    print("  max              the maximum number of tuples to check")
    print("  relation         the name of the .csv to check: " + relations)
    exit(0)

checkers = { # '_Server_Interesting_Type.csv'  : importedClassExists,             \
             'Reachable.csv'                 : importedMethodExists,            \
             'Server_Class_Subtype.csv'      : importedClassSubtypeExists,      \
             'Server_IField_Values.csv'      : importedIFieldValueExists,       \
             'Server_Method_Subtype.csv'     : importedMethodSubtypeExists,     \
             'Server_SField_Values.csv'      : importedSFieldValueExists,       \
             'Server_Type_Can_Access_Field.csv' : importedTypeCanAccessFieldExists, \
             'Server_Field_ShadowedBy.csv'   : importedFieldShadowedByExists,   \
             'Server_Invocation_Values.csv'  : importedInvocationValueExists,   \
             'Server_Var_Values.csv'         : importedVarValueExists,          \
             'Server_Var_Return.csv'         : importedVarReturnExists,         \
             'Server_Array_Values.csv'       : importedArrayValueExists         \
           }
relations = ", ".join(checkers.keys())

if (len(sys.argv) < 6):
    usage()

elasticConnection = sys.argv[1]
userId0 = sys.argv[2]
bundleId0 = sys.argv[3]
outDir = sys.argv[4]

if len(sys.argv) > 5:
    try:
        maxTuples = int(sys.argv[5])
    except ValueError:
        usage()
else:
    maxTuples = None

if len(sys.argv) > 6:
    relation = sys.argv[6]
else:
    relation = None

logName = "import-checker.log"
logFile = open(logName, "w")
schema = ElasticLib(elasticConnection, userId0, bundleId0, logFile)

bundleDir = outDir + "/" + userId0 + "/" + bundleId0
factsDir = bundleDir + "/facts"
print("factsDir = " + factsDir)

print("Checking if facts have been correctly imported into Elasticsearch...")
print("")

for analysis in schema.findAllAnalysesForBundle():
    anId0 = analysis.id
    schema.setAnalysisId(anId0)
    schema.printAnalysis(analysis)
    databaseDir = bundleDir + "/analyses/" + anId0 + "/database"
    # print("databaseDir = " + databaseDir)
    for f, c in checkers.items():
        if (relation == None) or (relation == f):
            checkFactsFile(f, maxTuples, c, databaseDir)
    print("")

print("Check log file " + logName + " for failed checks.")
logFile.close()
