#!/usr/bin/python

# Monitors the clue-server's console output for Elasticsearch crashes
# and restarts Elasticsearch. Run it as follows:
#
#   monitor-clue-elastic.py location/of/console/log ~/.elasticsearch_pid $ELASTIC_HOME
#

import os
import subprocess
import sys
import time

def restartElastic(elasticPidFile, elasticHome):
    print("Restarting Elastic")
    es_stop = "if [ -f " + elasticPidFile + " ]; then kill `cat " + elasticPidFile + "`; fi"
    es_start = elasticHome + "/bin/elasticsearch -d -p " + elasticPidFile
    print(es_stop)
    subprocess.Popen(es_stop, shell=True)
    print(es_start)
    subprocess.Popen(es_start, shell=True)

def getLastCrashTime(logPath):

    # The following command is for doop.log
    # cmd1 = "grep -F 'None of the configured nodes are available:' " + logPath
    # cmd2 = "grep -P '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'"

    # The following command is for console log
    cmd1 = "grep -F 'org.restlet.resource.ServerResource doCatch' -A 2 " + logPath
    cmd2 = "grep -F 'NoNodeAvailableException[None of the configured nodes are available' -B 2"

    # print(cmd1 + " | " + cmd2)
    p1 = subprocess.Popen(cmd1, shell=True, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(cmd2, shell=True, stdout=subprocess.PIPE, stdin=p1.stdout)
    out = p2.stdout.read().split('\n')
    if len(out) > 3:
        lastLine = out[-4]
        return lastLine[0:23]
    else:
        return ""

if (len(sys.argv) != 4):
    print("Usage: monitor-clue-elastic.py logPath elasticPidFile elasticHome")
    print("Monitors the clue-server log and restarts Elasticsearch when failure is detected.")
    print("Parameters:")
    print("  logPath            path to the server's console log (not doop.log)")
    print("  elasticPidFile     the location of .elasticsearch_pid")
    print("  elasticHome        the home directory of Elasticseach")
    exit(0)

logPath = sys.argv[1]
elasticPidFile = sys.argv[2]
elasticHome = sys.argv[3]

waitTime = 30

lastCrashTime = getLastCrashTime(logPath)

while True:
    crashTime = getLastCrashTime(logPath)
    # print(crashTime, lastCrashTime)
    restart = False
    if crashTime != lastCrashTime:
        print("* Found Elastic crash: " + crashTime)
        restart = True
    elif not os.path.isfile(elasticPidFile):
        print("* No PID file found: " + elasticPidFile)
        restart = True
    if restart:
        restartElastic(elasticPidFile, elasticHome)
        lastCrashTime = crashTime
        restart = False
    else:
        print("* Elasticsearch seems to work, last crash: " + lastCrashTime)
    # This time should be at least as much as the time needed by
    # Elasticsearch to fully start.
    print("Waiting for " + str(waitTime) + "s...")
    time.sleep(waitTime)
