# Library that represents the "schema" of Elasticsearch with some
# assumptions/conventions.

import re
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl.query import Q

primitiveArrayTypes = [ "boolean[]", "byte[]", "char[]", "short[]", "int[]", "long[]", "float[]", "double[]" ]

pseudoHeaps = [ "<<null pseudo heap>>", "<<string-builder>>", "<<string-constant>>", "<<system-thread-group>>", "<<main-thread-group>>", "<<string-buffer>>", "<<main-thread>>", "<<reflection-string-constant>>", "<<main-thread-group>>", "<<XL-pt-set>>", "<<main method array content>>" ]

nativeHeaps = [ "<java.io.UnixFileSystem: java.lang.String[] list(java.io.File)>/new java.lang.String[]", "<java.io.UnixFileSystem: java.lang.String[] list(java.io.File)>/new java.lang.String", "<java.io.UnixFileSystem: java.lang.String[] list0(java.io.File)>/new java.lang.String[]", "<java.io.UnixFileSystem: java.lang.String[] list(java.io.File)>/new java.lang.String", "java.io.FileSystem.getFileSystem/new java.io.UnixFileSystem" ]

class ElasticLib:

    # input bundle relations
    bRelations = { "Method", "Field", "MethodInvocation", "Variable",
                   "HeapAllocation", "Usage", "Class" }
    # analysis relations
    aRelations = { "StaticFieldValues", "InstanceFieldValues", "VarValues",
                   "InvocationValues", "TypeCanAccessField", "ClassSubtype",
                   "FieldShadowedBy", "MethodSubtype", "ReachableSymbol",
                   "VarReturn", "ArrayValues" }

    # value prefixes followed by type
    valuesWType = { "<class ", "<android inferred object of type " }

    def __init__(self, elasticConnection, userId0, bundleId0, logFile0):
        self.client = Elasticsearch(elasticConnection)
        self.userId0 = userId0
        self.bundleId0 = bundleId0
        self.logFile = logFile0

    def setAnalysisId(self, analysisId0):
        self.analysisId0 = analysisId0

    def failCheck(self, msg):
        self.logFile.write("Check failed: " + msg + "\n")

    def findAll(self, _type0, extraFilters):
        if _type0 is None:
            baseFilters = []
        elif _type0 in ElasticLib.bRelations:
            baseFilters = [ Q("match", _type = _type0), Q("match", rootElemId = self.bundleId0) ]
        elif _type0 in ElasticLib.aRelations:
            baseFilters = [ Q("match", _type = _type0), Q("match", rootElemId = self.analysisId0) ]
        else:
            print("Relation not recognized: "+ _type0)
            exit(-1)
        filters = baseFilters + extraFilters
        # print("Querying with filters: " + str(filters))
        s = Search(using=self.client, index="doop").query(Q('bool', must = filters))
        return s.scan()

    def findAllAnalysesForBundle(self):
        filters = [ Q("match", _type = "ServerSideAnalysis"), Q("match", bundleId = self.bundleId0) ]
        # print("Querying with filters: " + str(filters))
        s = Search(using=self.client, index="doop").query(Q('bool', must = filters))
        return s.scan()

    def classExists(self, classId, tupleDescription):
        if classId in primitiveArrayTypes:
            return True
        if classId.endswith('[]'):
            return self.classExists(classId[0 : -2], tupleDescription)
        for klass in self.findAll("Class", [ Q("match", doopId = str(classId)) ]):
            return True
        self.failCheck(tupleDescription + " points to non-existent class " + classId)
        return False

    def printAnalysis(self, row):
        try:
            print("Name                 : " + row.id)
            print("Running time         : " + str(row.durationsOfPhases.RUNNING))
            print("Post-processing time : " + str(row.durationsOfPhases.POST_PROCESSING))
        except AttributeError:
            print("error reading analysis properties in row: " + str(row))

    def getAnalysisPath(self):
        filters = [ Q("match", userId = self.userId0),
                    Q("match", id = self.rootElemId0),
                    Q("match", _type = "ServerSideAnalysis") ]
        s = Search(using=self.client, index="doop").query(Q('bool', must = filters))
        results = s.scan()
        for row in results:
            return self.userId0 + "/" + row.anOptions.ANALYSIS + "/" + self.rootElemId0
        return None

    def varExists(self, varId, tupleDescription):
        if "/@tmp-arraycopy" in varId:
            return True
        for v in self.findAll("Variable", [ Q("match", doopId = varId) ]):
            return True
        if (varId.endswith('/@this')) or \
           (varId.endswith('/this')) or \
           (re.search(r'/@parameter\d+$', varId) is not None) or \
           (re.search(r'/l\d+$', varId) is not None) or \
           (re.search(r'/\$r\d+$', varId) is not None) or \
           (re.search(r'/\$r\d+_\$\$A_\d+$', varId) is not None) or \
           (re.search(r'/\$i\d+$', varId) is not None) or \
           (re.search(r'/\$i\d+_\$\$A_\d+$', varId) is not None) or \
           (re.search(r'/\$l\d+$', varId) is not None) or \
           (re.search(r'/\$l\d+_\$\$A_\d+$', varId) is not None) or \
           (re.search(r'/\$z\d+$', varId) is not None) or \
           (re.search(r'/\$z\d+_\$\$A_\d+$', varId) is not None) or \
           (re.search(r'/\$stringconstant\d+$', varId) is not None) or \
           (re.search(r'/\$null\d+$', varId) is not None) or \
           (re.search(r'/\$numconstant\d+$', varId) is not None) or \
           (re.search(r'/\$classconstant\d+$', varId) is not None):
            return True
        self.failCheck(tupleDescription + " points to non-existent variable " + varId)
        return False

    def valueExists(self, valueId, tupleDescription):
        if (valueId in pseudoHeaps) or (valueId in nativeHeaps) or valueId.startswith("<android component object ")  or valueId.startswith("<android library object ") or valueId.startswith("<layout control object ") or valueId.startswith("<<reified ") or valueId.startswith("<reflective ") or valueId.startswith("num-constant-") or valueId.startswith("<string constant ") or valueId.startswith("<mock android ") or valueId.startswith("<special object for missing ") or ("::: (Mock)" in valueId) or ("::: ASSIGN" in valueId):
            return True
        else:
            for vt in self.valuesWType:
                if valueId.startswith(vt):
                    return self.classExists(valueId[len(vt):-1], tupleDescription)
        for heap in self.findAll("HeapAllocation", [ Q("match", doopId = valueId) ]):
            return True
        if self.unknownValueOfExistingType(valueId):
            return True
        self.failCheck(tupleDescription + " points to non-existent value " + valueId)
        return False

    def fieldExists(self, fieldId, tupleDescription):
        for field in self.findAll("Field", [ Q("match", doopId = fieldId) ]):
            return True
        self.failCheck(tupleDescription + " points to non-existent field " + fieldId)
        return False

    def methodExists(self, methodId, tupleDescription):
        for method in self.findAll("Method", [ Q("match", doopId = methodId) ]):
            return True
        # Ignore 'access$N' methods, the jcplugin seems to take care of them.
        if re.search(r' access\$\d+', methodId) is not None:
            return True
        self.failCheck(tupleDescription + " points to non-existent method " + methodId)
        return False

    def methodNameExists(self, klassPrefix, methodName, arity, tupleDescription):
        flag = False
        for method in self.findAll("Method", [ Q("match", name = methodName) ]):
            # The last check is a workaround due to this query not
            # matching exactly field "name".
            if (len(method.doopId.split(",")) == arity) and method.doopId.startswith(klassPrefix) and ((" " + methodName + "(") in method.doopId):
                print("method found = " + method.doopId)
                flag = True
        if flag:
            return True
        self.failCheck(tupleDescription + " points to non-existent method name " + klassPrefix[1:] + "." + methodName + "/" + str(arity))
        return False

    def invocationExists(self, invoId, tupleDescription):
        for invo in self.findAll("MethodInvocation", [ Q("match", doopId = invoId) ]):
            return True
        # Ignore string builders and iterators; they may be the result of
        # javac compiling string operations and for-each loops.
        if (re.search(r'/java.lang.StringBuilder.<init>/\d+$', invoId) is not None) or \
           (re.search(r'/java.lang.StringBuilder.append/\d+$', invoId) is not None) or \
           (re.search(r'/java.lang.StringBuilder.toString/\d+$', invoId) is not None) or \
           (re.search(r'/java.util.Iterator.next/\d+$', invoId) is not None):
            return True
        # Ignore 'access$N' methods, the jcplugin seems to take care of them.
        if re.search(r'.access\$\d+', invoId) is not None:
            return True
        self.failCheck(tupleDescription + " points to non-existent method invocation " + invoId)
        return False

    def nonArrayClassExists(self, c, tupleDescription):
        return c.endswith("[]") or self.classExists(c, tupleDescription)

    def unknownValueOfExistingType(self, valueId):
        idx1 = valueId.rfind("/new ")
        idx2 = valueId.rfind("/")
        if (idx1 != -1) and (idx2 != -1):
            eType = valueId[idx1 + 5 : idx2]
            return self.classExists(eType, "inner check for value " + valueId + " shows unknown allocation of type " + eType)
        else:
            return False

