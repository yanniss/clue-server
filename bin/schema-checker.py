#!/usr/bin/python

# Check the Elastic data of a given analysis for consistency. This is
# a sanity check, not full verification.
#
# Example usage:
#
#   schema-checker.py ${HOST}:${PORT} ${USER} ${ANALYSIS_ID}
#

import sys
from elastic_lib import *

from elasticsearch_dsl.query import Q

global schema

def fieldsNotEqual(fieldId1, fieldId2, tupleDescription):
    if fieldId1 == fieldId2:
        schema.failCheck(tupleDescription + " contains equal fields [" + fieldId1 + "]")
    return True

def maybeMergedException(s):
    idx1 = s.rfind("/new ")
    idx2 = s.rfind("/")
    if (idx1 != -1) and (idx2 != -1):
        eType = s[idx1 + 5 : idx2]
        test = eType.endswith("Exception") or eType.endswith("Error")
        if test:
            return schema.classExists(eType, "possibly merged exception " + eType)
        else:
            return False
    else:
        return False

def checkRelationWith(relationName, maxTuples, checkFunc):
    sys.stdout.write('Checking: {0:25} maxTuples: {1:10}'.format(relationName, str(maxTuples)))
    sys.stdout.flush()
    failed = 0
    count = 0
    batch = maxTuples / 10 or 1
    bars = 0
    for row in schema.findAll(relationName, []):
        tupleDescription = "#"+ str(count) + ", " + relationName + " with id = " + row.meta.id
        # print("Checking " + tupleDescription)
        if not checkFunc(row, tupleDescription):
            failed = failed + 1
        count = count + 1
        if count % batch == 0:
            bars = bars + 1
            sys.stdout.write('#')
            sys.stdout.flush()
        if count == maxTuples:
            # print("Checked " + str(maxTuples) + " " + relationName + " tuples")
            break
    if count == 0:
        sys.stdout.write('          ')
        sys.stdout.flush()
        hStr = "empty"
    else:
        if bars < 10:
            for i in range(bars, 10):
                sys.stdout.write(' ')
                sys.stdout.flush()
        h = 100. * (count - failed) / count
        hStr = "%.2f" % round(h, 2)
    print("  health: {0:>6} %  (checked: {1:>7})".format(hStr, str(count)))

def checkMethod(method, tupleDescription):
    return schema.classExists(str(method.declaringClassDoopId), tupleDescription)

def checkField(field, tupleDescription):
    return schema.classExists(field.declaringClassDoopId, tupleDescription)

def checkVariable(var, tupleDescription):
    return schema.methodExists(var.declaringMethodDoopId, tupleDescription)

def checkMethodInvocation(methodInvocation, tupleDescription):
    return schema.methodExists(methodInvocation.invokingMethodDoopId, tupleDescription)

def checkHeapAllocation(heap, tupleDescription):
    if hasattr(heap, 'allocatingMethodDoopId'):
        return schema.methodExists(heap.allocatingMethodDoopId, tupleDescription)
    if hasattr(heap, 'allocatedTypeDoopId'):
        return schema.classExists(heap.allocatedTypeDoopId, tupleDescription)
    # Some values (e.g. constant string fields) don't have allocatingMethodDoopId.
    return True

def checkStaticFieldValues(sfv, tupleDescription):
    return schema.valueExists(sfv.valueId, tupleDescription) \
       and schema.fieldExists(sfv.fieldId, tupleDescription)

def checkInstanceFieldValues(ifv, tupleDescription):
    return schema.valueExists(ifv.valueId, tupleDescription) \
       and schema.fieldExists(ifv.fieldId, tupleDescription) \
       and schema.valueExists(ifv.baseValueId, tupleDescription)

def checkVarValues(vv, tupleDescription):
    return schema.valueExists(vv.valueId, tupleDescription) \
       and schema.varExists(vv.varId, tupleDescription)

def checkInvocationValues(iv, tupleDescription):
    return schema.methodExists(iv.toMethodId, tupleDescription) \
       and ((")>/native " in iv.invocationId) or \
            schema.invocationExists(iv.invocationId, tupleDescription))

def checkTypeCanAccessField(crf, tupleDescription):
    return schema.classExists(crf.classId, tupleDescription) \
       and schema.fieldExists(crf.fieldId, tupleDescription)

def checkReachableSymbol(mr, tupleDescription):
    if mr.doopId.startswith('<'):
        return schema.methodExists(mr.doopId, tupleDescription)
    else:
        return schema.classExists(mr.doopId, tupleDescription)

def checkClassSubtype(cs, tupleDescription):
    return schema.nonArrayClassExists(cs.classId, tupleDescription) \
       and schema.nonArrayClassExists(cs.subClassId, tupleDescription)

def checkFieldShadowedBy(fsb, tupleDescription):
    return schema.fieldExists(fsb.fieldId, tupleDescription) \
       and schema.fieldExists(fsb.shadowFieldId, tupleDescription) \
       and fieldsNotEqual(fsb.fieldId, fsb.shadowFieldId, tupleDescription)

def checkMethodSubtype(ms, tupleDescription):
    return schema.methodExists(ms.methodId, tupleDescription) \
       and schema.methodExists(ms.subMethodId, tupleDescription)

def checkUsage(usage, tupleDescription):
    usageId = usage.meta.id
    usageDoopId = usage.doopId
    for row in schema.findAll(None, [ Q("match", doopId = usageDoopId) ]):
        try:
            if row.meta.id != usageId:
                return True
        except AttributeError:
            print("error, no meta.id in row: " + str(row))
    schema.failCheck("Usage #" + usage.meta.id + " points to non-existent Doop data with doopId = " + usageDoopId)
    return False

# input bundle relation checkers
bRelations = { "Method"                  : checkMethod,                  \
               "Field"                   : checkField,                   \
               "MethodInvocation"        : checkMethodInvocation,        \
               "Variable"                : checkVariable,                \
               "HeapAllocation"          : checkHeapAllocation,          \
               "Usage"                   : checkUsage                    }
# analysis relation checkers
aRelations = { "StaticFieldValues"       : checkStaticFieldValues,       \
               "InstanceFieldValues"     : checkInstanceFieldValues,     \
               "VarValues"               : checkVarValues,               \
               "InvocationValues"        : checkInvocationValues,        \
               "TypeCanAccessField"      : checkTypeCanAccessField,      \
               "ClassSubtype"            : checkClassSubtype,            \
               "FieldShadowedBy"         : checkFieldShadowedBy,         \
               "MethodSubtype"           : checkMethodSubtype,           \
               "ReachableSymbol"         : checkReachableSymbol          }
availableRelations = ", ".join(bRelations.keys() + aRelations.keys())
maxTuplesDefault = 2000

if (len(sys.argv) < 4):
    print("Usage: schema-checker.py hostname:port userId bundleId [maxTuples] [relation]")
    print("Runs some sanity checks on the Elastic data of an analysis.")
    print("Parameters:")
    print("  hostname:port    the Elastic connection point")
    print("  userId           the user ID")
    print("  bundleId         the input bundle ID")
    print("  maxTuples        the maximum number of tuples to check, default: " + str(maxTuplesDefault))
    print("  relation         relation to check (" + availableRelations + ") -- if omitted, all relations are checked")
    exit(0)

elasticConnection = sys.argv[1]
userId0 = sys.argv[2]
bundleId0 = sys.argv[3]
logName = "schema-checker.log"
logFile = open(logName, "w")
schema = ElasticLib(elasticConnection, userId0, bundleId0, logFile)

print("Running Elastic sanity check...")

if (len(sys.argv) < 5):
    maxTuples = maxTuplesDefault
else:
    maxTuples = int(sys.argv[4])

if (len(sys.argv) < 6):
    for r, c in bRelations.items():
        checkRelationWith(r, maxTuples, c)
    print("")
    for analysis in schema.findAllAnalysesForBundle():
        anId0 = analysis.id
        schema.setAnalysisId(anId0)
        schema.printAnalysis(analysis)
        for r, c in aRelations.items():
            checkRelationWith(r, maxTuples, c)
        print("")
else:
    relation = sys.argv[5]
    if bRelations.has_key(relation):
        checkRelationWith(relation, maxTuples, bRelations[relation])
    elif aRelations.has_key(relation):
        for analysis in schema.findAllAnalysesForBundle():
            anId0 = analysis.id
            schema.setAnalysisId(anId0)
            schema.printAnalysis(analysis)
            checkRelationWith(relation, maxTuples, aRelations[relation])
    else:
        print("Relation " + relation + " not supported, select one of the following: " + availableRelations)
        exit(0)

print("Check log file " + logName + " for failed checks.")
logFile.close()
