#!/usr/bin/python

# Show the invocations tree of a method (given as a full signature).
#
# Example usage:
#
# invo-tree ${HOST}:${PORT} ${USER} ${ANALYSIS_ID} "${METHOD_ID}" 4
#

import sys
from elastic_lib import *

from elasticsearch_dsl.query import Q

ALREADY_FOUND="+"
NEW_ENTRY="-"

def methodSimpleName(methodSig):
    parts = methodSig.split(' ')
    klass = parts[0][1:-1]
    # Simple name.
    mname = (parts[2].split('('))[0] + "()"
    # Name with parameter types.
    # mname = parts[2]
    return klass + ": " + mname

def label(methodSig, curDepth, found):
    symbol = ALREADY_FOUND if found else NEW_ENTRY
    return ("    " * curDepth) + "" + symbol + " " + methodSimpleName(methodSig)

def findMethInvocations(methodSig, curDepth):
    try:
        if seenMethods[methodSig]:
            print(label(methodSig, curDepth, True))
            return
    except KeyError:
        seenMethods[methodSig] = True
    print(label(methodSig, curDepth, False))
    if (curDepth == depth):
        return
    for meth in schema.findAll("Method", [ Q("match", doopId = methodSig) ]):
        # print("!")
        for invo in schema.findAll("InvocationValues", [ Q("match", toMethodId = meth.doopId) ]):
            # print((' ' * curDepth) + "| " + invo.fromMethodId)
            findMethInvocations(invo.fromMethodId, curDepth + 1)

if (len(sys.argv) < 6):
    print("Usage: invo-tree.py hostname:port userId anId methodSig depth")
    print("Parameters:")
    print("  hostname:port    the Elastic connection point")
    print("  userId           the user ID")
    print("  anId             the analysis ID")
    print("  methodSig        the method signature")
    print("  depth            the max depth of the resulting invocations tree")
    exit(0)

elasticConnection = sys.argv[1]
userId0 = sys.argv[2]
anId0 = sys.argv[3]
methodSig = sys.argv[4]
depth = int(sys.argv[5])
logFile = open("invo-tree.log", "w")

print("Computing invocation tree for: " + methodSig + ", depth: " + str(depth))
schema = ElasticLib(elasticConnection, userId0, anId0, logFile)
schema.showAnalysisInfo()

seenMethods = {}

WIDTH=70
print('-' * WIDTH)
findMethInvocations(methodSig, 0)
print('-' * WIDTH)

print("Legend:")
print(NEW_ENTRY + ": new entry")
print(ALREADY_FOUND + ": existing entry")

logFile.close()
