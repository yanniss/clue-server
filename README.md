# Doop Server 

This is the readme file for the Doop server: a multi-tenant framework for running doop analyses on the server-side.

The doop server builds upon the core Doop Java/Groovy API to offer (a) a web UI and (b) a RESTful API 
for submitting, running and querying analyses, using [elastic](http://www.elastic.co) for storing
all data (except files). 

## Directory Structure

The project contains the following directories:

* *gradle*: contains the gradle wrapper (gradlew) files.
* *src*: contains the source files of the project (Groovy, js/css/image files).

## Setting up Elastic Search

The doop server uses the latest `5.x` version of elastic search 
(namely `5.6.2`). The following steps describe the process of setting
it up and running it locally for development purposes.

### Download

First, download elastic 5.6.2 from [here](https://www.elastic.co/downloads/past-releases/elasticsearch-5-6-2)
and unzip / untar in a directory of your choice, e.g. `~/elasticsearch-5.6.2`.

### Startup command (better placed in a script)

    $ ~/elasticsearch-5.6.2/bin/elasticsearch -d -p ~/es_pid
    
### Shutdown command (better placed in a script)

    $ kill `cat ~/es_pid`
    
## Configuration

### Caution 

The elastic server uses the `cluster.name` configuration variable to 
determine the cluster to join when starting up. If two or more
elastic nodes are configured to use the same cluster name, they will 
automatically create a cluster with each other (leading to unexpected results, if such
a setup was not part of the initial intention). 

*Thus, for development
purposes, where everyone needs to run his own independent environment,
we should be cautious to provide unique cluster names.*

### Elastic

Edit the `~/elasticsearch-5.6.2/config/elasticsearch.yml` file
and provide a unique `cluster.name` value for your local network.
E.g., a good name is a string that contains the username and the
intention (.e.g. saiko-local-dev). You may also need to set the
values of `http.port` and `transport.tcp.port` . See the [elastic
page](https://bitbucket.org/yanniss/clyze-wiki/wiki/Elastic%20Search).
There is also a simple but handy script `bin/status.sh` that shows
the current processes, ports, and config file entries for elastic
and for the doop server.

### Doop server

Edit the `src/main/webapp/config/elastic_remote.properties` file
and set the `clusterName` and the `transport.tcp.port` propertiew 
to the same values configured above. There is a template file
for reference.

## Running the doop server

To run the server, we issue:

    $ ./gradlew appRun

or equivalently

    $ ./gradlew jettyRun
    
By default, the necessary configuration variables are expected to be placed in the ```~/.gradle/gradle.properties``` file. The default values of these variables are defined in the [build.gradle file](https://bitbucket.org/yanniss/clue-server/src/4e0bcae4c75068c7d833da1f17cd15176702a7ff/build.gradle?at=master&fileviewer=file-view-default#build.gradle-132). We can also define custom files (e.g. per machine) in the ```deploy``` directory, such as [saiko@centauri](https://bitbucket.org/yanniss/clue-server/src/4e0bcae4c75068c7d833da1f17cd15176702a7ff/deploy/saiko@centauri.properties?at=master), for example and use the following invocation to include them:

    $ ./gradlew appRun -Pdeploy=saiko@centauri

The doop server web application will be accessible at the following URL:

    http://localhost:8013/clue
    
### Clean deploy of the data
To clean-up the elastic index (database) and create the initial
user accounts, we issue the following, while the server is running:

    $ curl -X POST localhost:8013/clue/clean/deploy
        
### Deploying the server to a Java container
The doop server is compatible with any Java-based application server/container (Tomcat, Jetty, etc.). To generate
the respective web application archive (WAR), we issue:

    $ ./gradlew war
    
The generated `clue-server.war`, placed in build/libs, can then be deployed to the container of choice.

### Forwarding to a different machine
You can forward your ports from a central serve machine (used for analyses) to a machine with open ports, using ssh tunneling. Standard syntax includes, e.g.:

    $ ssh -L <localport>:localhost:<serverport> <server>