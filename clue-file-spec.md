[TOC]

# Clue file specification

__Work in progress:__ this is an ongoing effort to specify the syntax and semantics of clue supported rules & directives. The following refer to the structure and interpretation of the clue config file (and not to doop, credex or any other component of the clue pipeline).

## Contents of a Clue file

The contents of a clue file can be grouped as follows:

* Optimization pass directives: directives that define and customize the optimization passes to be executed.
__TODO__
 
* Keep rules: the keep rules define the methods to be kept (i.e., entry points), as in Proguard (see respective section below). The keep rules, when
expanded, yield keep-method directives.

* Remove rules: the remove rules define the methods to be removed (i.e., dead code). The remove rules are driven mostly by the analysis (the analysis generates remove-method directives, which can also be expressed in terms of rules). 
Users can import the analysis-driven directives / rules into their clue-file and can also add their own custom remove rules.

## Use cases

The basic use cases we aim to support are the following:

1. The user can upload 0 or more proguard files (with a wizard-like UX). The file is processed and the user can select the keep
rules to be imported in a newly created clue-file.
1. The user can upload 0 or more clue files (during bundle creation and at any other time, too). The system generates a default clue file when the user-provided one is missing. The clue-file lifecycle contains the following:    
    1. The user can externalize (export/download) a clue-file.
    1. The user can clone a clue-file.
    1. The user can rename a clue-file.
    1. The user can delete a clue-file.
    1. The user can manage the rules contained in a clue-file:
        1. The user can add new rules in a clue-file.
        1. The user can edit an existing rule contained in a clue-file.
        1. The user can delete a rule contained in a clue-file.
    1. The user can import the contents of a clue-file into another (using a Proguard-like `include` facility).
1. The user can run 0 or more analyses on a bundle, where each analysis generates a set of remove-method directives (contained in an
analysis-file).
    1. The user can import the contents of an analysis-file into a clue-file (using a newly created `includedirectives` facility).
    1. The user can manage the directives contained in an analysis file:
        1. The user can delete selected directives from an analysis-file.
        1. The user can move selected directives from an analysis-file to a clue-file, transforming them into rules. 
    1. The user can externalize (export/download) an analysis-file.
    1. The user can clone an analysis-file.
    1. The user can rename an analysis-file.
    1. The user can delete an analysis-file.
1. The user can run 0 or more optimizations on a bundle, always providing the clue-file to be used as an input. This action can
be also triggered programmatically (for integrating the clue service with CI/CD tools and also support other integrations).

## Compatibility with proguard

Clue supports the Proguard keep options and rules (?).

Proguard keep rule semantics depend on the particular application step (shrinking or obfuscation) and -in some cases- on the preservation of the respective class. For example, how should we interpret `keepaclassmembers` and `keepclassmembernames` rules, given that they are applied only if their respective class is preserved (not removed)? We do not remove classes, do we?

The remainder of this section provides an overview of Proguard, primarily focusing on its code removal/shrinking capabilities.

### Proguard at a glance

The syntax for specifying a Proguard keep rule is the following:

``` [keepRule] [,modifier,...] class_specification ```

Each [keepRule] is followed by an optional set of modifiers and a mandatory specification of the classes and class members (fields and methods) to which it should be applied. [More about class specifications](https://www.guardsquare.com/en/products/proguard/manual/usage#classspecification)

The following summarizes the various [keepRule] options:

* Code shrinking and obfuscation
    * `keep`: Keep classes and class members from being removed or renamed.
    * `keepclassmembers`: Keep class members from being removed or renamed (specifies class members to be preserved, if their classes are preserved as well).
    * `keepclasseswithmembers`: Keep classes and class members from being removed or renamed, only if all class members are present (specifies classes and class members to be preserved, on the condition that all of the specified class members are present).


* Code obfuscation
    * `keepnames`: Keep classes and class members from being renamed.
    * `keepclassmembernames`: Keep class members from being renamed.
    * `keepclasseswithmembernames`: Keep classes and class members from being renamed, only if all class members are present.

* Conditional `if class_specification`
    * Specifies classes and class members that must be present to activate the subsequent keep option (keep, keepclassmembers, ...). The condition and the subsequent keep option can share wildcards and references to wildcards.

__Notes:__ 
 
* If you specify a class, without class members, ProGuard only preserves the class and its parameterless constructor as entry points. It may still remove, optimize, or obfuscate its other class members.
* If you specify a method, ProGuard only preserves the method as an entry point. Its code may still be optimized and adapted.
* By default, shrinking is applied; all classes and class members are removed, except for the ones listed by the various keep options, and the ones on which they depend, directly or indirectly. A shrinking step is also applied after each optimization step, since some optimizations may open the possibility to remove more classes and class members. [More about Proguard shrinking](https://www.guardsquare.com/en/products/proguard/manual/usage#shrinkingoptions)
* By default, optimization is enabled; all methods are optimized at a bytecode level. [More about Proguard optimizations](https://www.guardsquare.com/en/products/proguard/manual/usage#optimizationoptions)
* By default, obfuscation is applied; classes and class members receive new short random names, except for the ones listed by the various keep options. Internal attributes that are useful for debugging, such as source files names, variable names, and line numbers are removed. [More about Proguard obfuscation](https://www.guardsquare.com/en/products/proguard/manual/usage#obfuscationoptions)

The following list describes the various [keepRule] modifiers:

* `includedescriptorclasses`: Specifies that any classes in the type descriptors of the methods and fields that the keep option keeps should be kept as well. This is typically useful when keeping native method names, to make sure that the parameter types of native methods aren't renamed either. Their signatures then remain completely unchanged and compatible with the native libraries.

* `includecode`: Specifies that code attributes of the methods that the fields that the keep option keeps should be kept as well, i.e. may not be optimized or obfuscated. This is typically useful for already optimized or obfuscated classes, to make sure that their code is not modified during optimization.

* `allowshrinking`: Specifies that the entry points specified in the keep option may be shrunk, even if they have to be preserved otherwise. That is, the entry points may be removed in the shrinking step, but if they are necessary after all, they may not be optimized or obfuscated.

* `allowoptimization`: Specifies that the entry points specified in the keep option may be optimized, even if they have to be preserved otherwise. That is, the entry points may be altered in the optimization step, but they may not be removed or obfuscated. This modifier is only useful for achieving unusual requirements.

* `allowobfuscation`: Specifies that the entry points specified in the keep option may be obfuscated, even if they have to be preserved otherwise. That is, the entry points may be renamed in the obfuscation step, but they may not be removed or optimized. This modifier is only useful for achieving unusual requirements.

Finally, the following keep options are also applicable when obfuscating:

* `keeppackagenames [package_filter]`: Specifies not to obfuscate the given package names. The optional filter is a comma-separated list of package names. Package names can contain ?, *, and ** wildcards, and they can be preceded by the ! negator. 

* `keepattributes [attribute_filter]`: Specifies any optional attributes to be preserved. The attributes can be specified with one or more keepattributes directives. The optional filter is a comma-separated list of attribute names that Java virtual machines and ProGuard support. Attribute names can contain ?, *, and ** wildcards, and they can be preceded by the ! negator. [More](https://www.guardsquare.com/en/products/proguard/manual/usage/attributes)

* `keepparameternames`: Specifies to keep the parameter names and types of methods that are kept. This option actually keeps trimmed versions of the debugging attributes LocalVariableTable and LocalVariableTypeTable.

### Other Proguard features

In addition to the above, Proguard offers an extended set of obfuscation, optimization and preverification capabilities. See the [Proguard Manual](https://www.guardsquare.com/en/products/proguard/manual/introduction) for more information.    

### Proguard keep rules

Specify interpretation of `keep` (as in Proguard), `keepaclassmembers` (?), `keepclasseswithmembers` (as in Proguard), `keepnames` (?), `keepclassmembernames` (?), `keepclasseswithmembernames` (?), `if` (as in Proguard).

## Clue Keep method directive

We may use Proguard's `keep` rule to specify the keep method directives (the semantics are identical). For example the following keep directives: 

```
keep <org.springframework.util.concurrent.FutureAdapter: java.lang.Object adaptInternal(java.lang.Object)>
keep <org.springframework.util.concurrent.FutureAdapter: java.lang.Object get()>
```

can be equivalently expressed using Proguard `keep`: 

```
keep class org.springframework.util.concurrent.FutureAdapter {
    java.lang.Object adaptInternal(java.lang.Object);
    java.lang.Object get();
}
```

__Note:__ Proguard uses lower case letters for its rules (e.g. keepclassnames) and prefixes each rule with a dash (-). In terms of syntactic compatibility, perhaps, we can drop the dashes!

## Clue Remove method directive(s) 

We should introduce a new `remove` rule for expressing method removals. For example the following remove directives: 

```
remove <org.springframework.util.concurrent.FutureAdapter: java.lang.Object adaptInternal(java.lang.Object)>
remove <org.springframework.util.concurrent.FutureAdapter: java.lang.Object get()>
```

can be expressed using the a new `remove` rule:

```
remove class org.springframework.util.concurrent.FutureAdapter {
    java.lang.Object adaptInternal(java.lang.Object);
    java.lang.Object get();
}
```

We should allow the user to define the precedence of the removal (compared to a more general keep rule that matches the same class members), by introducing an additional `forceremove` rule to indicate such precedence.

``` 
forceremove class ... {
    method spec 1;
    method spec 2;
}
```

The `remove` rule removes the method of the specific class, only if the method is not implicitly or explicitly kept by a `keep` rule. The `forceremove` rule removes the method of the specific class, regardless of any `keep` rules.
